use std::str::CharIndices;
use std::iter::Peekable;
use std::cmp::Ordering;
use std::ops::Range;

#[derive(Clone)]
pub struct TokTree<'input> {
	pub line: usize,
	pub range: Range<usize>,
	pub tok: TokType<'input>,
}

#[derive(Clone)]
pub enum TokType<'input> {
	/// Sequence of values with spaces between
	TokSequence(Vec<TokTree<'input>>),
	/// Sequence of commas or newlines
	TokList(Vec<TokTree<'input>>),
	/// Assignment of a tree to another
	TokAssign(Box<(TokTree<'input>,TokTree<'input>)>),
	/// Index operator applied to a tree
	TokIndex(Box<(TokTree<'input>,TokTree<'input>)>),
	/// Token with arguments passed
	TokParams(Box<(TokTree<'input>,Vec<TokTree<'input>>)>),
	/// Comparison between two values $1 <order> $2
	TokComparison(Box<(TokTree<'input>,TokTree<'input>,Ordering)>),
	/// Colon metadata assignment
	TokMetadata(Box<(TokTree<'input>,TokTree<'input>)>),
	/// Configuration Variable Token
	TokConfig(&'input str),
	/// Variable Token
	TokVariable(&'input str),
	/// Identifier Token
	TokIdent(&'input str),
	/// Wildcard Token
	TokWildcard,
	/// Integer Token
	TokInteger(u128),
	/// BitToken
	TokBits(u128)
}

#[derive(Copy, Clone)]
pub struct ParseError<'r,'input: 'r> {
	pub reason: &'static str,
	pub token: &'r TokTree<'input>
}


impl<'input> TokTree<'input> {
	pub fn new_error(&self, reason: &'static str) -> ParseError<'_,'input> {
		ParseError {
			reason,
			token: self
		}
	}
	
	pub fn eval_numeric(&self) -> Result<u128,ParseError<'_,'input>> {
		match &self.tok {
			TokType::TokInteger(v) => Ok(*v),
			TokType::TokComparison(v) => {
				unimplemented!()
			},
			TokType::TokConfig(id) => {
				unimplemented!()
			},
			_ => Err(self.new_error("Invalid Token for Numeric Evaluation"))
		}
	}
}




/// Lexer Token Result
pub enum Token<'input> {
	/// '\n'
	NewLine,
	/// '{'
	LSection,
	/// '}'
	RSection,
	/// '['
	LSquare,
	/// ']'
	RSquare,
	/// ')'
	LBracket,
	/// '('
	RBracket,
	/// '='
	Assign,
	/// '*'
	Wildcard,
	/// '+'
	Add,
	/// ','
	Comma,
	/// ':'
	Colon,
	/// '=='
	Equals,
	/// '<'
	LessThan,
	/// '>'
	GreaterThan,
	/// '<='
	LessThanEq,
	/// '>='
	GreaterThanEq,
	/// '"string"'
	String(&'input str),
	/// '$var'
	Variable(&'input str),
	/// '@constant'
	Constant(&'input str),
	/// identifier
	Identifier(&'input str),
	/// '123'
	Integer(u128),
	/// '#01001001'
	Bits(u128,u8),
}

pub struct RecipeLexer<'input> {
	iter: CharIndices<'input>,
	peek: Option<(Option<(usize,char)>,&'input str)>,
}
impl<'input> RecipeLexer<'input> {
	pub fn new(src: &'input str) -> RecipeLexer<'input> {
		RecipeLexer {
			iter: src.char_indices(),
			peek: None
		}
	}
	fn next_char(&mut self) -> (Option<(usize,char)>,&'input str) {
		match self.peek.take() {
			Some(res) => {
				res
			},
			None => {
				let iter_str = self.iter.as_str();
				(self.iter.next(),iter_str)
			}
		}
	}
	fn peek_char(&mut self) -> (Option<(usize,char)>,&'input str) {
		*self.peek.get_or_insert_with(|| {
			let s = self.iter.as_str();
			let res = self.iter.next();
			(res,s)
		})
	}
	fn consume_peek(&mut self) -> () {
		self.peek = None
	}
	
	fn match_eq(
		&mut self, idx: usize,
		tok: Token<'input>, tok_eq: Token<'input>
	) -> Option<Result<(usize, Token<'input>, usize), (&'input str, &'static str)>> {
		match self.peek_char().0 {
			Some((_,'=')) => {
				self.consume_peek();
				Some(Ok((idx,tok_eq,idx + 2)))
			},
			_ => Some(Ok((idx,tok,idx + 1)))
		}
	}
	
	fn match_ident(
		&mut self, idx: usize,
		val: char, tok_type: Option<bool>,
		content: &'input str
	) -> Option<Result<(usize, Token<'input>, usize), (&'input str, &'static str)>> {
		let mut last_idx = idx + 1;
		while let Some((index,value)) = self.peek_char().0 {
			match value {
				'a'..='z' | 'A'..='Z' | '_' => {
					self.consume_peek();
					last_idx = index + 1;
				},
				_ => break
			}
		}
		let res = &content[..(last_idx - idx)];
		let tok = match tok_type {
			Some(true) => Token::Variable(res),
			Some(false) => Token::Constant(res),
			None => Token::Identifier(res)
		};
		Some(Ok((idx,tok,last_idx)))
	}
}

impl<'input> Iterator for RecipeLexer<'input> {
	type Item = Result<(usize, Token<'input>, usize), (&'input str, &'static str)>;
	fn next(&mut self) -> Option<Self::Item> {
		loop {
			let (src,data) = self.next_char();
			if let Some((idx,val)) = src {
				match val {
					' ' | '\t' | '\r' => (),
					'\n' => return Some(Ok((idx,Token::NewLine,idx + 1))),
					'{' => return Some(Ok((idx,Token::LSection,idx + 1))),
					'}' => return Some(Ok((idx,Token::RSection,idx + 1))),
					'[' => return Some(Ok((idx,Token::LSquare,idx + 1))),
					']' => return Some(Ok((idx,Token::RSquare,idx + 1))),
					'(' => return Some(Ok((idx,Token::LBracket,idx + 1))),
					')' => return Some(Ok((idx,Token::RBracket,idx + 1))),
					'*' => return Some(Ok((idx,Token::Wildcard,idx + 1))),
					'+' => return Some(Ok((idx,Token::Add,idx + 1))),
					',' => return Some(Ok((idx,Token::Comma,idx + 1))),
					':' => return Some(Ok((idx,Token::Colon,idx + 1))),
					'=' => return self.match_eq(idx,Token::Equals,Token::Assign),
					'<' => return self.match_eq(idx,Token::LessThanEq,Token::LessThan),
					'>' => return self.match_eq(idx,Token::GreaterThanEq,Token::GreaterThan),
					val0 @ '0'..='9' => {
						let mut counter : u128 = ((val0 as u32) - ('0' as u32)) as u128;
						let mut last_idx = idx + 1;
						while let Some((num_idx,num)) = self.peek_char().0 {
							match num {
								'0'..='9' => {
									last_idx = num_idx + 1;
									self.consume_peek();
									let val = (num as u32) - ('0' as u32);
									let new_counter =  counter.checked_mul(10).and_then(|v| {
										v.checked_add(val as u128)
									});
									counter = match new_counter {
										Some(v) => v,
										None => return Some(Err(
											(&data[..(last_idx - idx)],"Number Too Large")
										))
									};
								}
								_ => break
							}
						}
						return Some(Ok((idx,Token::Integer(counter),last_idx)));
					},
					'#' => {
						let mut bits : u128 = 0;
						let mut bit_count : u8 = 0;
						let mut last_idx = idx + 1;
						
						while let Some((num_idx,num)) = self.peek_char().0 {
							match num {
								a @ '0' | '1' => {
									last_idx = num_idx + 1;
									self.consume_peek();
									if bit_count >= 128 {
										return Some(Err((
											(&data[..(last_idx - idx)],"Bits Too Large")
										)))
									}
									let bit_val : u64 = (a as u64) & !('0' as u64);
									bits = (bits << 1) | bit_val;
									bit_count += 1;
								},
								_ => break
							}
						}
						if bit_count == 0 {
							return Some(Err((&data[..(last_idx - idx)],"Missing Bits")));
						}
						return Some(Ok((idx,Token::Bits(bits,bit_count),last_idx)));
					}
					key @ '$' | '@' => {
						let (new_val,content) = self.next_char();
						if let Some((new_idx,new_val)) = new_val {
							match new_val {
								'a'..='z' | 'A'..='Z' => {
									let tok_type = key == '$';
									return self.match_ident(
										new_idx,new_val,
										Some(tok_type),content
									);
								},
								_ => {
									return Some(Err((&data[..2],"Invalid start of ident")));
								}
							}
						}else{
							return Some(Err((data,"Required ident after char")));
						}
					},
					'"' => {
						let start_idx = idx + 1;
						let quote_start = self.peek_char().1;
						let end_idx = loop {
							if let Some((new_index,b)) = self.next_char().0 {
								match b {
									'"' => break new_index + 1,
									_ => ()
								}
							}else{
								return Some(Err((data,"Quote Not Closed")));
							}
						};
						let quote = &quote_start[..(end_idx - start_idx)];
						return Some(Ok((start_idx,Token::String(quote),end_idx)));
					},
					'a'..='z' | 'A'..='Z' => {
						return self.match_ident(idx,val,None,data);
					},
					_ => {
						return Err((&data[..1],"Invalid Char"))
					}
				}
			}else{
				return None
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	fn assert_lex(input: &str, tok: Token) {
		{
			let mut lexer = RecipeLexer::new(input);
			let next = lexer.next().unwrap().unwrap();
			assert_eq!(next.0,0);
			assert_eq!(next.1,tok);
			assert_eq!(next.2,input.len());
			assert!(lexer.next().is_none());
		}
		{
			let mut new_str = String::new();
			new_str.push_str("   \t  ");
			new_str.push_str(input);
			new_str.push_str("\t  \t   \t  ");
			let mut lexer = RecipeLexer::new(new_str.as_str());
			let next = lexer.next().unwrap().unwrap().1;
			assert_eq!(next,tok);
			assert!(lexer.next().is_none());
		}
	}
	
	#[test]
	fn test_keys() {
		assert_lex("hello",Token::Identifier("hello"));
		assert_lex("I",Token::Identifier("I"));
		assert_lex("a_b_c",Token::Identifier("a_b_c"));
		assert_lex("#0",Token::Bits(0,1));
		assert_lex("#1",Token::Bits(1,1));
		assert_lex("0",Token::Integer(0));
		assert_lex("000",Token::Integer(0));//TODO: FIX
		assert_lex("123",Token::Integer(123));
		assert_lex("$var",Token::Variable("var"));
		assert_lex("@const",Token::Constant("const"));
	}
}
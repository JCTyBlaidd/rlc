use super::*;
use crate::parser::meta_instructions::MetaInstructionConfig;
use crate::parser::isa_instruction::{
	InstructionMetadata,
	LayoutAssignment
};
use hashbrown::{HashSet, HashMap};
use hashbrown::hash_map::Entry;

pub fn generate_meta_instructions(
	meta_isa: &[MetaInstruction],
	meta_enum: &[ParseMetaEnum],
	isa_list: &[ISAMetadata],
	alias_list: &[ISAAlias],
	contents: &mut String,
) {
	*contents += "#[derive(Clone, PartialEq, Eq, Debug)]\n";
	*contents += "enum McInstruction<\n";
	*contents += "\tR: Clone + PartialEq + Eq + Debug,\n";
	*contents += "\tI: Clone + PartialEq + Eq + Debug\n";
	*contents += "> {\n";
	
	for insn in meta_isa.iter() {
		*contents += "\t";
		*contents += &insn.enum_name;
		*contents += " {\n";
		
		if insn.output_reg_count != 0 {
			*contents += "\t\toutputs: [R;";
			*contents += &insn.output_reg_count.to_string();
			*contents += "],\n";
		}
		if insn.input_reg_count != 0 {
			*contents += "\t\tinputs: [R;";
			*contents += &insn.input_reg_count.to_string();
			*contents += "],\n";
		}
		if insn.input_immediate_count != 0 {
			*contents += "\t\timmediate: [I;";
			*contents += &insn.input_immediate_count.to_string();
			*contents += "],\n";
		}
		for (k,v) in insn.isa_config.iter() {
			*contents += "\t\t";
			*contents += k;
			*contents += ": ";
			match v {
				MetaInstructionConfig::BooleanConfig => {
					*contents += "bool";
				},
				MetaInstructionConfig::IntegerConfig(size) => {
					*contents += "u";
					*contents += &size.to_string();
				},
				MetaInstructionConfig::EnumConfig(name) => {
					*contents += name;
				}
			}
			*contents += ",\n";
		}
		
		*contents += "\t},\n";
	}
	*contents += "}\n\n\n";
	
	*contents += "impl<\n";
	*contents += "\tR: Clone + PartialEq + Eq + Debug,\n";
	*contents += "\tI: Clone + PartialEq + Eq + Debug\n";
	*contents += "> McInstruction<R,I> {\n\n";
	
	*contents += "\tfn get_inputs(&self) -> &[R] {\n";
	*contents += "\t\tuse self::McInstruction::*;\n";
	*contents += "\t\tmatch self {\n";
	for insn in meta_isa.iter() {
		*contents += "\t\t\t";
		*contents += &insn.enum_name;
		if insn.input_reg_count != 0 {
			*contents += " { inputs, .. } => {\n";
			*contents += "\t\t\t\tinputs\n";
		}else{
			*contents += " { .. } => {\n";
			*contents += "\t\t\t\tDefault::default()\n";
		}
		*contents += "\t\t\t}\n";
	}
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	
	*contents += "\tfn get_outputs(&self) -> &[R] {\n";
	*contents += "\t\tuse self::McInstruction::*;\n";
	*contents += "\t\tmatch self {\n";
	for insn in meta_isa.iter() {
		*contents += "\t\t\t";
		*contents += &insn.enum_name;
		if insn.output_reg_count != 0 {
			*contents += " { outputs, .. } => {\n";
			*contents += "\t\t\t\toutputs\n";
		}else{
			*contents += " { .. } => {\n";
			*contents += "\t\t\t\tDefault::default()\n";
		}
		*contents += "\t\t\t}\n";
	}
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	
	*contents += "\tfn get_immediate(&self) -> &[I] {\n";
	*contents += "\t\tuse self::McInstruction::*;\n";
	*contents += "\t\tmatch self {\n";
	for insn in meta_isa.iter() {
		*contents += "\t\t\t";
		*contents += &insn.enum_name;
		if insn.input_immediate_count != 0 {
			*contents += " { immediate, .. } => {\n";
			*contents += "\t\t\t\timmediate\n";
		}else{
			*contents += " { .. } => {\n";
			*contents += "\t\t\t\tDefault::default()\n";
		}
		*contents += "\t\t\t}\n";
	}
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	
	*contents += "\tfn convert_type<\n";
	*contents += "\t R2: Clone + PartialEq + Eq + Debug,\n";
	*contents += "\t I2: Clone + PartialEq + Eq + Debug,\n";
	*contents += "\t FuncR: Fn(R) -> R2,\n";
	*contents += "\t FuncI: Fn(I) -> I2\n";
	*contents += "\t>(self, op_r: FuncR, op_i: FuncI) -> McInstruction<R2,I2> {\n";
	*contents += "\t\tuse self::McInstruction::*;\n";
	*contents += "\t\tmatch self {\n";
	for insn in meta_isa.iter() {
		*contents += "\t\t\t";
		*contents += &insn.enum_name;
		*contents += " {\n";
		if insn.input_reg_count != 0 {
			*contents += "\t\t\t\tinputs,\n";
		}
		if insn.output_reg_count != 0 {
			*contents += "\t\t\t\toutputs,\n";
		}
		if insn.input_immediate_count != 0 {
			*contents += "\t\t\t\timmediate,\n";
		}
		for (k,_) in &insn.isa_config {
			*contents += "\t\t\t\t";
			*contents += &k;
			*contents += ",\n";
		}
		*contents += "\t\t\t} => {\n";
		if insn.input_reg_count != 0 {
			*contents += "\t\t\t\tlet [";
			for i in 0..insn.input_reg_count {
				if i != 0 {
					*contents += ", ";
				}
				*contents += "in";
				*contents += &i.to_string();
			}
			*contents += "] = inputs;\n";
		}
		if insn.output_reg_count != 0 {
			*contents += "\t\t\t\tlet [";
			for i in 0..insn.output_reg_count {
				if i != 0 {
					*contents += ", ";
				}
				*contents += "out";
				*contents += &i.to_string();
			}
			*contents += "] = outputs;\n";
		}
		if insn.input_immediate_count != 0 {
			*contents += "\t\t\t\tlet [";
			for i in 0..insn.input_immediate_count {
				if i != 0 {
					*contents += ", ";
				}
				*contents += "imm";
				*contents += &i.to_string();
			}
			*contents += "] = immediate;\n";
		}
		*contents += "\t\t\t\t";
		*contents += &insn.enum_name;
		*contents += " {\n";
		if insn.input_reg_count != 0 {
			*contents += "\t\t\t\t\tinputs: [";
			for i in 0..insn.input_reg_count {
				if i != 0 {
					*contents += ", ";
				}
				*contents += "op_r(in";
				*contents += &i.to_string();
				*contents += ")";
			}
			*contents += "],\n";
		}
		if insn.output_reg_count != 0 {
			*contents += "\t\t\t\t\toutputs: [";
			for i in 0..insn.output_reg_count {
				if i != 0 {
					*contents += ", ";
				}
				*contents += "op_r(out";
				*contents += &i.to_string();
				*contents += ")";
			}
			*contents += "],\n";
		}
		if insn.input_immediate_count != 0 {
			*contents += "\t\t\t\t\timmediate: [";
			for i in 0..insn.input_immediate_count {
				if i != 0 {
					*contents += ", ";
				}
				*contents += "op_i(imm";
				*contents += &i.to_string();
				*contents += ")";
			}
			*contents += "],\n";
		}
		for (k,_) in &insn.isa_config {
			*contents += "\t\t\t\t\t";
			*contents += &k;
			*contents += ",\n";
		}
		*contents += "\t\t\t\t}\n";
		*contents += "\t\t\t},\n";
	}
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	
	*contents += "}\n";
}

fn generate_isa_insn_enum(
	contents: &mut String,
	insn: &InstructionMetadata
) {
	*contents += "\t";
	*contents += &insn.insn_name;
	*contents += " {\n";
	
	let mut immediate_list = HashMap::new();
	for assign in insn.bit_assignments.iter() {
		match assign {
			LayoutAssignment::RegisterAssignment {
				idx,
				is_input,
				assignment_bits,
				..
			}=>{
				*contents += "\t\t";
				if *is_input {
					*contents += "rs";
				}else{
					*contents += "rd";
				}
				*contents += &*idx.to_string();
				*contents += ": ";
				if *assignment_bits <= 8 {
					*contents += "u8"
				}else if *assignment_bits <= 16 {
					*contents += "u16"
				}else{
					panic!("Unknown Reg Count")
				}
				*contents += ",\n";
			},
			LayoutAssignment::ImmediateAssignment {
				assignment_bits,
				immediate_subset_offset,
				idx,
				..
			}=>{
				let limit = *assignment_bits + *immediate_subset_offset;
				match immediate_list.entry(*idx) {
					Entry::Occupied(mut occ) => {
						let v = *occ.get();
						if limit > v {
							let _ = occ.insert(limit);
						}
					},
					Entry::Vacant(vacant) => {
						vacant.insert(limit);
					},
				}
			}
		}
	}
	for (imm,lim) in immediate_list.iter() {
		*contents += "\t\timm";
		*contents += &*imm.to_string();
		*contents += ": ";
		if *lim <= 8 {
			*contents += "u8";
		}else if *lim <= 16 {
			*contents += "u16";
		}else if *lim <= 32 {
			*contents += "u32";
		}else if *lim <= 64 {
			*contents += "u64";
		}else if *lim <= 128 {
			*contents += "u128";
		}else{
			panic!("Invalid Immediate Content Size")
		}
		*contents += ",\n";
	}
	*contents += "\t},\n";
	for sub_instruction in insn.sub_instruction.iter() {
		generate_isa_insn_enum(contents,sub_instruction)
	}
}

pub fn generate_isa_instructions(
	meta_isa: &[MetaInstruction],
	meta_enum: &[ParseMetaEnum],
	isa_gen: &ISAMetadata,
	contents: &mut String,
) {
	*contents += "#[derive(Copy, Clone, PartialEq, Eq, Debug)]\n";
	*contents += "pub enum Instruction {\n";
	for insn in isa_gen.instructions.iter() {
		generate_isa_insn_enum(contents,insn)
	}
	*contents += "}\n";
}
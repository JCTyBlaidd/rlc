use super::parser::{
	MetaInstruction,
	ParseMetaEnum,
	ISAMetadata,
	ISAAlias
};

mod instruction_gen;

use heck::CamelCase;
pub fn generate_isa_format(
	meta_isa: &[MetaInstruction],
	meta_enum: &[ParseMetaEnum],
	isa: &ISAMetadata,
) -> String {
	let mut contents = String::new();
	contents += "use super::*;\n\n";
	
	let camel_name = isa.name.clone().to_camel_case();
	
	contents += "struct ";
	contents += &camel_name;
	contents += " ;\n\n";

	contents += "impl ISA for ";
	contents += &camel_name;
	contents += "{\n\n";
	
	contents += "\tfn get_pointer_size(&self) -> u8 {\n";
	contents += "\t\t";
	contents += &isa.base_size.to_string();
	contents += "\n\t}\n\n";
	
	contents += "\tfn supports_instruction<\n";
	contents += "\t\tR: Clone + PartialEq + Eq + Debug,\n";
	contents += "\t\tI: Clone + PartialEq + Eq + Debug\n";
	contents += "\t>(&self, isa: &McInstruction<R,I>) -> bool{\n";
	contents += "\t\ttrue\n";
	contents += "\t}\n\n";
	
	contents += "}\n\n\n";
	
	instruction_gen::generate_isa_instructions(
		meta_isa,
		meta_enum,
		isa,
		&mut contents
	);
	
	contents
}


pub fn generate_impl_main(
	meta_isa: &[MetaInstruction],
	meta_enum: &[ParseMetaEnum],
	isa_list: &[ISAMetadata],
	alias_list: &[ISAAlias]
) -> String {
	let mut contents = String::new();
	contents += "use std::fmt::Debug;\n";
	contents += "\n\n//Generated ISA Modules\n";
	for isa in isa_list.iter() {
		contents += "mod ";
		contents += &isa.name.clone().to_ascii_lowercase();
		contents += ";\n";
	}
	
	contents += "\ntrait ISA {\n\n";
	contents += "\tfn get_pointer_size(&self) -> u8;\n\n";
	contents += "\tfn supports_instruction<\n";
	contents += "\t\tR: Clone + PartialEq + Eq + Debug,\n";
	contents += "\t\tI: Clone + PartialEq + Eq + Debug\n";
	contents += "\t>(&self, isa: &McInstruction<R,I>) -> bool;\n\n";
	contents += "}\n\n\n";
	
	for enum_val in meta_enum.iter() {
		if enum_val.enum_type == "Enum-u8" {
			contents += "#[repr(u8)]\n";
			contents += "#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]\n";
		}
		contents += "enum ";
		contents += &enum_val.name;
		contents += "{\n";
		for value in enum_val.values.iter() {
			contents += "\t";
			contents += value;
			contents += ",\n";
		}
		contents += "}\n\n";
	}
	
	instruction_gen::generate_meta_instructions(
		meta_isa,
		meta_enum,
		isa_list,
		alias_list,
		&mut contents,
	);
	
	contents
}
use std::fs::{
	read_to_string,
	write
};
use std::path::Path;

#[macro_use]
extern crate serde_derive;

pub mod parser;
pub mod isa_gen;

pub mod recipe_file;

pub mod ir_gen;

fn write_at(path: &Path, id: String, contents: String) {
	let new_path = path.join(id);
	write(new_path,contents.as_bytes()).unwrap();
}

fn main() {
	let self_path = std::env::current_dir().unwrap();
	let rlc_path = self_path.parent().unwrap();
	println!("{:?}",rlc_path);
	
	let input_path = self_path.join("input");
	let output_path = rlc_path.join("src").join("isa").join("impls");
	println!("Input Path: {:?}", input_path);
	println!("Output Path: {:?}", output_path);
	
	
	self::ir_gen::generate(
		&input_path,
		rlc_path.join("src").join("code_section").join("ir").as_path(),
		rlc_path.join("src").join("code_section").join("io").as_path()
	);
	
	let mut meta_instructions = Vec::new();
	let mut meta_enums = Vec::new();
	let mut isa_defs = Vec::new();
	let mut isa_alias = Vec::new();
	
	for entry in input_path.read_dir().unwrap() {
		if let Ok(entry) = entry {
			let name = entry.file_name().into_string().unwrap();
			println!("Loading: {}",&name);
			let contents = read_to_string(entry.path()).unwrap();
			if &name == "meta.json" {
				if let Ok((meta_insn,meta_enum)) = parser::load_meta_file(&contents) {
					meta_instructions = meta_insn;
					meta_enums = meta_enum;
				}
			}else{
				if let Ok((mut meta,mut alias)) = parser::load_isa_file(&contents) {
					isa_defs.append(&mut meta);
					isa_alias.append(&mut alias);
				}
			}
		}
	}
	
	let mod_file = isa_gen::generate_impl_main(
		&meta_instructions,
		&meta_enums,
		&isa_defs,
		&isa_alias
	);
	write_at(&output_path,"mod.rs".to_string(),mod_file);
	for isa_def in isa_defs.iter() {
		let isa_file = isa_gen::generate_isa_format(
			&meta_instructions,
			&meta_enums,
			isa_def
		);
		write_at(&output_path,isa_def.name.clone().to_ascii_lowercase() + ".rs",isa_file);
	}
	
	//let path = Path::new("./rlc_generator/input/risc_v.json");
	//println!("{:?}",path.canonicalize().unwrap());
	//let str_v = read_to_string(path).unwrap();
	//if let Ok((meta,alias)) = parser::load_isa_file(str_v.as_str()) {
	//	println!("{:#?}",&alias);
	//}
}
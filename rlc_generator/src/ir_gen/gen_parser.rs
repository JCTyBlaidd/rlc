use hashbrown::HashMap;
use indexmap::IndexMap;
use super::{MainMeta, MetaNode};

pub fn gen_parser_grammar(
	src: &str,
	lexer: &HashMap<String,String>,
	default: &String,
	default_parse: &String,
	scope: &IndexMap<String, String>,
	meta: &IndexMap<String,MetaNode>,
	main: &IndexMap<String,MainMeta>,
) -> String {
	let mut split = src.split("#insert keywords");
	let start = split.next().unwrap();
	let rem_str = split.next().unwrap();
	assert!(split.next().is_none());
	
	let mut split = rem_str.split("#insert parser_gen");
	let middle = split.next().unwrap();
	let end = split.next().unwrap();
	assert!(split.next().is_none());
	
	let mut output_string = start.to_string();
	
	// Generate Lexer Definitions for all keywords
	for (k,v) in lexer.iter() {
		output_string += "\t\t\"";
		output_string += k.as_str();
		output_string += "\" => TokLexedKeyword(LexKeyTok::";
		output_string += v.as_str();
		output_string += "),\n";
	}
	let _ = output_string.pop();
	let _ = output_string.pop();
	output_string += "\n";
	
	output_string += middle;
	
	// Generate Parser Definitions for all operations
	// "op.is" <op_config> ARG_? DEP_? META_? "\n"
	// final_op = switch of all possible ops?
	
	output_string += "OPERATION = {\n";
	output_string += "\t\"";
	output_string += default_parse.as_str();
	output_string += "\" => TokLexedKeyword(LexKeyTok::";
	output_string += default.as_str();
	output_string += "),\n";
	
	for (scope_name,scope_ident) in scope.iter() {
		output_string += "\t\"";
		output_string += scope_ident.as_str();
		output_string += "\" <STATES> => unimplemented!(),\n";
	}
	for (_,main_info) in main.iter() {
		for (def_name, def_parse) in main_info.defs.iter() {
			output_string += "\t\"";
			output_string += def_parse.as_str();
			output_string += "\" ";
			for (argi,argv) in main_info.vars.iter().enumerate() {
				output_string += "<arg";
				output_string += argi.to_string().as_str();
				output_string += ": ARG_T_";
				output_string += argv.as_str();
				output_string += "> ";
			}
			if main_info.in_meta.len() != 0 {
				output_string += "<vars: VAR_";
				output_string += main_info.in_meta.len().to_string().as_str();
				output_string += "> ";
			}
			if main_info.out_meta.len() != 0 {
				output_string += "<meta: META_";
				output_string += main_info.out_meta.len().to_string().as_str();
				output_string += "> ";
			}
			output_string += "=> unimplemented!(),\n";
		}
	}
	for (id,meta) in meta.iter() {
		output_string += "\t\"";
		output_string += meta.node.as_str();
		output_string += "\" <ARGS> <STATES> <META> => unimplemented!(),\n";
	}
	output_string += "}\n";
	
	output_string += end;
	output_string
}
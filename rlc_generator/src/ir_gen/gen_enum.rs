use super::*;
use std::env::var;

fn var_type(var: &str) -> &'static str {
	match var {
		"$INT_TYPE"
		| "$INT_TYPE_SRC"
		| "$INT_TYPE_DST"
		| "$FLOAT_TYPE"
		| "$FLOAT_TYPE_SRC"
		| "$FLOAT_TYPE_DST"
		| "$VEC_SIZE" => "NonZeroU8",
		"$STRUCT_INDEX" => "u16", /* ??? */
		"$INT_VEC_TYPE" => "IntType",
		"$FLOAT_VEC_TYPE" => "FloatType",
		"$POINTER_TYPE" => "PointerType",
 		_ => panic!("Unknown Type: {}", var)
	}
}

fn var_name(var: &str) -> &'static str {
	match var {
		"$INT_TYPE_SRC"
		| "$FLOAT_TYPE_SRC" => "src",
		"$INT_TYPE_DST"
		| "$FLOAT_TYPE_DST" => "dst",
		"$VEC_SIZE" => "elements",
		"$POINTER_TYPE" => "ptr",
		"$STRUCT_INDEX" => "index",
		_ => panic!("Unknown Type: {}", var)
	}
}

fn is_any_int(x: &str) -> bool {
	x == "$INT_TYPE" || x == "$INT_TYPE_SRC" || x == "$INT_TYPE_DST"
}

fn is_any_float(x: &str) -> bool {
	x == "$FLOAT_TYPE" || x == "$FLOAT_TYPE_SRC" || x == "$FLOAT_TYPE_DST"
}

fn is_vec_or_ptr(x: &str) -> bool {
	x == "$INT_VEC_TYPE" || x == "$FLOAT_VEC_TYPE" || x == "$POINTER_TYPE"
}

fn write_node_output_op(op: &str, var_types: &[String], variables: &[String]) -> String {
	let mut content = String::new();
	match op {
		"$0" | "$1" | "$2" | "$3" => {
			let idx = op.chars().nth(1).unwrap() as usize - '0' as usize;
			assert!(idx <= 3);
			if is_any_int(var_types[idx].as_str()) {
				content += "IntType::from_size(*";
				content += variables[idx].as_str();
				content += ").into_type()";
			}else if is_any_float(var_types[idx].as_str()) {
				content += "FloatType::from_size(*";
				content += variables[idx].as_str();
				content += ").into_type()";
			}else if is_vec_or_ptr(var_types[idx].as_str()) {
				content += variables[idx].as_str();
				content += ".into_type()";
			}else{
				panic!("Unknown Default Type: {}", var_types[idx])
			}
		},
		"<$0 x $2>" | "<$1 x $2>" => {
			let flag = op == "<$0 x $2>";
			let idx1 = if flag { 0 } else { 1 };
			let idx2 = 2;
			if is_any_int(var_types[idx1].as_str()) {
				content += "IntType::new(*";
				content += variables[idx1].as_str();
				content += ",*";
				content += variables[idx2].as_str();
				content += ").into_type()"
			}else if is_any_float(var_types[idx1].as_str()) {
				content += "FloatType::new(*";
				content += variables[idx1].as_str();
				content += ",*";
				content += variables[idx2].as_str();
				content += ").into_type()"
			}else {
				panic!("Bad Vector Constructor: {}", var_types[0])
			}
		}
		"$0 as INT_TYPE" => {
			if var_types[0] == "$POINTER_TYPE" {
				content += "meta.get_ptr_int().into_type()";
			}else if var_types[0] == "$FLOAT_TYPE" {
				content += "FloatType::from_size(*";
				content += variables[0].as_str();
				content += ").into_type()"
			}else{
				panic!("Bad Integer Conversion Src: {}", var_types[0])
			}
		},
		"$0 as FLOAT_TYPE" => {
			assert_eq!(var_types[0],"$INT_TYPE");
			content += "IntType::from_size(*";
			content += variables[0].as_str();
			content += ").into_type()"
		}
		"$FLAG_TYPE" => content += "FlagType",
		"$OPAQUE_UNION_PTR" => content += "unimplemented!(\"opaque_ptr\")",
		"$0 INTO PTR_CONTENTS" => content += "unimplemented!(\"ptr_contents\")",
		"$0 AT $1 STRUCT_ELEM" => content += "unimplemented!(\"struct_elem\")",
		"$POINTER_EXACT" => content += "panic!(\"ptr_exact is ???\")",
		_ => panic!("Unknown Type Operation: {}", op)
	}
	content
}

fn write_node_output(outputs: &[String], var_types: &[String], variables: &[String]) -> String {
	let mut content = String::new();
	if outputs.len() == 0 {
		content += "invalid_set(set)";
	}else{
		content += "if set == 0 {\n\t\t\t\t";
		let mut idx = 0;
		while idx < outputs.len() {
			let line_content = write_node_output_op(
				outputs[idx].as_str(),
				var_types,variables
			);
			let mut idx_lim = idx + 1;
			while idx_lim < outputs.len() {
				if line_content == write_node_output_op(
					outputs[idx_lim].as_str(),
					var_types,variables
				) {
					idx_lim += 1;
				}else{
					break;
				}
			}
			content += "if ";
			for idx_match in idx..idx_lim {
				if idx_match != idx {
					content += " || ";
				}
				content += "idx == ";
				content += format!("{}", idx_match).as_str();
			}
			content += " {\n\t\t\t\t\t";
			content += line_content.as_str();
			content += "\n\t\t\t\t} else ";
			idx = idx_lim;
		}
		content += "{\n\t\t\t\t\tinvalid_idx(set,idx)\n\t\t\t\t}";
		content += "\n\t\t\t} else {\n\t\t\t\tinvalid_set(set)\n\t\t\t}";
	}
	content
}

pub fn gen_enum_decl(
	default: &String,
	default_parse: &String,
	scope: &IndexMap<String, String>,
	meta: &IndexMap<String,MetaNode>,
	main: &IndexMap<String,MainMeta>,
	contents: &mut String,
) {
	*contents += "pub enum OpNode {\n";
	*contents += "\t//Default Node\n";
	*contents += "\t";
	*contents += default.as_str();
	*contents += ",\n\n";
	
	*contents += "\t//Scope Nodes\n";
	for scope_name in scope.keys() {
		*contents += "\t";
		*contents += scope_name.as_str();
		*contents += ",\n";
	}
	*contents += "\n";
	
	for (section_name,main_info) in main.iter() {
		*contents += "\t//Section: ";
		*contents += section_name.as_str();
		*contents += "\n";
		for (id,_) in &main_info.defs {
			*contents += "\t";
			*contents += id.as_str();
			if main_info.vars.len() > 0 {
				if main_info.vars.len() == 1 {
					*contents += "(";
					*contents += var_type(main_info.vars[0].as_str());
					*contents += ")";
				} else {
					*contents += "{\n";
					for var in &main_info.vars {
						*contents += "\t\t";
						*contents += var_name(var.as_str());
						*contents += ": ";
						*contents += var_type(var.as_str());
						*contents += ",\n";
					}
					*contents += "\t}";
				}
			}
			*contents += ",\n";
		}
		*contents += "\n";
	}
	
	*contents += "\t//Section: Control Flow\n";
	for (id,meta) in meta.iter() {
		*contents += "\t";
		*contents += id.as_str();
		*contents += "(Box<";
		*contents += meta.node.as_str();
		*contents += ">),\n";
	}
	
	*contents += "}\n\n";
	
	
	let gen_matches = |
		default_result: &str,
		scope_result: &dyn Fn(&String,&String) -> String,
		group_main: bool,
		main_default: &dyn Fn(&MainMeta,&String,&[String],usize) -> (String,bool),
		scope_default: &dyn Fn(&MetaNode) -> (String,bool)
	| -> String {
		let mut raw_contents = String::new();
		let contents = &mut raw_contents;
		*contents += "\t\t\t";
		*contents += default.as_str();
		*contents += " => ";
		*contents += default_result;
		*contents += ",\n";
		for (scope_name,scope_value) in scope.iter() {
			*contents += "\t\t\t";
			*contents += scope_name.as_str();
			*contents += " => ";
			*contents += scope_result(scope_name,scope_value).as_str();
			*contents += ",\n";
		}
		for (section_name,main_info) in main.iter() {
			
			let mut var_access_list = Vec::new();
			if main_info.vars.len() == 1 {
				var_access_list.push("data".to_string());
			}else if main_info.vars.len() > 1 {
				for var in &main_info.vars {
					var_access_list.push(var_name(var).to_string());
				}
			}
			
			let group_result = if group_main {
				main_default(
					&main_info,
					section_name,
					var_access_list.as_slice(),
					0
				)
			}else {
				("".to_string(), false)
			};
			
			for (idx,(id,r)) in main_info.defs.iter().enumerate() {
				*contents += "\t\t\t";
				if group_main {
					if idx != 0 {
						*contents += "| ";
					}
				}
				*contents += id.as_str();
				
				let (r,s) = if group_main {
					group_result.clone()
				}else{
					main_default(
						&main_info,
						section_name,
						var_access_list.as_slice(),
						idx
					)
				};
				
				if main_info.vars.len() == 1 {
					if s {
						*contents += "(data)";
					}else{
						*contents += "(_)";
					}
				} else if main_info.vars.len() > 0 {
					if s {
						*contents += "{\n";
						for var in &main_info.vars {
							*contents += "\t\t\t\t";
							*contents += var_name(var.as_str());
							*contents += ",\n";
						}
						*contents += "\t\t\t}";
					}else {
						*contents += "{ .. }";
					}
				}
				
				if !group_main {
					*contents += " => ";
					*contents += r.as_str();
					*contents += ",\n";
				}else if idx + 1 != main_info.defs.len() {
					*contents += "\n";
				}
			}
			
			if group_main && !main_info.defs.is_empty() {
				*contents += " => ";
				*contents += group_result.0.as_str();
				*contents += ",\n";
			}
			*contents += "\n";
		}
		for (id,meta) in meta.iter() {
			*contents += "\t\t\t";
			*contents += id.as_str();
			let (r,s) = scope_default(meta);
			if s {
				*contents += "(data) => ";
			}else{
				*contents += "(_) => ";
			}
			*contents += r.as_str();
			*contents += ",\n";
		}
		raw_contents
	};
	
	
	//Generate Impl Blocks
	*contents += "impl OpNode {\n\n";
	//////////////////////////////////////////////////////
	*contents += "\tpub fn input_sets(&self, scope: &dyn ScopeTypeMeta) -> u8 {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		"0",
		&|val,_| {
			if val == "OpScopeInput" {
				"0".to_string()
			}else{
				"scope.scope_output_sets()".to_string()
			}
		},
		true,
		&|a,b,c,d| {
			if a.in_meta.len() == 0 {
				("0".to_string(),false)
			}else {
				("1".to_string(),false)
			}
		},
		&|_| ("data.node_input_sets()".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	//////////////////////////////////////////////////////
	*contents += "\tpub fn output_sets(&self, scope: &dyn ScopeTypeMeta) -> u8 {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		"0",
		&|val,_| {
			if val == "OpScopeInput" {
				"scope.scope_input_sets()".to_string()
			}else{
				"0".to_string()
			}
		},
		true,
		&|a,b,c,d| {
			if a.out_meta.len() == 0 {
				("0".to_string(),false)
			}else {
				("1".to_string(),false)
			}
		},
		&|_| ("data.node_output_sets()".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	//////////////////////////////////////////////////////
	*contents += "\tpub fn input_count(&self, set: u8, scope: &dyn ScopeTypeMeta) -> u16 {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		"invalid_set(set)",
		&|val,_| {
			if val == "OpScopeInput" {
				"invalid_set(set)".to_string()
			}else{
				"scope.scope_output_count(set)".to_string()
			}
		},
		true,
		&|a,b,c,d| {
			if a.in_meta.len() == 0 {
				("invalid_set(set)".to_string(),false)
			}else {
				(
					"if set == 0 { ".to_string()
						+ a.in_meta.len().to_string().as_str()
						+ "} else { invalid_set(set) }",
					false
				)
			}
		},
		&|_| ("data.node_input_count(set)".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	//////////////////////////////////////////////////////
	*contents += "\tpub fn output_count(&self, set: u8, scope: &dyn ScopeTypeMeta) -> u16 {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		"invalid_set(set)",
		&|val,_| {
			if val == "OpScopeInput" {
				"scope.scope_input_count(set)".to_string()
			}else{
				"invalid_set(set)".to_string()
			}
		},
		true,
		&|a,b,c,d| {
			if a.out_meta.len() == 0 {
				("invalid_set(set)".to_string(),false)
			}else {
				(
					"if set == 0 { ".to_string()
						+ a.out_meta.len().to_string().as_str()
						+ "} else { invalid_set(set) }",
					false
				)
			}
		},
		&|_| ("data.node_output_count(set)".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	//////////////////////////////////////////////////////
	*contents += "\tpub fn input_type(&self, set: u8, idx: u16, scope: &dyn ScopeTypeMeta, meta: &CompileMeta) -> BasicType {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		"invalid_set(set)",
		&|val,_| {
			if val == "OpScopeInput" {
				"invalid_set(set)".to_string()
			}else{
				"scope.scope_output_type(set,idx)".to_string()
			}
		},
		true,
		&|meta,_,var_names,_| {
			(write_node_output(
				meta.in_meta.as_slice(),
				meta.vars.as_slice(),
				var_names
			),true)
		},
		&|_| ("data.node_input_type(set,idx)".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	//////////////////////////////////////////////////////
	*contents += "\tpub fn output_type(&self, set: u8, idx: u16, scope: &dyn ScopeTypeMeta, meta: &CompileMeta) -> BasicType {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		"invalid_set(set)",
		&|val,_| {
			if val == "OpScopeInput" {
				"scope.scope_input_type(set,idx)".to_string()
			}else{
				"invalid_set(set)".to_string()
			}
		},
		true,
		&|meta,_,var_names,_| {
			(write_node_output(
				meta.out_meta.as_slice(),
				meta.vars.as_slice(),
				var_names
			),true)
		},
		&|_| ("data.node_output_type(set,idx)".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	//////////////////////////////////////////////////////
	*contents += "}\n\n";
	
	//Generate Impl Traits
	
	*contents += "impl Display for OpNode {\n";
	*contents += "\tfn fmt(&self, f: &mut Formatter) -> FmtResult {\n";
	*contents += "\t\tuse OpNode::*;\n";
	*contents += "\t\tmatch self {\n";
	*contents += gen_matches(
		("write!(f,\"".to_string() + default_parse + "\")").as_str(),
		&|val,id| {
			"write!(f,\"".to_string() + id.as_str() + "\")"
		},
		false,
		&|meta,_,var_names,idx| {
			let (_,write) = meta.defs.iter().nth(idx).unwrap();
			let mut output = String::new();
			output += "write!(f,\"";
			
			let mut split = write.split("@");
			let strip_str = split.next().unwrap();
			let use_unsigned = split.next().map_or(false,|v| {
				v == "unsigned"
			});
			output += strip_str;
			for (idx,_) in var_names.iter().enumerate() {
				let idx_type = meta.vars[idx].as_str();
				if is_any_int(idx_type) {
					if use_unsigned {
						output += " u{}";
					}else{
						output += " i{}";
					}
				}else if is_any_float(idx_type) {
					output += " f{}";
				}else {
					output += " {}";
				}
			}
			output += "\"";
			for (idx,var_id) in var_names.iter().enumerate() {
				output += ",";
				if use_unsigned && meta.vars[idx] == "$INT_VEC_TYPE" {
					output += var_id.as_str();
					output += ".fmt_unsigned()";
				}else {
					output += var_id.as_str();
				}
			}
			output += ")";
			(output,true)
		},
		&|_| ("unimplemented!()".to_string(),true)
	).as_str();
	*contents += "\t\t}\n";
	*contents += "\t}\n";
	*contents += "}\n\n";
}
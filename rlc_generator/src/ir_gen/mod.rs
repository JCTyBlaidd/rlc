use std::path::Path;
use std::fs::{
	read_to_string,
	write,
};
use hashbrown::HashSet;
use indexmap::IndexMap;
use serde_json::{
	from_str,
	from_value,
	error::Result as JsonResult,
	Value
};
use std::ops::Index;

mod gen_enum;

mod gen_lexer;

mod gen_parser;

#[derive(Deserialize)]
pub struct MainMeta {
	pub vars: Vec<String>,
	#[serde(rename = "in")]
	pub in_meta: Vec<String>,
	#[serde(rename = "out")]
	pub out_meta: Vec<String>,
	pub side_effects: String,
	pub defs: IndexMap<String,String>,
}

#[derive(Deserialize)]
pub struct MetaNode {
	pub node: String,
}

pub fn generate(in_path: &Path, out_path: &Path, out_parser_path: &Path) {
	let ir_json = in_path.join("ir.json");
	let out_rs = out_path.join("nodes.rs");
	let lexer_gen_rs = out_parser_path.join("lexer_gen.rs");
	let value : Value = from_str(&read_to_string(ir_json).unwrap()).unwrap();
	
	let defines = &value["defines"];
	let main_enum = &defines["$MAIN"];
	
	let no_op_parse = &main_enum["$DEFAULT"];
	assert_eq!(no_op_parse.as_object().unwrap().len(), 1);
	let default_op_data_src : (&String,&Value) = no_op_parse.as_object().unwrap().iter().next().unwrap();
	let default_op = default_op_data_src.0.to_string();
	let default_parse: String = default_op_data_src.1.as_str().unwrap().to_string();
	
	let scope_parse: IndexMap<String,String> = from_value(main_enum["$SCOPES"].clone()).unwrap();
	
	let control_flow_parse = &main_enum["$CONTROL_FLOW"];
	let meta_nodes : IndexMap<String,MetaNode> = from_value(control_flow_parse.clone()).unwrap();
	
	let main_enum_meta_value = main_enum.as_object().unwrap().clone();
	let main_enum_meta : IndexMap<String,MainMeta> = main_enum_meta_value.into_iter().filter_map(|(key,value): (String,Value)| {
		if &key == "$DEFAULT" || &key == "$CONTROL_FLOW" || &key == "$SCOPES" {
			None
		}else {
			println!("PARSING: {}", &key);
			Some((key, from_value(value).unwrap()))
		}
	}).collect();
	
	let mut contents = String::new();
	contents += "//! Auto-Generated Code: rlc_generator::ir\n";
	contents += "//!  do not modify directly\n";
	contents += "//!  edit generator instead\n\n";
	contents += "use std::num::NonZeroU8;\n";
	contents += "use std::fmt::{Display, Result as FmtResult, Formatter};\n";
	contents += "use crate::code_section::metadata::CompileMeta;\n";
	contents += "use super::{\n";
	contents += "\tFloatType, IntType, PointerType,\n";
	contents += "\tBasicType, FlagType,\n";
	contents += "\tScopeTypeMeta,\n";
	contents += "\tBranchMeta, SwitchMeta, LoopMeta,\n";
	contents += "\tCallMeta, InvokeMeta,\n";
	contents += "\tLocalIdx,\n";
	contents += "\tinvalid_set, invalid_idx\n";
	contents += "};\n\n";
	
	//contents += "#[cold]\n";
	//contents += "#[inline(never)]\n";
	//contents += "fn invalid_type_idx(idx: u32) -> BasicType {\n";
	//contents += "\tpanic!(\"Invalid Type Idx: {}\", idx)\n";
	//contents += "}\n\n\n";
	
	//Generate Code
	gen_enum::gen_enum_decl(
		&default_op,
		&default_parse,
		&scope_parse,
		&meta_nodes,
		&main_enum_meta,
		&mut contents
	);
	contents += "\n\n\n";
	//GEN_IMPL BLOCKS
	
	write(out_rs, contents);
	
	
	//Generate Lexer
	let mut str_set : HashSet<String> = main_enum_meta.values().map(
		|v| v.defs.values().cloned()
	).flatten().collect();
	str_set.extend(scope_parse.values().cloned());
	str_set.insert("ops.none".to_string());
	str_set.insert("flow.branch".to_string());
	str_set.insert("flow.switch".to_string());
	str_set.insert("flow.loop".to_string());
	str_set.insert("flow.call".to_string());
	str_set.insert("flow.invoke".to_string());
	//Additional Keywords
	str_set.insert("functions".to_string());
	str_set.insert("inputs".to_string());
	str_set.insert("outputs".to_string());
	str_set.insert("induction".to_string());
	str_set.insert("code".to_string());
	str_set.insert("ops".to_string());
	str_set.insert("ops_true".to_string());
	str_set.insert("ops_false".to_string());
	str_set.insert("ops_if".to_string());
	str_set.insert("ops_default".to_string());
	str_set.insert("ops_return".to_string());
	str_set.insert("ops_unwind".to_string());
	str_set.insert("alias".to_string());
	str_set.insert("metadata".to_string());
	str_set.insert("types".to_string());
	str_set.insert("globals".to_string());
	str_set.insert("config".to_string());
	str_set.insert("target".to_string());
	str_set.insert("passes".to_string());
	//FIXME: ADD ANY MORE
	println!("{:?}",&str_set);
	
	let mut lexer_gen_contents = String::new();
	lexer_gen_contents += "//! Auto-Generated Code: rlc_generator::ir\n";
	lexer_gen_contents += "//!  do not modify directly\n";
	lexer_gen_contents += "//!  edit generator instead\n\n";
	lexer_gen_contents += "use super::{\n";
	lexer_gen_contents += "\tlexer::{Lexer, Tok, Tok::TokLexedKeyword}\n";
	lexer_gen_contents += "};\n";
	lexer_gen_contents += "use phf::{Map, Slice};\n\n";
	
	let lexer_map = gen_lexer::gen_lexer_decls(
		&str_set,
		&mut lexer_gen_contents
	);
	
	write(lexer_gen_rs,lexer_gen_contents);
	
	//Generate Parser
	let in_lalrpop = in_path.join("grammar_prelude.lalrpop");
	let prelude = read_to_string(in_lalrpop).unwrap();
	let parser = gen_parser::gen_parser_grammar(
		prelude.as_str(),
		&lexer_map,
		&default_op,
		&default_parse,
		&scope_parse,
		&meta_nodes,
		&main_enum_meta,
	);
	println!("{}",parser);
	//Write Generated Parser Spec Somewhere
	/*
	let tmp_parent = in_path.parent().unwrap();
	let tmp_file = tmp_parent.join("parser_gen.lalrpop");
	assert!(!tmp_file.exists());
	write(tmp_file,parser.as_str());
	
	//Generate Parser Spec
	let cfg = lalrpop::Configuration::new()
		.set_in_dir(tmp_parent)
		.set_out_dir(out_path)
		.process_file(tmp_file.as_path())
		.unwrap();
	
	//remove_file??
	*/
}
use hashbrown::{HashMap, HashSet};

pub fn gen_lexer_decls(
	input: &HashSet<String>,
	output: &mut String
) -> HashMap<String, String> {
	let mut mapping : HashMap<String,String> = input.iter()
		.map(|in_str| {
			let strip_str = in_str.split("@").next().unwrap();
			let mut new_name = "LTok".to_string();
			let mut make_caps = true;
			for val in strip_str.chars() {
				match val {
					'.' => {
						new_name.extend("Dot".chars());
						make_caps = true;
					},
					'_' => {
						make_caps = true;
					},
					'-' => {
						new_name.extend("Dash".chars());
						make_caps = true;
					},
					v @ 'a'..='z' => {
						if make_caps {
							new_name.push(v.to_ascii_uppercase());
							make_caps = false;
						}else{
							new_name.push(v);
						}
					}
					v @ 'A'..='Z' => {
						new_name.push(v)
					}
					fail => {
						panic!("Unsupported Char {}",fail)
					}
				}
			}
			(strip_str.to_string(),new_name)
	}).collect();
	
	//Generate Enum
	*output += "#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]\n";
	*output += "pub enum LexKeyTok {\n";
	for value in mapping.values() {
		*output += "\t";
		*output += value.as_str();
		*output += ",\n";
	}
	*output += "}\n\n\n";
	
	*output += "pub fn match_str<'input>(v: &str) -> Option<Tok<'input>> {\n";
	*output += "\tmatch LEXER_MAP.get(v) {\n";
	*output += "\t\tSome(v) => Some(TokLexedKeyword(*v)),\n";
	*output += "\t\tNone => None\n";
	*output += "\t}\n";
	*output += "}\n\n";
	
	
	let new_mapping : HashMap<String,String> = mapping.iter().map(|(k,v)| {
		let nv = "LexKeyTok::".to_string() + v.as_str();
		(k.clone(),nv)
	}).collect();
	let mut phf_map = phf_codegen::Map::new();
	for (k,v) in new_mapping.iter() {
		phf_map.entry(k.as_str(),v.as_str());
	}
	let mut out_vec = Vec::new();
	phf_map.build(&mut out_vec);
	
	*output += "static LEXER_MAP : Map<&'static str,LexKeyTok> = ";
	let map = String::from_utf8(out_vec).unwrap().replace("::phf::","");
	*output += map.as_str();
	*output += ";\n";
	
	*output += "\n\n\n\n";
	*output += "#[cfg(test)]\n";
	*output += "use super::lexer::perform_test;\n\n";
	*output += "#[cfg(test)]\n";
	*output += "mod tests {\n";
	*output += "\tuse super::*;\n\n";
	*output += "\t#[test]\n";
	*output += "\tfn test_lex_gen() {\n";
	for (k,v) in new_mapping.iter() {
		*output += "\t\tperform_test(\"";
		*output += k.as_str();
		*output += "\",TokLexedKeyword(";
		*output += v.as_str();
		*output += "));\n";
	}
	*output += "\t}\n";
	*output += "}\n";
	
	/*
	//Generate Matcher
	*output += "pub fn match_ident<'input>(\n";
	*output += "\tstart: usize,\n";
	*output += "\tinit: char,\n";
	*output += "\tlex: &mut Lexer<'input>\n";
	*output += ") -> Option<Result<(usize, Tok<'input>, usize), usize>> {\n";
	*output += "\tlet end = start;\n";
	*output += "\t";
	generate_lexer_matcher(
		mapping.keys().map(|v| (v.as_str(),v.as_str())).collect(),
		&mapping,
		"\t".to_string(),
		output,
		true
	);
	*output += "\n}\n\n";
	*/
	mapping
}

fn shared_prefix<'a,'b: 'a>(a: &'a str, b: &'b str) -> &'a str {
	let res = a.chars().zip(b.chars())
		.enumerate().find(|v| {
		let (c1,c2) = v.1;
		c1 != c2
	});
	match res {
		Some((loc,_)) => &a[..loc],
		None => if a.len() > b.len() {
			b
		}else{
			a
		}
	}
}

pub fn generate_lexer_matcher(
	input: HashMap<&str,&str>,
	map: &HashMap<String,String>,
	prefix: String,
	output: &mut String,
	use_init_flag: bool,
) {
	//Find all first characters:
	let mut has_none = false;
	let mut has_some = false;
	let first_chars : HashSet<Option<char>> = input.keys().map(|v| {
		let res = v.chars().next();
		if res.is_some() {
			has_some = true;
		}else{
			has_none = true;
		}
		res
	}).collect();
	
	//Fallback Matcher
	let mut new_prefix = prefix.clone();
	new_prefix.push('\t');
	
	if has_some && !has_none && first_chars.len() == 1 && !use_init_flag {
		//Only 1 matchable value - merge into slice if applicable
		let mut iter = input.keys();
		let start = *iter.next().unwrap();
		let shared_prefix = iter.fold(start,|merge,with| {
			shared_prefix(merge,with)
		});
		if shared_prefix.len() > 1 {
			//Generate Prefix Consume
			*output += "match lex.match_slice(end,&['";
			let mut prefix_iter = shared_prefix.chars();
			output.push(prefix_iter.next().unwrap());
			for value in prefix_iter {
				*output += "','";
				output.push(value);
			}
			*output += "']) {\n";
			*output += prefix.as_str();
			*output += "\tOk(end) => ";
			{
				let new_input_cull = shared_prefix.len();
				let new_input : HashMap<&str,&str> = input.iter().map(|(k,v)| {
					(&k[new_input_cull..],*v)
				}).collect();
				generate_lexer_matcher(
					new_input,
					map,
					new_prefix.clone(),
					output,
					false
				);
				*output += ",\n";
			}
			*output += prefix.as_str();
			*output += "\tErr(end) => Some(Err(end))\n";
			*output += prefix.as_str();
			*output += "}";
			return;
		}
	}
	
	
	if has_some {
		if has_none {
			assert!(!use_init_flag);
			*output += "match lex.peek_char() {\n";
			new_prefix.push('\t');
		}else{
			if use_init_flag {
				*output += "match init {\n";
			}else {
				*output += "match lex.next_char() {\n";
			}
		}
		for char_w in first_chars.iter() {
			if let Some(val) = *char_w {
				*output += prefix.as_str();
				if use_init_flag {
					*output += "\t'";
					output.push(val);
					*output += "' => ";
				}else {
					*output += "\tSome((end,'";
					output.push(val);
					*output += "')) => ";
				}
				if has_none {
					*output += "{\n";
					*output += prefix.as_str();
					*output += "\t\tlex.consume_peek();\n";
					*output += prefix.as_str();
					*output += "\t\t";
				}
				let new_input = input.iter().filter_map(|(k,&v)| {
					let mut iter = k.chars();
					if let Some(start_char) = iter.next() {
						if start_char == val {
							return Some((iter.as_str(),v));
						}
					}
					None
				}).collect();
				generate_lexer_matcher(
					new_input,
					map,
					new_prefix.clone(),
					output,
					false
				);
				if has_none {
					*output += "\n";
					*output += prefix.as_str();
					*output += "\t}";
				}
				*output += ",\n"
			}
		}
		*output += prefix.as_str();
		if has_none {
			*output += "\t_ => ";
			let val = input[""];
			let mut new_input = HashMap::new();
			new_input.insert("",val);
			generate_lexer_matcher(
				new_input,
				map,
				new_prefix.clone(),
				output,
				false
			);
			*output += "\n";
		}else{
			*output += "\t_ => Some(Err(start))\n";
		}
		*output += prefix.as_str();
		*output += "}";
	}else{
		assert!(has_none);
		let result = input[""];
		
		//Only Accept valid termination characters after
		*output += "if lex.peek_not_keyword() {\n";
		*output += prefix.as_str();
		*output += "\tSome(Ok((start,TokLexedKeyword(LexKeyTok::";
		*output += map[result].as_str();
		*output += "),end)))\n";
		*output += prefix.as_str();
		*output += "} else {\n";
		*output += prefix.as_str();
		*output += "\tSome(Err(end))\n";
		*output += prefix.as_str();
		*output += "}"
	}
}
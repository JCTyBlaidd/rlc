use hashbrown::HashMap;
use serde_json::{
	Value,
	from_value,
};


#[derive(Clone, Debug, Default)]
#[serde(transparent)]
#[derive(Serialize, Deserialize)]
pub struct Filter(HashMap<String,Value>);

impl Filter {
	///Return the set of features required to accept if it can be accepted
	pub fn apply_filter(&self,meta: &ParseGenerateISA) -> Option<Vec<String>> {
		let mut feature_flags = Vec::new();
		for (k,v) in self.0.iter() {
			if k == "flags" {
				feature_flags = from_value(v.clone()).ok()?;
			} else if k == "base_size" {
				match from_value(v.clone()) {
					Ok(t) => {
						let v : u8 = t;
						if v != meta.base_size {
							return None
						}
					}
					Err(_) => {
						let tmp : HashMap<String,i64> = from_value(v.clone()).ok()?;
						for (constraint,value) in tmp.iter() {
							if constraint == "==" && *value != meta.base_size as i64 {
								return None;
							}else if constraint == ">=" && *value > meta.base_size as i64{
								return None;
							}else if constraint == "<=" && *value < meta.base_size as i64{
								return None;
							}else if constraint == ">" && *value >= meta.base_size as i64{
								return None;
							}else if constraint == "<" && *value <= meta.base_size as i64{
								return None;
							}
						}
					}
				};
			} else {
				let contains : bool = from_value(v.clone()).ok()?;
				let has_key = meta.flags.contains(k);
				if contains != has_key {
					return None;
				}
			}
		}
		for flag in feature_flags.iter() {
			if !meta.features.contains(flag) {
				return None;
			}
		}
		Some(feature_flags)
	}
	
	pub fn apply_filter_full(&self,meta: &ParseGenerateISA) -> Option<()> {
		match self.apply_filter(meta) {
			Some(v) => if v.is_empty() {
				Some(())
			}else {
				None
			},
			None => None
		}
	}
}


#[derive(Clone, Debug)]
#[serde(transparent)]
#[derive(Serialize, Deserialize)]
pub struct FilteredInteger(Value);

#[derive(Serialize, Deserialize)]
struct IntegerFilterEntry {
	value: i64,
	filter: Filter,
}

impl FilteredInteger{
	pub fn generate_value(&self,meta: &ParseGenerateISA) -> Option<i64> {
		match from_value(self.0.clone()) {
			Ok(v) => {
				Some(v)
			},
			Err(_) => {
				let tmp : Vec<IntegerFilterEntry> = from_value(self.0.clone()).ok()?;
				for val in tmp {
					let filter =  val.filter.apply_filter(meta);
					if let Some(req) = filter {
						if req.is_empty() {
							return Some(val.value);
						}
					}
				}
				None
			}
		}
	}
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseFeatureDependency {
	pub name: String,
	pub requirements: Vec<String>
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseGenerateISA {
	pub name: String,
	pub base_size: u8,
	pub flags: Vec<String>,
	pub features: Vec<String>,
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseGenerateISAAlias {
	pub name: String,
	pub base_name: String,
	pub enabled_features: Vec<String>,
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseMetadata {
	pub feature_support_unaligned_memory: bool,
	
	pub feature_flags: Vec<String>,
	
	pub feature_deps: HashMap<String,Vec<String>>,
	
	pub feature_composite: Vec<ParseFeatureDependency>,
	
	pub generated_isa_list: Vec<ParseGenerateISA>,
	
	pub generated_isa_alias: Vec<ParseGenerateISAAlias>,
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseRegisterSpecial {
	
	#[serde(default)]
	pub filter: Filter,
	
	pub id: u32,
	
	pub case: String,
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseRegisterBank {
	pub name: String,
	
	#[serde(default)]
	pub filter: Filter,
	
	#[serde(rename = "type")]
	pub reg_type: String,
	
	#[serde(default)]
	pub make_default_view: bool,
	
	pub register_prefix: String,
	
	#[serde(default)]
	pub special_case: Vec<ParseRegisterSpecial>,
	
	pub count: FilteredInteger,
	
	pub size: FilteredInteger,
	
	#[serde(default)]
	pub reg_names: Vec<String>
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseRegisterView {
	
	pub name: String,
	
	pub bank: String,
	
	#[serde(default)]
	pub filter: Filter,
	
	pub view_size: FilteredInteger,
	
	pub view_count: FilteredInteger,
	
	pub register_per_view: FilteredInteger,
	
	pub reg_offset: FilteredInteger,
	
	pub reg_stride: FilteredInteger,
	
	#[serde(default)]
	pub special_case: Vec<ParseRegisterSpecial>,
	
	#[serde(default)]
	pub reg_names: Vec<String>
}



#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseInstruction {
	pub name: String,
	
	#[serde(default)]
	pub filter: Filter,
	
	#[serde(rename = "type")]
	pub meta_name: String,
	
	pub args: HashMap<String,String>,
	
	#[serde(default)]
	pub sub_instructions: Vec<ParseInstruction>,
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseLayoutMeta {
	
	pub name: String,
	
	pub bits: u8,
	
	#[serde(default)]
	pub reg_bank: Option<String>,
	
	#[serde(default)]
	pub imm_mode: Option<String>,
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseLayout {
	pub name: String,
	
	#[serde(default)]
	pub filter: Filter,
	
	pub layout: Vec<ParseLayoutMeta>,
	
	pub instructions: Vec<ParseInstruction>
}


#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct ParseISAFile {
	pub meta: ParseMetadata,
	
	pub reg_banks: Vec<ParseRegisterBank>,
	
	#[serde(default)]
	pub reg_views: Vec<ParseRegisterView>,
	
	pub layouts: Vec<ParseLayout>,
}
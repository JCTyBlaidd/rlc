use serde_json::{
	de::from_str,
	error::Result as JsonResult
};

pub mod isa_instruction;
pub mod isa_register;
pub mod isa_meta;
pub mod isa_parse_data;

pub mod meta_parse_data;
pub mod meta_instructions;

use self::isa_parse_data::ParseISAFile;
use self::meta_parse_data::ParseMetaFile;
pub use self::isa_meta::{
	ISAMetadata,
	ISAAlias
};
pub use self::meta_instructions::{
	MetaInstruction
};
pub use self::meta_parse_data::ParseMetaEnum;

pub fn load_meta_file(s: &str) -> JsonResult<(Vec<MetaInstruction>,Vec<ParseMetaEnum>)>{
	let val : JsonResult<ParseMetaFile> = from_str(s);
	match val {
		Ok(parsed) => {
			let mut meta = Vec::new();
			let mut enums = Vec::new();
			for meta_layout in parsed.meta_instructions.iter() {
				meta.append(&mut MetaInstruction::parse_from(meta_layout));
			}
			for enum_layout in parsed.meta_data{
				enums.push(enum_layout)
			}
			Ok((meta,enums))
		},
		Err(e) => {
			println!("Error: {:?}",e);
			Err(e)
		}
	}
}

pub fn load_isa_file(s: &str) -> JsonResult<(Vec<ISAMetadata>,Vec<ISAAlias>)>{
	let val : JsonResult<ParseISAFile> = from_str(s);
	match val {
		Ok(parsed) => {
			let mut meta = Vec::new();
			let mut alias = Vec::new();
			for meta_parse in parsed.meta.generated_isa_list.iter() {
				meta.push(ISAMetadata::load_isa_metadata(&parsed,&meta_parse));
			}
			for meta_alias in parsed.meta.generated_isa_alias.iter() {
				alias.push(ISAAlias::load_isa_metadata(&parsed,&meta_alias));
			}
			Ok((meta,alias))
		},
		Err(e) => {
			println!("Error: {:?}",e);
			Err(e)
		}
	}
}
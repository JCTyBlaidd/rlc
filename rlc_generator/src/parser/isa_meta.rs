use super::isa_instruction::*;
use super::isa_register::*;
use super::isa_parse_data::*;
use hashbrown::HashMap;


/// All parsed meta-data regarding
///  an instruction-set-architecture
#[derive(Clone, Debug)]
pub struct ISAMetadata {
	
	/// Do standard memory accesses support
	///  un-aligned memory access even if
	///  slower than aligned
	pub supports_unaligned_memory: bool,
	
	/// List of possible features
	pub feature_flags: Vec<String>,
	
	/// List of features dependencies
	pub feature_dependencies: HashMap<String,Vec<String>>,
	
	/// Composite features
	pub feature_composites: Vec<(String,Vec<String>)>,
	
	/// The name of the ISA
	pub name: String,
	
	/// The size of the pointer of the ISA in bits
	pub base_size: u8,
	
	/// The register banks of the ISA
	pub register_banks: Vec<RegisterBank>,
	
	/// The register views of the ISA
	pub register_views: Vec<RegisterView>,
	
	/// The instructions of this ISA
	pub instructions: Vec<InstructionMetadata>
}

impl ISAMetadata {
	pub fn load_isa_metadata(parse_file: &ParseISAFile, parse_isa: &ParseGenerateISA) -> ISAMetadata {
		let banks = RegisterBank::load_register_banks(
			parse_file,
			parse_isa
		);
		let views = RegisterView::load_register_views(
			parse_file,
			parse_isa,
			banks.as_slice()
		);
		ISAMetadata {
			supports_unaligned_memory: parse_file.meta.feature_support_unaligned_memory,
			feature_flags: parse_isa.features.clone(),
			feature_dependencies: parse_file.meta.feature_deps.iter().filter_map(|(k,v)| {
				if parse_isa.features.contains(k) {
					Some((k.clone(),v.clone()))
				}else{
					None
				}
			}).collect(),
			feature_composites: parse_file.meta.feature_composite.iter().filter_map(|v| {
				if v.requirements.iter().all(|req| parse_isa.features.contains(req)) {
					Some((v.name.clone(),v.requirements.clone()))
				}else{
					None
				}
			}).collect(),
			name: parse_isa.name.clone(),
			base_size: parse_isa.base_size,
			register_banks: banks,
			register_views: views,
			instructions: InstructionMetadata::load_instructions(parse_file,parse_isa)
		}
	}
}

#[derive(Clone, Debug)]
pub struct ISAAlias {
	
	/// Name of the Alias ISA
	pub alias_name: String,
	
	/// Name of the Base ISA
	pub isa_name: String,
	
	/// Enabled features of the ISA
	pub feature_list: Vec<String>
	
}

impl ISAAlias {
	pub fn load_isa_metadata(_parse_file: &ParseISAFile, parse_isa: &ParseGenerateISAAlias) -> ISAAlias {
		ISAAlias {
			alias_name: parse_isa.name.clone(),
			isa_name: parse_isa.base_name.clone(),
			feature_list: parse_isa.enabled_features.clone()
		}
	}
}





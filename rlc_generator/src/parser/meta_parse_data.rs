use hashbrown::HashMap;

#[derive(Serialize, Deserialize)]
pub struct ParseMetaEnum {
	pub name: String,
	
	#[serde(rename = "type")]
	pub enum_type: String,
	
	pub values: Vec<String>,
}

#[derive(Serialize, Deserialize)]
pub struct ParseMetaInstruction {
	
	pub inputs: Vec<String>,

	pub outputs: Vec<String>,
	
	#[serde(default)]
	pub config: HashMap<String,String>,
	
	#[serde(default)]
	pub name_prefix: String,

	#[serde(default)]
	pub name_suffix: String,

	#[serde(default)]
	pub alias_prefix: String,

	#[serde(default)]
	pub alias_suffix: String,

	pub instructions: Vec<String>,
}

#[derive(Serialize, Deserialize)]
pub struct ParseMetaFile {
	pub meta_data: Vec<ParseMetaEnum>,
	
	pub meta_instructions: Vec<ParseMetaInstruction>,
	
}
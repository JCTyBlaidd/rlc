use super::isa_parse_data::*;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum RegisterType {
	
	/// The register is standard
	///  with no unique properties
	StandardRegister,
	
	/// The register always reads as 0
	///  and ignores all write operations
	ZeroRegister,
	
	/// The register stores the program
	///  counter
	ProgramCounter,
	
	/// The register stores program
	///  flags set by site effects of
	///  other instructions
	FlagRegister,
	
	/// The register is undefined
	///  and cannot be used
	UndefinedRegister,
}


#[derive(Clone, Debug)]
pub struct RegisterBank {
	
	/// Name of the register bank
	pub bank_id: String,
	
	/// Size of the register in bits
	pub register_size: u32,
	
	/// Number of registers in the bank
	pub register_count: u32,
	
	/// Standard Type of the register in the
	///  bank
	pub bank_type: RegisterType,
	
	/// Registers with special properties
	///  compared to the other registers in the bank
	pub special_registers: Vec<(u32,RegisterType)>,
}


fn get_type(input: &str) -> RegisterType {
	if input == "STANDARD_REGISTER" {
		RegisterType::StandardRegister
	}else if input == "ZERO_REGISTER" {
		RegisterType::ZeroRegister
	}else if input == "PROGRAM_COUNTER" {
		RegisterType::ProgramCounter
	}else if input == "FLAG_REGISTER" {
		RegisterType::FlagRegister
	}else if input == "UNDEFINED_REGISTER" {
		RegisterType::UndefinedRegister
	}else {
		println!("WARN: unknown reg type: {}",input);
		RegisterType::UndefinedRegister
	}
}

impl RegisterBank {
	pub fn load_register_banks(parse_file: &ParseISAFile, parse_isa: &ParseGenerateISA) -> Vec<RegisterBank> {
		parse_file.reg_banks.iter().filter_map(|bank| {
			match bank.filter.apply_filter_full(parse_isa) {
				Some(()) => {
					Some(RegisterBank {
						bank_id: bank.name.clone(),
						register_size: bank.size.generate_value(parse_isa).unwrap() as u32,
						register_count: bank.count.generate_value(parse_isa).unwrap() as u32,
						bank_type: get_type(bank.reg_type.as_str()),
						special_registers: bank.special_case.iter().filter_map(|case| {
							match case.filter.apply_filter_full(parse_isa) {
								Some(()) => {
									Some((case.id,get_type(case.case.as_str())))
								},
								None => None
							}
						}).collect()
					})
				},
				None => None
			}
		}).collect()
	}
}


#[derive(Clone, Debug)]
pub struct RegisterView {
	
	/// Name of the register view
	pub view_id: String,
	
	/// Name of the associated register bank
	pub bank_id: String,
	
	/// Size of the register view in bits
	pub register_view_size: u32,
	
	/// Number of registers views to use
	pub register_view_count: u32,
	
	/// Number of raw registers per register view
	pub register_per_view: u32,
	
	/// Offset of first view in registers
	pub register_offset: u8,
	
	/// Stride of views per register
	///  default = register_per_view
	pub register_stride: u8,
	
	/// Registers Views with special properties
	///  compared to the other registers views in the view
	pub special_register_views: Vec<(u32,RegisterType)>,
	
	/// The names of the registers
	pub register_names: Vec<String>
}

fn default_reg_names(name: &str, count: u32) -> Vec<String> {
	let mut vec = Vec::new();
	for i in 0..count {
		vec.push(name.to_string() + i.to_string().as_str())
	}
	vec
}

impl RegisterView {
	pub fn load_register_views(
		parse_file: &ParseISAFile,
		parse_isa: &ParseGenerateISA,
		reg_banks: &[RegisterBank]
	) -> Vec<RegisterView> {
		let mut raw_views : Vec<RegisterView> = parse_file.reg_views.iter().filter_map(|view| {
			if view.filter.apply_filter_full(parse_isa).is_some() {
				let view_count = view.view_count.generate_value(parse_isa).unwrap() as u32;
				let reg_offset = view.reg_offset.generate_value(parse_isa).unwrap() as u8;
				let reg_stride = view.reg_stride.generate_value(parse_isa).unwrap() as u8;
				Some(RegisterView {
					view_id: view.name.clone(),
					bank_id: view.bank.clone(),
					register_view_size: view.view_size.generate_value(parse_isa).unwrap() as u32,
					register_view_count: view_count,
					register_per_view: view.register_per_view.generate_value(parse_isa).unwrap() as u32,
					register_offset: reg_offset,
					register_stride: reg_stride,
					special_register_views: reg_banks.iter().find(|bank| {
						bank.bank_id == view.bank
					}).map(|bank| {
						bank.special_registers.iter().filter_map(|(idx,val)| {
							if (*idx - reg_offset as u32) % reg_stride as u32 == 0 {
								Some((*idx,val.clone()))
							}else{
								None
							}
						}).collect()
					}).unwrap(),
					register_names: default_reg_names(view.name.as_str(),view_count)
				})
			}else{
				None
			}
		}).collect();
		let mut gen_views : Vec<RegisterView> = parse_file.reg_banks.iter().filter_map(|bank| {
			if bank.filter.apply_filter_full(parse_isa).is_some() && bank.make_default_view {
				let view_count = bank.count.generate_value(parse_isa).unwrap() as u32;
				Some(RegisterView {
					view_id: bank.name.clone(),
					bank_id: bank.name.clone(),
					register_view_size: bank.size.generate_value(parse_isa).unwrap() as u32,
					register_view_count: view_count,
					register_per_view: 1,
					register_offset: 0,
					register_stride: 1,
					special_register_views: bank.special_case.iter().filter_map(|case| {
						match case.filter.apply_filter_full(parse_isa) {
							Some(()) => {
								Some((case.id,get_type(case.case.as_str())))
							},
							None => None
						}
					}).collect(),
					register_names: default_reg_names(bank.name.as_str(),view_count)
				})
			}else{
				None
			}
		}).collect();
		raw_views.append(&mut gen_views);
		raw_views
	}
}
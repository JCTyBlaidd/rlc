use super::isa_parse_data::*;
use hashbrown::{
	HashMap,
	HashSet
};

#[derive(Copy, Clone, Debug)]
pub struct RawBits {
	num_bits: u8,
	raw_bits: u128,
}

impl RawBits {
	pub fn generate_sized(raw: &[ParseLayoutMeta]) -> RawBits {
		let mut tmp = RawBits {
			num_bits: 0,
			raw_bits: 0,
		};
		for layout in raw {
			tmp.num_bits += layout.bits;
		}
		assert_eq!(tmp.num_bits % 8,0);
		tmp
	}
	pub fn specialize_into(&self,raw: &[ParseLayoutMeta], map: &HashMap<String,String>) -> RawBits {
		let mut tmp = *self;
		let mut offset = 0;
		for layout in raw {
			if let Some(raw_str) = map.get(layout.name.as_str()) {
				assert_eq!(raw_str.len(),layout.bits as usize);
				let parse_bits : u128 = u128::from_str_radix(raw_str,2).unwrap();
				tmp.raw_bits |= parse_bits << offset;
			}
			offset += layout.bits;
		}
		tmp
	}
	pub fn modify(&self, value: u128, offset: u8, size: u8) -> RawBits {
		let bit_mask = (1 << size) - 1;
		let or_mask = (value & bit_mask) << offset;
		let and_mask = (!value & bit_mask) << offset;
		RawBits {
			num_bits: self.num_bits,
			raw_bits: (self.raw_bits & and_mask) | or_mask,
		}
	}
}

#[derive(Clone, Debug)]
pub enum LayoutAssignment {
	RegisterAssignment {
		
		/// Instruction Offset for assignment
		offset_bits: u8,
		
		/// Number of bits available
		assignment_bits: u8,
		
		///Idx of this register (in/out)
		idx: u8,
		
		///Is this register a input
		is_input: bool,
		
		/// 4 x ASCII ID of the associated view
		bank: String,
	},
	ImmediateAssignment {
		
		/// Instruction Offset for assignment
		offset_bits: u8,
		
		/// Number of bits available
		assignment_bits: u8,
		
		/// Offset of the immediate sub-set encoded
		///  e.g. imm1(1:10) = 1 as offset
		immediate_subset_offset: u8,
		
		///Idx of this immediate
		idx: u8,
		
		///If the immediate stored is signed
		is_signed: bool,
		
		///If the immediate is sign extended
		///  before use
		is_sign_extended: bool,
	}
}

//FIXME: HARDCODED REGISTERS
//FIXME: Instruction Costs (throughput / latency)?


#[derive(Clone, Debug)]
pub struct InstructionMetadata {
	
	/// Required Features
	pub require_features: HashSet<String>,
	
	/// The ISA Instruction Name
	pub insn_name: String,
	
	///The meta-instruction name of this
	/// instruction
	pub meta_name: String,
	
	///The base bit-field of the instruction
	pub bit_base: RawBits,
	
	///Bit-fields that are assignable to
	/// based on some metadata
	pub bit_assignments: Vec<LayoutAssignment>,
	
	///Instructions that are a strict sub-set
	/// of the valid instructions of this type
	/// e.g. addi a, 0, b => mv a, b (for some isa)
	pub sub_instruction: Vec<InstructionMetadata>
}


impl InstructionMetadata {
	fn parse_idx(name: &str) -> u8 {
		if name == "rd" {
			0
		}else if name == "rs" {
			0
		}else if name == "imm" {
			0
		}else if name.starts_with("rd") {
			name[2..].parse().map_err(|e| {
				eprintln!("Failed to parse: {}",name);
				e
			}).unwrap()
		}else if name.starts_with("rs") {
			name[2..].parse().map_err(|e| {
				eprintln!("Failed to parse: {}",name);
				e
			}).unwrap()
		}else if name.starts_with("imm") {
			if let Some(pos) = name.chars().position(|c| c == '(') {
				name[3..pos].parse().map_err(|e| {
					eprintln!("Failed to parse: {}",name);
					e
				}).unwrap()
			}else {
				name[3..].parse().map_err(|e| {
					eprintln!("Failed to parse: {}", name);
					e
				}).unwrap()
			}
		}else{
			panic!("Unknown Idx Reg: {}",name);
		}
	}
	
	fn parse_offset(name: &str, bits: u8) -> u8 {
		if name.starts_with("imm") {
			if let Some(pos) = name.chars().position(|c| c == '(') {
				let sub_str = &name[pos+1..name.len() - 1];
				if let Some(mid) = sub_str.chars().position(|c| c == ':') {
					let pre = &sub_str[..mid];
					let post = &sub_str[mid+1..];
					let pre_val : u8 = pre.parse().map_err(|e| {
						eprintln!("Failed parse offset: {} [{}]",e,sub_str);
						e
					}).unwrap();
					let post_val : u8 = post.parse().map_err(|e| {
						eprintln!("Failed to parse offset: {} [{}]",e,sub_str);
						e
					}).unwrap();
					assert_eq!(post_val - pre_val + 1,bits);
					pre_val
				}else{
					panic!("Invalid SubStr Offset: {}", sub_str);
				}
			}else{
				0
			}
		}else{
			0
		}
	}
	
	
	fn create_instruction_metadata(
		parse_isa: &ParseGenerateISA,
		layout: &ParseLayout,
		features: &HashSet<String>,
		insn: &ParseInstruction,
		raw_bits: RawBits,
		set_args: &HashSet<String>
	) -> Option<InstructionMetadata> {
		
		//Merge of required features for super-filter + this
		let extra_features = insn.filter.apply_filter(parse_isa)?;
		let mut new_features = features.clone();
		for feature in extra_features {
			new_features.insert(feature);
		}
		
		//Increase set args
		let mut new_args = set_args.clone();
		for arg in insn.args.keys() {
			new_args.insert(arg.clone());
		}
		
		//New Layout Bits
		let new_bits = raw_bits.specialize_into(&layout.layout,&insn.args);
		
		//Temp Storage so that the assignments get the
		// correct offset values
		let mut bit_offset = 0;
		
		Some(InstructionMetadata {
			require_features: new_features.clone(),
			insn_name: insn.name.clone(),
			meta_name: insn.meta_name.clone(),
			bit_base: new_bits,
			bit_assignments: layout.layout.iter().filter_map(|assign| {
				bit_offset += assign.bits;
				if !new_args.contains(assign.name.as_str()) {
					if let Some(reg_bank) = &assign.reg_bank {
						Some(LayoutAssignment::RegisterAssignment {
							offset_bits: bit_offset - assign.bits,
							assignment_bits: assign.bits,
							idx: Self::parse_idx(&assign.name),
							is_input: assign.name.starts_with("rs"),
							bank: reg_bank.clone()
						})
					}else if let Some(imm_mode) = &assign.imm_mode {
						Some(LayoutAssignment::ImmediateAssignment {
							offset_bits: bit_offset - assign.bits,
							assignment_bits: assign.bits,
							immediate_subset_offset: Self::parse_offset(&assign.name,assign.bits),
							idx: Self::parse_idx(&assign.name),
							is_signed: imm_mode.contains("S+"),
							is_sign_extended: imm_mode.contains("+SEXT")
						})
					}else{
						panic!("Contains neither reg_bank nor imm_mode: {} => {}", insn.name,assign.name)
					}
				} else {
					None
				}
			}).collect(),
			sub_instruction: insn.sub_instructions.iter().filter_map(|sub_insn| {
				Self::create_instruction_metadata(
					parse_isa,
					layout,
					&new_features,
					sub_insn,
					new_bits,
					&new_args,
				)
			}).collect()
		})
	}
	
	pub fn load_instructions(
		parse_file: &ParseISAFile,
		parse_isa: &ParseGenerateISA
	) -> Vec<InstructionMetadata> {
		let mut isa_list = Vec::new();
		for layout in parse_file.layouts.iter() {
			if let Some(features) = layout.filter.apply_filter(parse_isa) {
				for insn in layout.instructions.iter() {
					
					//Generate size w/ no specialization (done in `create_instruction_metadata`)
					let raw_bits = RawBits::generate_sized(&layout.layout);
					
					let mut base_feature_set = HashSet::new();
					base_feature_set.extend(features.iter().map(Clone::clone));
					
					//If not filtered - push to instruction list
					if let Some(meta) = Self::create_instruction_metadata(
						parse_isa,
						layout,
						&base_feature_set,
						insn,
						raw_bits,
						&HashSet::new()
					) {
						isa_list.push(meta);
					}
				}
			}
		}
		isa_list
	}
}
use hashbrown::HashMap;
use super::meta_parse_data::{
	ParseMetaEnum,
	ParseMetaInstruction
};

#[derive(Clone, Debug)]
pub enum MetaInstructionConfig {
	BooleanConfig,
	IntegerConfig(u8),
	EnumConfig(String),
}

#[derive(Clone, Debug)]
pub enum MetaInstructionAssignment {
	BooleanValue(bool),
	IntegerValue(u8,u128),
	EnumValue(String,String),
}

#[derive(Clone, Debug)]
pub struct MetaInstruction {
	pub enum_name: String,
	
	pub ref_name: String,
	
	pub isa_config: HashMap<String,MetaInstructionConfig>,
	
	pub input_reg_count: u8,
	pub input_immediate_count: u8,
	pub output_reg_count: u8,
}


impl MetaInstruction {
	
	pub fn parse_from(parse: &ParseMetaInstruction) -> Vec<MetaInstruction> {
		let mut new_config = HashMap::new();
		for (k,v) in parse.config.iter() {
			let res = if v == "bool" {
				MetaInstructionConfig::BooleanConfig
			}else if v == "u8" {
				MetaInstructionConfig::IntegerConfig(8)
			}else if v == "u16" {
				MetaInstructionConfig::IntegerConfig(16)
			}else if v == "u32" {
				MetaInstructionConfig::IntegerConfig(32)
			}else if v == "u64" {
				MetaInstructionConfig::IntegerConfig(64)
			}else {
				MetaInstructionConfig::EnumConfig(v.clone())
			};
			new_config.insert(k.clone(),res);
		}
		let input_reg = parse.inputs.iter().filter(|&v| {
			v.starts_with("rs")
		}).count();
		let input_imm = parse.inputs.iter().filter(|&v| {
			v.starts_with("imm")
		}).count();
		let output_reg = parse.outputs.iter().filter(|&v| {
			v.starts_with("rd")
		}).count();
		assert_eq!(input_reg + input_imm,parse.inputs.len());
		assert_eq!(output_reg,parse.outputs.len());
		let base_instruction = MetaInstruction {
			enum_name: String::new(),
			ref_name: String::new(),
			isa_config: new_config,
			input_reg_count: input_reg as u8,
			input_immediate_count: input_imm as u8,
			output_reg_count: output_reg as u8
		};
		
		let mut instruction_list = Vec::new();
		for instruction in parse.instructions.iter() {
			let mut name_isa = String::new();
			for value in instruction.chars() {
				if value.is_ascii_uppercase() && name_isa.len() != 0 {
					name_isa.push('-');
				}
				name_isa.push(value);
			}
			instruction_list.push(MetaInstruction {
				enum_name: parse.alias_prefix.clone() + instruction.as_str() + parse.alias_suffix.as_str(),
				ref_name: parse.name_prefix.clone() + name_isa.as_str() + parse.name_suffix.as_str(),
				..base_instruction.clone()
			});
		}
		instruction_list
	}
	
	
	pub fn try_assignments_for(
		&self,
		name: &str,
		enums: &[ParseMetaEnum]
	) -> Option<HashMap<String,MetaInstructionAssignment>> {
		let assign_me : Vec<&str> = self.ref_name.split('-').collect();
		let assign_name : Vec<&str> = name.split('-').collect();
		
		if assign_me.len() != assign_name.len() {
			return None;
		}
		
		let mut assign = HashMap::new();
		for i in 0..assign_me.len() {
			if assign_me[i] != assign_name[i] {
				let mut found = false;
				for (key_assignment,assign_type) in self.isa_config.iter() {
					let new_key = "{".to_string() + key_assignment.as_str() + "}";
					if &new_key == assign_me[i] {
						match assign_type {
							MetaInstructionConfig::BooleanConfig => {
								if assign_name[i] == "{true}" {
									assign.insert(key_assignment.clone(),MetaInstructionAssignment::BooleanValue(true));
									found = true;
									break;
								}else if assign_name[i] == "{false}" {
									assign.insert(key_assignment.clone(), MetaInstructionAssignment::BooleanValue(false));
									found = true;
									break;
								}
							},
							MetaInstructionConfig::EnumConfig(id) => {
								for enum_val in enums {
									if &enum_val.name == id {
										for enum_key in enum_val.values.iter() {
											let new_assign = "{".to_string() + enum_key.as_str() + "}";
											if assign_name[i] == &new_assign {
												assign.insert(
													key_assignment.clone(),
													MetaInstructionAssignment::EnumValue(
														enum_val.name.clone(),
														enum_key.clone()
													)
												);
												found = true;
												break;
											}
										}
										break;
									}
								}
								break;
							},
							MetaInstructionConfig::IntegerConfig(size) => {
								let sub_name = &assign_name[i][1usize..assign_name.len() - 1];
								if &assign_name[i][0..1] == "{" && &assign_name[i][assign_name.len() - 1..assign_name.len()] == "}"{
									if let Ok(val) = sub_name.parse() {
										assign.insert(
											key_assignment.clone(),
											MetaInstructionAssignment::IntegerValue(
												*size,
												val
											)
										);
										found = true;
									}
								}
							}
						}
					}
				}
				if !found {
					return None;
				}
			}
		}
		Some(assign)
	}
}
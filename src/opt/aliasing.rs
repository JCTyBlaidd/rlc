

/// Internal Optimization Structure
///  to store aliasing deductions
/// e.g:
///  A never aliases B
///  A might alias B
///  A always aliases B
pub struct AliasingDB {

}

#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum AliasResult {
	NeverAlias,
	MaybeAlias,
	AlwaysAlias,
}
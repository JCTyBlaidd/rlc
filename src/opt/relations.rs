use hashbrown::{
	HashSet,
	HashMap,
};
/*

use crate::code_section::NodeIdx;

/// Internal optimization structure
///  that stores and deduces relational
///  facts for integer and floating-point
///  values.
/// Example Facts:
///  A > B, A <= B, etc..
///  A != 0, A >= 42, etc..
///  A % 5 == 3, etc..
///  A is power of 2
///  A & 0b1001010 == ZERO_BITS (bits are always 0)
///  A | 0b1001010 == ONE_BITS  (bits are always 1)
pub struct RelationalDB {
	facts: HashMap<NodeIdx,HashSet<RelationFact>>,
}

impl RelationalDB {
	fn register_fact(&mut self, fact: RelationFact) {
		if let Some(val) = fact.get_first() {
			self.facts.entry(val).or_default().insert(fact);
		}
		if let Some(val) = fact.get_second() {
			self.facts.entry(val).or_default().insert(fact);
		}
		if let Some(val) = fact.get_third() {
			self.facts.entry(val).or_default().insert(fact);
		}
	}
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum RelationResult {
	AlwaysTrue,
	Unknown,
	AlwaysFalse,
}

//FIXME: IMPL PROPERLY
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
enum RelationValue {
	SignedConstant(i128),
	UnsignedConstant(u128),
	NodeValue(NodeIdx),
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
enum RelationFact {
	
	/// self.0 >= self.1
	GreaterOrEqual(RelationValue,RelationValue),
	
	/// self.0 <= self.1
	LesserOrEqual(RelationValue,RelationValue),
	
	/// self.0 > self.1
	Greater(RelationValue,RelationValue),
	
	/// self.0 < self.1
	Lesser(RelationValue,RelationValue),
	
	/// self.0 == self.1
	Equal(RelationValue,RelationValue),
	
	/// self.0 != self.1
	NotEqual(RelationValue,RelationValue),
	
	/// self.0 is a power of 2
	PowerOfTwo(RelationValue),
	
	/// self.0 & self.1 = 0b0000000000...
	ZeroBits(RelationValue,u128),
	
	/// self.0 | self.1 == 0b111111111...
	OneBits(RelationValue,u128),
	
	/// self.0 % self.1 == self.2
	KnownRemainder(RelationValue,RelationValue,RelationValue),
}

impl RelationFact {
	pub fn get_first(&self) -> Option<NodeIdx> {
		use self::RelationFact::*;
		use self::RelationValue::*;
		match self {
			GreaterOrEqual(NodeValue(t),_) => Some(*t),
			LesserOrEqual(NodeValue(t),_) => Some(*t),
			Greater(NodeValue(t),_) => Some(*t),
			Lesser(NodeValue(t),_) => Some(*t),
			Equal(NodeValue(t),_) => Some(*t),
			NotEqual(NodeValue(t),_) => Some(*t),
			PowerOfTwo(NodeValue(t)) => Some(*t),
			ZeroBits(NodeValue(t),_) => Some(*t),
			OneBits(NodeValue(t),_) => Some(*t),
			KnownRemainder(NodeValue(t),_,_) => Some(*t),
			_ => None
		}
	}
	pub fn get_second(&self) -> Option<NodeIdx> {
		use self::RelationFact::*;
		use self::RelationValue::*;
		match self {
			GreaterOrEqual(_,NodeValue(t)) => Some(*t),
			LesserOrEqual(_,NodeValue(t)) => Some(*t),
			Greater(_,NodeValue(t)) => Some(*t),
			Lesser(_,NodeValue(t)) => Some(*t),
			Equal(_,NodeValue(t)) => Some(*t),
			NotEqual(_,NodeValue(t)) => Some(*t),
			KnownRemainder(_,NodeValue(t),_) => Some(*t),
			_ => None
		}
	}
	pub fn get_third(&self) -> Option<NodeIdx> {
		use self::RelationFact::*;
		use self::RelationValue::*;
		match self {
			KnownRemainder(_,_,NodeValue(t)) => Some(*t),
			_ => None
		}
	}
}

*/
#![feature(slice_index_methods)]
#![feature(try_trait)]
#![feature(concat_idents)]
#![feature(box_into_raw_non_null)]
#![feature(maybe_uninit_extra)]
#![feature(arbitrary_enum_discriminant)]
#![feature(const_generics)]
#![feature(const_if_match)]
#![feature(saturating_neg)]

//Suppress unused warnings during development
#![allow(unused)]

#![feature(trace_macros)]
trace_macros!(false);


//#[macro_use]
//extern crate serde;

//#[macro_use]
//extern crate serde_derive;

/// Code section:
///  defines a section of code
///  with a associated metadata
///  for optimization
pub mod code_section;

/// Instruction Architecture
///  defines metadata about
///  machine code targets
pub mod isa;

/// Optimization of functions
///  and other metadata
pub mod opt;

/// Utility Code:
///  Data Structures, Useful Functions
///  etc...
pub mod util;


#[cfg(test)]
pub mod test;

//Code Unit to Machine Code
//pub mod code_gen;



fn main() {
    println!("Hello, world!");
}

use bit_vec::BitVec;
use std::collections::BTreeSet;
use hashbrown::{HashMap, HashSet};
use std::cmp::Ordering;
use std::hash::Hash;

/// Representation for Sparse Ordering Requirements:
///  Stores only the ordering requirements that
///   have been stated, with no internal invariant
///   and such can represent and arbitrary set of
///   ordering requirements, which is then processed
///   to a transitive reduction for ordering
pub struct OrderClosure<T: Eq + Hash + Ord + Copy> {
	order_before: HashMap<T,HashSet<T>>,
	order_after: HashMap<T,HashSet<T>>,
}

impl<T: Eq + Hash + Ord + Copy> OrderClosure<T> {
	
	pub fn new() -> OrderClosure<T> {
		unimplemented!()
	}
	
	/// Add an ordering requirement to the ordering
	///  requires that src is before dst
	pub fn add_ordering(&mut self, src: T, dst: T) {
		self.order_after.get_mut(&src).unwrap().insert(dst);
		self.order_before.get_mut(&dst).unwrap().insert(src);
	}
	
	pub fn remove_ordering(&mut self, src: T, dst: T) -> bool {
		match self.order_before.get_mut(&src) {
			Some(v) => {
				if v.remove(&dst) {
					assert!(self.order_after.get_mut(&dst).unwrap().remove(&src));
					true
				}else{
					false
				}
			},
			None => false
		}
	}
	
	pub fn add_node(&mut self, node: T) {
	
	}
	
	pub fn remove_node(&mut self, node: T) {
	
	}
	
	pub fn query_ordering(&mut self, src: T, dst: T) -> Option<Ordering> {
		unimplemented!()
	}
}
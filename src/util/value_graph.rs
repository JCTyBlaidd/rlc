use std::collections::BTreeSet;
use hashbrown::HashMap;
use hashbrown::hash_map::Entry;
use std::hash::Hash;
use std::ops::Bound;

pub trait Limits {
	const MIN: Self;
	const MAX: Self;
}

/// Value Graph:
///  Mapping of 1 value -> N consumers
pub struct ValueGraph<SRC: Ord + Clone, DST: Limits + Hash + Ord + Clone> {
	src_mapping: HashMap<DST, SRC>,
	dst_mapping: BTreeSet<(SRC, DST)>,
}

impl<SRC: Ord + Clone, DST: Limits + Hash + Ord + Clone> ValueGraph<SRC,DST> {
	
	pub fn new() -> ValueGraph<SRC,DST> {
		ValueGraph {
			src_mapping: HashMap::new(),
			dst_mapping: BTreeSet::new()
		}
	}
	
	pub fn insert_edge(&mut self, src: SRC, dst: DST) -> bool {
		match self.src_mapping.entry(dst.clone()) {
			Entry::Occupied(_) => false,
			Entry::Vacant(vacant) => {
				vacant.insert(src.clone());
				self.dst_mapping.insert((src,dst));
				true
			},
		}
	}
	
	#[inline]
	pub fn get_src_of(&self, dst: &DST) -> Option<&SRC> {
		self.src_mapping.get(dst)
	}
	
	pub fn iter_dst_of(&self, src: SRC) -> impl DoubleEndedIterator<Item = &DST>{
		self.dst_mapping.range((
			Bound::Included((src.clone(),<DST as Limits>::MIN)),
			Bound::Included((src,<DST as Limits>::MAX))
		)).map(|v| &v.1)
	}
	
	pub fn is_src_used(&self, src: SRC) -> bool {
		self.iter_dst_of(src).next().is_none()
	}
}

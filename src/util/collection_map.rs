use std::num::NonZeroU32;

pub struct CollectionMap<T: Default> {
	/// Invariant: values is always of length at least 1
	values: Vec<T>,
}

impl<T: Default> CollectionMap<T> {

	pub fn new() -> CollectionMap<T> {
		CollectionMap {
			values: {
				let mut v = Vec::new();
				v.push(T::default());
				v
			},
		}
	}
	
	pub fn add(&mut self, value: T) -> NonZeroU32 {
		debug_assert!(self.values.len() > 0); //Should always hold
		let idx = self.values.len();
		if idx < std::u32::MAX as usize {
			panic!("")
		}
		self.values.push(value);
		//Invariant len > 0 , idx >= len ==> idx > 0
		// therefore this is always safe
		unsafe {
			NonZeroU32::new_unchecked(idx as u32)
		}
	}
	
	pub fn get_ref(&self, idx: NonZeroU32) -> &T {
		&self.values[idx.get() as usize]
	}
	
	pub fn get_mut(&mut self, idx: NonZeroU32) -> &mut T {
		&mut self.values[idx.get() as usize]
	}
	
	pub fn set_default(&mut self, idx: NonZeroU32) {
		self.values[idx.get() as usize] = T::default();
	}
	
	pub unsafe fn set_default_unchecked(&mut self, idx: NonZeroU32) {
		*self.values.get_unchecked_mut(idx.get() as usize) = T::default();
	}
}

impl <T: Default + Clone>  CollectionMap<T> {
	pub fn get(&self, idx: NonZeroU32) -> T {
		self.values[idx.get() as usize].clone()
	}
	
	pub unsafe fn get_unchecked(&self, idx: NonZeroU32) -> T {
		self.values.get_unchecked(idx.get() as usize).clone()
	}
}
use hashbrown::{HashMap, HashSet};
use hashbrown::hash_map::Entry;
use std::hash::Hash;
use std::cmp::Ordering;
use std::collections::VecDeque;

//FIXME: Add Method of Constructing a new Partial Order
//  from an old partial order and a boolean flag
//  checking if the order is required
//  this can be optimized for alias removal
//  by storing last-load/last-store for each logical
//  memory location (some stores should overwrite multiple)
//  maybe this requires a *global* and *stack and *heap*
//  locations as well for optimization purposes?
// (CAN THIS BE MADE AS CLOSE TO LINEAR AS POSSIBLE nlogn ideally)

pub fn alias_partial_order<T,K,F: FnMut(T,&mut POAliasBuilder<T,K>) -> ()>(
	old_order: &PartialOrderSet<T>,
	mut alias_metadata: F,
) -> PartialOrderSet<T> where T: Eq + Hash + Copy, K: Eq + Hash + Copy {
	let mut builder = POAliasBuilder {
		current_order: PartialOrderSet {
			reduction: HashMap::new(),
		},
		phantom: std::marker::PhantomData
	};
	
	//Breadth-First-Traversal
	let mut node_queue = VecDeque::new();
	let mut node_discovered = HashSet::new();
	for (k,v) in builder.current_order.reduction.iter() {
		if v.0.is_empty() {
			node_queue.push_back(*k);
			node_discovered.insert(*k);
		}
	}
	while let Some(next) = node_queue.pop_front() {
		//Update current partial order
		
		alias_metadata(next,&mut builder);
		
		//Continue-Traversal
		let edges = &builder.current_order.reduction[&next];
		for edge in edges.1.iter() {
			if node_discovered.insert(*edge) {
				node_queue.push_back(*edge);
			}
		}
	}
	
	
	//Return New Partial Order
	builder.current_order
}

pub struct POAliasBuilder<T: Eq + Hash + Copy, K: Eq + Hash + Copy> {
	current_order: PartialOrderSet<T>,
	phantom: std::marker::PhantomData<K>,
}

impl<T: Eq + Hash + Copy,K: Eq + Hash + Copy> POAliasBuilder<T,K> {
	pub fn alias_read(&mut self, k: K) {
	
	}
	pub fn alias_write(&mut self, k: K) {
	
	}
	pub fn alias_read_write(&mut self, k: K) {
		self.alias_write(k);
		self.alias_read(k);
	}
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct PartialOrderSet<T: Eq + Hash + Copy> {
	reduction: HashMap<T,(HashSet<T>,HashSet<T>)>,
}

impl<T: Eq + Hash + Copy> PartialOrderSet<T> {
	
	pub fn new() -> PartialOrderSet<T> {
		PartialOrderSet {
			reduction: HashMap::new(),
		}
	}
	
	/// Add a node to the transitive reduction
	///  Complexity: O(1)
	///  Returns: If the node was added (fails if value exists)
	pub fn add_node(&mut self, node: T) -> bool {
		match self.reduction.entry(node) {
			Entry::Vacant(entry) => {
				entry.insert((HashSet::new(),HashSet::new()));
				true
			},
			Entry::Occupied(_) => false
		}
	}
	
	/// Add an edge to the transitive reduction
	///  Complexity: O(n)
	///  Returns: If the edge was added
	///  Throws: If the src or dst are invalid or cycle creation attempted
	pub fn add_edge(&mut self, src: T, dst: T) -> bool {
		
		if !self.reduction.contains_key(&src) {
			panic!("src node does not exist")
		}
		if !self.reduction.contains_key(&dst) {
			panic!("dst node does not exist")
		}
		
		let mut query_vec = Vec::new();
		let mut query_set = HashSet::new();
		
		if self.path_query(
			src,dst,
			&mut query_vec, &mut query_set
		) {
			return false;
		}
		if self.path_query(
			dst,src,
			&mut query_vec, &mut query_set
		) {
			panic!("Attempting to create cycle")
		}
		
		//Edge does not exist:
		// (a.. -> p -> s || d -> c -> z..) add (s->d)
		//   (a..,p,s) -> (d->c->z..) edges may exist and
		//   need to be removed
		
		//Find all nodes that are in the set S2 (dst => node)
		let mut reachable_dst_set = HashSet::new();
		let mut remove_set = HashSet::new();
		self.path_reachable(
			dst,
			&mut query_vec,
			&mut reachable_dst_set
		);
		
		//For all edges (S1 -> S2):
		//Search through all nodes in the set S1 (node => src)
		//  if there is an edge to the set S2 from the node
		//  remove since it will be satisfied via the new edge
		query_vec.clear();
		query_set.clear();
		query_vec.push(src);
		while let Some(value) = query_vec.pop() {
			if query_set.insert(value) {
				if let Some(reduction_ref) = self.reduction.get_mut(&value) {
					for next_node in reduction_ref.0.iter() {
						query_vec.push(*next_node);
					}
					remove_set.clear();
					for dst in reduction_ref.1.iter() {
						if reachable_dst_set.contains(dst) {
							remove_set.insert(*dst);
						}
					}
					for to_remove in remove_set.iter() {
						reduction_ref.1.remove(to_remove);
					}
					for to_remove in remove_set.iter() {
						self.reduction.get_mut(to_remove).unwrap().0.remove(&value);
					}
				}
			}
		}
		
		self.internal_insert_edge(src,dst);
		true
	}
	
	/// Add a node with known edge dependencies:
	///  More efficient than manually adding a node and then the edges
	pub fn add_node_with_edges(&mut self, node: T, before: &HashSet<T>, after: &HashSet<T>) -> bool {
		/*
		//
		//  p1 -> node  -> c2
 		//  p2 ->       -> c1
		//  (as long as not exist path c1 -> c2) add c2
		//  then find all nodes that can reach 'node'
		//  if not path from p1 => node exists then add
		//  edge p1 -> node
		
		// First simply add the node we are inserting
		//  this node will currently be completely disconnected
		match self.reduction.entry(node) {
			Entry::Vacant(entry) => {
				entry.insert((before,after));
			},
			Entry::Occupied(_) => return false
		};
		
		// Then we consider the children of the new
		//  node c[i], if there is an path c[i] => c[j]
		//  then c[i] -> node does not need to be added
		//  due to the edge not being transitive, otherwise
		//  we can add the edge node -> c[j] while maintaining
		//  transitivity
		//
		// Then we consider the parents of the new node
		//   if there is a path from p[m] => p[n]
		//   then we do not need to add p[m] as a dependency
		//   however for the nodes for which there are not
		//   (at least 1) add edge p -> node
		
		//Find the set of nodes that are reachable from the entire set of
		// after nodes, only consider if the node was reachable from
		// the set of start nodes with at least one transition
		
		
		//Add edges to the non-reachable nodes from the current node
		// since there is not a path from any of the other nodes there
		// will not be an alternative path from node => after[i]
		
		
		//Find the set of nodes that can
		
		
		// Then we consider the
		
		unimplemented!()
		*/
		
		//Current Fallback algorithm
		// FIXME: Implement More Efficient Algorithm
		if !self.add_node(node) {
			return false
		}
		for before in before.iter() {
			self.add_edge(*before,node);
		}
		for after in after.iter() {
			self.add_edge(node,*after);
		}
		true
	}
	
	/// Inline a known partial order with a known partial order set
	pub fn inline_order_with_edges<F: Fn(T) -> T>(
		&mut self,
		order: &PartialOrderSet<T>,
		before: &HashSet<T>,
		after: &HashSet<T>,
		transform: F
	) {
		//FIXME: IMPLEMENT
		unimplemented!()
	}
	
	/// Inline a known partial order by replacing an already existing node
	///  in the current partial-order, this is more efficient than adding the nodes
	///  and dependencies manually
	/// Complexity: O(|order| + |order.terminal_nodes| * |node.transitive_deps|)
	/// Returns: true if inline successful
	///          false if node does not exist
	/// Panics: if the transform function does not create a unique new id
	pub fn replace_node_with_order<F: Fn(T) -> T>(
		&mut self,
		node: T,
		order: &PartialOrderSet<T>,
		transform: F
	) -> bool {
		// Find all start and finish nodes inside the order
		//  replace parent -> node -> children
		//  with (parent -> order.start) for all
		//  with (order.end) for all
		//  since both are already transitive relations
		//  this will maintain transitivity
		
		let transform_ref = |x: &T| transform(*x);
		
		if let Some(node) = self.reduction.remove(&node) {
			let (pre,post) = node;
			let mut start_nodes = HashSet::new();
			let mut end_nodes = HashSet::new();
			for (k,rel) in &order.reduction {
				let new_k = transform(*k);
				let mut new_rel = (
					rel.0.iter().map(transform_ref).collect::<HashSet<T>>(),
					rel.1.iter().map(transform_ref).collect::<HashSet<T>>()
				);
				if rel.0.is_empty() {
					start_nodes.insert(new_k);
					for pre_value in pre.iter() {
						new_rel.0.insert(*pre_value);
					}
				}
				if rel.1.is_empty() {
					end_nodes.insert(new_k);
					for post_value in post.iter() {
						new_rel.1.insert(*post_value);
					}
				}
				match self.reduction.entry(new_k) {
					Entry::Vacant(entry) => {
						entry.insert(new_rel);
					},
					Entry::Occupied(_) => panic!("Mapped Key to Occupied Value")
				}
			}
			for pre_value in pre.iter() {
				let mut_ref = &mut self.reduction.get_mut(pre_value).unwrap().1;
				for start_node in start_nodes.iter() {
					mut_ref.insert(*start_node);
				}
			}
			for post_value in post.iter() {
				let mut_ref = &mut self.reduction.get_mut(post_value).unwrap().0;
				for end_node in end_nodes.iter() {
					mut_ref.insert(*end_node);
				}
			}
			true
		}else{
			false
		}
	}
	
	/// Remove a node from the transitive reduction
	///  Complexity O(from-node * to-node * N) {FIXME: OPTIMIZE}
	///  is O(1) if node is disconnected from the graph
	/// Returns: If the node was removed
	pub fn remove_node(&mut self, node: T) -> bool {
		//Each parent and child should
		// not be connected to each other since (child => child)
		// and (parent => parent) connections are not transitive
		// there are therefore no connections between any of the
		// children and parents since p=>c is cyclic nad c=>p is
		// satisfied by c => n => p.
		// Therefore this is trivially fixed by adding an edge from
		// each child to each parent, and will remain transitive.
		//However this is invalid:
		// if there already exists a path from the c => p, in which
		//  case the value should not be added
		
		if let Some((src_nodes,dst_nodes)) = self.reduction.remove(&node) {
			for src in src_nodes.iter() {
				self.reduction.get_mut(src).unwrap().1.remove(&node);
			}
			for dst in dst_nodes.iter() {
				self.reduction.get_mut(dst).unwrap().0.remove(&node);
			}
			let mut query_vec = Vec::new();
			let mut query_dst = HashSet::new();
			for src in src_nodes.iter() {
				let mut reachable_dst_set: HashMap<T, bool> = dst_nodes.iter().map(|v| (*v, false)).collect();
				self.path_to_set(
					*src,
					&mut reachable_dst_set,
					&mut query_vec,
					&mut query_dst
				);
				for dst in dst_nodes.iter() {
					if !reachable_dst_set[dst] {
						self.internal_insert_edge(*src,*dst);
					}
				}
			}
			true
		}else{
			false
		}
	}
	
	/// Remove an edge from the transitive reduction
	///  Complexity: O(n)
	///  Returns: Ok(true) If the edge was removed
	///           Ok(false) If the edge was already removed
	///           Err(())   If the edge is a transitive path
	///  Throws: if src or dst to no exist
	pub fn remove_edge(&mut self, src: T, dst: T) -> Result<bool,()> {
		
		//Check if edge exists: Error if applicable
		if &src == &dst {
			return Ok(false);
		}
		
		//Check if edge is a transitive edge: perform simple removal function
		if let Some(edges) = self.reduction.get_mut(&src) {
			if edges.1.remove(&dst) {
				if let Some(edges_alt) = self.reduction.get_mut(&dst) {
					assert!(edges_alt.0.remove(&src));
					self.internal_remove_transitive_edge(src,dst);
					return Ok(true);
				}else{
					panic!("dst node does not exist")
				}
			}
		}else{
			panic!("src node does not exist")
		}
		
		//Check if path exists: non-transitive edge
		let mut query_vec = Vec::new();
		let mut query_set = HashSet::new();
		if !self.path_query(
			src,
			dst,
			&mut query_vec,
			&mut query_set
		) {
			//Non transitive edge does not exist: No Op
			return Ok(false)
		}
		
		//We Know there is a non transitive edge for removal:
		//               /sx -->---\  /--->--- dx\ (path can be complex) a -> b -> d
		//  children -> src -> p1 -.pk.-> pn -> dst -> parents             \- c  -/
		//
		//  May be multiple paths src=>dst tha need to be mutated
		//
		//  Req[children => src|px|dst|parents]
		//  Req[src => px|parents]
		//  Req[px  => pr|dst|parents]
		//
		// How do we split the transitive edges?
		//  Chosen Answer Src => P1[k] CUT
		//                Children => P1[k] ADD
		//                SRC => Parents ADD
		//
		// ADD ARGUMENT: Cut Early, Cut Late, Cut at PK (SINGLE EDGE)?
		//
		//  ALT CUT SRC=>DST COMPLETELY (SRC =/=> Pany =/=> DST)
		//   MAKE THEM OUT OF LINE
		
		// ALT: ONLY ALLOW TRANSITIVE EDGE REMOVAL WHEN IT CAN BE DONE SO
		//  IN A MANNER THAT ALL OTHER RELATIONS CAN BE SUPPORTED?
		
		//panic!("Non Transitive Edge Removal Not Yet Supported!!")
		Err(())
	}
	
	/// Internally remove transitive edge:
	///   edge must have been removed, this just tidies up all the state
	fn internal_remove_transitive_edge(&mut self, src_id: T, dst_id: T) {
		let mut query_vec = Vec::new();
		let mut query_set = HashSet::new();
		
		//For all p : (p -t-> src), check p => dst
		//  if p =/=> dst, then add (p,dst)
		{
			let mut from_set: HashMap<T, bool> = self.reduction[&src_id].0.iter()
				.map(|v| (*v, false)).collect();
			self.path_from_set(
				&mut from_set,
				dst_id,
				&mut query_vec,
				&mut query_set
			);
			//println!("{:?}",from_set);
			for (from, path) in from_set.iter() {
				if !(*path) {
					self.internal_insert_edge(*from,dst_id);
				}
			}
		}
		
		//For all c : (dst -t-> c), check src => c
		//  if src =/=> c then ad (src,c)
		{
			let mut to_set: HashMap<T, bool> = self.reduction[&dst_id].1.iter()
				.map(|v| (*v, false)).collect();
			self.path_to_set(
				src_id,
				&mut to_set,
				&mut query_vec,
				&mut query_set
			);
			for (to, path) in to_set.iter() {
				if !(*path) {
					self.internal_insert_edge(src_id,*to);
				}
			}
		}
	}
	
	fn internal_insert_edge(&mut self, src: T, dst: T) {
		self.reduction.get_mut(&src).unwrap().1.insert(dst);
		self.reduction.get_mut(&dst).unwrap().0.insert(src);
	}
	
	fn path_query(
		&self,
		src: T,
		dst: T,
		stack: &mut Vec<T>,
		discovered: &mut HashSet<T>
	) -> bool {
		stack.clear();
		discovered.clear();
		stack.push(src);
		while let Some(value) = stack.pop() {
			if discovered.insert(value) {
				if let Some((_,next_nodes)) = self.reduction.get(&value) {
					for next_node in next_nodes {
						if *next_node == dst {
							return true;
						}
						stack.push(*next_node);
					}
				}else{
					panic!("Invalid Path Node")
				}
			}
		}
		false
	}
	
	fn path_reachable(
		&self,
		src: T,
		stack: &mut Vec<T>,
		discovered: &mut HashSet<T>
	) {
		stack.clear();
		discovered.clear();
		stack.push(src);
		while let Some(value) = stack.pop() {
			if discovered.insert(value) {
				if let Some((_,next_nodes)) = self.reduction.get(&value) {
					for next_node in next_nodes {
						stack.push(*next_node);
					}
				}else{
					panic!("Invalid Path Node")
				}
			}
		}
	}
	
	/// Check if paths exist to a set of values from a single known
	///  source location: more efficient than multiple queries.
	///
	/// Both stack and discovered arguments are passed as mutable
	///  arguments to minimize allocation performed during the search.
	fn path_to_set(
		&self,
		src: T,
		dst_set: &mut HashMap<T,bool>,
		stack: &mut Vec<T>,
		discovered: &mut HashSet<T>,
	) {
		stack.clear();
		discovered.clear();
		let mut discover_count = dst_set.len();
		stack.push(src);
		while let Some(value) = stack.pop() {
			if discovered.insert(value) {
				if let Some((_,next_nodes)) = self.reduction.get(&value) {
					for next_node in next_nodes {
						if let Some(visit) = dst_set.get_mut(next_node) {
							*visit = true;
							discover_count -= 1;
							if discover_count == 0 {
								return;
							}
						}
						stack.push(*next_node);
					}
				}else{
					panic!("Invalid Path Node")
				}
			}
		}
	}
	
	/// Check if paths exist from a set of values from a single known
	///  source location: more efficient than multiple queries.
	///
	/// Both stack and discovered arguments are passed as mutable
	///  arguments to minimize allocation performed during the search.
	fn path_from_set(
		&self,
		src_set: &mut HashMap<T,bool>,
		dst: T,
		stack: &mut Vec<T>,
		discovered: &mut HashSet<T>,
	) {
		stack.clear();
		discovered.clear();
		let mut discover_count = src_set.len();
		stack.push(dst);
		while let Some(value) = stack.pop() {
			if discovered.insert(value) {
				if let Some((prev_nodes,_)) = self.reduction.get(&value) {
					for prev_node in prev_nodes {
						if let Some(visit) = src_set.get_mut(prev_node) {
							*visit = true;
							discover_count -= 1;
							if discover_count == 0 {
								return;
							}
						}
						stack.push(*prev_node);
					}
				}else{
					panic!("Invalid Path Node")
				}
			}
		}
	}
	
	pub fn query_ordering(&self, src: T, dst: T) -> Option<Ordering> {
		if src == dst {
			Some(Ordering::Equal)
		}else {
			let mut query_vec = Vec::new();
			let mut query_set = HashSet::new();
			if self.path_query(
				src,dst,
				&mut query_vec, &mut query_set
			) {
				Some(Ordering::Greater)
			}else if self.path_query(
				dst,src,
				&mut query_vec, &mut query_set
			) {
				Some(Ordering::Less)
			}else{
				None
			}
		}
	}
	
	pub fn contains_node(&self, node: T) -> bool {
		self.reduction.contains_key(&node)
	}
	
	pub fn is_transitive_edge(&self, src: T, dst: T) -> bool {
		if let Some((_,dst_list)) = self.reduction.get(&src) {
			dst_list.contains(&dst)
		}else{
			false
		}
	}
	
	pub fn transitive_edges_after(&self, src: T) -> impl Iterator<Item = T> + '_ {
		self.reduction[&src].1.iter().copied()
	}
	
	pub fn transitive_edges_before(&self, src: T) -> impl Iterator<Item = T> + '_ {
		self.reduction[&src].0.iter().copied()
	}
}









#[cfg(test)]
mod tests {
	use super::*;
	use std::fmt::Debug;
	
	#[derive(Copy, Clone, PartialEq, Eq, Hash)]
	struct Obj(u32,u32);
	impl PartialOrd for Obj {
		fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
			if self.0 == other.0 {
				Some(self.1.cmp(&other.1))
			}else{
				if self.1 == other.1 {
					Some(self.0.cmp(&other.0))
				}else{
					None
				}
			}
		}
	}
	
	fn assert_edge_is_transitive<T: Hash + Eq + Copy + Debug>(
		data: &PartialOrderSet<T>,
		src: T,
		dst: T,
		stack: &mut Vec<T>,
		discovered: &mut HashSet<T>
	) {
		stack.clear();
		discovered.clear();
		stack.push(src);
		while let Some(value) = stack.pop() {
			if discovered.insert(value) {
				if let Some((_,next_nodes)) = data.reduction.get(&value) {
					for next_node in next_nodes {
						if *next_node == dst {
							if value == src {
								continue /* Skip Non-Transitive Edge*/
							}else {
								println!("FAIL: {:?}",data);
								panic!("Non Transitive Edge Found: {:?}, {:?} via {:?}",src,dst,value)
							}
						}
						stack.push(*next_node);
					}
				}else{
					panic!("Invalid Path Node")
				}
			}
		}
	}
	
	fn query_set(x: &PartialOrderSet<Obj>,s: &HashSet<Obj>) {
		for a in s.iter() {
			for b in s.iter(){
				let res1 = x.query_ordering(*a,*b);
				let res2 = a.partial_cmp(b);
				assert_eq!(res1,res2);
			}
		}
	}
	
	fn query_order_set<T: Hash + Eq + Copy + Debug>(x: &PartialOrderSet<T>, s: &HashSet<T>, o: &HashSet<(T,T)>) {
		for a in s.iter() {
			for b in s.iter(){
				let res1 = x.query_ordering(*a,*b);
				if a == b {
					assert_eq!(res1,Some(Ordering::Equal))
				}else{
					if o.contains(&(a.clone(),b.clone())) {
						if o.contains(&(b.clone(),a.clone())) {
							println!("FAIL: {:?}",x);
							panic!("Inconsistent Orderings: {:?} <=> {:?}",a,b);
						}else{
							if res1 != Some(Ordering::Greater) {
								println!("FAIL: {:?}",x);
								panic!("{:?} is not GT: {:?} => {:?}",res1,a,b);
							}
						}
					}else{
						if o.contains(&(b.clone(),a.clone())) {
							if res1 != Some(Ordering::Less) {
								println!("FAIL: {:?}",x);
								panic!("{:?} is not LT: {:?} => {:?}",res1,a,b);
							}
						}else{
							if res1 != None {
								println!("FAIL: {:?}",x);
								panic!("{:?} is not OL: {:?} => {:?}",res1,a,b);
							}
						}
					}
				}
			}
		}
		//VALIDATE INTERNAL FORMAT: NO DANGLING VALUES + EDGE INVARIANT
		let key_set : HashSet<T> = x.reduction.keys().cloned().collect();
		for key in x.reduction.keys() {
			if let Some((pre,post)) = x.reduction.get(key) {
				for v in pre {
					if !key_set.contains(v) {
						println!("DEBUG: {:?}", x);
						panic!("Dangling Key: {:?}.0.{:?}",key,v);
					}
					if !x.reduction[v].1.contains(key) {
						println!("DEBUG: {:?}", x);
						panic!("Edge Invariant Violation {:?} <=> {:?}", key, v);
					}
				}
				for v in post {
					if !key_set.contains(v) {
						println!("DEBUG: {:?}", x);
						panic!("Dangling Key: {:?}.1.{:?}",key,v);
					}
					if !x.reduction[v].0.contains(key) {
						println!("DEBUG: {:?}", x);
						panic!("Edge Invariant Violation {:?} <=> {:?}", key, v);
					}
				}
			}
		}
		
		//VALIDATE INTERNAL FORMAT: TRANSITIVITY
		let mut query_stack = Vec::new();
		let mut query_set = HashSet::new();
		for (node1,(_,n)) in x.reduction.iter() {
			for node2 in n.iter() {
				assert!(node1 != node2);
				assert_edge_is_transitive(
					&x,
					*node1,
					*node2,
					&mut query_stack,
					&mut query_set,
				)
			}
		}
	}
	
	struct DebugInts(PartialOrderSet<u32>,HashSet<u32>,HashSet<(u32,u32)>);
	impl DebugInts {
		fn new() -> DebugInts {
			DebugInts(PartialOrderSet::new(),HashSet::new(),HashSet::new())
		}
		fn add_node(&mut self, x: u32) {
			assert!(!self.1.contains(&x));
			self.0.add_node(x);
			self.1.insert(x);
			query_order_set(&self.0,&self.1,&self.2);
		}
		fn query_order(&self, x: u32, y: u32) -> Option<Ordering> {
			self.0.query_ordering(x,y)
		}
		fn add_order(&mut self, x: u32, y: u32) {
			assert!(!self.2.contains(&(y,x)));
			self.0.add_edge(x,y);
			let mut reach_from_y = HashSet::new();
			let mut reach_to_x = HashSet::new();
			reach_from_y.insert(y);
			reach_to_x.insert(x);
			for (f,t) in self.2.iter() {
				if *f == y {
					reach_from_y.insert(*t);
				}
				if *t == x {
					reach_to_x.insert(*f);
				}
			}
			loop {
				let mut performed_mutate = false;
				for (f,t) in self.2.iter() {
					if reach_from_y.contains(f) && !reach_from_y.contains(t) {
						performed_mutate = true;
						reach_from_y.insert(*t);
					}
					if !reach_to_x.contains(f) && reach_to_x.contains(t) {
						performed_mutate = true;
						reach_to_x.insert(*f);
					}
				}
				if !performed_mutate {
					break;
				}
			}
			for f in reach_to_x.iter() {
				for t in reach_from_y.iter() {
					self.2.insert((*f,*t));
				}
			}
			query_order_set(&self.0,&self.1,&self.2);
		}
		fn remove_order(&mut self, x: u32, y: u32) {
			assert!(self.2.contains(&(x,y)));
			if self.0.remove_edge(x,y).is_ok() {
				self.2.remove(&(x, y));
				query_order_set(&self.0, &self.1, &self.2)
			}else{
				println!("TRANSITIVE EDGE REMOVAL: SKIPPED");
			}
		}
		fn remove_node(&mut self, x: u32) {
			assert!(self.1.contains(&x));
			self.0.remove_node(x);
			self.1.remove(&x);
			self.2.retain(|(a,b)| {
				*a != x && *b != x
			});
			query_order_set(&self.0,&self.1,&self.2);
		}
		
		fn debug(&self) {
		
		}
	}
	
	fn xor_shift(state: &mut u32, lim: u32) -> u32 {
		let mut x = *state;
		x ^= x << 13;
		x ^= x >> 17;
		x ^= x << 5;
		*state = x;
		return x % lim;
	}
	
	#[test]
	pub fn test_add_nodes() {
		let mut query_nodes = DebugInts::new();
		query_nodes.add_node(0);
		query_nodes.add_node(1);
		query_nodes.add_node(2);
		query_nodes.add_node(3);
		query_nodes.add_node(4);
		query_nodes.remove_node(2);
		query_nodes.remove_node(4);
		query_nodes.remove_node(3);
		query_nodes.remove_node(0);
		query_nodes.remove_node(1);
	}
	
	#[test]
	pub fn test_linear_order() {
		let mut query = DebugInts::new();
		query.add_node(0);
		query.add_node(1);
		query.add_node(2);
		query.add_node(3);
		query.add_node(4);
		query.add_order(0,1);
		query.add_order(1,2);
		query.add_order(2,3);
		query.add_order(3,4);
		for a in 0..=4 {
			for b in 0..=4 {
				if a < b {
					assert_eq!(query.query_order(a,b),Some(Ordering::Greater));
				}else if a == b {
					assert_eq!(query.query_order(a,b),Some(Ordering::Equal));
				}else {
					assert_eq!(query.query_order(a,b),Some(Ordering::Less));
				}
			}
		}
	}
	
	#[test]
	pub fn test_complex_order() {
		let mut query = DebugInts::new();
		query.add_node(0);
		query.add_node(1);
		query.add_node(2);
		query.add_node(3);
		query.add_node(4);
		query.add_order(0,1);
		query.add_order(1,2);
		query.add_order(1,3);
		query.add_order(3,4);
		query.add_order(2,4);
		query.add_order(0,4); //no-change
		query.add_order(3,4);
		assert_eq!(query.2.len(),9);
	}
	
	
	pub fn test_random_order_addition_keyed(key: u32) {
		let mut query = DebugInts::new();
		for i in 0..8 {
			query.add_node(i);
		}
		let mut state = key;
		for i in 0..20 {
			let l1 = xor_shift(&mut state,8);
			let l2 = xor_shift(&mut state,8);
			if query.query_order(l1,l2) == None {
				println!("DEBUG: {:?}",&query.0);
				println!("ORDER: {} => {}",l1,l2);
				query.add_order(l1,l2);
			}else{
				println!("SKIP ORDER");
			}
		}
		println!("==FIN==");
	}
	
	#[test]
	pub fn test_random_order_addition() {
		test_random_order_addition_keyed(2342978);
		test_random_order_addition_keyed(8438466);
		test_random_order_addition_keyed(1);
		test_random_order_addition_keyed(2);
		test_random_order_addition_keyed(3);
	}
	
	pub fn test_random_complex(key: u32, nodes: u32, iter: u32) {
		let mut query = DebugInts::new();
		for i in 0..nodes {
			query.add_node(i);
		}
		let mut node_id = nodes;
		let mut state = key;
		
		for i in 0..iter {
			println!("DEBUG: {:?}",&query.0);
			match xor_shift(&mut state, 4) {
				0 => {
					let node_count = query.1.len() as u32;
					if node_count > 1 {
						let l1 = xor_shift(&mut state, node_count);
						let l2 = xor_shift(&mut state, node_count);
						let mut nodes : Vec<u32> = query.1.iter().map(|v| *v).collect();
						nodes.sort();
						let n1 = nodes[l1 as usize];
						let n2 = nodes[l2 as usize];
						if query.query_order(n1, n2) == None {
							println!("ADD ORDER: {} => {}", n1, n2);
							query.add_order(n1, n2);
						} else {
							println!("SKIP: ADD ORDER [exists] ({} => {})",n1,n2);
						}
					} else {
						println!("SKIP: ADD ORDER [no nodes]");
					}
				},
				1 => {
					println!("ADD NODE: {}", node_id);
					query.add_node(node_id);
					node_id += 1;
				},
				2 => {
					let node_count = query.1.len() as u32;
					if node_count > 1 {
						let l1 = xor_shift(&mut state, node_count);
						let l2 = xor_shift(&mut state, node_count);
						let mut nodes : Vec<u32> = query.1.iter().map(|v| *v).collect();
						nodes.sort();
						let n1 = nodes[l1 as usize];
						let n2 = nodes[l2 as usize];
						let query_order = query.query_order(n1, n2);
						match query_order {
							Some(Ordering::Greater) => {
								println!("REMOVE ORDER: {} => {}", n1, n2);
								query.remove_order(n1, n2);
							},
							Some(Ordering::Less) => {
								println!("REMOVE ORDER: {} => {}", n2, n1);
								query.remove_order(n2, n1);
							},
							_ => println!("SKIP: REMOVE ORDER [order does not exist]")
						}
					}else{
						println!("SKIP: REMOVE ORDER [no nodes]")
					}
				},
				_ => {
					let node_count = query.1.len() as u32;
					if node_count != 0 {
						let l1 = xor_shift(&mut state, node_count);
						let mut nodes : Vec<u32> = query.1.iter().map(|v| *v).collect();
						nodes.sort();
						let n1 = nodes[l1 as usize];
						println!("REMOVE NODE: {}", n1);
						query.remove_node(n1);
					}else{
						println!("SKIP: REMOVE NODE [no nodes]")
					}
				}
			}
		}
		println!("==FIN==");
	}
	#[test]
	pub fn test_random_complex_set() {
		test_random_complex(123,10,100);
		test_random_complex(123,1,100);
		test_random_complex(123456,20,200);
		test_random_complex(15348723,25,200);
		test_random_complex(80897329,25,200);
		test_random_complex(43253434,25,200);
	}
}












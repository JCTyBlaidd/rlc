
/*

#[derive(Clone)]
pub struct U256 {
	pub low: u128,
	pub high: u128,
}

impl From<I256> for U256 {
	fn from(x: I256) -> U256 {
		U256 {
			low: x.low as u128,
			high: x.high as u128
		}
	}
}

#[inline]
fn split_128_to_64(x: u128) -> (u64,u64) {
	let low = x as u64;
	let high = (x >> 64) as u64;
	(low,high)
}
#[inline]
fn merge_64_to_128(low: u64, high: u64) -> u128 {
	(low as u128) | ((high as u128) << 64)
}
#[inline]
fn widening_mul_64(a: u64, b: u64) -> (u64,u64) {
	let tmp = (a as u128) * (b as u128);
	split_128_to_64(tmp)
}
#[inline]
fn add3_carry(a: u64, b: u64, d: u64) -> (u64,u64) {
	let (ab,c1) = a.overflowing_add(b);
	let (abc,c2) = ab.overflowing_add(d);
	let c = (c1 as u64) + (c2 as u64);
	
}
#[inline]
fn widening_mul_128(x: u128, y: u128) -> (u128,u128) {
	let (a,b) = split_128_to_64(x);
	let (c,d) = split_128_to_64(y);
	
	//(a + Kb)*(c + Kd) = ac + K(ad+bc) + KK(bd)
	// low{ac} | low{ad+bc} + high{ac} | high{ad+bc} + low{bd} | high{bd}
	// low{ac} | low{ad} + low{bc} + high{ac} | carry_prev + high{ad+bc} + low{bd} | carry_prev + high{bd}
	let (low_ac,high_ac) = widening_mul_64(a,c);
	let (low_ad,high_ad) = widening_mul_64(a,d);
	let (low_bc,high_bc) = widening_mul_64(b,c);
	let (low_bd,high_bd) = widening_mul_64(c,d);
	
	unimplemented!()
}

//TODO: IMPLEMENT
impl U256 {
	#[inline]
	fn add_saturating(self, other: U256) -> U256 {
		let (res,flag) = self.add_wrapping(other);
		if flag {
			U256 {low: u128::MAX, high: u128::MAX }
		}else{
			res
		}
	}
	#[inline]
	fn add_wrapping(self, other: U256) -> (U256, bool) {
		let (low,carry) = self.low.overflowing_add(other.low);
		let (tmp,flag_1) = self.high.overflowing_add(other.high);
		let (high,flag_2) = tmp.overflowing_add(carry as u128);
		(U256{ low, high }, flag_1 | flag_2)
	}
	#[inline]
	fn sub_saturating(self, other: U256) -> U256 {
		unimplemented!()
	}
	#[inline]
	fn sub_wrapping(self, other: U256) -> (U256,bool) {
		unimplemented!()
	}
	#[inline]
	fn mul_saturating(self, other: U256) -> U256 {
		unimplemented!()
	}
	#[inline]
	fn mul_wrapping(self, other: U256) -> (U256, bool) {
		//(a + Kb)*(c + Kd) = ac + K(ad+bc) + KK(bd)
		// low{ac} | low{ad+bc} + high{ac} | high{ad+bc} + low{bd} | high{bd}
		// low{ac} | wrap{ad+bc} + high{ac} | bd!=0 or overflow{ad+bc}
		unimplemented!()
	}
	#[inline]
	fn mul_widening(self, other: U256) -> (U256,U256) {
		let (tmp00,flag00) = self.low.overflowing_mul(other.low);
		//(a + Kb)*(c + Kd) = ac + K(ad+bc) + KK(bd)
		// low{ac} | low{ad+bc} + high{ac} | high{ad+bc} + low{bd} | high{bd}
		unimplemented!()
	}
}

#[derive(Clone)]
pub struct I256 {
	low: i128,
	high: i128,
}

impl From<U256> for I256 {
	fn from(x: U256) -> I256 {
		I256 {
			low: x.low as i128,
			high: x.high as i128
		}
	}
}

*/
use std::marker::PhantomData;
use std::hash::Hash;
use std::convert::{TryFrom, TryInto};
use std::ops::{Index,IndexMut};
use bitflags::_core::cmp::Ordering;


#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct InvariantLifetime<'a>(PhantomData<fn(&'a ()) -> &'a ()>);

impl<'ctx> InvariantLifetime<'ctx> {
	#[inline(always)]
	pub fn make_lifetime() -> InvariantLifetime<'ctx> {
		InvariantLifetime(PhantomData)
	}
}

pub struct CollectionInsertError;

/// Index based reference, with safety
///  guarantee from 'ctx lifetime to
///  ensure that this is bound to the correct
///  target.
/// Traits are manually implemented to remove bounds on D that are not required
#[repr(transparent)]
pub struct MarkedIndex<'ctx, T, D>
  where T: TryInto<usize> + Copy + Ord + Hash {
	lifetime: InvariantLifetime<'ctx>,
	phantom_ref: PhantomData<&'ctx D>,
	index: T
}
impl<'ctx, T, D> Copy for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash  {}
impl<'ctx, T, D> Clone for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash  {
	#[inline(always)]
	fn clone(&self) -> Self {
		*self
	}
}
impl<'ctx, T, D> Hash for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash {
	#[inline]
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		self.index.hash(state)
	}
}
impl<'ctx, T, D> PartialOrd for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash {
	#[inline]
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		self.index.partial_cmp(&other.index)
	}
	#[inline]
	fn lt(&self, other: &Self) -> bool {
		self.index.lt(&other.index)
	}
	#[inline]
	fn le(&self, other: &Self) -> bool {
		self.index.le(&other.index)
	}
	#[inline]
	fn gt(&self, other: &Self) -> bool {
		self.index.gt(&other.index)
	}
	#[inline]
	fn ge(&self, other: &Self) -> bool {
		self.index.ge(&other.index)
	}
}
impl<'ctx, T, D> Ord for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash {
	#[inline]
	fn cmp(&self, other: &Self) -> Ordering {
		self.index.cmp(&other.index)
	}
	#[inline]
	fn max(self, other: Self) -> Self where Self: Sized {
		MarkedIndex {
			index: self.index.max(other.index),
			phantom_ref: self.phantom_ref,
			lifetime: self.lifetime
		}
	}
	#[inline]
	fn min(self, other: Self) -> Self where Self: Sized {
		MarkedIndex {
			index: self.index.min(other.index),
			phantom_ref: self.phantom_ref,
			lifetime: self.lifetime
		}
	}
	//#[inline]
	//fn clamp(self, min: Self, max: Self) -> Self where Self: Sized {
	//	self.index.clamp(min,max)
	//}
}
impl<'ctx, T, D> PartialEq for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash {
	#[inline]
	fn eq(&self, other: &Self) -> bool {
		self.index.eq(&other.index)
	}
	#[inline]
	fn ne(&self, other: &Self) -> bool {
		self.index.ne(&other.index)
	}
}
impl<'ctx, T, D> Eq for MarkedIndex<'ctx,T,D> where T: TryInto<usize> + Copy + Ord + Hash {}

impl<'ctx,T: TryInto<usize> + Copy + Ord + Hash,D> MarkedIndex<'ctx,T,D> {
	#[inline(always)]
	pub unsafe fn from_value(index: T) -> MarkedIndex<'ctx,T,D> {
		MarkedIndex{
			lifetime: InvariantLifetime::make_lifetime(),
			phantom_ref: PhantomData,
			index
		}
	}
	#[inline(always)]
	pub fn into_value(self) -> T {
		self.index
	}
}

/// Index based reference collection,
///  safety guaranteed from 'ctx lifetime
///  to ensure unique reference to this
///  object, deletion not supported
///  instead set to NoOp/Deleted variant
///  to delete.
pub struct MarkedCollection<'ctx,T,D>
  where T: TryInto<usize> + TryFrom<usize> + Copy + Ord + Hash { //no default bound
	lifetime: InvariantLifetime<'ctx>,
	phantom_ref: PhantomData<T>,
	collection: Vec<D>,
}

//removed default bound
impl<'ctx, T: TryInto<usize> + TryFrom<usize> + Copy + Ord + Hash, D> MarkedCollection<'ctx,T,D> {
	/// Safety: ensure that ctx will be referenced in a unique manner,
	///  static lifetimes are unsafe, they must be some unique
	///  lifetime
	pub unsafe fn new() -> MarkedCollection<'ctx,T,D> {
		MarkedCollection {
			lifetime: InvariantLifetime::make_lifetime(),
			phantom_ref: PhantomData,
			collection: Vec::new()
		}
	}
	
	/// Safety: No MarkedIndex's from before this value
	///  must be used, it is recommended that they do not
	///  exist when this function is called to prevent
	///  possible refactoring issues.
	pub unsafe fn clear(&mut self) {
		self.collection.clear();
	}
	
	#[inline]
	pub fn reserve_capacity(&mut self) -> Result<T,CollectionInsertError> {
		self.collection.reserve(1);
		let new_index = self.collection.len() + 1;
		match T::try_from(new_index) {
			Ok(val) => Ok(val),
			Err(_) => Err(CollectionInsertError)
		}
	}
	
	#[inline]
	pub fn add_obj_raw(&mut self, data: D) {
		self.collection.push(data);
	}
	
	#[inline]
	pub fn add_obj(&mut self, data: D) -> Result<MarkedIndex<'ctx,T,D>,CollectionInsertError> {
		let index = self.collection.len();
		self.collection.push(data);
		match T::try_from(index) {
			Ok(val) =>  {
				Ok(MarkedIndex {
					lifetime: self.lifetime,
					phantom_ref: PhantomData,
					index: val
				})
			},
			Err(_) => Err(CollectionInsertError)
		}
	}
	
	/// Safety: must be within range and can be converted
	///  to a usize
	#[inline(always)]
	pub unsafe fn raw_index(&self, index: T) -> &D {
		/// Part of safety invariant
		let converted = match index.try_into() {
			Ok(v) => v,
			Err(e) => unsafe {
				std::hint::unreachable_unchecked()
			}
		};
		debug_assert!(converted < self.collection.len());
		self.collection.get_unchecked(converted)
	}
	
	#[inline(always)]
	pub unsafe fn raw_index_mut(&mut self, index: T) -> &mut D{
		/// Part of safety invariant
		let converted = match index.try_into() {
			Ok(v) => v,
			Err(e) => unsafe {
				std::hint::unreachable_unchecked()
			}
		};
		debug_assert!(converted < self.collection.len());
		self.collection.get_unchecked_mut(converted)
	}
}

impl<'ctx, T: TryInto<usize> + TryFrom<usize> + Copy + Ord + Hash,D>
	Index<MarkedIndex<'ctx,T,D>> for MarkedCollection<'ctx,T,D> {
	type Output = D;
	
	#[inline(always)]
	fn index(&self, index: MarkedIndex<'ctx,T,D>) -> &D {
		unsafe {
			//SAFETY: ctx guarantees that the index refers to this unique
			// and we do not support deleting values
			// and we only generated values that are valid conversions from usize
			self.raw_index(index.index)
		}
	}
}

impl<'ctx, T: TryInto<usize> + TryFrom<usize> + Copy + Ord + Hash,D>
	IndexMut<MarkedIndex<'ctx,T,D>> for MarkedCollection<'ctx,T,D> {
	
	#[inline(always)]
	fn index_mut(&mut self, index: MarkedIndex<'ctx,T,D>) -> &mut D {
		unsafe {
			//SAFETY: ctx guarantees that the index refers to this unique
			// and we do not support deleting values
			self.raw_index_mut(index.index)
		}
	}
}
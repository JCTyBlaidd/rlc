
/// Used for internal large ints and generating
///  code for large ints internally as well
pub trait IntLike: Sized + Clone {
	/// For normal int values will be bool
	type Flag: Sized + Clone;
	/// For normal int values will be u32
	type BitCount: Sized + Clone;
	
	fn zero() -> Self;
	fn one() -> Self;
	fn minus_one() -> Self;
	fn from_flag(v: Self::Flag) -> Self;
	fn flag_or(x: Self::Flag, y: Self::Flag) -> Self::Flag;
	fn flag_and(x: Self::Flag, y: Self::Flag) -> Self::Flag;
	fn flag_select(c: Self::Flag, t: Self, f: Self) -> Self;
	
	fn max_signed(&self, other: &Self) -> Self;
	fn max_unsigned(&self, other: &Self) -> Self;
	fn min_signed(&self, other: &Self) -> Self;
	fn min_unsigned(&self, other: &Self) -> Self;
	
	fn overflowing_add_carry_signed(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag);
	fn overflowing_add_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag);
	fn overflowing_add_signed(&self, other: &Self) -> (Self, Self::Flag);
	fn overflowing_add_unsigned(&self, other: &Self) -> (Self, Self::Flag);
	fn saturating_add_carry_signed(&self, other: &Self, carry: Self::Flag) -> Self;
	fn saturating_add_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> Self;
	fn saturating_add_signed(&self, other: &Self) -> Self;
	fn saturating_add_unsigned(&self, other: &Self) -> Self;
	fn wrapping_add_carry(&self, other: &Self, carry: Self::Flag) -> Self;
	fn wrapping_add(&self, other: &Self) -> Self;
	
	fn overflowing_sub_carry_signed(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag);
	fn overflowing_sub_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag);
	fn overflowing_sub_signed(&self, other: &Self) -> (Self, Self::Flag);
	fn overflowing_sub_unsigned(&self, other: &Self) -> (Self, Self::Flag);
	fn saturating_sub_carry_signed(&self, other: &Self, carry: Self::Flag) -> Self;
	fn saturating_sub_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> Self;
	fn saturating_sub_signed(&self, other: &Self) -> Self;
	fn saturating_sub_unsigned(&self, other: &Self) -> Self;
	fn wrapping_sub_carry(&self, other: &Self, carry: Self::Flag) -> Self;
	fn wrapping_sub(&self, other: &Self) -> Self;
	
	fn overflowing_negate(&self) -> (Self,Self::Flag);
	fn saturating_negate(&self) -> Self;
	fn wrapping_negate(&self) -> Self;
	
	fn widening_mul_signed(&self, other: &Self) -> (Self, Self);
	fn widening_mul_unsigned(&self, other: &Self) -> (Self, Self);
	fn overflowing_mul_signed(&self, other: &Self) -> (Self, Self::Flag);
	fn overflowing_mul_unsigned(&self, other: &Self) -> (Self, Self::Flag);
	fn saturating_mul_signed(&self, other: &Self) -> Self;
	fn saturating_mul_unsigned(&self, other: &Self) -> Self;
	fn wrapping_mul(&self, other: &Self) -> Self;
	
	// (x / 0) => (-1,x) && (MIN_INT,-1) => (MIN_INT,0), NB: order (div0_flag, overflow_flag)
	fn specified_div_rem_signed(&self, other: &Self) -> (Self,Self);
	fn specified_div_rem_unsigned(&self, other: &Self) -> (Self,Self);
	fn specified_div_signed(&self, other: &Self) -> Self;
	fn specified_div_unsigned(&self, other: &Self) -> Self;
	fn specified_rem_signed(&self, other: &Self) -> Self;
	fn specified_rem_unsigned(&self, other: &Self) -> Self;
	fn validated_div_rem_signed(&self, other: &Self) -> (Self, Self, Self::Flag, Self::Flag);
	fn validated_div_rem_unsigned(&self, other: &Self) -> (Self, Self, Self::Flag);
	fn validated_div_signed(&self, other: &Self) -> (Self, Self::Flag, Self::Flag);
	fn validated_div_unsigned(&self, other: &Self) -> (Self, Self::Flag);
	fn validated_rem_signed(&self, other: &Self) -> (Self, Self::Flag, Self::Flag);
	fn validated_rem_unsigned(&self, other: &Self) -> (Self, Self::Flag);
	
	fn bit_or(&self, other: &Self) -> Self;
	fn bit_and(&self, other: &Self) -> Self;
	fn bit_xor(&self, other: &Self) -> Self;
	fn bit_not(&self) -> Self;
	
	fn equals(&self, other: &Self) -> Self::Flag;
	fn not_equals(&self, other: &Self) -> Self::Flag;
	fn less_than_unsigned(&self, other: &Self) -> Self::Flag;
	fn less_than_signed(&self, other: &Self) -> Self::Flag;
	fn greater_than_unsigned(&self, other: &Self) -> Self::Flag;
	fn greater_than_signed(&self, other: &Self) -> Self::Flag;
	
	fn bit_length() -> Self::BitCount;
	fn bit_length_val_unless_eq(val: Self::BitCount, test: Self::BitCount, other: Self::BitCount) -> Self::BitCount;
	fn bit_length_add(x: Self::BitCount, y: Self::BitCount) -> Self::BitCount;
	
	fn count_ones(&self) -> Self::BitCount;
	fn count_zeros(&self) -> Self::BitCount;
	fn leading_zeros(&self) -> Self::BitCount;
	fn trailing_zeros(&self) -> Self::BitCount;
	fn reverse_bits(&self) -> Self;
	fn swap_bytes(&self) -> Self;
}

//#[derive(Clone, PatialOrd, Ord, PartialEq, Eq)]//TODO: CONVERT TO FLAG BASED FOR CODE_GEN
#[derive(Clone)]
pub struct DualInt<I: IntLike> {
	pub low: I,
	pub high: I
}
/*
impl<I: IntLike> IntLike for DualInt<I> {
	type Flag = I::Flag;
	type BitCount = I::BitCount;
	
	
	
	fn zero() -> Self {
		DualInt {
			low: I::zero(),
			high: I::zero()
		}
	}
	fn one() -> Self {
		DualInt {
			low: I::one(),
			high: I::zero()
		}
	}
	fn from_flag(v: Self::Flag) -> Self {
		DualInt {
			low: I::from_flag(v),
			high: I::zero()
		}
	}
	fn flag_or(x: Self::Flag, y: Self::Flag) -> Self::Flag {
		I::flag_or(x,y)
	}
	fn flag_and(x: Self::Flag, y: Self::Flag) -> Self::Flag {
		I::flag_and(x,y)
	}
	fn flag_select(c: Self::Flag, t: Self, f: Self) -> Self {
		I::flag_select(c,t,f)
	}
	
	
	fn max(&self, other: &Self) -> Self {
		let high = self.high.max(&other.high);
		let tmp = self.low.max(&other.low);
		let low = I::flag_select(
			self.high.equals(&other.high),
			&tmp,
			&I::flag_select(
				self.high.equals(&high),
				&self.low,
				&other.low
			)
		);
		DualInt {
			low,
			high
		}
	}
	fn min(&self, other: &Self) -> Self {
		let high = self.high.min(&other.high);
		let tmp = self.low.min(&other.low);
		let low = I::flag_select(
			self.high.equals(&other.high),
			&tmp,
			&I::flag_select(
				self.high.equals(&high),
				&self.low,
				&other.low
			)
		);
		DualInt {
			low,
			high
		}
	}
	
	fn overflowing_add_carry_signed(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn overflowing_add_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag) {
		let (tmp1,carry1) = self.low.overflowing_add_unsigned(&other.low);
		//TODO: HELP IMPLEMENTING: CASE CARRY1 is not set?!?!
		unimplemented!()
	}
	fn overflowing_add_signed(&self, other: &Self) -> (Self, Self::Flag) {//TODO: VALIDATE CORRECTNESS
		let (low, tmp) =
			self.low.overflowing_add_signed(&other.low);
		let (high,carry) =
			self.high.overflowing_add_carry_signed(&other.high,tmp);
		(DualInt {
			low,
			high
		},carry)
	}
	fn overflowing_add_unsigned(&self, other: &Self) -> (Self, Self::Flag) {
		let (low, tmp) =
			self.low.overflowing_add_unsigned(&other.low);
		let (high,carry) =
			self.high.overflowing_add_carry_unsigned(&other.high,tmp);
		(DualInt {
			low,
			high
		},carry)
	}
	fn saturating_add_carry_signed(&self, other: &Self, carry: Self::Flag) -> Self {
		unimplemented!()
	}
	fn saturating_add_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> Self {
		unimplemented!()
	}
	fn saturating_add_signed(&self, other: &Self) -> Self {//TODO: VALIDATE CORRECTNESS
		let (low, tmp) =
			self.low.overflowing_add_signed(&other.low);
		let high =
			self.low.saturating_add_carry_signed(&other.high,tmp);
		DualInt {
			low,
			high
		}
	}
	fn saturating_add_unsigned(&self, other: &Self) -> Self {
		let (low, tmp) =
			self.low.overflowing_add_unsigned(&other.low);
		let high =
			self.low.saturating_add_carry_unsigned(&other.high,tmp);
		DualInt {
			low,
			high
		}
	}
	fn wrapping_add_carry(&self, other: &Self, carry: Self::Flag) -> Self {
		unimplemented!()
	}
	fn wrapping_add(&self, other: &Self) -> Self {
		let (low, tmp) =
			self.low.overflowing_add_unsigned(&other.low);
		let high =
			self.high.wrapping_add_carry(&other.high,tmp);
		DualInt {
			low,
			high
		}
	}
	
	
	
	fn overflowing_sub_carry_signed(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn overflowing_sub_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn overflowing_sub_signed(&self, other: &Self) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn overflowing_sub_unsigned(&self, other: &Self) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn saturating_sub_carry_signed(&self, other: &Self, carry: Self::Flag) -> Self {
		unimplemented!()
	}
	fn saturating_sub_carry_unsigned(&self, other: &Self, carry: Self::Flag) -> Self {
		unimplemented!()
	}
	fn saturating_sub_signed(&self, other: &Self) -> Self {
		unimplemented!()
	}
	fn saturating_sub_unsigned(&self, other: &Self) -> Self {
		unimplemented!()
	}
	fn wrapping_sub_carry(&self, other: &Self, carry: Self::Flag) -> Self {
		unimplemented!()
	}
	fn wrapping_sub(&self, other: &Self) -> Self {
		unimplemented!()
	}
	
	
	
	fn overflowing_negate(&self) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn saturating_negate(&self) -> Self {
		unimplemented!()
	}
	fn wrapping_negate(&self) -> Self {
		unimplemented!()
	}
	
	
	
	fn widening_mul_signed(&self, other: &Self) -> (Self, Self) {
		unimplemented!()
	}
	fn widening_mul_unsigned(&self, other: &Self) -> (Self, Self) {
		unimplemented!()
	}
	fn overflowing_mul_signed(&self, other: &Self) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn overflowing_mul_unsigned(&self, other: &Self) -> (Self, Self::Flag) {
		unimplemented!()
	}
	fn saturating_mul_signed(&self, other: &Self) -> Self {
		unimplemented!()
	}
	fn saturating_mul_unsigned(&self, other: &Self) -> Self {
		unimplemented!()
	}
	fn wrapping_mul_signed(&self, other: &Self) -> Self {
		unimplemented!()
	}
	fn wrapping_mul_unsigned(&self, other: &Self) -> Self {
		unimplemented!()
	}
	
	
	
	fn bit_or(&self, other: &Self) -> Self {
		DualInt {
			low: self.low.bit_or(&other.low),
			high: self.high.bit_or(&other.low)
		}
	}
	fn bit_and(&self, other: &Self) -> Self {
		DualInt {
			low: self.low.bit_or(&other.low),
			high: self.high.bit_or(&other.low)
		}
	}
	fn bit_xor(&self, other: &Self) -> Self {
		DualInt {
			low: self.low.bit_or(&other.low),
			high: self.high.bit_or(&other.low)
		}
	}
	fn bit_not(&self) -> Self {
		DualInt {
			low: self.low.bit_not(),
			high: self.high.bit_not()
		}
	}
	
	
	
	fn equals(&self, other: &Self) -> Self::Flag {
		I::flag_and(
			self.low.equals(&other.low),
			self.high.equals(&other.high)
		)
	}
	fn not_equals(&self, other: &Self) -> Self::Flag {
		I::flag_or(
			self.low.not_equals(&other.low),
			self.high.not_equals(&other.high)
		)
	}
	fn less_than(&self, other: &Self) -> Self::Flag {
		I::flag_or(
			self.high.less_than(&other.high),
			I::flag_and(
				self.high.equals(&other.high),
				self.low.less_than(&other.low)
			)
		)
	}
	fn greater_than(&self, other: &Self) -> Self::Flag {
		I::flag_or(
			self.high.greater_than(&other.high),
			I::flag_and(
				self.high.equals(&other.high),
				self.low.greater_than(&other.low)
			)
		)
	}
	
	fn bit_length() -> Self::BitCount {
		I::bit_length_add(I::bit_length(),I::bit_length())
	}
	fn bit_length_val_unless_eq(val: Self::BitCount, test: Self::BitCount, other: Self::BitCount) -> Self::BitCount {
		I::bit_length_val_unless_eq(val,test,other)
	}
	fn bit_length_add(x: Self::BitCount, y: Self::BitCount) -> Self::BitCount {
		I::bit_length_add(x,y)
	}
	
	fn count_ones(&self) -> Self::BitCount {
		I::bit_length_add(self.low.count_ones(),self.high.count_ones())
	}
	
	fn count_zeros(&self) -> Self::BitCount {
		I::bit_length_add(self.low.count_zeros(),self.high.count_zeros())
	}
	
	fn leading_zeros(&self) -> Self::BitCount {
		I::bit_length_val_unless_eq(
			self.high.leading_zeros(),
			DualInt::<I>::bit_length(),
			I::bit_length_add(
				DualInt::<I>::bit_length(),
				self.low.leading_zeros()
			)
		)
	}
	
	fn trailing_zeros(&self) -> Self::BitCount {
		I::bit_length_val_unless_eq(
			self.low.trailing_zeros(),
			DualInt::<I>::bit_length(),
			I::bit_length_add(
				DualInt::<I>::bit_length(),
				self.high.trailing_zeros()
			)
		)
	}
	
	fn reverse_bits(&self) -> Self {
		DualInt {
			low: self.high.reverse_bits(),
			high: self.low.reverse_bits()
		}
	}
	
	fn swap_bytes(&self) -> Self {
		DualInt {
			low: self.high.swap_bytes(),
			high: self.low.swap_bytes()
		}
	}
}
*/

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct IntWrap<T>(pub T);

macro_rules! impl_integer_wrap {
	(default,$uint:ident,$int:ident,$uint2:ident,$int2:ident,$size:expr) => {
//Default Implementation
impl IntLike for IntWrap<$uint> {
	type Flag = bool;
	type BitCount = u32;
	
	
	fn zero() -> Self {
		IntWrap(0)
	}
	fn one() -> Self {
		IntWrap(1)
	}
	fn minus_one() -> Self {
		IntWrap($uint::max_value())
	}
	fn from_flag(v: bool) -> Self {
		IntWrap(v as $uint)
	}
	fn flag_or(x: bool, y: bool) -> bool {
		x || y
	}
	fn flag_and(x: bool, y: bool) -> bool {
		x && y
	}
	fn flag_select(c: bool, t: Self, f: Self) -> Self {
		if c {
			t
		}else{
			f
		}
	}
	
	
	fn max_signed(&self, other: &Self) -> Self {
		IntWrap($int::max(self.0 as $int,other.0 as $int) as $uint)
	}
	fn max_unsigned(&self, other: &Self) -> Self {
		IntWrap($uint::max(self.0,other.0))
	}
	fn min_signed(&self, other: &Self) -> Self {
		IntWrap($int::min(self.0 as $int,other.0 as $int) as $uint)
	}
	fn min_unsigned(&self, other: &Self) -> Self {
		IntWrap($uint::min(self.0,other.0))
	}
	
	
	fn overflowing_add_carry_signed(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = $int::overflowing_add(self.0 as $int, other.0 as $int);
		let (val, c2) = $int::overflowing_add(tmp, carry as $int);
		(IntWrap(val as $uint), c1 || c2)
	}
	fn overflowing_add_carry_unsigned(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = $uint::overflowing_add(self.0, other.0);
		let (val, c2) = $uint::overflowing_add(tmp, carry as $uint);
		(IntWrap(val), c1 || c2)
	}
	fn overflowing_add_signed(&self, other: &Self) -> (Self, bool) {
		let (a,b) = $int::overflowing_add(self.0 as $int,other.0 as $int);
		(IntWrap(a as $uint),b)
	}
	fn overflowing_add_unsigned(&self, other: &Self) -> (Self, bool) {
		let (a,b) = $uint::overflowing_add(self.0,other.0);
		(IntWrap(a),b)
	}
	fn saturating_add_carry_signed(&self, other: &Self, carry: bool) -> Self {
		let tmp = $int::saturating_add(self.0 as $int,other.0 as $int);
		let val = $int::saturating_add(tmp, carry as $int);
		IntWrap(val as $uint)
	}
	fn saturating_add_carry_unsigned(&self, other: &Self, carry: bool) -> Self {
		let tmp = $uint::saturating_add(self.0,other.0);
		let val = $uint::saturating_add(tmp, carry as $uint);
		IntWrap(val)
	}
	fn saturating_add_signed(&self, other: &Self) -> Self {
		IntWrap($int::saturating_add(self.0 as $int, other.0 as $int) as $uint)
	}
	fn saturating_add_unsigned(&self, other: &Self) -> Self {
		IntWrap($uint::saturating_add(self.0,other.0))
	}
	fn wrapping_add_carry(&self, other: &Self, carry: bool) -> Self {
		let tmp = $uint::wrapping_add(self.0,other.0);
		let val = $uint::wrapping_add(tmp, carry as $uint);
		IntWrap(val)
	}
	fn wrapping_add(&self, other: &Self) -> Self {
		IntWrap($uint::wrapping_add(self.0,other.0))
	}
	
	
	fn overflowing_sub_carry_signed(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = $int::overflowing_sub(self.0 as $int, other.0 as $int);
		let (val, c2) = $int::overflowing_sub(tmp, carry as $int);
		(IntWrap(val as $uint), c1 || c2)
	}
	fn overflowing_sub_carry_unsigned(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = $uint::overflowing_sub(self.0, other.0);
		let (val, c2) = $uint::overflowing_sub(tmp, carry as $uint);
		(IntWrap(val), c1 || c2)
	}
	fn overflowing_sub_signed(&self, other: &Self) -> (Self, bool) {
		let (a,b) = $int::overflowing_sub(self.0 as $int,other.0 as $int);
		(IntWrap(a as $uint),b)
	}
	fn overflowing_sub_unsigned(&self, other: &Self) -> (Self, bool) {
		let (a,b) = $uint::overflowing_sub(self.0,other.0);
		(IntWrap(a),b)
	}
	fn saturating_sub_carry_signed(&self, other: &Self, carry: bool) -> Self {
		let tmp = $int::saturating_sub(self.0 as $int,other.0 as $int);
		let val = $int::saturating_sub(tmp, carry as $int);
		IntWrap(val as $uint)
	}
	fn saturating_sub_carry_unsigned(&self, other: &Self, carry: bool) -> Self {
		let tmp = $uint::saturating_sub(self.0,other.0);
		let val = $uint::saturating_sub(tmp, carry as $uint);
		IntWrap(val)
	}
	fn saturating_sub_signed(&self, other: &Self) -> Self {
		IntWrap($int::saturating_sub(self.0 as $int, other.0 as $int) as $uint)
	}
	fn saturating_sub_unsigned(&self, other: &Self) -> Self {
		IntWrap($uint::saturating_sub(self.0,other.0))
	}
	fn wrapping_sub_carry(&self, other: &Self, carry: bool) -> Self {
		let tmp = $uint::wrapping_sub(self.0,other.0);
		let val = $uint::wrapping_sub(tmp, carry as $uint);
		IntWrap(val)
	}
	fn wrapping_sub(&self, other: &Self) -> Self {
		IntWrap($uint::wrapping_sub(self.0,other.0))
	}
	
	
	fn overflowing_negate(&self) -> (Self, bool) {
		let (a,b) = $int::overflowing_neg(self.0 as $int);
		(IntWrap(a as $uint), b)
	}
	fn saturating_negate(&self) -> Self {
		IntWrap($int::saturating_neg(self.0 as $int) as $uint)
	}
	fn wrapping_negate(&self) -> Self {
		IntWrap($int::wrapping_neg(self.0 as $int) as $uint)
	}
	
	
	fn widening_mul_signed(&self, other: &Self) -> (Self, Self) {
		let a = ((self.0 as $int) as $int2);
		let b = ((other.0 as $int) as $int2);
		let c = a * b;
		let d = ((c as $uint2) as $uint);
		let e = (((c as $uint2) >> $size) as $uint);
		(IntWrap(d),IntWrap(e))
	}
	fn widening_mul_unsigned(&self, other: &Self) -> (Self, Self) {
		let a = self.0 as $uint2;
		let b = other.0 as $uint2;
		let c = a * b;
		let d= c as $uint;
		let e = (c >> $size) as $uint;
		(IntWrap(d),IntWrap(e))
	}
	fn overflowing_mul_signed(&self, other: &Self) -> (Self, bool) {
		let (a,b) = $int::overflowing_mul(self.0 as $int,other.0 as $int);
		(IntWrap(a as $uint),b)
	}
	fn overflowing_mul_unsigned(&self, other: &Self) -> (Self, bool) {
		let (a,b) = $uint::overflowing_mul(self.0,other.0);
		(IntWrap(a),b)
	}
	fn saturating_mul_signed(&self, other: &Self) -> Self {
		IntWrap($int::saturating_mul(self.0 as $int,other.0 as $int) as $uint)
	}
	fn saturating_mul_unsigned(&self, other: &Self) -> Self {
		IntWrap($uint::saturating_mul(self.0,other.0))
	}
	fn wrapping_mul(&self, other: &Self) -> Self {
		IntWrap($uint::wrapping_mul(self.0,other.0))
	}
	
	fn specified_div_rem_signed(&self, other: &Self) -> (Self, Self) {
		if other.0 == 0 {
			(IntWrap($uint::max_value()),IntWrap(self.0))
		}else{
			let(a,b) = $int::overflowing_div(self.0 as $int,other.0 as $int);
			let (c,d) = $int::overflowing_rem(self.0 as $int, other.0 as $int);
			assert_eq!(b,d);
			if b || d {
				(IntWrap($int::min_value() as $uint),IntWrap(0))
			}else{
				(IntWrap(a as $uint),IntWrap(c as $uint))
			}
		}
	}
	fn specified_div_rem_unsigned(&self, other: &Self) -> (Self, Self) {
		if other.0 == 0 {
			(IntWrap($uint::max_value()),IntWrap(self.0))
		}else{
			let a = $uint::wrapping_div(self.0,other.0);
			let b = $uint::wrapping_rem(self.0,other.0);
			(IntWrap(a),IntWrap(b))
		}
	}
	fn specified_div_signed(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap($uint::max_value())
		}else{
			let(a,b) = $int::overflowing_div(self.0 as $int,other.0 as $int);
			if b {
				IntWrap($int::min_value() as $uint)
			}else{
				IntWrap(a as $uint)
			}
		}
	}
	fn specified_div_unsigned(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap($uint::max_value())
		}else{
			let a = $uint::wrapping_div(self.0,other.0);
			(IntWrap(a))
		}
	}
	fn specified_rem_signed(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap(self.0)
		}else{
			let (a,b) = $int::overflowing_rem(self.0 as $int, other.0 as $int);
			if b {
				IntWrap(0)
			}else{
				IntWrap(a as $uint)
			}
		}
	}
	fn specified_rem_unsigned(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap(self.0)
		}else{
			let a = $uint::wrapping_rem(self.0,other.0);
			(IntWrap(a))
		}
	}
	fn validated_div_rem_signed(&self, other: &Self) -> (Self, Self, bool, bool) {
		if other.0 == 0 {
			(IntWrap($uint::max_value()),IntWrap(self.0),true,false)
		}else{
			let(a,b) = $int::overflowing_div(self.0 as $int,other.0 as $int);
			let (c,d) = $int::overflowing_rem(self.0 as $int, other.0 as $int);
			assert_eq!(b,d);
			(IntWrap(a as $uint),IntWrap(c as $uint),false,b || d)
		}
	}
	fn validated_div_rem_unsigned(&self, other: &Self) -> (Self, Self, bool) {
		if other.0 == 0 {
			(IntWrap($uint::max_value()),IntWrap(self.0),true)
		}else{
			let a = $uint::wrapping_div(self.0,other.0);
			let b = $uint::wrapping_rem(self.0,other.0);
			(IntWrap(a),IntWrap(b),false)
		}
	}
	fn validated_div_signed(&self, other: &Self) -> (Self, bool, bool) {
		if other.0 == 0 {
			(IntWrap($uint::max_value()),true,false)
		}else{
			let(a,b) = $int::overflowing_div(self.0 as $int,other.0 as $int);
			(IntWrap(a as $uint),false,b)
		}
	}
	fn validated_div_unsigned(&self, other: &Self) -> (Self, bool) {
		if other.0 == 0 {
			(IntWrap($uint::max_value()),true)
		}else{
			let a = $uint::wrapping_div(self.0,other.0);
			(IntWrap(a),false)
		}
	}
	fn validated_rem_signed(&self, other: &Self) -> (Self, bool, bool) {
		if other.0 == 0 {
			(IntWrap(self.0),true,false)
		}else{
			let(a,b) = $int::overflowing_rem(self.0 as $int,other.0 as $int);
			(IntWrap(a as $uint),false,b)
		}
	}
	fn validated_rem_unsigned(&self, other: &Self) -> (Self, bool) {
		if other.0 == 0 {
			(IntWrap(self.0),true)
		}else{
			let a = $uint::wrapping_rem(self.0,other.0);
			(IntWrap(a),false)
		}
	}
	
	
	fn bit_or(&self, other: &Self) -> Self {
		IntWrap(self.0 | other.0)
	}
	fn bit_and(&self, other: &Self) -> Self {
		IntWrap(self.0 & other.0)
	}
	fn bit_xor(&self, other: &Self) -> Self {
		IntWrap(self.0 ^ other.0)
	}
	fn bit_not(&self) -> Self {
		IntWrap(!self.0)
	}
	
	
	fn equals(&self, other: &Self) -> bool {
		self.0 == other.0
	}
	fn not_equals(&self, other: &Self) -> bool {
		self.0 != other.0
	}
	fn less_than_unsigned(&self, other: &Self) -> bool {
		self.0 < other.0
	}
	fn less_than_signed(&self, other: &Self) -> bool {
		(self.0 as $int) < (other.0 as $int)
	}
	fn greater_than_unsigned(&self, other: &Self) -> bool {
		self.0 > other.0
	}
	fn greater_than_signed(&self, other: &Self) -> bool {
		(self.0 as $int) > (other.0 as $int)
	}
	
	
	fn bit_length() -> u32 {
		$size
	}
	fn bit_length_val_unless_eq(val: u32, test: u32, other: u32) -> u32 {
		if val == test {
			other
		}else{
			val
		}
	}
	fn bit_length_add(x: u32, y: u32) -> u32 {
		x + y
	}
	
	
	fn count_ones(&self) -> u32 {
		self.0.count_ones()
	}
	fn count_zeros(&self) -> u32 {
		self.0.count_zeros()
	}
	fn leading_zeros(&self) -> u32{
		self.0.leading_zeros()
	}
	fn trailing_zeros(&self) -> u32 {
		self.0.trailing_zeros()
	}
	fn reverse_bits(&self) -> Self {
		IntWrap(self.0.reverse_bits())
	}
	fn swap_bytes(&self) -> Self {
		IntWrap(self.0.swap_bytes())
	}
}
	}
}

impl_integer_wrap!(default,u8,i8,u16,i16,8);
impl_integer_wrap!(default,u16,i16,u32,i32,16);
impl_integer_wrap!(default,u32,i32,u64,i64,32);
impl_integer_wrap!(default,u64,i64,u128,i128,64);
//impl_integer_wrap!(impl,u128,i128,{},{});


/*
impl IntLike for IntWrap<u32> {
	type Flag = bool;
	type BitCount = u32;
	
	
	fn zero() -> Self {
		IntWrap(0)
	}
	fn one() -> Self {
		IntWrap(1)
	}
	fn minus_one() -> Self {
		IntWrap(u32::max_value())
	}
	fn from_flag(v: bool) -> Self {
		IntWrap(v as u32)
	}
	fn flag_or(x: bool, y: bool) -> bool {
		x || y
	}
	fn flag_and(x: bool, y: bool) -> bool {
		x && y
	}
	fn flag_select(c: bool, t: Self, f: Self) -> Self {
		if c {
			t
		}else{
			f
		}
	}
	
	
	fn max_signed(&self, other: &Self) -> Self {
		IntWrap(i32::max(self.0 as i32,other.0 as i32) as u32)
	}
	fn max_unsigned(&self, other: &Self) -> Self {
		IntWrap(u32::max(self.0,other.0))
	}
	fn min_signed(&self, other: &Self) -> Self {
		IntWrap(i32::min(self.0 as i32,other.0 as i32) as u32)
	}
	fn min_unsigned(&self, other: &Self) -> Self {
		IntWrap(u32::min(self.0,other.0))
	}
	
	
	fn overflowing_add_carry_signed(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = i32::overflowing_add(self.0 as i32, other.0 as i32);
		let (val, c2) = i32::overflowing_add(tmp, c as i32);
		(IntWrap(val as u32), c1 || c2)
	}
	fn overflowing_add_carry_unsigned(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = u32::overflowing_add(self.0, other.0);
		let (val, c2) = u32::overflowing_add(tmp, c as u32);
		(IntWrap(val), c1 || c2)
	}
	fn overflowing_add_signed(&self, other: &Self) -> (Self, bool) {
		let (a,b) = i32::overflowing_add(self.0 as i32,other.0 as i32);
		(IntWrap(a as u32),b)
	}
	fn overflowing_add_unsigned(&self, other: &Self) -> (Self, bool) {
		let (a,b) = u32::overflowing_add(self.0,other.0);
		(IntWrap(a),b)
	}
	fn saturating_add_carry_signed(&self, other: &Self, carry: bool) -> Self {
		let tmp = i32::saturating_add(self.0 as i32,other.0 as i32);
		let val = i32::saturating_add(tmp,carry as i32);
		IntWrap(val as u32)
	}
	fn saturating_add_carry_unsigned(&self, other: &Self, carry: bool) -> Self {
		let tmp = u32::saturating_add(self.0,other.0);
		let val = u32::saturating_add(tmp,carry as u32);
		IntWrap(val)
	}
	fn saturating_add_signed(&self, other: &Self) -> Self {
		IntWrap(i32::saturating_add(self.0 as i32, other.0 as i32) as u32)
	}
	fn saturating_add_unsigned(&self, other: &Self) -> Self {
		IntWrap(u32::saturating_add(self.0,other.0))
	}
	fn wrapping_add_carry(&self, other: &Self, carry: bool) -> Self {
		let tmp = u32::wrapping_add(self.0,other.0);
		let val = u32::wrapping_add(tmp,carry as u32);
		IntWrap(val)
	}
	fn wrapping_add(&self, other: &Self) -> Self {
		IntWrap(u32::wrapping_add(self.0,other.0))
	}
	
	
	fn overflowing_sub_carry_signed(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = i32::overflowing_sub(self.0 as i32, other.0 as i32);
		let (val, c2) = i32::overflowing_sub(tmp, c as i32);
		(IntWrap(val as u32), c1 || c2)
	}
	fn overflowing_sub_carry_unsigned(&self, other: &Self, carry: bool) -> (Self, bool) {
		let (tmp, c1) = u32::overflowing_sub(self.0, other.0);
		let (val, c2) = u32::overflowing_sub(tmp, c as u32);
		(IntWrap(val), c1 || c2)
	}
	fn overflowing_sub_signed(&self, other: &Self) -> (Self, bool) {
		let (a,b) = i32::overflowing_sub(self.0 as i32,other.0 as i32);
		(IntWrap(a as u32),b)
	}
	fn overflowing_sub_unsigned(&self, other: &Self) -> (Self, bool) {
		let (a,b) = u32::overflowing_sub(self.0,other.0);
		(IntWrap(a),b)
	}
	fn saturating_sub_carry_signed(&self, other: &Self, carry: bool) -> Self {
		let tmp = i32::saturating_sub(self.0 as i32,other.0 as i32);
		let val = i32::saturating_sub(tmp,carry as i32);
		IntWrap(val as u32)
	}
	fn saturating_sub_carry_unsigned(&self, other: &Self, carry: bool) -> Self {
		let tmp = u32::saturating_sub(self.0,other.0);
		let val = u32::saturating_sub(tmp,carry as u32);
		IntWrap(val)
	}
	fn saturating_sub_signed(&self, other: &Self) -> Self {
		IntWrap(i32::saturating_sub(self.0 as i32, other.0 as i32) as u32)
	}
	fn saturating_sub_unsigned(&self, other: &Self) -> Self {
		IntWrap(u32::saturating_sub(self.0,other.0))
	}
	fn wrapping_sub_carry(&self, other: &Self, carry: bool) -> Self {
		let tmp = u32::wrapping_sub(self.0,other.0);
		let val = u32::wrapping_sub(tmp,carry as u32);
		IntWrap(val)
	}
	fn wrapping_sub(&self, other: &Self) -> Self {
		IntWrap(u32::wrapping_sub(self.0,other.0))
	}
	
	
	fn overflowing_negate(&self) -> (Self, bool) {
		let (a,b) = i32::overflowing_neg(self.0 as i32);
		(IntWrap(a as u32), b)
	}
	fn saturating_negate(&self) -> Self {
		IntWrap(i32::saturating_neg(self.0 as i32) as u32)
	}
	fn wrapping_negate(&self) -> Self {
		IntWrap(i32::wrapping_neg(self.0 as i32) as u32)
	}
	
	
	fn widening_mul_signed(&self, other: &Self) -> (Self, Self) {
		let a = ((self.0 as i32) as i64);
		let b = ((other.0 as i32) as i64);
		let c = a * b;
		let d = ((c as u64) as u32);
		let e = (((c as u64) >> 32) as u32);
		(IntWrap(d),IntWrap(e))
	}
	fn widening_mul_unsigned(&self, other: &Self) -> (Self, Self) {
		let a = self.0 as u64;
		let b = other.0 as u64;
		let c = a * b;
		let d= c as u32;
		let e = (c >> 32) as u32;
		(IntWrap(d),IntWrap(e))
	}
	fn overflowing_mul_signed(&self, other: &Self) -> (Self, bool) {
		let (a,b) = i32::overflowing_mul(self.0 as i32,other.0 as i32);
		(IntWrap(a as u32),b)
	}
	fn overflowing_mul_unsigned(&self, other: &Self) -> (Self, bool) {
		let (a,b) = u32::overflowing_mul(self.0,other.0);
		(IntWrap(a),b)
	}
	fn saturating_mul_signed(&self, other: &Self) -> Self {
		IntWrap(i32::saturating_mul(self.0 as i32,other.0 as i32) as u32)
	}
	fn saturating_mul_unsigned(&self, other: &Self) -> Self {
		IntWrap(u32::saturating_mul(self.0,other.0))
	}
	fn wrapping_mul(&self, other: &Self) -> Self {
		IntWrap(u32::wrapping_mul(self.0,other.0))
	}
	
	fn specified_div_rem_signed(&self, other: &Self) -> (Self, Self) {
		if other.0 == 0 {
			(IntWrap(u32::max_value()),IntWrap(self.0))
		}else{
			let(a,b) = i32::overflowing_div(self.0 as i32,other.0 as i32);
			let (c,d) = i32::overflowing_rem(self.0 as i32, other.0 as i32);
			assert_eq!(b,d);
			if b || d {
				(IntWrap(i32::min_value() as u32),IntWrap(0))
			}else{
				(IntWrap(a as u32),IntWrap(c as u32))
			}
		}
	}
	fn specified_div_rem_unsigned(&self, other: &Self) -> (Self, Self) {
		if other.0 == 0 {
			(IntWrap(u32::max_value()),IntWrap(self.0))
		}else{
			let a = u32::wrapping_div(self.0,other.0);
			let b = u32::wrapping_rem(self.0,other.0);
			(IntWrap(a),IntWrap(b))
		}
	}
	fn specified_div_signed(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap(u32::max_value())
		}else{
			let(a,b) = i32::overflowing_div(self.0 as i32,other.0 as i32);
			if b {
				IntWrap(i32::min_value() as u32)
			}else{
				IntWrap(a as u32)
			}
		}
	}
	fn specified_div_unsigned(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap(u32::max_value())
		}else{
			let a = u32::wrapping_div(self.0,other.0);
			(IntWrap(a))
		}
	}
	fn specified_rem_signed(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap(self.0)
		}else{
			let (a,b) = i32::overflowing_rem(self.0 as i32, other.0 as i32);
			if b {
				IntWrap(0)
			}else{
				IntWrap(a as u32)
			}
		}
	}
	fn specified_rem_unsigned(&self, other: &Self) -> Self {
		if other.0 == 0 {
			IntWrap(self.0)
		}else{
			let a = u32::wrapping_rem(self.0,other.0);
			(IntWrap(a))
		}
	}
	fn validated_div_rem_signed(&self, other: &Self) -> (Self, Self, bool, bool) {
		if other.0 == 0 {
			(IntWrap(u32::max_value()),IntWrap(self.0),true,false)
		}else{
			let(a,b) = i32::overflowing_div(self.0 as i32,other.0 as i32);
			let (c,d) = i32::overflowing_rem(self.0 as i32, other.0 as i32);
			assert_eq!(b,d);
			(IntWrap(a as u32),IntWrap(c as u32),false,b || d)
		}
	}
	fn validated_div_rem_unsigned(&self, other: &Self) -> (Self, Self, bool) {
		if other.0 == 0 {
			(IntWrap(u32::max_value()),IntWrap(self.0),true)
		}else{
			let a = u32::wrapping_div(self.0,other.0);
			let b = u32::wrapping_rem(self.0,other.0);
			(IntWrap(a),IntWrap(b),false)
		}
	}
	fn validated_div_signed(&self, other: &Self) -> (Self, bool, bool) {
		if other.0 == 0 {
			(IntWrap(u32::max_value()),true,false)
		}else{
			let(a,b) = i32::overflowing_div(self.0 as i32,other.0 as i32);
			(IntWrap(a as u32),false,b)
		}
	}
	fn validated_div_unsigned(&self, other: &Self) -> (Self, bool) {
		if other.0 == 0 {
			(IntWrap(u32::max_value()),true)
		}else{
			let a = u32::wrapping_div(self.0,other.0);
			(IntWrap(a),false)
		}
	}
	fn validated_rem_signed(&self, other: &Self) -> (Self, bool, bool) {
		if other.0 == 0 {
			(IntWrap(self.0),true,false)
		}else{
			let(a,b) = i32::overflowing_rem(self.0 as i32,other.0 as i32);
			(IntWrap(a as u32),false,b)
		}
	}
	fn validated_rem_unsigned(&self, other: &Self) -> (Self, bool) {
		if other.0 == 0 {
			(IntWrap(self.0),true)
		}else{
			let a = u32::wrapping_rem(self.0,other.0);
			(IntWrap(a),false)
		}
	}
	
	
	fn bit_or(&self, other: &Self) -> Self {
		IntWrap(self.0 | other.0)
	}
	fn bit_and(&self, other: &Self) -> Self {
		IntWrap(self.0 & other.0)
	}
	fn bit_xor(&self, other: &Self) -> Self {
		IntWrap(self.0 ^ other.0)
	}
	fn bit_not(&self) -> Self {
		IntWrap(!self.0)
	}
	
	
	fn equals(&self, other: &Self) -> bool {
		self.0 == other.0
	}
	fn not_equals(&self, other: &Self) -> bool {
		self.0 != other.0
	}
	fn less_than_unsigned(&self, other: &Self) -> bool {
		self.0 < other.0
	}
	fn less_than_signed(&self, other: &Self) -> bool {
		(self.0 as i32) < (other.0 as i32)
	}
	fn greater_than_unsigned(&self, other: &Self) -> bool {
		self.0 > other.0
	}
	fn greater_than_signed(&self, other: &Self) -> bool {
		(self.0 as i32) > (other.0 as i32)
	}
	
	
	fn bit_length() -> u32 {
		32
	}
	fn bit_length_val_unless_eq(val: u32, test: u32, other: u32) -> u32 {
		if val == test {
			other
		}else{
			val
		}
	}
	fn bit_length_add(x: u32, y: u32) -> u32 {
		x + y
	}
	
	
	fn count_ones(&self) -> u32 {
		self.0.count_ones()
	}
	fn count_zeros(&self) -> u32 {
		self.0.count_zeros()
	}
	fn leading_zeros(&self) -> u32{
		self.0.leading_zeros()
	}
	fn trailing_zeros(&self) -> u32 {
		self.0.trailing_zeros()
	}
	fn reverse_bits(&self) -> Self {
		IntWrap(self.0.reverse_bits())
	}
	fn swap_bytes(&self) -> Self {
		IntWrap(self.0.swap_bytes())
	}
}*/
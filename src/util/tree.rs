use hashbrown::{HashMap, HashSet};
use hashbrown::hash_map::Entry;
use std::hash::Hash;
use std::ptr::NonNull;
use std::marker::PhantomData;

//FIXME: IDEA (next-child-ptr | Option<first-child-ptr>) ?? (allows for iterating children?)
//        is this needed?
//  insert-node: make-first-child: update-first-child-ptr=>inserted
//  remove-node: (may need previous-child to remain O(1)

struct QueryNode<L,R> {
	root_ptr: NonNull<QueryNode<L,R>>,
	root_distance: usize,
	child_count: usize,
	ptr_contents: Result<(NonNull<QueryNode<L,R>>,L),R>,
}

impl<L,R> QueryNode<L,R> {
	#[inline(always)]
	unsafe fn get_root(node: NonNull<Self>) -> NonNull<Self> {
		(*node.as_ptr()).root_ptr
	}
	#[inline(always)]
	unsafe fn set_root(node: NonNull<Self>, root: NonNull<Self>) {
		(*node.as_ptr()).root_ptr = root;
	}
	#[inline(always)]
	unsafe fn get_dist(node: NonNull<Self>) -> usize {
		(*node.as_ptr()).root_distance
	}
	#[inline(always)]
	unsafe fn set_dist(node: NonNull<Self>, dist: usize) {
		(*node.as_ptr()).root_distance = dist;
	}
	#[inline(always)]
	unsafe fn inc_dist(node: NonNull<Self>, delta: usize) {
		(*node.as_ptr()).root_distance += delta;
	}
	#[inline(always)]
	unsafe fn get_child(node: NonNull<Self>) -> usize {
		(*node.as_ptr()).child_count
	}
	#[inline(always)]
	unsafe fn dec_child(node: NonNull<Self>) {
		debug_assert!(QueryNode::get_child(node) != 0);
		(*node.as_ptr()).child_count -= 1;
	}
	#[inline(always)]
	unsafe fn inc_child(node: NonNull<Self>) {
		(*node.as_ptr()).child_count += 1;
	}
	#[inline(always)]
	unsafe fn is_leaf(node: NonNull<Self>) -> bool {
		(*node.as_ptr()).child_count == 0
	}
	#[inline(always)]
	unsafe fn is_root(node: NonNull<Self>) -> bool {
		(*node.as_ptr()).root_distance == 0
	}
	#[inline(always)]
	unsafe fn swap_parent_content(
		node: NonNull<Self>,
		content: &mut Result<(NonNull<QueryNode<L,R>>,L),R>
	) {
		let remote_content = &mut (*node.as_ptr()).ptr_contents;
		std::mem::swap(content,remote_content);
	}
	#[inline]
	unsafe fn get_parent_mut(node: NonNull<Self>) -> Option<NonNull<Self>> {
		if let Ok((parent,_)) = &mut (*node.as_ptr()).ptr_contents {
			Some(*parent)
		}else{
			None
		}
	}
	#[inline]
	fn get_contents(&self) -> Result<&L,&R> {
		match &self.ptr_contents {
			Ok((_,l)) => Ok(l),
			Err(e) => Err(e)
		}
	}
	#[inline(always)]
	unsafe fn get_leaf_contents_unchecked(&self) -> (NonNull<Self>,&L) {
		match &self.ptr_contents {
			Ok((parent,l)) => (*parent,l),
			Err(e) => std::hint::unreachable_unchecked()
		}
	}
	#[inline]
	unsafe fn drop_ptr(ptr: NonNull<Self>) {
		let boxed = Box::from_raw(ptr.as_ptr());
		std::mem::drop(boxed);
	}
}

///FIXME: RENAIM Forest??
pub struct QueryTree<K: Hash + Eq,L,R> {
	key_map: HashMap<K,NonNull<QueryNode<L,R>>>,
	_phantom: PhantomData<(*mut L, *mut R)>,
}

impl<K: Hash + Eq,L,R> QueryTree<K,L,R> {
	
	/// Create a new query tree
	pub fn new() -> QueryTree<K,L,R> {
		QueryTree {
			key_map: HashMap::new(),
			_phantom: PhantomData
		}
	}
	
	/// Get the number of children of a given node
	pub fn get_child_count(&self, key: &K) -> Option<usize> {
		match self.key_map.get(key) {
			Some(node) => {
				unsafe {
					Some(QueryNode::get_child(*node))
				}
			},
			None => None
		}
	}
	
	/// Get the distance from the root of a node
	pub fn get_root_distance(&self, key: &K) -> Option<usize> {
		match self.key_map.get(key) {
			Some(node) => {
				unsafe {
					Some(QueryNode::get_dist(*node))
				}
			},
			None => None
		}
	}
	
	/// Check if two keys are part of the same tree in the forest
	pub fn have_same_root(&self, key1: &K, key2: &K) -> Option<bool> {
		match self.key_map.get(key1) {
			Some(raw_node1) => {
				let mut node1 = *raw_node1;
				let root1 = unsafe {
					QueryNode::get_root(node1)
				};
				match self.key_map.get(key2) {
					Some(raw_node2) => {
						let mut node2 = *raw_node2;
						let root2 = unsafe {
							QueryNode::get_root(node2)
						};
						Some(root1 == root2)
					},
					None => None
				}
			},
			None => None
		}
	}
	
	/// Create a new root node: O(1)
	pub fn insert_root(&mut self, key: K, root: R) -> bool {
		match self.key_map.entry(key) {
			Entry::Occupied(_) => false,
			Entry::Vacant(mut vacant) => {
				let root_node = Box::new(QueryNode {
					root_ptr: NonNull::dangling(),
					root_distance: 0,
					child_count: 0,
					ptr_contents: Err(root)
				});
				let mut node = Box::into_raw_non_null(root_node);
				unsafe {
					QueryNode::set_root(node,node);
				}
				vacant.insert(node);
				true
			},
		}
	}
	
	/// Create a new leaf node: O(1)
	pub fn insert_leaf(&mut self, key: K, leaf: L, parent: K) -> bool {
		let mut parent_node = match self.key_map.get_mut(&parent) {
			Some(v) => *v,
			None => return false
		};
		match self.key_map.entry(key) {
			Entry::Occupied(_) => false,
			Entry::Vacant(vacant) => {
				unsafe {
					parent_node.as_mut().child_count += 1;
				}
				let root_node = Box::new(QueryNode {
					root_ptr: unsafe {
						QueryNode::get_root(parent_node)
					},
					root_distance: unsafe {
						QueryNode::get_dist(parent_node) + 1
					},
					child_count: 0,
					ptr_contents: Ok((parent_node,leaf))
				});
				let mut node = Box::into_raw_non_null(root_node);
				vacant.insert(node);
				true
			},
		}
	}
	
	/// Remove a leaf node: O(1)
	pub fn remove_leaf(&mut self, key: K) -> bool {
		match self.key_map.entry(key) {
			Entry::Occupied(mut occupied) => {
				let mut node = *occupied.get_mut();
				unsafe {
					if QueryNode::is_leaf(node) {
						let mut parent_node = QueryNode::get_parent_mut(node);
						QueryNode::drop_ptr(occupied.remove());
						if let Some(mut parent) = parent_node {
							QueryNode::dec_child(parent);
						}
						true
					} else {
						false
					}
				}
			},
			Entry::Vacant(_) => false
		}
	}
	
	/// Remove a rooted tree from the root: O(n_forest)
	pub fn remove_rooted_tree(&mut self, root: K) -> bool {
		match self.key_map.entry(root) {
			Entry::Occupied(mut occupied) => {
				let mut node = *occupied.get_mut();
				unsafe {
					if QueryNode::is_root(node) {
						self.key_map.retain(|_,val| {
							let mut ptr = *val;
							if QueryNode::get_root(ptr) == node {
								QueryNode::drop_ptr(ptr);
								false
							}else{
								true
							}
						});
						true /* Success */
					} else {
						false /* Not a Root Node */
					}
				}
			},
			Entry::Vacant(_) => false /* Node does not exist */
		}
	}
	
	/// Transfer a leaf node from its current parent to another: O(1)
	pub fn transfer_leaf(&mut self, key: &K, parent_key: &K) -> bool {
		let new_parent = match self.key_map.get_mut(parent_key) {
			Some(v) => *v,
			None => return false
		};
		let root_ptr = unsafe { QueryNode::get_root(new_parent) };
		let root_dist = unsafe { QueryNode::get_dist(new_parent) + 1 };
		match self.key_map.get_mut(key) {
			Some(child_ref) => {
				let child = *child_ref;
				unsafe {
					if let Ok((old_parent,_)) = &mut (*child.as_ptr()).ptr_contents {
						QueryNode::dec_child(*old_parent);
						*old_parent = new_parent;
					}
					QueryNode::inc_child(new_parent);
					QueryNode::set_dist(child,root_dist);
					QueryNode::set_root(child,root_ptr);
					true
				}
			},
			None => false
		}
	}
	
	///Transfer a rooted tree via its root into a leaf of another tree: O(n_forest)
	pub fn transfer_rooted_tree(&mut self, root: &K, parent_key: &K, leaf_value: L) -> Option<R> {
		let mut new_parent = match self.key_map.get_mut(parent_key) {
			Some(v) => *v,
			None => return None
		};
		let mut new_root_ptr = unsafe { QueryNode::get_root(new_parent) };
		let root_dist_delta = unsafe { QueryNode::get_dist(new_parent) + 1 };
		
		let (old_root_ptr,r_data) = match self.key_map.get_mut(root) {
			Some(rr) => {
				let moved_root = *rr;
				unsafe {
					if QueryNode::is_root(moved_root) && moved_root != new_root_ptr {
						let mut new_content = Ok((new_parent,leaf_value));
						QueryNode::swap_parent_content(moved_root,&mut new_content);
						QueryNode::inc_child(new_parent);
						if let Err(r_data) = new_content {
							(moved_root,r_data)
						}else{
							std::hint::unreachable_unchecked()
						}
					}else{
						return None
					}
				}
			},
			None => return None
		};
		for value in self.key_map.values_mut() {
			let node = *value;
			unsafe {
				if old_root_ptr == QueryNode::get_root(node){
					QueryNode::set_root(node,new_root_ptr);
					QueryNode::inc_dist(node, root_dist_delta);
				}
			}
		}
		Some(r_data)
	}
	
	/// Perform a lookup-query on two nodes
	///  using functions to transfer intermediate
	///  state: O(height) (fast in practice)
	pub fn perform_query<S,U,M,I,F>(
		&self,
		node1: &K,
		node2: &K,
		mut start_fn: S,
		mut update_fn: U,
		mut merge_fn: M,
	) -> Option<F>
		where S: FnMut(&K,Result<&L,&R>) -> I,
	          U: FnMut(&L,I) -> I,
			  M: FnMut(I,I,Result<&L,&R>) -> F
	{
		let mut ptr1 = *self.key_map.get(node1)?;
		let mut ptr2 = *self.key_map.get(node2)?;
		
		//Short-cut: Return None if they are part of distinct roots
		unsafe {
			if !std::ptr::eq(
				QueryNode::get_root(ptr1).as_ptr(),
				QueryNode::get_root(ptr2).as_ptr()
			) {
				return None;
			}
		}
		
		//Find initial iteration values for the function
		let mut val1 : I = unsafe { start_fn(node1,QueryNode::get_contents(ptr1.as_ref())) };
		let mut val2 : I = unsafe { start_fn(node1,QueryNode::get_contents(ptr2.as_ref())) };
		
		//While they have not meet up
		while ptr1 != ptr2 {
			unsafe {
				if QueryNode::get_dist(ptr1) > QueryNode::get_dist(ptr2) {
					let (next, data1) = ptr1.as_ref().get_leaf_contents_unchecked();
					val1 = update_fn(data1,val1);
					ptr1 = next;
				}else{
					let (next, data2) = ptr2.as_ref().get_leaf_contents_unchecked();
					val2 = update_fn(data2,val2);
					ptr2 = next;
				}
			}
		}
		
		Some(merge_fn(val1,val2,unsafe { ptr1.as_ref().get_contents() }))
	}
}

impl<K: Hash + Eq,L,F> Drop for QueryTree<K,L,F> {
	fn drop(&mut self) {
		for v in self.key_map.values_mut() {
			unsafe {
				QueryNode::drop_ptr(*v);
			}
		}
	}
}


#[cfg(test)]
mod tests {
	use super::*;
	use std::sync::atomic::AtomicIsize;
	use std::sync::atomic::Ordering::SeqCst;
	
	fn validate_query_tree<K: Hash + Eq,L,R>(tree: &QueryTree<K,L,R>) {
		let assert_valid = |ptr: NonNull<QueryNode<L,R>>| {
			assert_ne!(ptr,NonNull::dangling());
			let mut iter = tree.key_map.values();
			let _ = iter.find(|v| **v == ptr).unwrap();
			assert!(iter.find(|v| **v == ptr).is_none());
		};
		
		//Validate Invariant: Valid Ptr
		for &value in tree.key_map.values() {
			assert_valid(value);
			unsafe {
				assert_valid(QueryNode::get_root(value));
				if let Ok((parent,_)) = &(*value.as_ptr()).ptr_contents {
					assert_valid(*parent);
				}
			}
		}
		//Validate Invariant: Root Distance
		for &value in tree.key_map.values() {
			let mut ptr = value;
			let mut old_distance = unsafe {QueryNode::get_dist(ptr)};
			unsafe {
				if let Ok((parent,_)) = &(*ptr.as_ptr()).ptr_contents {
					let parent = *parent;
					let new_distance = QueryNode::get_dist(parent);
					assert_eq!(new_distance + 1,old_distance);
					old_distance = new_distance;
					ptr = parent;
				}
			}
		}
		//Validate Invariant: Root Ptr
		for &value in tree.key_map.values() {
			unsafe {
				if let Ok(_) = &(*value.as_ptr()).ptr_contents {
					assert_ne!(QueryNode::get_dist(value),0);
					assert_ne!(QueryNode::get_root(value),value);
				} else {
					assert!(QueryNode::is_root(value));
					assert_eq!(QueryNode::get_dist(value),0);
					assert_eq!(QueryNode::get_root(value),value);
				}
			}
		}
		//Validate Invariant: Child Count
		let mut child_count_map = HashMap::new();
		for &value in tree.key_map.values() {
			unsafe {
				if let Ok((parent, _)) = &(*value.as_ptr()).ptr_contents {
					let parent = *parent;
					let value = child_count_map.entry(parent).or_insert(0usize);
					*value += 1;
				}
			}
		}
		for &value in tree.key_map.values() {
			unsafe {
				assert_eq!(QueryNode::get_child(value),*child_count_map.get(&value).unwrap_or(&0));
			}
		}
	}
	
	struct DropBomb<'a>(&'a AtomicIsize,isize);
	impl<'a> DropBomb<'a> {
		pub fn new(a: &'a AtomicIsize, x: isize) -> DropBomb<'a> {
			a.fetch_add(x,SeqCst);
			DropBomb(a,x)
		}
	}
	impl<'a> Drop for DropBomb<'a> {
		fn drop(&mut self) {
			self.0.fetch_sub(self.1,SeqCst);
		}
	}
	
	#[test]
	pub fn test_add() {
		let drop_val = AtomicIsize::new(0);
		let mut query = QueryTree::new();
		assert!(query.insert_root(0usize,()));
		validate_query_tree(&query);
		assert!(query.insert_leaf(1,DropBomb::new(&drop_val,4),0));
		validate_query_tree(&query);
		assert_eq!(drop_val.load(SeqCst),4);
		assert!(!query.insert_leaf(4,DropBomb::new(&drop_val,10),4));
		validate_query_tree(&query);
		assert_eq!(drop_val.load(SeqCst),4);
		assert!(query.remove_leaf(1));
		validate_query_tree(&query);
		assert_eq!(drop_val.load(SeqCst),0);
		assert!(query.insert_leaf(1,DropBomb::new(&drop_val,4),0));
		validate_query_tree(&query);
		assert_eq!(drop_val.load(SeqCst),4);
		
		std::mem::drop(query);
		assert_eq!(drop_val.load(SeqCst),0);
	}
	
	#[test]
	pub fn test_delete() {
		let mut query = QueryTree::new();
		assert!(query.insert_root(0,()));          validate_query_tree(&query);
		assert!(query.insert_leaf(1,(),0)); validate_query_tree(&query);
		assert!(query.insert_leaf(2,(),0)); validate_query_tree(&query);
		assert!(query.insert_leaf(3,(),0)); validate_query_tree(&query);
		assert!(query.insert_leaf(4,(),2)); validate_query_tree(&query);
		assert!(!query.remove_leaf(2));                  validate_query_tree(&query);
		assert_eq!(query.get_child_count(&0),Some(3));   validate_query_tree(&query);
		assert_eq!(query.get_child_count(&1),Some(0));   validate_query_tree(&query);
		assert_eq!(query.get_child_count(&1),Some(0));   validate_query_tree(&query);
		assert_eq!(query.get_child_count(&2),Some(1));   validate_query_tree(&query);
		assert_eq!(query.get_child_count(&4),Some(0));   validate_query_tree(&query);
		assert_eq!(query.get_child_count(&5),None);      validate_query_tree(&query);
		assert_eq!(query.get_root_distance(&4),Some(2)); validate_query_tree(&query);
		assert!(!query.remove_rooted_tree(2));           validate_query_tree(&query);
		assert!(query.remove_rooted_tree(0));           validate_query_tree(&query);
	}
	
	#[test]
	pub fn test_move_leaf() {
		let mut query = QueryTree::new();
		assert!(query.insert_root(0,()));
		assert!(query.insert_leaf(1,(),0));
		assert!(query.insert_leaf(2,(),0));
		assert!(query.insert_leaf(3,(),1));
		assert!(query.insert_leaf(4,(),1));
		assert!(query.insert_leaf(5,(),3));
		validate_query_tree(&query);
		assert!(query.transfer_leaf(&5,&3));
		validate_query_tree(&query);
		assert!(query.transfer_leaf(&5,&1));
		validate_query_tree(&query);
		assert!(query.transfer_leaf(&5,&0));
		validate_query_tree(&query);
	}
	
	#[test]
	pub fn test_move_tree() {
		let mut query = QueryTree::new();
		assert!(query.insert_root(0,()));
		assert!(query.insert_leaf(1,(),0));
		assert!(query.insert_leaf(2,(),0));
		assert!(query.insert_leaf(3,(),1));
		assert!(query.insert_leaf(4,(),1));
		assert!(query.insert_leaf(5,(),3));
		validate_query_tree(&query);
		
		assert!(query.insert_root(10,()));
		assert!(query.insert_leaf(11,(),10));
		assert!(query.insert_leaf(12,(),10));
		assert!(query.insert_leaf(13,(),11));
		assert!(query.insert_leaf(14,(),11));
		assert!(query.insert_leaf(15,(),13));
		validate_query_tree(&query);
		
		
		assert_eq!(query.have_same_root(&10,&0),Some(false));
		assert_eq!(query.have_same_root(&10,&11),Some(true));
		assert_eq!(query.have_same_root(&10,&50),None);
		assert_eq!(query.have_same_root(&50,&2),None);
		
		assert!(query.transfer_rooted_tree(&11,&12,()).is_none());
		validate_query_tree(&query);
		assert!(query.transfer_rooted_tree(&11,&5,()).is_none());
		validate_query_tree(&query);
		assert!(query.transfer_rooted_tree(&10,&15,()).is_none());
		validate_query_tree(&query);
		assert!(query.transfer_rooted_tree(&10,&10,()).is_none());
		validate_query_tree(&query);
		
		assert!(query.transfer_rooted_tree(&10,&5,()).is_some());
		validate_query_tree(&query);
	}
}

use crate::util::{Limits,ValueGraph, PartialOrderSet};

/// Represents a node in the graph
///  that can have a variable number of entry
///  values in use.
/// Uniquely corresponds to an OpNode inside the OperationGraph
#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct NodeIdx(u32);

/// Represents a entry node
#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct EntrySetIdx(u8);

#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct EntryValueIdx(u16);

#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct ElementIdx(u8);

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct NodeEntry {
	pub node: NodeIdx,
	pub set: EntrySetIdx,
	pub value: EntryValueIdx,
}

/// Wrapped in a u64 for performance reasons (ord and eq are faster)
///  compared to the alternative implementation
#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct NodeElement(u64);

impl NodeElement {
	#[inline(always)]
	pub fn node(self) -> NodeIdx {
		NodeIdx((self.0 >> 32) as u32)
	}
	#[inline(always)]
	pub fn set(self) -> EntrySetIdx {
		EntrySetIdx((self.0 >> 8) as u8)
	}
	#[inline(always)]
	pub fn value(self) -> EntryValueIdx {
		EntryValueIdx((self.0 >> 16) as u16)
	}
	#[inline(always)]
	pub fn element(self) -> ElementIdx {
		ElementIdx(self.0 as u8)
	}
	#[inline(always)]
	pub fn new(
		node: NodeIdx, set: EntrySetIdx,
		value: EntryValueIdx, element: ElementIdx
	) -> NodeElement {
		NodeElement(
			((node.0 as u64) << 32 | (value.0 as u64) << 16)
				| ((set.0 as u64) << 8 | element.0 as u64)
		)
	}
}

impl Limits for NodeElement {
	const MIN: Self = NodeElement(std::u64::MIN);
	const MAX: Self = NodeElement(std::u64::MAX);
}

///Static Assert Type Sizes:
/// Prevent unexpected changes in memory usage
const _NODE_ENTRY : [(); std::mem::size_of::<NodeEntry>() - 8] = [];
const _NODE_VALUE : [(); std::mem::size_of::<NodeElement>() - 8] = [];

/// Represents a value state graph
///  of operations
pub struct OperationGraph<T> {
	
	/// Element Mapping: (May be no-op, in which case it is ignored)
	elements: Vec<T>,
	
	/// Element Mapping: element=0 for ALL VALUES!! (element != 0 is an error)
	values: ValueGraph<NodeElement, NodeElement>,
	
	/// State Mapping: (element = 0 ALWAYS)
	states: PartialOrderSet<NodeElement>,
}

impl<T> OperationGraph<T> {
	pub fn new() -> OperationGraph<T> {
		OperationGraph {
			elements: Vec::new(),
			values: ValueGraph::new(),
			states: PartialOrderSet::new()
		}
	}
	
	
	//TODO: Add All Operational Configuration
}

/*


//FIXME: Convert to dual u16,u16 (set,idx)
#[repr(transparent)]
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct EntryIdx(pub u16);

/// Represents the index of a input
///  or output of a node in the
///  value-state graph
impl EntryIdx {
	#[inline(always)]
	pub fn new(x: usize) -> EntryIdx {
		EntryIdx(x as u16)
	}
	#[inline(always)]
	pub fn get(&self) -> usize {
		self.0 as usize
	}
}

/// Represents a unique ID of a operation
///  node in the value-state graph
#[repr(transparent)]
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct NodeIdx(pub u32);

impl NodeIdx {
	#[inline(always)]
	pub fn new(x: usize) -> NodeIdx {
		NodeIdx(x as u32)
	}
	#[inline(always)]
	pub fn get(&self) -> usize {
		self.0 as usize
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct NodeEntry {
	pub node: NodeIdx,
	pub entry: EntryIdx,
}

/// Value dependence map
/// each value connection,
/// given by (node,idx,in/out)
/// is setup so that each unique
/// input port has exactly one
/// associated value
#[derive(Clone, Eq, PartialEq, Default, Debug)]
pub struct ValueDependenceMap {
	value_assigned_from: HashMap<NodeEntry,NodeEntry>,
	value_assigned_to: HashMap<NodeEntry,SmallVec<[NodeEntry;2]>>
}

impl ValueDependenceMap {
	pub fn new() -> ValueDependenceMap {
		ValueDependenceMap {
			value_assigned_from: HashMap::new(),
			value_assigned_to: HashMap::new()
		}
	}
	
	pub fn get_input_src(&self, input: NodeEntry) -> Option<NodeEntry> {
		self.value_assigned_from.get(&input).map(Clone::clone)
	}
	
	pub fn get_output_dst(&self, output: NodeEntry) -> &[NodeEntry] {
		self.value_assigned_to.get(&output).map_or(&[],SmallVec::as_slice)
	}
	
	pub fn get_output_dst_vec(&self, output: NodeEntry) -> SmallVec<[NodeEntry;2]> {
		self.get_output_dst(output).into_iter().map(Clone::clone).collect()
	}
	
	/// Creates a data dependency from [output] to [input]
	pub fn set_dependency(&mut self, input: NodeEntry, output: NodeEntry) {
		match self.value_assigned_from.entry(input) {
			Entry::Vacant(entry) => {
				entry.insert(output);
			},
			Entry::Occupied(mut entry) => {
				let old_output = entry.insert(output);
				Self::remove_occupied_output(&mut self.value_assigned_to,input,old_output);
			}
		}
		
		//Update other dependency
		self.value_assigned_to.entry(output).or_default().push(input);
	}
	
	/// Remove the dependency for [input]
	///  returns true if the connection was removed
	///  returns false if the connection did not exist
	pub fn remove_dependency(&mut self, input: NodeEntry) -> bool {
		if let Entry::Occupied(entry) = self.value_assigned_from.entry(input) {
			let (_,output) = entry.remove_entry();
			Self::remove_occupied_output(&mut self.value_assigned_to,input,output);
			true
		}else{
			false
		}
	}
	
	fn remove_occupied_output(
		map: &mut HashMap<NodeEntry,SmallVec<[NodeEntry;2]>>,
		input: NodeEntry,
		output: NodeEntry
	) {
		match map.entry(output) {
			Entry::Occupied(mut entry) => {
				let vec = entry.get_mut();
				
				//Find old dependency - should exist
				let idx = vec.as_slice()
					.iter()
					.position(|&x| x == input)
					.unwrap();
				
				//Remove old dependency & clean-up
				vec.swap_remove(idx);
				if vec.is_empty() {
					entry.remove_entry();
				}
			}
			Entry::Vacant(_) => unreachable!(),
		}
	}
}

impl Serialize for ValueDependenceMap {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where
			S: Serializer {
		self.value_assigned_from.serialize(serializer)
	}
}

impl<'de> Deserialize<'de> for ValueDependenceMap {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
		where
			D: Deserializer<'de> {
		let raw_map = HashMap::deserialize(deserializer)?;
		let mut other_map = HashMap::new();
		for (k,v) in raw_map.iter() {
			let vec : &mut SmallVec<[NodeEntry;2]> = other_map.entry(*v).or_default();
			vec.push(*k);
		}
		Ok(ValueDependenceMap {
			value_assigned_from: raw_map,
			value_assigned_to: other_map
		})
	}
}


#[derive(Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct StateDependenceOutput {
	input: NodeIdx,
	input_idx: EntryIdx,
	pub node: NodeIdx,
	pub entry: EntryIdx,
}

impl StateDependenceOutput {
	#[inline(always)]
	pub fn as_node_entry(&self) -> NodeEntry {
		NodeEntry {
			node: self.node,
			entry: self.entry,
		}
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct StateDependenceInput {
	output: NodeIdx,
	output_idx: EntryIdx,
	pub node: NodeIdx,
	pub entry: EntryIdx,
}

impl StateDependenceInput {
	#[inline(always)]
	pub fn as_node_entry(&self) -> NodeEntry {
		NodeEntry {
			node: self.node,
			entry: self.entry,
		}
	}
}

/// State dependence map
/// each (node,idx,in/out)
/// is configured so that an
/// arbitrary number of nodes
/// have
#[derive(Clone, Eq, PartialEq, Default, Debug)]
pub struct StateDependenceMap {
	
	/// Set of (input,input_idx,output_output_idx)
	data_depends_on: BTreeSet<StateDependenceInput>,
	
	/// Set of (output,output_idx,input_input_idx)
	data_is_dependency: BTreeSet<StateDependenceOutput>,
}

impl StateDependenceMap {
	
	pub fn new() -> StateDependenceMap {
		StateDependenceMap {
			data_depends_on: BTreeSet::new(),
			data_is_dependency: BTreeSet::new(),
		}
	}
	
	/// Add a state dependency where
	///  [dependency] must be marked as completed before [entry]
	///  can be executed.
	pub fn add_dependency(&mut self, entry: NodeEntry, dependency: NodeEntry) {
		self.data_depends_on.insert(StateDependenceInput {
			output: dependency.node,
			output_idx: dependency.entry,
			node: entry.node,
			entry: entry.entry,
		});
		self.data_is_dependency.insert(StateDependenceOutput {
			input: entry.node,
			input_idx: entry.entry,
			node: dependency.node,
			entry: dependency.entry
		});
	}
	
	/// Remove a state dependency where
	///  [dependency] must be marked as completed before [entry]
	///  can be executed.
	pub fn remove_dependency(&mut self, entry: NodeEntry, dependency: NodeEntry) -> bool {
		self.data_depends_on.remove(&StateDependenceInput {
			output: dependency.node,
			output_idx: dependency.entry,
			node: entry.node,
			entry: entry.entry,
		});
		self.data_is_dependency.remove(&StateDependenceOutput {
			input: entry.node,
			input_idx: entry.entry,
			node: dependency.node,
			entry: dependency.entry
		})
	}
	
	/// Returns if [entry] depends on [dependency]
	///  i.e. if [entry] must be executed after [dependency]
	pub fn has_dependency(&self, entry: NodeEntry, dependency: NodeEntry) -> bool {
		
		//FIXME: Decide which internal btree to use
		let connection = StateDependenceInput {
			output: dependency.node,
			output_idx: dependency.entry,
			node: entry.node,
			entry: entry.entry,
		};
		self.data_depends_on.contains(&connection)
	}
	
	/// Get the list of values
	///  that depend on ([node], an output)
	/// i.e. get the list of values which must be executed
	///  before [node] can be executed
	pub fn get_depends_on(&self, node: NodeEntry) -> Range<StateDependenceInput> {
		let pre = StateDependenceInput {
			output: node.node,
			output_idx: node.entry,
			node: NodeIdx(0),
			entry: EntryIdx(0),
		};
		let post = StateDependenceInput {
			output: node.node,
			output_idx: EntryIdx(node.entry.0 + 1),
			node: NodeIdx(0),
			entry: EntryIdx(0),
		};
		self.data_depends_on.range(pre..post)
	}
	
	/// [get_depends_on] output converted to a SmallVec
	pub fn get_depends_on_vec(&self, node: NodeEntry) -> SmallVec<[NodeEntry;2]> {
		self.get_depends_on(node).map(StateDependenceInput::as_node_entry).collect()
	}
	
	/// Get the the list of values
	///  that ([node], an input) depends on
	/// i.e. get the list of values which [node]
	///  must be executed directly before they can be
	pub fn get_dependencies_of(&self, node: NodeEntry) -> Range<StateDependenceOutput> {
		let pre = StateDependenceOutput {
			input: node.node,
			input_idx: node.entry,
			node: NodeIdx(0),
			entry:EntryIdx(0),
		};
		let post = StateDependenceOutput {
			input: node.node,
			input_idx: EntryIdx(node.entry.0 + 1),
			node: NodeIdx(0),
			entry: EntryIdx(0),
		};
		self.data_is_dependency.range(pre..post)
	}
	
	/// [get_dependencies_of] output converted to a SmallVec
	pub fn get_dependencies_of_vec(&self, node: NodeEntry) -> SmallVec<[NodeEntry;2]> {
		self.get_dependencies_of(node).map(StateDependenceOutput::as_node_entry).collect()
	}
}

impl Serialize for StateDependenceMap {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where
			S: Serializer {
		self.data_depends_on.serialize(serializer)
	}
}

impl<'de> Deserialize<'de> for StateDependenceMap {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
		where
			D: Deserializer<'de> {
		let depend_on : BTreeSet<StateDependenceInput> = BTreeSet::deserialize(deserializer)?;
		let mut is_depend = BTreeSet::new();
		for value in depend_on.iter() {
			is_depend.insert(StateDependenceOutput {
				input: value.node,
				input_idx: value.entry,
				node: value.output,
				entry: value.output_idx
			});
		}
		
		Ok(StateDependenceMap {
			data_depends_on: depend_on,
			data_is_dependency: is_depend
		})
	}
}


*/
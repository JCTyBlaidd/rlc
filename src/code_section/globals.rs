use std::ops::{Index, IndexMut};
use crate::util::arena::{
	InvariantLifetime,
	MarkedCollection,
	MarkedIndex,
	CollectionInsertError
};

pub struct GlobalMap<'ctx>(MarkedCollection<'ctx,u32,GlobalDecl<'ctx>>);

impl<'ctx> GlobalMap<'ctx> {
	/// Safety: ensure that ctx will be referenced in a unique manner,
	///  static lifetimes are unsafe, they must be some unique
	///  lifetime
	pub unsafe fn new() -> GlobalMap<'ctx> {
		GlobalMap(MarkedCollection::new())
	}
	
	pub unsafe fn clear(&mut self) {
		self.0.clear();
	}
	
	pub fn add_obj(&mut self, data: GlobalDecl<'ctx>) -> Result<GlobalIdx<'ctx>,CollectionInsertError> {
		self.0.add_obj(data).map(GlobalIdx)
	}
}
impl<'ctx> Index<GlobalIdx<'ctx>> for GlobalMap<'ctx> {
	type Output = GlobalDecl<'ctx>;
	#[inline]
	fn index(&self, index: GlobalIdx<'ctx>) -> &GlobalDecl<'ctx> {
		self.0.index(index.0)
	}
}
impl<'ctx> IndexMut<GlobalIdx<'ctx>> for GlobalMap<'ctx> {
	#[inline]
	fn index_mut(&mut self, index: GlobalIdx<'ctx>) -> &mut GlobalDecl<'ctx> {
		self.0.index_mut(index.0)
	}
}

#[repr(transparent)]
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct GlobalIdx<'ctx>(MarkedIndex<'ctx,u32,GlobalDecl<'ctx>>);

pub struct GlobalDecl<'ctx> {
	pub read_only: bool,
	pub thread_local: bool, //TODO: optional data type and alias??
	//TODO: pub visibility: TODO
	pub global_data: GlobalData<'ctx>,
}

pub enum GlobalData<'ctx> {
	Repeated {
		amount: u64,
		data: Box<GlobalData<'ctx>>,
	},
	Sequence(Box<[GlobalData<'ctx>]>),
	Bytes(Box<[u8]>),
	GlobalPtr {
		index: GlobalIdx<'ctx>,
		offset: u64,
	},
	FunctionPtr {
		//TODO: IMPLMEMENT
	}
	//TODO: UNDEFINED
}

/*

//use super::SimpleDataType;
use hashbrown::HashMap;

#[repr(transparent)]
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Hash)]
pub struct GlobalId(pub u32);

#[derive(Serialize, Deserialize)]
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct GlobalDecls {
	pub global_map: HashMap<GlobalId,GlobalDef>,
}

//FIXME: GLOBALS SHOULD BE NAMED!!

#[derive(Serialize, Deserialize)]
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct GlobalDef {
	//pub global_type: SimpleDataType, /* CLAIM TO BE OF TYPE T: CHECK INVARIANTS? */
	pub global_name: String,
	pub global_data: Vec<u8>,
	
	//Offset + Value: Set to Pointer to
	// targeted value
	pub global_pointers: Vec<(u64,String)>
}

//Global[x]
//LogicalMem[x]
//HEAP
//STACK
//StackMem[x]

//@global_1254 => $32

*/
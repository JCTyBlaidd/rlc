use super::{IntType, TypeIdx, BasicType};
use crate::util::CollectionMap;
use crate::code_section::io::PrettyFmt;
use hashbrown::{HashSet,HashMap};
use std::{
	num::NonZeroU32,
	cmp::Ordering,
	fmt::{Display, Formatter, Result as FmtResult}
};

/// Identifier for a basic block in the control
///  flow graph that describes a known function.
#[repr(transparent)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BlockId(pub NonZeroU32);

impl Display for BlockId {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f,"#{}",self.0)
	}
}


/// Identifier for a node with evaluated value in
///  the function, this value is unique per function
///  and so is shared across basic-blocks.
#[repr(transparent)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NodeId(pub NonZeroU32);

impl Display for NodeId {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f,"${}",self.0)
	}
}

/// Identifier for a value produced by a node,
///  consists of a node-id as well as an index for the produced value
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ValueId(pub NodeId,pub u32);

impl Display for ValueId {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		if self.1 == 0 {
			self.0.fmt(f)
		}else{
			write!(f,"{}:{}",self.0,self.1)
		}
	}
}

/// Identifier for a loop structure within the function
///  can be used to identify the start and end blocks
///  within a loop, and if the loop terminates.
#[repr(transparent)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LoopId(NonZeroU32);

impl Display for LoopId {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f,"&{}",self.0)
	}
}



/// Describes the control flow of
///  a function, with associated value metadata,
///  basic blocks, operation nodes,
///  and loop metadata.
/// Also contains information on value and state
///  dependence for optimization purposes
pub struct ControlFlow {
	//FIXME: ALL PAIRWISE (grouped so (a,b,c) = same)
	//  common start nodes: (must exist)
	//  (allow for code motion between distantly related blocks)
	//  (also track alias information so valid code motion can be determined)
	//  {allows for the same metadata given bs rvsdg but with better representation}
	//  [also requires that looping nodes be track]
	//  {ensure canonicalised form (NO UNCONDITIONAL GOTO to loop nodes)}
	//   @remove goto - SRC_ARRAY -> [code.. GOTO DST] (dst is not loop => c?))
	//    ...
	//   MAP {OP => Code Block}
	//   CodeBlock => {Always Previous Blocks & Next Unambigious Merge Block for If/Switch/Unwind etc..}
	//   CodeBlock => {Variable Phi Nodes (Multi-Load), KeepAliveNodes(node))(reborrow unabiguous previous nodes) }
	//    PhiMerge(newID,[oldID,oldBlock])
	//    Forward(ID,PrevBlock,SrcBlock) (needed or just use mapped op => block with set of always previous nodes)
	//   aliasing
	//    PerBlock
	//   A =/= B inside iteration OR inside any iteration (how to represent) {need for vectorization and loop opts}
	//  SCOPE-INTERNAL & SCOPE-EXTERNAL OR BLOCK SET?
	//  set of back-edge targets representing each applicable loop initial state (no goto -unconditional backedge)
	//  for each loop consider variant and invariant relations
	//  logical-allocation: L
	//  L1 =/= L2
	//  L1 === L2 {merge not represent}
	//  L1 =?= L2
	//  (L1 u L2) =/= (L3 u L4) => (does not imply L1 =/= L3 since there could be additional rules unknown)
	//  (sparse bit matrix (grow size = 1,2,4,8,16,...))
	//  add @noalias(Lx,scope) {never alias any other node inside the scope} (or add as efficient sparse matrix)
	//  add derive-from relations as well
	//  (memory intensive)
	//  loop-(invariant/variant) {do we make this per loop?} alias information as well
	//  loc-to-int (escape logical location information)
	//  heap wildcard
	//  stack wildcard
	//  all wildcard
	//  ... (linear memory ideally) {some method of ensure non-insane memory usage blow-up (early skip?)}
	//  nlog(n) max
	// optimize rust read-only-const (recursive) and unique references
	
	
	// FIXME: IDEA FOR OPT: (Code Motion)
	//   for value dependent r = a + b
	//   try move evaluation as late as possible
	//   unless the value is used in both code paths
	//   in different ways, if only used in later merged
	//   location, then shift to there
	//  --
	//  -- Note: This assists optimization, since later evaluation can allow for additional
	//  --  numeric constraints to be derived in some circumstances
	//  -- But: There needs to be a method for moving them backwards if reg-alloc/code-motion
	//  --  deems it to be useful
	//  --
	//  ------RegAlloc/Linearization
	//   can then delay the evaluation earlier
	//   as and if required to minimize register usage
	//   and fallback to spilling at the latest,
	//  .....
	//  However this does not allow for code-motion
	//   for pointer loads / stores, so also store
	//   which abstract allocations are accessed
	//   by each basic block to allow for code motion
	//   of read writes as well, need proper treating
	//   each basic-block as a set of read/write/atomic/fence
	//   operations, therefore allowing for late allocation
	//  .....
	//  LATENCY + THROUGHPUT
	//  (call => result) (limit between invokations)
	//  {is forward of backwards generation better??}
	//  ....
	//  use forward, and try move calculation as early as possible
	//  store set of operations that can be materialized as soon as possible
	//  still need to consider accross fuction invokations and if/loop statements
	
	// FIXME: IDEA FOR PASSES (Pass Manager)
	//  Three Types:
	//   function local - easy for parallel [e.g. tidy]
	//   unordered global - no complex determinism [e.g. function-mark-unwind/no-return/no-recurse]
	//   ordered/complex global - required for deterministic inlining {in recursive inlining case}
	//       {could to a simple non-recursive inlininer first with error on any recursive pass}
	//       {and then mark all recursive inlining candidates for complex handling??}
	
	//FIXME: POINTER ANNOTATIONS FOR FUNCTIONS:
	//  inbounds / writeonly (both = return_value)
	//  inbounds = (never writes out of bounds of the pointer, (is single reference not an array))
	//  writeonly = does not read any values
	//  readonly = does not write any values {combine  readonly, writeonly to flags, _ignored}
	//  constant = does not mutate either via atomic mutation or otherwise for duration of pointer
	//  non_null = is not null
	//  dereferencable(x)  {like llvm merge with above}
	//  ....
	//  .IDEA: Ptr Lifetime-End Annotation, or something similar for use by allocators??
	//  ....
	//  RUST &ref = readonly + constant + inbounds + non_null + dereferencable(x)
	//  RUST &mut ref = inbounds + noalias(or equiv?) + non_null + dereferencable(x)
	//  RUST &UnsafeCell<> = inbounds + non_null + dereferencable(x)  {Option<x> wrapping remove non_null}
	//  RUTS *const/mut ptr = NONE (read + write)
	//  RUST-RETURN-WRITE-PTR = inbounds + writeonly + non_null
	//  ....
	//  is_ret_value(idx), or equiv, the argument is unconditionally returned
	//   maybe add equivalent for multi-return values??, or just treat as union for recommended
	//   return code??
	//  ....
	//  noalias or alias annotation equivalent?? (or just require appending by codegen?)
	//  .
	// ADDITIONAL METADATA:
	//  integer / float constraints for inputs(undef), outputs, and return-indirect-outputs
	
	// FIXME: PASSES
	//   if function is only called directly, (no indirect calls)
	//   ALT: for all direct calls only and contains arguments that
	//   are never used or called/
	
	
	//FIXME: NEW IDEA: Block Basic Start for Optimization Purposes: Delay Calculation as much
	// as possible (need to consider exponential duplication issues || Find method of updating)
	// values to allow for {let x = a+b; if a == 0 { [we know x == b] }} for this and other
	// complex cases.  {vertial evaluation of all remapped constants given new constraints
	//                  and advance if the op can be simplified with new knowledge?}
	// ..
	// ..
	// Degenerate into Simple Value-State-Dependence-Graph for Register Allocation?
	//  (or use virtual global value-state analysis) for moving stuff
	//  and consider merging values from previous duplication, if there are applicable
	// ..
	// ..
	// Register Allocation and Instruction Linearization: Virtual VSDG
	//  add valid movement edges where applicable, as well as sets of values that
	//  are known to be identical, so the old one does not need to be removed.
	// This is probably after conversion of pointer operations to simple maths
	//  and general simplification to VSDG:
	//  allow knowledge of *x = reg; .... ;  replace reg with *x (load x)
	//  for cache efficient cases: {stack allocation inside the function only!}
	// Then apply register colouring as well; with constraint solver
	//  {op; Vec<RegMovements>; op2}
	// Maybe add simplification passes after this?
	//  {recolour to remove movements}
	
	
	//FIXME: Loop Metadata for cache-awere (maybe polyhedral optimization)
	// Loop Transformations: https://www.youtube.com/watch?v=QpvZt9w-Jik
	//                       https://www.youtube.com/watch?v=TIimzQtZ6UA
	// ...
	// Add additional complexity of layers
	//  to allow for varying precision
	//  ensure there is an O(n) O1 Good Optimization Path
	//  as well as an O0, no optimization path
	
	
	//FIXME: FOR CRAY-STYLE-VAR-LEN-VECTORIZATION: SEE RISCV    {
	// 	note: risc-v has parseable metadata: on git - use that
	// }
	// --------------------
	//  #For support for CRAY vectors <risc-v esque vectors>
	//  # and packed simd fallback
	//  # and linear no-simd/vector fallback
	//  vectorization_plan {
	//      len: N, stride(N)? <usage pattern??> {e.g. allow all / even numbers / mix between in&out}
	//      in_ptr1: *a: stride(N*k), align(X);
	//      inout_ptr2: *b: stride(N), align(X);
	//      out_ptr: *c: stride(N), align(X);
	//  .
	//      invoke {
	//          #Only allow pure function calls: overwise cancel vectoziation
	//          #Or allow full control flow: But may limit vectorization potential
	//          #For the given architecture:
	//  .
	//        vec_ref<f32> = vec_load(in_ptr);
	//  .
	//          #convert ifs to masks
	//          #fallback if massive if inlining is too much
	//          # where vectoziation_plan compiles to simple
	//          # linear loop: Basically No Different ?
	//      }
	//  }
	//  .
	//  vec-order-stuff {
	//      {vec-loop-#1: Ordered/Unordered} [can reorder:
	//      //Fully unordered ==> no between iteration dependency, with no ordering requirements
	//      // can be fully converted: e.g. 2D memcpy
	//  }
	//  .
	//  .
	//  .
	//  loop-metadata: Add vectorization Op??  VEC_$££
	//  .
	//  --------------------------------------------------
	
	/// Collection Map of all nodes
	///  that could be deleted
	pub nodes: CollectionMap<()>,
	
	/// Mapping of index to basic block,
	///  values should only be None
	///  if the basic block has been deleted
	pub blocks: CollectionMap<Option<Box<BasicBlock>>>,
	
	pub loop_meta: CollectionMap<Option<Box<()>>>,
	
	pub value_meta: (),
	
	pub ptr_meta: (),
	
	pub value_dependencies: (),
	
	//FIXME: VALUE + STATE DEPENDENCIES
}


//FIXME: LOOP IDS: SOURCE NODES AND TERMINATION NODES (AND LOOPBACK NODES?)
//       each id corresponds to data and loop invariant alias information
//       (loop invariant numeric id not required?)

pub struct BasicBlock {
	
	/// Nodes used by a common predecessor for all predecessor nodes:
	///  all nodes should correspond to a block inside `common_previous`
	///  the nodes are remapped to a new id since the condition may have
	///  given them additional invariants: e.g. if a == 0 { a == 0 always }
	///  for optimization purposes
	pub remap_nodes: HashMap<NodeId,(NodeId,BlockId)>,
	
	/// Phi Nodes (nodes generated from a non-common predecessor)
	///  all phi nodes sources should be marked as `consume_nodes`
	///  all phi node source blocks should be `direct_previous`
	pub merge_nodes: HashMap<NodeId,HashMap<BlockId,NodeId>>,
	
	/// Nodes that are used by successor blocks,
	///  all nodes are assumed to be consumed and hence are not
	///  removed as unnecessary code
	pub consume_nodes: Vec<NodeId>,
	
	/// Id of the loop that this node is an initial node of
	///  this cannot be the start of two distinct loops since
	///  they can instead be represented as one loop with an if
	///  at the start logically
	pub loop_start: Option<LoopId>,
	
	/// Set of loops that this block is logically a part of the execution
	///  of, and hence can be executed multiple times during the evaluation
	///  of any of these loops. And can only be considered executing once
	///  for a single simultaneous iteration of these loops
	pub loops: HashSet<LoopId>,
	
	/// All basic blocks that directly come before this nodes execution
	///  and therefore the terminator must specify this block as a potential
	///  destination.
	/// The value is a flag if this is a back-edge for a loop: with known loop id
	pub direct_previous: HashMap<BlockId,Option<LoopId>>,
	
	/// All basic blocks that have been unconditionally visited before
	///  this nodes execution
	pub common_previous: HashSet<BlockId>,
	
	/// Set of all nodes that are present inside this basic-block of
	///  code.
	pub node_set: HashSet<NodeId>,
	
	//FIXME: ADD NEXT BLOCK THAT ALL NODES ARE GUARANTEED TO VISIT, (MAY BE NONE)
	
	//FIXME: (MAKE GLOBAL? VALUE?) partial_order & value dependencies
	//FIXME: Add Partial Order of nodes
	
	/// The terminator of this block
	///  this corresponds to the node idx given by
	pub terminator: Terminator,
}



#[derive(Clone, PartialEq, Eq, Hash)]
pub struct BlockTarget {
	pub block: BlockId,
	pub back_edge: Option<LoopId>,
}

impl Display for BlockTarget {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		if let Some(loop_id) = self.back_edge {
			write!(f,"{} !back-edge({})",self.block,loop_id)
		}else{
			write!(f,"{}",self.block)
		}
	}
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct InclusiveRange {
	pub start: u128,
	pub end: u128
}

impl Display for InclusiveRange {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		if self.start == self.end {
			write!(f,"{}",self.start)
		}else{
			write!(f,"{}..={}",self.start,self.end)
		}
	}
}

pub enum Terminator {
	
	/// Go to one of two different
	///  nodes depending on the
	///  value given by a condition  FIXME: Add Probabilities
	Branch {
		condition: ValueId,
		true_block: BlockTarget,
		false_block: BlockTarget
	},
	
	/// Go to a number of different
	///  branches depending on an integer
	///  value, supports integer exact, ranges
	///  and a default block target  FIXME: Add Probability: (MostLikely,Rest=[Common or Rare])
	Switch {
		int_type: IntType,
		int_value: ValueId,
		default_block: Option<BlockTarget>,
		blocks_maps: HashMap<InclusiveRange,BlockTarget>,
	},
	
	/// Invoke a function that might unwind, and one or more valid
	///  return pointers. All functions with no unwinding and a single
	///  return pointer should be converted to call instructions and
	///  inlined inside a basic-block
	Invoke {
		func_type: TypeIdx,  //FIXME: Invoke & InvokeIndirect
		func_decl: Option<String>,
		function_arguments: Vec<ValueId>,
		return_blocks: Vec<BlockTarget>,
		unwind_block: Option<BlockTarget>,
	},
	
	/// Finishes the execution of the function
	///  and returns the set of simple types
	///  to a known return_index for the function prototype
	Return {
		return_index: usize,
		return_values: Vec<(BasicType,ValueId)>,//BASIC-TYPE FIXME: Reference Instead
	},
	
	/// Finishes the execution of the function via the unwind
	///  path, FIXME: ADD ANY METADATA??  (ExceptionTypeId,ExceptionPtr??)
	Unwind {//FIXME: Allow or Ban or what unwind from non unwind sources??
		__exception__: (),//FIXME: IMPL (NOT REQUIRED?)
	},
	
	/// Executing this block is undefined behaviour
	Unreachable
}

impl Terminator {
	fn fmt_pretty(&self, mut fmt: PrettyFmt) -> FmtResult {
		match self {
			Terminator::Branch {
				condition,
				true_block,
				false_block
			} => {
				fmt.pad_and_write("branch {\n")?;
				fmt.pad_and_fmt(format_args!("\tcond: {}\n",condition))?;
				fmt.pad_and_fmt(format_args!("\ttrue: {}\n",true_block))?;
				fmt.pad_and_fmt(format_args!("\tfalse: {}\n",false_block))?;
				fmt.pad_and_write("}")
			},
			Terminator::Switch {
				int_type,
				int_value,
				default_block,
				blocks_maps,
			} => {
				//Ensure Deterministic Ordering
				let mut key_vec = blocks_maps.keys().collect::<Vec<_>>();
				key_vec.sort_by(|a,b| {
					match a.start.cmp(&b.start) {
						Ordering::Equal => a.end.cmp(&b.end),
						other => other
					}
				});
				
				fmt.pad_and_write("switch {\n")?;
				
				let mut inner_fmt = fmt.get_inc_padding();
				inner_fmt.pad_and_fmt(format_args!("type: {}\n",int_type))?;
				inner_fmt.pad_and_fmt(format_args!("value: {}\n",int_value))?;
				if let Some(def) = default_block {
					inner_fmt.pad_and_fmt(format_args!("default: {}\n",def))?;
				}
				inner_fmt.pad_and_write("\ttargets: {\n")?;
				
				let mut mapping_fmt = inner_fmt.get_inc_padding();
				for key in &key_vec {
					let value = &blocks_maps[key];
					mapping_fmt.pad_and_fmt(format_args!("\t{} => {}\n", key, value))?;
				}
				
				inner_fmt.pad_and_write("}\n")?;
				fmt.pad_and_write("}")
			},
			Terminator::Invoke {
				func_type,
				func_decl,
				return_blocks,
				unwind_block
			} => {
				/*
				writeln!(f,"invoke {{")?;
				writeln!(f,"\ttype: {}", func_type)?;
				if let Some(decl) = func_decl {
					writeln!(f,"\tdecl: @{}",decl)?;
				}
				writeln!(f,"\treturn: {{")?;
				for (idx,target) in return_blocks.iter().enumerate() {
					writeln!(f,"\t\t{} => {}",idx,target)?;
				}
				writeln!(f,"\t}}")?;
				if let Some(unwind) = unwind_block {
					writeln!(f,"\tunwind: {}",unwind)?;
				}
				writeln!(f,"}}")
				*/
				
				fmt.pad_and_write("invoke {\n")?;
				
				let mut inner_fmt = fmt.get_inc_padding();
				//FIXME: NEED TO ADD ARGUMENT SOURCES &&
				//  NEED TO CHANGE TYPE METADATA??
				
				fmt.pad_and_write("}")
			},
			Terminator::Return {
				return_index,
				return_values
			} => {
				fmt.pad_and_write("return {\n")?;
				
				let mut inner_fmt = fmt.get_inc_padding();
				inner_fmt.pad_and_fmt(format_args!("index: {}",return_index))?;
				inner_fmt.pad_and_write("values: {")?;
				
				let mut value_fmt = inner_fmt.get_inc_padding();
				for (idx,(r_type,r_src)) in return_values.iter().enumerate() {
					value_fmt.pad_and_fmt(format_args!("{} => ( {}, {} )", idx, r_type.into_adv_type(), r_src))?;
				}
				//FIXME: RETURN-TYPE-METADATA SHOULD BE SCOPE-GLOBAL AND THEREFORE NOT NEEDED
				
				inner_fmt.pad_and_write("}\n");
				fmt.pad_and_write("}")
			},
			Terminator::Unwind {
				__exception__
			} => {
				fmt.pad_and_write("unwind") //FIXME: ANY ADDITIONAL METADATA
			},
			Terminator::Unreachable => {
				fmt.pad_and_write("unreachable")
			},
		}
	}
}

impl Display for Terminator {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		self.fmt_pretty(PrettyFmt::new(f,0))
	}
}


/*

#1 = {
	remap: [32,54,2,54],
	merge: {
		 0 = [32 if #43, 54 if #54, 75 if #34]
	},
	consume: [23,53,7],
	direct_prev: [#43,#54,#34]
	common_prev: [#1,01]
	code: {
		$0 = phi u32
		$0 = remap $32   //this is the same node but with additional optimization metadata
		$1 = forward u32 $2;
		$1 = iadd i32 $4 $7 !43,
		$2 = iadd i32 $4 $7 !53
		$3 = return(#display terminator here?) #32 $2
	},
	terminator: if {
		cond: $32,
		true: #32 !backedge=1,
		false: #64
	} //FIXME: LOOP METADATA (NEED TO IDENTIFY POSSIBLE LOOPS)
	terminator: unreachable,
	terminator: switch { values w/ ranges, opt default }
	terminator: invoke { return or unwind (both maybe never so psuedo unreachable) }
	terminator: return { value }
	terminator: unwind { throw-value?? }
	//FIXME: ADD UNWIND-SWITCH & UNWIND PATH FLAGS?!
}



// for(i = 0 i < n; i++) {
//   ..code
// }
//
// #loop_init if (i >= n) goto #end else #loop
// #loop ..code.. goto #loop_init {BAD = UNCONDITIONAL GOTO BACK-EDGE}
//
// => (loop canonicalise)
//
// #loop_start if (n == 0) goto #end
// #loop ..code.. if (i < n) goto #loop else #end


@func123 {
	start = #32
    finish = #32 ?multiple?
}

*/
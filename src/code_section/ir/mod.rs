
/// Control Flow Nodes
pub mod control_flow;

/// Type Metadata
pub mod types;

///auto-generated enum
/// for abstract intermediate
/// format operations
pub mod nodes;

/// Constant values for
///  integers and floats
pub mod constants;

type LocalIdx = u32;

pub use self::control_flow::{
	SwitchProbability,
	ScopeTypeMeta,
	BranchMeta, SwitchMeta,
	LoopMeta,
	CallMeta, InvokeMeta
};
pub use self::types::{
	BasicType, AdvType,
	IntType, FloatType, PointerType,
	BasicType::FlagType, BasicType::ExceptionFlagType,
	TypeIdx
};
pub use self::nodes::{
	OpNode,
};
pub use self::constants::{
	FloatConstant, IntConstant
};

///FIXME: MAYBE REPLACE VS GRAPHS INSIDE CFG
pub mod flow_graph;

///Static Assert Type Sizes:
/// Prevent unexpected changes in memory usage
const _INT : [(); std::mem::size_of::<IntType>() - 2] = [];
const _FLOAT : [(); std::mem::size_of::<FloatType>() - 2] = [];
const _POINTER : [(); std::mem::size_of::<PointerType>() - 4] = [];
const _BASIC_TYPE : [(); std::mem::size_of::<BasicType>() - 8] = [];
const _NODE : [(); std::mem::size_of::<OpNode>() - 16] = [];

#[cold]
#[inline(never)]
fn invalid_set(set: u8) -> ! {
	panic!("Requested Value of Invalid Set: {}", set)
}

#[cold]
#[inline(never)]
fn invalid_idx(set: u8, idx: u16) -> ! {
	panic!("Requested Value of Invalid Index: {}, in Set: {}", idx, set)
}
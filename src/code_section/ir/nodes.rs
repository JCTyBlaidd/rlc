//! Auto-Generated Code: rlc_generator::ir
//!  do not modify directly
//!  edit generator instead

use std::num::NonZeroU8;
use std::fmt::{Display, Result as FmtResult, Formatter};
use crate::code_section::metadata::CompileMeta;
use super::{
	FloatType, IntType, PointerType,
	BasicType, FlagType,
	ScopeTypeMeta,
	BranchMeta, SwitchMeta, LoopMeta,
	CallMeta, InvokeMeta,
	LocalIdx,
	invalid_set, invalid_idx
};

pub enum OpNode {
	//Default Node
	OpNone,

	//Scope Nodes
	OpScopeInput,
	OpScopeOutput,

	//Section: IntOpsUnary
	OpIntBitNot(IntType),
	OpIntAbs(IntType),
	OpIntCount1(IntType),
	OpIntLead0(IntType),
	OpIntTrail0(IntType),
	OpIntByteSwap(IntType),
	OpIntBitRev(IntType),

	//Section: IntOpsBinary
	OpIntAdd(IntType),
	OpIntAddWrap(IntType),
	OpIntAddSSat(IntType),
	OpIntAddUSat(IntType),
	OpIntSub(IntType),
	OpIntSubWrap(IntType),
	OpIntSubSSat(IntType),
	OpIntSubUSat(IntType),
	OpIntMul(IntType),
	OpIntMulWrap(IntType),
	OpIntSMax(IntType),
	OpIntUMax(IntType),
	OpIntLShift(IntType),
	OpIntARShift(IntType),
	OpIntLRShift(IntType),
	OpIntRRotate(IntType),
	OpIntLRotate(IntType),
	OpIntAnd(IntType),
	OpIntOr(IntType),
	OpIntXor(IntType),

	//Section: IntOpsCompare
	OpIntEqual(NonZeroU8),
	OpIntNotEqual(NonZeroU8),
	OpIntSLessThan(NonZeroU8),
	OpIntSGreaterThan(NonZeroU8),
	OpIntULessThan(NonZeroU8),
	OpIntUGreaterThan(NonZeroU8),

	//Section: IntOpsChecked
	OpIntAddSOverflow(NonZeroU8),
	OpIntAddUOverflow(NonZeroU8),
	OpIntSubSOverflow(NonZeroU8),
	OpIntSubUOverflow(NonZeroU8),
	OpIntMulSOverflow(NonZeroU8),
	OpIntMulUOverflow(NonZeroU8),

	//Section: InOpsDivide
	OpIntSDivide(IntType),
	OpIntUDivide(IntType),

	//Section: InOpsToInt
	OpIntTruncate{
		src: NonZeroU8,
		dst: NonZeroU8,
	},
	OpIntZeroExt{
		src: NonZeroU8,
		dst: NonZeroU8,
	},
	OpIntSignExt{
		src: NonZeroU8,
		dst: NonZeroU8,
	},

	//Section: IntOpsToFloat
	OpSIntToFloat{
		src: NonZeroU8,
		dst: NonZeroU8,
		elements: NonZeroU8,
	},
	OpUIntToFloat{
		src: NonZeroU8,
		dst: NonZeroU8,
		elements: NonZeroU8,
	},

	//Section: IntOpsToFloatExact
	OpIntToFloatBits(NonZeroU8),

	//Section: FloatOpsToInt
	OpFloatToSInt{
		src: NonZeroU8,
		dst: NonZeroU8,
		elements: NonZeroU8,
	},
	OpFloatToUInt{
		src: NonZeroU8,
		dst: NonZeroU8,
		elements: NonZeroU8,
	},
	OpFloatToSIntSat{
		src: NonZeroU8,
		dst: NonZeroU8,
		elements: NonZeroU8,
	},
	OpFloatToUIntSat{
		src: NonZeroU8,
		dst: NonZeroU8,
		elements: NonZeroU8,
	},

	//Section: FloatOpsToIntExact
	OpFloatToIntBits(NonZeroU8),

	//Section: FlagOpsToInt
	OpFlagToInt(NonZeroU8),

	//Section: FloatOpsUnary
	OpFloatNeg(FloatType),
	OpFloatAbs(FloatType),

	//Section: FloatOpsBinary
	OpFloatAdd(FloatType),
	OpFloatSub(FloatType),
	OpFloatMul(FloatType),
	OpFloatDiv(FloatType),
	OpFloatRem(FloatType),
	OpFloatMin(FloatType),
	OpFloatMax(FloatType),

	//Section: FloatOpsCompare
	OpFloatEqual(NonZeroU8),
	OpFloatNotEqual(NonZeroU8),
	OpFloatLessThan(NonZeroU8),
	OpFloatGreaterThan(NonZeroU8),

	//Section: FloatOpsToFloat
	OpFloatTruncate{
		src: NonZeroU8,
		dst: NonZeroU8,
	},
	OpFloatZeroExt{
		src: NonZeroU8,
		dst: NonZeroU8,
	},

	//Section: LocalPointerOps
	OpAllocStack(PointerType),

	//Section: GetPointerElemOps
	OpGetStruct{
		ptr: PointerType,
		index: u16,
	},
	OpGetStructNoWrap{
		ptr: PointerType,
		index: u16,
	},
	OpGetStructInbounds{
		ptr: PointerType,
		index: u16,
	},

	//Section: GetPointerArrayOps
	OpGetArray(PointerType),
	OpGetArrayNoWrap(PointerType),
	OpGetArrayInbounds(PointerType),

	//Section: GetPointerUnionOps
	OpUnionSpec(PointerType),
	OpUnionSpecInbounds(PointerType),

	//Section: PointerArithOps
	PointerSubtract(PointerType),

	//Section: LoadOps
	OpPtrLoad(PointerType),
	OpPtrLoadUnAlign(PointerType),
	OpPtrLoadVolatile(PointerType),
	OpPtrLoadVolatileUnAlign(PointerType),

	//Section: AtomicLoadOps
	OpAtomicLoadRelaxed(PointerType),
	OpAtomicLoadAcquire(PointerType),
	OpAtomicLoadSeqCst(PointerType),

	//Section: StoreOps
	OpPtrStore(PointerType),
	OpPtrStoreUnAlign(PointerType),
	OpPtrStoreVolatile(PointerType),
	OpPtrStoreVolatileUnAlign(PointerType),

	//Section: AtomicStoreOps
	OpAtomicStoreRelaxed(PointerType),
	OpAtomicStoreRelease(PointerType),
	OpAtomicStoreSeqCst(PointerType),

	//Section: AtomicCompareSwapOps

	//Section: AtomicHints

	//Section: DebugNodes
	OpBlackBox,

	//Section: AssumeNode
	OpAssumeTrue,

	//Section: LifetimeNode
	OpLifetimeStart(PointerType),
	OpLifetimeFinish(PointerType),

	//Section: Control Flow
	BranchNode(Box<BranchMeta>),
	SwitchNode(Box<SwitchMeta>),
	LoopNode(Box<LoopMeta>),
	CallNode(Box<CallMeta>),
	InvokeNode(Box<InvokeMeta>),
}

impl OpNode {

	pub fn input_sets(&self, scope: &dyn ScopeTypeMeta) -> u8 {
		use OpNode::*;
		match self {
			OpNone => 0,
			OpScopeInput => 0,
			OpScopeOutput => scope.scope_output_sets(),
			OpIntBitNot(_)
			| OpIntAbs(_)
			| OpIntCount1(_)
			| OpIntLead0(_)
			| OpIntTrail0(_)
			| OpIntByteSwap(_)
			| OpIntBitRev(_) => 1,

			OpIntAdd(_)
			| OpIntAddWrap(_)
			| OpIntAddSSat(_)
			| OpIntAddUSat(_)
			| OpIntSub(_)
			| OpIntSubWrap(_)
			| OpIntSubSSat(_)
			| OpIntSubUSat(_)
			| OpIntMul(_)
			| OpIntMulWrap(_)
			| OpIntSMax(_)
			| OpIntUMax(_)
			| OpIntLShift(_)
			| OpIntARShift(_)
			| OpIntLRShift(_)
			| OpIntRRotate(_)
			| OpIntLRotate(_)
			| OpIntAnd(_)
			| OpIntOr(_)
			| OpIntXor(_) => 1,

			OpIntEqual(_)
			| OpIntNotEqual(_)
			| OpIntSLessThan(_)
			| OpIntSGreaterThan(_)
			| OpIntULessThan(_)
			| OpIntUGreaterThan(_) => 1,

			OpIntAddSOverflow(_)
			| OpIntAddUOverflow(_)
			| OpIntSubSOverflow(_)
			| OpIntSubUOverflow(_)
			| OpIntMulSOverflow(_)
			| OpIntMulUOverflow(_) => 1,

			OpIntSDivide(_)
			| OpIntUDivide(_) => 1,

			OpIntTruncate{ .. }
			| OpIntZeroExt{ .. }
			| OpIntSignExt{ .. } => 1,

			OpSIntToFloat{ .. }
			| OpUIntToFloat{ .. } => 1,

			OpIntToFloatBits(_) => 1,

			OpFloatToSInt{ .. }
			| OpFloatToUInt{ .. }
			| OpFloatToSIntSat{ .. }
			| OpFloatToUIntSat{ .. } => 1,

			OpFloatToIntBits(_) => 1,

			OpFlagToInt(_) => 1,

			OpFloatNeg(_)
			| OpFloatAbs(_) => 1,

			OpFloatAdd(_)
			| OpFloatSub(_)
			| OpFloatMul(_)
			| OpFloatDiv(_)
			| OpFloatRem(_)
			| OpFloatMin(_)
			| OpFloatMax(_) => 1,

			OpFloatEqual(_)
			| OpFloatNotEqual(_)
			| OpFloatLessThan(_)
			| OpFloatGreaterThan(_) => 1,

			OpFloatTruncate{ .. }
			| OpFloatZeroExt{ .. } => 1,

			OpAllocStack(_) => 0,

			OpGetStruct{ .. }
			| OpGetStructNoWrap{ .. }
			| OpGetStructInbounds{ .. } => 1,

			OpGetArray(_)
			| OpGetArrayNoWrap(_)
			| OpGetArrayInbounds(_) => 1,

			OpUnionSpec(_)
			| OpUnionSpecInbounds(_) => 1,

			PointerSubtract(_) => 1,

			OpPtrLoad(_)
			| OpPtrLoadUnAlign(_)
			| OpPtrLoadVolatile(_)
			| OpPtrLoadVolatileUnAlign(_) => 1,

			OpAtomicLoadRelaxed(_)
			| OpAtomicLoadAcquire(_)
			| OpAtomicLoadSeqCst(_) => 1,

			OpPtrStore(_)
			| OpPtrStoreUnAlign(_)
			| OpPtrStoreVolatile(_)
			| OpPtrStoreVolatileUnAlign(_) => 1,

			OpAtomicStoreRelaxed(_)
			| OpAtomicStoreRelease(_)
			| OpAtomicStoreSeqCst(_) => 1,



			OpBlackBox => 0,

			OpAssumeTrue => 1,

			OpLifetimeStart(_)
			| OpLifetimeFinish(_) => 1,

			BranchNode(data) => data.node_input_sets(),
			SwitchNode(data) => data.node_input_sets(),
			LoopNode(data) => data.node_input_sets(),
			CallNode(data) => data.node_input_sets(),
			InvokeNode(data) => data.node_input_sets(),
		}
	}
	pub fn output_sets(&self, scope: &dyn ScopeTypeMeta) -> u8 {
		use OpNode::*;
		match self {
			OpNone => 0,
			OpScopeInput => scope.scope_input_sets(),
			OpScopeOutput => 0,
			OpIntBitNot(_)
			| OpIntAbs(_)
			| OpIntCount1(_)
			| OpIntLead0(_)
			| OpIntTrail0(_)
			| OpIntByteSwap(_)
			| OpIntBitRev(_) => 1,

			OpIntAdd(_)
			| OpIntAddWrap(_)
			| OpIntAddSSat(_)
			| OpIntAddUSat(_)
			| OpIntSub(_)
			| OpIntSubWrap(_)
			| OpIntSubSSat(_)
			| OpIntSubUSat(_)
			| OpIntMul(_)
			| OpIntMulWrap(_)
			| OpIntSMax(_)
			| OpIntUMax(_)
			| OpIntLShift(_)
			| OpIntARShift(_)
			| OpIntLRShift(_)
			| OpIntRRotate(_)
			| OpIntLRotate(_)
			| OpIntAnd(_)
			| OpIntOr(_)
			| OpIntXor(_) => 1,

			OpIntEqual(_)
			| OpIntNotEqual(_)
			| OpIntSLessThan(_)
			| OpIntSGreaterThan(_)
			| OpIntULessThan(_)
			| OpIntUGreaterThan(_) => 1,

			OpIntAddSOverflow(_)
			| OpIntAddUOverflow(_)
			| OpIntSubSOverflow(_)
			| OpIntSubUOverflow(_)
			| OpIntMulSOverflow(_)
			| OpIntMulUOverflow(_) => 1,

			OpIntSDivide(_)
			| OpIntUDivide(_) => 1,

			OpIntTruncate{ .. }
			| OpIntZeroExt{ .. }
			| OpIntSignExt{ .. } => 1,

			OpSIntToFloat{ .. }
			| OpUIntToFloat{ .. } => 1,

			OpIntToFloatBits(_) => 1,

			OpFloatToSInt{ .. }
			| OpFloatToUInt{ .. }
			| OpFloatToSIntSat{ .. }
			| OpFloatToUIntSat{ .. } => 1,

			OpFloatToIntBits(_) => 1,

			OpFlagToInt(_) => 1,

			OpFloatNeg(_)
			| OpFloatAbs(_) => 1,

			OpFloatAdd(_)
			| OpFloatSub(_)
			| OpFloatMul(_)
			| OpFloatDiv(_)
			| OpFloatRem(_)
			| OpFloatMin(_)
			| OpFloatMax(_) => 1,

			OpFloatEqual(_)
			| OpFloatNotEqual(_)
			| OpFloatLessThan(_)
			| OpFloatGreaterThan(_) => 1,

			OpFloatTruncate{ .. }
			| OpFloatZeroExt{ .. } => 1,

			OpAllocStack(_) => 1,

			OpGetStruct{ .. }
			| OpGetStructNoWrap{ .. }
			| OpGetStructInbounds{ .. } => 1,

			OpGetArray(_)
			| OpGetArrayNoWrap(_)
			| OpGetArrayInbounds(_) => 1,

			OpUnionSpec(_)
			| OpUnionSpecInbounds(_) => 1,

			PointerSubtract(_) => 1,

			OpPtrLoad(_)
			| OpPtrLoadUnAlign(_)
			| OpPtrLoadVolatile(_)
			| OpPtrLoadVolatileUnAlign(_) => 1,

			OpAtomicLoadRelaxed(_)
			| OpAtomicLoadAcquire(_)
			| OpAtomicLoadSeqCst(_) => 1,

			OpPtrStore(_)
			| OpPtrStoreUnAlign(_)
			| OpPtrStoreVolatile(_)
			| OpPtrStoreVolatileUnAlign(_) => 0,

			OpAtomicStoreRelaxed(_)
			| OpAtomicStoreRelease(_)
			| OpAtomicStoreSeqCst(_) => 0,



			OpBlackBox => 0,

			OpAssumeTrue => 0,

			OpLifetimeStart(_)
			| OpLifetimeFinish(_) => 0,

			BranchNode(data) => data.node_output_sets(),
			SwitchNode(data) => data.node_output_sets(),
			LoopNode(data) => data.node_output_sets(),
			CallNode(data) => data.node_output_sets(),
			InvokeNode(data) => data.node_output_sets(),
		}
	}
	pub fn input_count(&self, set: u8, scope: &dyn ScopeTypeMeta) -> u16 {
		use OpNode::*;
		match self {
			OpNone => invalid_set(set),
			OpScopeInput => invalid_set(set),
			OpScopeOutput => scope.scope_output_count(set),
			OpIntBitNot(_)
			| OpIntAbs(_)
			| OpIntCount1(_)
			| OpIntLead0(_)
			| OpIntTrail0(_)
			| OpIntByteSwap(_)
			| OpIntBitRev(_) => if set == 0 { 1} else { invalid_set(set) },

			OpIntAdd(_)
			| OpIntAddWrap(_)
			| OpIntAddSSat(_)
			| OpIntAddUSat(_)
			| OpIntSub(_)
			| OpIntSubWrap(_)
			| OpIntSubSSat(_)
			| OpIntSubUSat(_)
			| OpIntMul(_)
			| OpIntMulWrap(_)
			| OpIntSMax(_)
			| OpIntUMax(_)
			| OpIntLShift(_)
			| OpIntARShift(_)
			| OpIntLRShift(_)
			| OpIntRRotate(_)
			| OpIntLRotate(_)
			| OpIntAnd(_)
			| OpIntOr(_)
			| OpIntXor(_) => if set == 0 { 2} else { invalid_set(set) },

			OpIntEqual(_)
			| OpIntNotEqual(_)
			| OpIntSLessThan(_)
			| OpIntSGreaterThan(_)
			| OpIntULessThan(_)
			| OpIntUGreaterThan(_) => if set == 0 { 2} else { invalid_set(set) },

			OpIntAddSOverflow(_)
			| OpIntAddUOverflow(_)
			| OpIntSubSOverflow(_)
			| OpIntSubUOverflow(_)
			| OpIntMulSOverflow(_)
			| OpIntMulUOverflow(_) => if set == 0 { 2} else { invalid_set(set) },

			OpIntSDivide(_)
			| OpIntUDivide(_) => if set == 0 { 2} else { invalid_set(set) },

			OpIntTruncate{ .. }
			| OpIntZeroExt{ .. }
			| OpIntSignExt{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpSIntToFloat{ .. }
			| OpUIntToFloat{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpIntToFloatBits(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatToSInt{ .. }
			| OpFloatToUInt{ .. }
			| OpFloatToSIntSat{ .. }
			| OpFloatToUIntSat{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpFloatToIntBits(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFlagToInt(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatNeg(_)
			| OpFloatAbs(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatAdd(_)
			| OpFloatSub(_)
			| OpFloatMul(_)
			| OpFloatDiv(_)
			| OpFloatRem(_)
			| OpFloatMin(_)
			| OpFloatMax(_) => if set == 0 { 2} else { invalid_set(set) },

			OpFloatEqual(_)
			| OpFloatNotEqual(_)
			| OpFloatLessThan(_)
			| OpFloatGreaterThan(_) => if set == 0 { 2} else { invalid_set(set) },

			OpFloatTruncate{ .. }
			| OpFloatZeroExt{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpAllocStack(_) => invalid_set(set),

			OpGetStruct{ .. }
			| OpGetStructNoWrap{ .. }
			| OpGetStructInbounds{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpGetArray(_)
			| OpGetArrayNoWrap(_)
			| OpGetArrayInbounds(_) => if set == 0 { 2} else { invalid_set(set) },

			OpUnionSpec(_)
			| OpUnionSpecInbounds(_) => if set == 0 { 1} else { invalid_set(set) },

			PointerSubtract(_) => if set == 0 { 2} else { invalid_set(set) },

			OpPtrLoad(_)
			| OpPtrLoadUnAlign(_)
			| OpPtrLoadVolatile(_)
			| OpPtrLoadVolatileUnAlign(_) => if set == 0 { 1} else { invalid_set(set) },

			OpAtomicLoadRelaxed(_)
			| OpAtomicLoadAcquire(_)
			| OpAtomicLoadSeqCst(_) => if set == 0 { 1} else { invalid_set(set) },

			OpPtrStore(_)
			| OpPtrStoreUnAlign(_)
			| OpPtrStoreVolatile(_)
			| OpPtrStoreVolatileUnAlign(_) => if set == 0 { 2} else { invalid_set(set) },

			OpAtomicStoreRelaxed(_)
			| OpAtomicStoreRelease(_)
			| OpAtomicStoreSeqCst(_) => if set == 0 { 2} else { invalid_set(set) },



			OpBlackBox => invalid_set(set),

			OpAssumeTrue => if set == 0 { 1} else { invalid_set(set) },

			OpLifetimeStart(_)
			| OpLifetimeFinish(_) => if set == 0 { 1} else { invalid_set(set) },

			BranchNode(data) => data.node_input_count(set),
			SwitchNode(data) => data.node_input_count(set),
			LoopNode(data) => data.node_input_count(set),
			CallNode(data) => data.node_input_count(set),
			InvokeNode(data) => data.node_input_count(set),
		}
	}
	pub fn output_count(&self, set: u8, scope: &dyn ScopeTypeMeta) -> u16 {
		use OpNode::*;
		match self {
			OpNone => invalid_set(set),
			OpScopeInput => scope.scope_input_count(set),
			OpScopeOutput => invalid_set(set),
			OpIntBitNot(_)
			| OpIntAbs(_)
			| OpIntCount1(_)
			| OpIntLead0(_)
			| OpIntTrail0(_)
			| OpIntByteSwap(_)
			| OpIntBitRev(_) => if set == 0 { 1} else { invalid_set(set) },

			OpIntAdd(_)
			| OpIntAddWrap(_)
			| OpIntAddSSat(_)
			| OpIntAddUSat(_)
			| OpIntSub(_)
			| OpIntSubWrap(_)
			| OpIntSubSSat(_)
			| OpIntSubUSat(_)
			| OpIntMul(_)
			| OpIntMulWrap(_)
			| OpIntSMax(_)
			| OpIntUMax(_)
			| OpIntLShift(_)
			| OpIntARShift(_)
			| OpIntLRShift(_)
			| OpIntRRotate(_)
			| OpIntLRotate(_)
			| OpIntAnd(_)
			| OpIntOr(_)
			| OpIntXor(_) => if set == 0 { 1} else { invalid_set(set) },

			OpIntEqual(_)
			| OpIntNotEqual(_)
			| OpIntSLessThan(_)
			| OpIntSGreaterThan(_)
			| OpIntULessThan(_)
			| OpIntUGreaterThan(_) => if set == 0 { 1} else { invalid_set(set) },

			OpIntAddSOverflow(_)
			| OpIntAddUOverflow(_)
			| OpIntSubSOverflow(_)
			| OpIntSubUOverflow(_)
			| OpIntMulSOverflow(_)
			| OpIntMulUOverflow(_) => if set == 0 { 2} else { invalid_set(set) },

			OpIntSDivide(_)
			| OpIntUDivide(_) => if set == 0 { 2} else { invalid_set(set) },

			OpIntTruncate{ .. }
			| OpIntZeroExt{ .. }
			| OpIntSignExt{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpSIntToFloat{ .. }
			| OpUIntToFloat{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpIntToFloatBits(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatToSInt{ .. }
			| OpFloatToUInt{ .. }
			| OpFloatToSIntSat{ .. }
			| OpFloatToUIntSat{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpFloatToIntBits(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFlagToInt(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatNeg(_)
			| OpFloatAbs(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatAdd(_)
			| OpFloatSub(_)
			| OpFloatMul(_)
			| OpFloatDiv(_)
			| OpFloatRem(_)
			| OpFloatMin(_)
			| OpFloatMax(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatEqual(_)
			| OpFloatNotEqual(_)
			| OpFloatLessThan(_)
			| OpFloatGreaterThan(_) => if set == 0 { 1} else { invalid_set(set) },

			OpFloatTruncate{ .. }
			| OpFloatZeroExt{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpAllocStack(_) => if set == 0 { 1} else { invalid_set(set) },

			OpGetStruct{ .. }
			| OpGetStructNoWrap{ .. }
			| OpGetStructInbounds{ .. } => if set == 0 { 1} else { invalid_set(set) },

			OpGetArray(_)
			| OpGetArrayNoWrap(_)
			| OpGetArrayInbounds(_) => if set == 0 { 1} else { invalid_set(set) },

			OpUnionSpec(_)
			| OpUnionSpecInbounds(_) => if set == 0 { 1} else { invalid_set(set) },

			PointerSubtract(_) => if set == 0 { 1} else { invalid_set(set) },

			OpPtrLoad(_)
			| OpPtrLoadUnAlign(_)
			| OpPtrLoadVolatile(_)
			| OpPtrLoadVolatileUnAlign(_) => if set == 0 { 1} else { invalid_set(set) },

			OpAtomicLoadRelaxed(_)
			| OpAtomicLoadAcquire(_)
			| OpAtomicLoadSeqCst(_) => if set == 0 { 1} else { invalid_set(set) },

			OpPtrStore(_)
			| OpPtrStoreUnAlign(_)
			| OpPtrStoreVolatile(_)
			| OpPtrStoreVolatileUnAlign(_) => invalid_set(set),

			OpAtomicStoreRelaxed(_)
			| OpAtomicStoreRelease(_)
			| OpAtomicStoreSeqCst(_) => invalid_set(set),



			OpBlackBox => invalid_set(set),

			OpAssumeTrue => invalid_set(set),

			OpLifetimeStart(_)
			| OpLifetimeFinish(_) => invalid_set(set),

			BranchNode(data) => data.node_output_count(set),
			SwitchNode(data) => data.node_output_count(set),
			LoopNode(data) => data.node_output_count(set),
			CallNode(data) => data.node_output_count(set),
			InvokeNode(data) => data.node_output_count(set),
		}
	}
	pub fn input_type(&self, set: u8, idx: u16, scope: &dyn ScopeTypeMeta, meta: &CompileMeta) -> BasicType {
		use OpNode::*;
		match self {
			OpNone => invalid_set(set),
			OpScopeInput => invalid_set(set),
			OpScopeOutput => scope.scope_output_type(set,idx),
			OpIntBitNot(data)
			| OpIntAbs(data)
			| OpIntCount1(data)
			| OpIntLead0(data)
			| OpIntTrail0(data)
			| OpIntByteSwap(data)
			| OpIntBitRev(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntAdd(data)
			| OpIntAddWrap(data)
			| OpIntAddSSat(data)
			| OpIntAddUSat(data)
			| OpIntSub(data)
			| OpIntSubWrap(data)
			| OpIntSubSSat(data)
			| OpIntSubUSat(data)
			| OpIntMul(data)
			| OpIntMulWrap(data)
			| OpIntSMax(data)
			| OpIntUMax(data)
			| OpIntLShift(data)
			| OpIntARShift(data)
			| OpIntLRShift(data)
			| OpIntRRotate(data)
			| OpIntLRotate(data)
			| OpIntAnd(data)
			| OpIntOr(data)
			| OpIntXor(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntEqual(data)
			| OpIntNotEqual(data)
			| OpIntSLessThan(data)
			| OpIntSGreaterThan(data)
			| OpIntULessThan(data)
			| OpIntUGreaterThan(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					IntType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntAddSOverflow(data)
			| OpIntAddUOverflow(data)
			| OpIntSubSOverflow(data)
			| OpIntSubUOverflow(data)
			| OpIntMulSOverflow(data)
			| OpIntMulUOverflow(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					IntType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntSDivide(data)
			| OpIntUDivide(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntTruncate{
				src,
				dst,
			}
			| OpIntZeroExt{
				src,
				dst,
			}
			| OpIntSignExt{
				src,
				dst,
			} => if set == 0 {
				if idx == 0 {
					IntType::from_size(*src).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpSIntToFloat{
				src,
				dst,
				elements,
			}
			| OpUIntToFloat{
				src,
				dst,
				elements,
			} => if set == 0 {
				if idx == 0 {
					IntType::new(*src,*elements).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntToFloatBits(data) => if set == 0 {
				if idx == 0 {
					IntType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatToSInt{
				src,
				dst,
				elements,
			}
			| OpFloatToUInt{
				src,
				dst,
				elements,
			}
			| OpFloatToSIntSat{
				src,
				dst,
				elements,
			}
			| OpFloatToUIntSat{
				src,
				dst,
				elements,
			} => if set == 0 {
				if idx == 0 {
					FloatType::new(*src,*elements).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatToIntBits(data) => if set == 0 {
				if idx == 0 {
					FloatType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFlagToInt(data) => if set == 0 {
				if idx == 0 {
					FlagType
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatNeg(data)
			| OpFloatAbs(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatAdd(data)
			| OpFloatSub(data)
			| OpFloatMul(data)
			| OpFloatDiv(data)
			| OpFloatRem(data)
			| OpFloatMin(data)
			| OpFloatMax(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatEqual(data)
			| OpFloatNotEqual(data)
			| OpFloatLessThan(data)
			| OpFloatGreaterThan(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					FloatType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatTruncate{
				src,
				dst,
			}
			| OpFloatZeroExt{
				src,
				dst,
			} => if set == 0 {
				if idx == 0 {
					FloatType::from_size(*src).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpAllocStack(data) => invalid_set(set),

			OpGetStruct{
				ptr,
				index,
			}
			| OpGetStructNoWrap{
				ptr,
				index,
			}
			| OpGetStructInbounds{
				ptr,
				index,
			} => if set == 0 {
				if idx == 0 {
					ptr.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpGetArray(data)
			| OpGetArrayNoWrap(data)
			| OpGetArrayInbounds(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else if idx == 1 {
					meta.get_ptr_int().into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpUnionSpec(data)
			| OpUnionSpecInbounds(data) => if set == 0 {
				if idx == 0 {
					unimplemented!("opaque_ptr")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			PointerSubtract(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpPtrLoad(data)
			| OpPtrLoadUnAlign(data)
			| OpPtrLoadVolatile(data)
			| OpPtrLoadVolatileUnAlign(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpAtomicLoadRelaxed(data)
			| OpAtomicLoadAcquire(data)
			| OpAtomicLoadSeqCst(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpPtrStore(data)
			| OpPtrStoreUnAlign(data)
			| OpPtrStoreVolatile(data)
			| OpPtrStoreVolatileUnAlign(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else if idx == 1 {
					unimplemented!("ptr_contents")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpAtomicStoreRelaxed(data)
			| OpAtomicStoreRelease(data)
			| OpAtomicStoreSeqCst(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else if idx == 1 {
					unimplemented!("ptr_contents")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},



			OpBlackBox => invalid_set(set),

			OpAssumeTrue => if set == 0 {
				if idx == 0 {
					FlagType
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpLifetimeStart(data)
			| OpLifetimeFinish(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			BranchNode(data) => data.node_input_type(set,idx),
			SwitchNode(data) => data.node_input_type(set,idx),
			LoopNode(data) => data.node_input_type(set,idx),
			CallNode(data) => data.node_input_type(set,idx),
			InvokeNode(data) => data.node_input_type(set,idx),
		}
	}
	pub fn output_type(&self, set: u8, idx: u16, scope: &dyn ScopeTypeMeta, meta: &CompileMeta) -> BasicType {
		use OpNode::*;
		match self {
			OpNone => invalid_set(set),
			OpScopeInput => scope.scope_input_type(set,idx),
			OpScopeOutput => invalid_set(set),
			OpIntBitNot(data)
			| OpIntAbs(data)
			| OpIntCount1(data)
			| OpIntLead0(data)
			| OpIntTrail0(data)
			| OpIntByteSwap(data)
			| OpIntBitRev(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntAdd(data)
			| OpIntAddWrap(data)
			| OpIntAddSSat(data)
			| OpIntAddUSat(data)
			| OpIntSub(data)
			| OpIntSubWrap(data)
			| OpIntSubSSat(data)
			| OpIntSubUSat(data)
			| OpIntMul(data)
			| OpIntMulWrap(data)
			| OpIntSMax(data)
			| OpIntUMax(data)
			| OpIntLShift(data)
			| OpIntARShift(data)
			| OpIntLRShift(data)
			| OpIntRRotate(data)
			| OpIntLRotate(data)
			| OpIntAnd(data)
			| OpIntOr(data)
			| OpIntXor(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntEqual(data)
			| OpIntNotEqual(data)
			| OpIntSLessThan(data)
			| OpIntSGreaterThan(data)
			| OpIntULessThan(data)
			| OpIntUGreaterThan(data) => if set == 0 {
				if idx == 0 {
					FlagType
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntAddSOverflow(data)
			| OpIntAddUOverflow(data)
			| OpIntSubSOverflow(data)
			| OpIntSubUOverflow(data)
			| OpIntMulSOverflow(data)
			| OpIntMulUOverflow(data) => if set == 0 {
				if idx == 0 {
					IntType::from_size(*data).into_type()
				} else if idx == 1 {
					FlagType
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntSDivide(data)
			| OpIntUDivide(data) => if set == 0 {
				if idx == 0 || idx == 1 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntTruncate{
				src,
				dst,
			}
			| OpIntZeroExt{
				src,
				dst,
			}
			| OpIntSignExt{
				src,
				dst,
			} => if set == 0 {
				if idx == 0 {
					IntType::from_size(*dst).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpSIntToFloat{
				src,
				dst,
				elements,
			}
			| OpUIntToFloat{
				src,
				dst,
				elements,
			} => if set == 0 {
				if idx == 0 {
					FloatType::new(*dst,*elements).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpIntToFloatBits(data) => if set == 0 {
				if idx == 0 {
					IntType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatToSInt{
				src,
				dst,
				elements,
			}
			| OpFloatToUInt{
				src,
				dst,
				elements,
			}
			| OpFloatToSIntSat{
				src,
				dst,
				elements,
			}
			| OpFloatToUIntSat{
				src,
				dst,
				elements,
			} => if set == 0 {
				if idx == 0 {
					IntType::new(*dst,*elements).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatToIntBits(data) => if set == 0 {
				if idx == 0 {
					FloatType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFlagToInt(data) => if set == 0 {
				if idx == 0 {
					IntType::from_size(*data).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatNeg(data)
			| OpFloatAbs(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatAdd(data)
			| OpFloatSub(data)
			| OpFloatMul(data)
			| OpFloatDiv(data)
			| OpFloatRem(data)
			| OpFloatMin(data)
			| OpFloatMax(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatEqual(data)
			| OpFloatNotEqual(data)
			| OpFloatLessThan(data)
			| OpFloatGreaterThan(data) => if set == 0 {
				if idx == 0 {
					FlagType
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpFloatTruncate{
				src,
				dst,
			}
			| OpFloatZeroExt{
				src,
				dst,
			} => if set == 0 {
				if idx == 0 {
					FloatType::from_size(*dst).into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpAllocStack(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpGetStruct{
				ptr,
				index,
			}
			| OpGetStructNoWrap{
				ptr,
				index,
			}
			| OpGetStructInbounds{
				ptr,
				index,
			} => if set == 0 {
				if idx == 0 {
					unimplemented!("struct_elem")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpGetArray(data)
			| OpGetArrayNoWrap(data)
			| OpGetArrayInbounds(data) => if set == 0 {
				if idx == 0 {
					data.into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpUnionSpec(data)
			| OpUnionSpecInbounds(data) => if set == 0 {
				if idx == 0 {
					panic!("ptr_exact is ???")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			PointerSubtract(data) => if set == 0 {
				if idx == 0 {
					meta.get_ptr_int().into_type()
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpPtrLoad(data)
			| OpPtrLoadUnAlign(data)
			| OpPtrLoadVolatile(data)
			| OpPtrLoadVolatileUnAlign(data) => if set == 0 {
				if idx == 0 {
					unimplemented!("ptr_contents")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpAtomicLoadRelaxed(data)
			| OpAtomicLoadAcquire(data)
			| OpAtomicLoadSeqCst(data) => if set == 0 {
				if idx == 0 {
					unimplemented!("ptr_contents")
				} else {
					invalid_idx(set,idx)
				}
			} else {
				invalid_set(set)
			},

			OpPtrStore(data)
			| OpPtrStoreUnAlign(data)
			| OpPtrStoreVolatile(data)
			| OpPtrStoreVolatileUnAlign(data) => invalid_set(set),

			OpAtomicStoreRelaxed(data)
			| OpAtomicStoreRelease(data)
			| OpAtomicStoreSeqCst(data) => invalid_set(set),



			OpBlackBox => invalid_set(set),

			OpAssumeTrue => invalid_set(set),

			OpLifetimeStart(data)
			| OpLifetimeFinish(data) => invalid_set(set),

			BranchNode(data) => data.node_output_type(set,idx),
			SwitchNode(data) => data.node_output_type(set,idx),
			LoopNode(data) => data.node_output_type(set,idx),
			CallNode(data) => data.node_output_type(set,idx),
			InvokeNode(data) => data.node_output_type(set,idx),
		}
	}
}

impl Display for OpNode {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		use OpNode::*;
		match self {
			OpNone => write!(f,"noop"),
			OpScopeInput => write!(f,"scope.input"),
			OpScopeOutput => write!(f,"scope.output"),
			OpIntBitNot(data) => write!(f,"int.not {}",data),
			OpIntAbs(data) => write!(f,"int.abs {}",data),
			OpIntCount1(data) => write!(f,"int.ctpop {}",data),
			OpIntLead0(data) => write!(f,"int.ctlz {}",data),
			OpIntTrail0(data) => write!(f,"int.cttz {}",data),
			OpIntByteSwap(data) => write!(f,"int.bswap {}",data),
			OpIntBitRev(data) => write!(f,"int.bitreverse {}",data),

			OpIntAdd(data) => write!(f,"int.add {}",data),
			OpIntAddWrap(data) => write!(f,"int.add.wrap {}",data),
			OpIntAddSSat(data) => write!(f,"int.add.sat {}",data),
			OpIntAddUSat(data) => write!(f,"int.add.sat {}",data.fmt_unsigned()),
			OpIntSub(data) => write!(f,"int.sub {}",data),
			OpIntSubWrap(data) => write!(f,"int.sub.wrap {}",data),
			OpIntSubSSat(data) => write!(f,"int.sub.sat {}",data),
			OpIntSubUSat(data) => write!(f,"int.sub.sat {}",data.fmt_unsigned()),
			OpIntMul(data) => write!(f,"int.mul {}",data),
			OpIntMulWrap(data) => write!(f,"int.mul.wrap {}",data),
			OpIntSMax(data) => write!(f,"int.max {}",data),
			OpIntUMax(data) => write!(f,"int.max {}",data.fmt_unsigned()),
			OpIntLShift(data) => write!(f,"int.shl {}",data),
			OpIntARShift(data) => write!(f,"int.ashr {}",data),
			OpIntLRShift(data) => write!(f,"int.lshr {}",data),
			OpIntRRotate(data) => write!(f,"int.rotr {}",data),
			OpIntLRotate(data) => write!(f,"int.rotl {}",data),
			OpIntAnd(data) => write!(f,"int.and {}",data),
			OpIntOr(data) => write!(f,"int.sub {}",data),
			OpIntXor(data) => write!(f,"int.xor {}",data),

			OpIntEqual(data) => write!(f,"int.eq i{}",data),
			OpIntNotEqual(data) => write!(f,"int.neq i{}",data),
			OpIntSLessThan(data) => write!(f,"int.lt i{}",data),
			OpIntSGreaterThan(data) => write!(f,"int.gt i{}",data),
			OpIntULessThan(data) => write!(f,"int.lt u{}",data),
			OpIntUGreaterThan(data) => write!(f,"int.gt u{}",data),

			OpIntAddSOverflow(data) => write!(f,"int.add.over i{}",data),
			OpIntAddUOverflow(data) => write!(f,"int.add.over u{}",data),
			OpIntSubSOverflow(data) => write!(f,"int.sub.over i{}",data),
			OpIntSubUOverflow(data) => write!(f,"int.sub.over u{}",data),
			OpIntMulSOverflow(data) => write!(f,"int.mul.over i{}",data),
			OpIntMulUOverflow(data) => write!(f,"int.mul.over u{}",data),

			OpIntSDivide(data) => write!(f,"int.div_rem {}",data),
			OpIntUDivide(data) => write!(f,"int.div_rem {}",data.fmt_unsigned()),

			OpIntTruncate{
				src,
				dst,
			} => write!(f,"int.trunc i{} i{}",src,dst),
			OpIntZeroExt{
				src,
				dst,
			} => write!(f,"int.zext i{} i{}",src,dst),
			OpIntSignExt{
				src,
				dst,
			} => write!(f,"int.sext i{} i{}",src,dst),

			OpSIntToFloat{
				src,
				dst,
				elements,
			} => write!(f,"conv.itofp i{} f{} {}",src,dst,elements),
			OpUIntToFloat{
				src,
				dst,
				elements,
			} => write!(f,"conv.utofp i{} f{} {}",src,dst,elements),

			OpIntToFloatBits(data) => write!(f,"conv.ibitstofp i{}",data),

			OpFloatToSInt{
				src,
				dst,
				elements,
			} => write!(f,"conv.fptosi f{} i{} {}",src,dst,elements),
			OpFloatToUInt{
				src,
				dst,
				elements,
			} => write!(f,"conv.fptoui f{} u{} {}",src,dst,elements),
			OpFloatToSIntSat{
				src,
				dst,
				elements,
			} => write!(f,"conv.fptosi.sat f{} i{} {}",src,dst,elements),
			OpFloatToUIntSat{
				src,
				dst,
				elements,
			} => write!(f,"conv.fptoui.sat f{} u{} {}",src,dst,elements),

			OpFloatToIntBits(data) => write!(f,"conv.fpbitstoi f{}",data),

			OpFlagToInt(data) => write!(f,"flag.getint i{}",data),

			OpFloatNeg(data) => write!(f,"fp.neg {}",data),
			OpFloatAbs(data) => write!(f,"fp.abs {}",data),

			OpFloatAdd(data) => write!(f,"fp.add {}",data),
			OpFloatSub(data) => write!(f,"fp.sub {}",data),
			OpFloatMul(data) => write!(f,"fp.mul {}",data),
			OpFloatDiv(data) => write!(f,"fp.div {}",data),
			OpFloatRem(data) => write!(f,"fp.rem {}",data),
			OpFloatMin(data) => write!(f,"fp.min {}",data),
			OpFloatMax(data) => write!(f,"fp.max {}",data),

			OpFloatEqual(data) => write!(f,"fp.eq f{}",data),
			OpFloatNotEqual(data) => write!(f,"fp.neq f{}",data),
			OpFloatLessThan(data) => write!(f,"fp.lt f{}",data),
			OpFloatGreaterThan(data) => write!(f,"fp.gt f{}",data),

			OpFloatTruncate{
				src,
				dst,
			} => write!(f,"fp.trunc f{} f{}",src,dst),
			OpFloatZeroExt{
				src,
				dst,
			} => write!(f,"fp.ext f{} f{}",src,dst),

			OpAllocStack(data) => write!(f,"op.alloc_local {}",data),

			OpGetStruct{
				ptr,
				index,
			} => write!(f,"ptr.struct_elem {} {}",ptr,index),
			OpGetStructNoWrap{
				ptr,
				index,
			} => write!(f,"ptr.struct_elem.no_wrap {} {}",ptr,index),
			OpGetStructInbounds{
				ptr,
				index,
			} => write!(f,"ptr.struct_elem.inbounds {} {}",ptr,index),

			OpGetArray(data) => write!(f,"ptr.array_elem {}",data),
			OpGetArrayNoWrap(data) => write!(f,"ptr.array_elem.no_wrap {}",data),
			OpGetArrayInbounds(data) => write!(f,"ptr.array_elem.inbounds {}",data),

			OpUnionSpec(data) => write!(f,"ptr.union_spec {}",data),
			OpUnionSpecInbounds(data) => write!(f,"ptr.union_spec.inbounds {}",data),

			PointerSubtract(data) => write!(f,"ptr.psub {}",data),

			OpPtrLoad(data) => write!(f,"ptr.load {}",data),
			OpPtrLoadUnAlign(data) => write!(f,"ptr.load.un_align {}",data),
			OpPtrLoadVolatile(data) => write!(f,"ptr.load.volatile {}",data),
			OpPtrLoadVolatileUnAlign(data) => write!(f,"ptr.load.volatile.un_align {}",data),

			OpAtomicLoadRelaxed(data) => write!(f,"atomic.load.relaxed {}",data),
			OpAtomicLoadAcquire(data) => write!(f,"atomic.load.acquire {}",data),
			OpAtomicLoadSeqCst(data) => write!(f,"atomic.load.seq_cst {}",data),

			OpPtrStore(data) => write!(f,"ptr.store {}",data),
			OpPtrStoreUnAlign(data) => write!(f,"ptr.store.un_align {}",data),
			OpPtrStoreVolatile(data) => write!(f,"ptr.store.volatile {}",data),
			OpPtrStoreVolatileUnAlign(data) => write!(f,"ptr.store.volatile.un_align {}",data),

			OpAtomicStoreRelaxed(data) => write!(f,"atomic.store.relaxed {}",data),
			OpAtomicStoreRelease(data) => write!(f,"atomic.store.release {}",data),
			OpAtomicStoreSeqCst(data) => write!(f,"atomic.store.seq_cst {}",data),



			OpBlackBox => write!(f,"op.blackbox"),

			OpAssumeTrue => write!(f,"op.assume"),

			OpLifetimeStart(data) => write!(f,"lifetime.start {}",data),
			OpLifetimeFinish(data) => write!(f,"lifetime.finish {}",data),

			BranchNode(data) => unimplemented!(),
			SwitchNode(data) => unimplemented!(),
			LoopNode(data) => unimplemented!(),
			CallNode(data) => unimplemented!(),
			InvokeNode(data) => unimplemented!(),
		}
	}
}





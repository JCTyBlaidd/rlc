use super::*;


/// Switch Node:
///   Evaluated different blocks depending on an integer inputs value
///
///  Inputs:             (SwitchInt + TypeInputs)
///  Outputs:            (TypeOutputs)
///  BlockIf<ValidInts>: (TypeInputs) => (TypeOutputs)
///  BlocKDefault:       (TypeInputs) => (TypeOutputs)
///
pub struct SwitchMeta {
	pub int_switch_type: IntType,
	pub input_types: Vec<BasicType>,
	pub output_types: Vec<BasicType>
}

impl ScopeTypeMeta for SwitchMeta {
	
	fn scope_input_sets(&self) -> u8 {
		1
	}
	
	fn scope_output_sets(&self) -> u8 {
		1
	}
	
	fn scope_input_count(&self, set: u8) -> u16 {
		if set == 0 {
			self.input_types.len() as u16
		}else{
			invalid_set(set)
		}
	}
	
	fn scope_output_count(&self, set: u8) -> u16 {
		if set == 0 {
			self.output_types.len() as u16
		}else{
			invalid_set(set)
		}
	}
	
	fn scope_input_type(&self, set: u8, idx: u16) -> BasicType {
		if set == 0 {
			get_vec_idx(self.input_types.as_slice(),set,idx)
		}else{
			invalid_set(set)
		}
	}
	
	fn scope_output_type(&self, set: u8, idx: u16) -> BasicType {
		if set == 0 {
			get_vec_idx(self.output_types.as_slice(),set,idx)
		}else{
			invalid_set(set)
		}
	}
}

impl SwitchMeta {
	
	#[inline(always)]
	pub fn node_input_sets(&self) -> u8 {
		2
	}
	
	#[inline(always)]
	pub fn node_output_sets(&self) -> u8 {
		1
	}
	
	pub fn node_input_count(&self, set: u8) -> u16 {
		match set {
			0 => self.input_types.len() as u16,
			1 => 1,
			x => invalid_set(x)
		}
	}
	
	#[inline(always)]
	pub fn node_output_count(&self, set: u8) -> u16 {
		self.scope_output_count(set)
	}
	
	pub fn node_input_type(&self, set: u8, idx: u16) -> BasicType {
		match set {
			0 => get_vec_idx(self.input_types.as_slice(),set,idx),
			1 => if idx == 0 {
				self.int_switch_type.into_type()
			}else{
				invalid_idx(set,idx)
			},
			x => invalid_set(x)
		}
	}
	
	#[inline(always)]
	pub fn node_output_type(&self, set: u8, idx: u16) -> BasicType {
		self.scope_output_type(set,idx)
	}
}

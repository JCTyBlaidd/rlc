use super::*;

/// Branch Node:
///   Evaluates different blocks depending on flag state
///   true => BlockTrue, false => BlockFalse, undef => UB(NoExec)
///
/// Inputs:     (Flag + TypeInputs)
/// Outputs:    (TypeOutputs)
/// BlockTrue:  (TypeInputs) => (TypeOutputs)
/// BlockFalse: (TypeInputs) => (TypeOutputs)
pub struct BranchMeta {
	pub input_types: Vec<BasicType>,
	pub output_types: Vec<BasicType>,
	pub is_unwind_flag: bool,
	//FIXME: Add Metadata Regarding any flag output types: (Always Constant etc..)
	// If True in one and false in the other: allows better block threading with other
	// data sections
}

impl ScopeTypeMeta for BranchMeta {
	
	fn scope_input_sets(&self) -> u8 {
		1
	}
	
	fn scope_output_sets(&self) -> u8 {
		1
	}
	
	fn scope_input_count(&self, set: u8) -> u16 {
		if set == 0 {
			self.input_types.len() as u16
		} else {
			invalid_set(0)
		}
	}
	
	fn scope_output_count(&self, set: u8) -> u16 {
		if set == 0 {
			self.input_types.len() as u16
		} else {
			invalid_set(0)
		}
	}
	
	fn scope_input_type(&self, set: u8, idx: u16) -> BasicType {
		if set == 0 {
			get_vec_idx(self.input_types.as_slice(),set,idx)
		} else {
			invalid_set(set)
		}
	}
	
	fn scope_output_type(&self, set: u8, idx: u16) -> BasicType {
		if set == 0 {
			get_vec_idx(self.output_types.as_slice(),set,idx)
		} else {
			invalid_set(set)
		}
	}
}

impl BranchMeta {
	
	#[inline(always)]
	pub fn node_input_sets(&self) -> u8 {
		2
	}
	
	#[inline(always)]
	pub fn node_output_sets(&self) -> u8 {
		1
	}
	
	pub fn node_input_count(&self, set: u8) -> u16 {
		match set {
			0 => self.input_types.len() as u16,
			1 => 1,
			x => invalid_set(x)
		}
	}
	
	#[inline(always)]
	pub fn node_output_count(&self, set: u8) -> u16 {
		self.scope_output_count(set)
	}
	
	pub fn node_input_type(&self, set: u8, idx: u16) -> BasicType {
		match set {
			0 => get_vec_idx(self.input_types.as_slice(),set,idx),
			1 => if idx == 0 {
				if self.is_unwind_flag {
					ExceptionFlagType
				} else {
					FlagType
				}
			} else {
				invalid_idx(set,idx)
			},
			_ => invalid_set(set)
		}
	}
	
	#[inline(always)]
	pub fn node_output_type(&self, set: u8, idx: u16) -> BasicType {
		self.scope_output_type(set,idx)
	}
}

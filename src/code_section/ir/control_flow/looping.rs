use super::*;

/// Loop Node:
///  Repeat evaluation of internal block until internal continue flag is false
///
///  Inputs:    (TypeInputs + TypeInduction)
///  Outputs:   (TypeInduction)
///  BlockLoop:	(TypeInputs + TypeInduction) => (Flag + TypeInduction)
///
pub struct LoopMeta {
	pub input_types: Vec<BasicType>,
	pub induction_types: Vec<BasicType>,
}

impl ScopeTypeMeta for LoopMeta {
	
	fn scope_input_sets(&self) -> u8 {
		2
	}
	
	fn scope_output_sets(&self) -> u8 {
		2
	}
	
	fn scope_input_count(&self, set: u8) -> u16 {
		match set {
			0 => self.induction_types.len() as u16,
			1 => self.input_types.len() as u16,
			x => invalid_set(x)
		}
	}
	
	fn scope_output_count(&self, set: u8) -> u16 {
		match set {
			0 => self.induction_types.len() as u16,
			1 => 1,
			x => invalid_set(x)
		}
	}
	
	fn scope_input_type(&self, set: u8, idx: u16) -> BasicType {
		match set {
			0 => get_vec_idx(self.induction_types.as_slice(),set,idx),
			1 => get_vec_idx(self.input_types.as_slice(),set,idx),
			x => invalid_set(x)
		}
	}
	
	fn scope_output_type(&self, set: u8, idx: u16) -> BasicType {
		match set {
			0 => get_vec_idx(self.induction_types.as_slice(),set,idx),
			1 => if idx == 0 {
				FlagType
			}else{
				invalid_idx(set,idx)
			},
			x => invalid_set(x)
		}
	}
}

impl LoopMeta {
	
	#[inline(always)]
	pub fn node_input_sets(&self) -> u8 {
		1
	}
	
	#[inline(always)]
	pub fn node_output_sets(&self) -> u8 {
		1
	}
	
	#[inline(always)]
	pub fn node_input_count(&self, set: u8) -> u16 {
		self.scope_input_count(set)
	}
	
	pub fn node_output_count(&self, set: u8) -> u16 {
		if set == 0 {
			self.induction_types.len() as u16
		}else{
			invalid_set(set)
		}
	}
	
	#[inline(always)]
	pub fn node_input_type(&self, set: u8, idx: u16) -> BasicType {
		self.scope_input_type(set,idx)
	}
	
	pub fn node_output_type(&self, set: u8, idx: u16) -> BasicType {
		if set == 0 {
			get_vec_idx(self.induction_types.as_slice(),set,idx)
		}else{
			invalid_set(set)
		}
	}
}

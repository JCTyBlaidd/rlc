use super::*;

/// Call Node:
///  Execute an externally defined function, with given definition
///
///  Inputs: (Arguments)  {?make indirect-call a different definition?}
///  Outputs: (ReturnValue) {
/// 	return-value internally is a primitive type or a structure
///      of primitive types and other similarly defined structures
///     the return-value is the destructuring of the result into a set of primitives
///     use an outbound pointer to a local variable for more complex values
///     too complicated values may fail to compile if the ABI is not supported
/// }
pub struct CallMeta {
	pub target: Option<String>,
}

impl CallMeta {
	pub fn node_input_sets(&self) -> u8 {
		unimplemented!()
	}
	pub fn node_output_sets(&self) -> u8 {
		unimplemented!()
	}
	pub fn node_input_count(&self, set: u8) -> u16 {
		unimplemented!()
	}
	pub fn node_output_count(&self, set: u8) -> u16 {
		unimplemented!()
	}
	pub fn node_input_type(&self, set: u8, idx: u16) -> BasicType {
		unimplemented!()
	}
	pub fn node_output_type(&self, set: u8, idx: u16) -> BasicType {
		unimplemented!()
	}
}

/// invoke (inputs => return) (return => outputs) (except => outputs) (?add generic either case before?)
/// Invoke Node:
///   Execute an externally defined function, which could throw an exception,
///   Non unwinding behaviour similarly to the call-node, however this node has an additional
///   path that could be taken in the case the function called instead throws an exception
///
///  Inputs: (Arguments) {?make indirect-call a different definition?}
///  Outputs: (TypeOutputs)
///  BlockReturn: (ReturnValueDestructured) => (TypeOutputs)
///  BlockUnwind: (ExceptionFlag=True) => (TypeOutputs)
///
///  To Chain Unwinding use constant flag outputs and if statements for two different paths
///    the unwind-only path will be extracted post optimization and converted accordingly
///    to use the required exception handling for try,catch,throw and unwind etc..
pub struct InvokeMeta {
	pub target: Option<String>,
	pub output_types: Vec<BasicType>,
}

impl ScopeTypeMeta for InvokeMeta {
	
	fn scope_input_sets(&self) -> u8 {
		unimplemented!()
	}
	
	fn scope_output_sets(&self) -> u8 {
		unimplemented!()
	}
	
	fn scope_input_count(&self, set: u8) -> u16 {
		unimplemented!()
	}
	
	fn scope_output_count(&self, set: u8) -> u16 {
		unimplemented!()
	}
	
	fn scope_input_type(&self, set: u8, idx: u16) -> BasicType {
		unimplemented!()
	}
	
	fn scope_output_type(&self, set: u8, idx: u16) -> BasicType {
		unimplemented!()
	}
}

impl InvokeMeta {
	pub fn node_input_sets(&self) -> u8 {
		unimplemented!()
	}
	pub fn node_output_sets(&self) -> u8 {
		unimplemented!()
	}
	pub fn node_input_count(&self, set: u8) -> u16 {
		unimplemented!()
	}
	pub fn node_output_count(&self, set: u8) -> u16 {
		unimplemented!()
	}
	pub fn node_input_type(&self, set: u8, idx: u16) -> BasicType {
		unimplemented!()
	}
	pub fn node_output_type(&self, set: u8, idx: u16) -> BasicType {
		unimplemented!()
	}
}

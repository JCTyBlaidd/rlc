use super::{
	BasicType, IntType,
	FlagType, ExceptionFlagType,
	invalid_set, invalid_idx
};

mod branch;
mod switch;
mod looping;
mod call;

pub use self::{
	branch::BranchMeta,
	switch::SwitchMeta,
	looping::LoopMeta,
	call::{CallMeta,InvokeMeta}
};

#[inline]
fn get_vec_idx(vec: &[BasicType],set: u8, idx: u16) -> BasicType {
	match vec.get(idx as usize) {
		Some(basic_type) => *basic_type,
		None => invalid_idx(set, idx)
	}
}


/// Trait for scopes so that scope input and output
///  nodes can correctly resolve there types
pub trait ScopeTypeMeta {
	fn scope_input_sets(&self) -> u8;
	fn scope_output_sets(&self) -> u8;
	fn scope_input_count(&self, set: u8) -> u16;
	fn scope_output_count(&self, set: u8) -> u16;
	fn scope_input_type(&self, set: u8, idx: u16) -> BasicType;
	fn scope_output_type(&self, set: u8, idx: u16) -> BasicType;
}

/// Metadata describing the likelihood for a given code
///  path to be executed, this information is used to
///  determine the ordering of blocks in regard to jump
///  information.
/// High Probability Blocks are preferred for the linear
///  path to take advantage of caching of program
///  instructions.
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
#[repr(u8)]
pub enum SwitchProbability {
	
	/// Assumes nothing about the behaviour of
	///  branch unless some information can be
	///  inferred from other sources
	Unknown = 0,
	
	/// Assumes that the branch will be taken
	///  in a significantly low amount of cases
	///  and as such can be treated as cold.
	AlmostNever = 1,
	
	/// Assumes that the branch will be taken
	///  less often than the other branch,
	///  in a mostly predictable manner.
	Unlikely = 2,
	
	/// Assumes that the branch will be taken
	///  in a similar probability to the other
	///  branch and in a non-random manner.
	Similar = 3,
	
	/// Assumes that the branch will be taken
	///  more often than the other branch
	///  in a mostly predictable manner.
	Likely = 4,
	
	/// Assumes that the branch will be taken
	///  in a significantly high amount of cases
	///  and as such the other branch will
	///  be treated as cold.
	AlmostAlways = 5,
	
	/// Assumes that the likelihood of taking
	///  this branch depends on factors that
	///  cannot be easily predicted.
	Random = 6,
}

impl Default for SwitchProbability {
	#[inline(always)]
	fn default() -> Self {
		SwitchProbability::Unknown
	}
}

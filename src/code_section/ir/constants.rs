use std::num::NonZeroU8;
use std::fmt::{Display, Result as FmtResult, Formatter};
use serde::{Serialize, Deserialize};
use std::convert::TryInto;
use std::cmp::{PartialEq, Eq};
use std::hash::{Hasher, Hash};

#[derive(Clone)]
pub struct IntConstant(IntConstRepr);
impl IntConstant {
	pub fn from_value(size: NonZeroU8, value: u128) -> IntConstant {
		IntConstant(IntConstRepr::from_value(size,value))
	}
	pub fn get_size(&self) -> NonZeroU8 {
		self.0.get_size()
	}
	pub fn get_value(&self) -> u128 {
		self.0.get_value()
	}
}
impl Display for IntConstant {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f,"i{} {}",self.0.get_size(),self.0.get_value())
	}
}
impl PartialEq for IntConstant {
	fn eq(&self, other: &Self) -> bool {
		self.get_size() == other.get_size() &&
			self.get_value() == other.get_value()
	}
}
impl Eq for IntConstant {}
impl Hash for IntConstant {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.get_size().hash(state);
		self.get_value().hash(state);
	}
}


//FIXME: IntConstant For Vector Sizes (u8 x 4) minimised set
//       VARIANT VECTOR METADATA?!??!



#[derive(Clone)]
pub struct FloatConstant(FloatConstRepr);
impl FloatConstant {

}
impl Display for FloatConstant {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		Ok(())
	}
}
impl PartialEq for FloatConstant {
	fn eq(&self, other: &Self) -> bool {
		unimplemented!()
	}
}
impl Eq for FloatConstant {}
impl Hash for FloatConstant {
	fn hash<H: Hasher>(&self, state: &mut H) {
		unimplemented!()
	}
}

	
#[derive(Clone)]
enum IntConstRepr {
	IntSmallConst(NonZeroU8, i32),
	IntLargeConst(Box<(NonZeroU8, u128)>)
}
use IntConstRepr::{IntLargeConst, IntSmallConst};

impl IntConstRepr {
	pub fn from_value(size: NonZeroU8, value: u128) -> IntConstRepr {
		if let Ok(v) = (value as i128).try_into() {
			IntSmallConst(size, v)
		} else {
			IntLargeConst(Box::new((size, value)))
		}
	}
	pub fn get_size(&self) -> NonZeroU8 {
		match self {
			IntSmallConst(size, _) => *size,
			IntLargeConst(value) => value.0
		}
	}
	pub fn get_value(&self) -> u128 {
		match self {
			IntSmallConst(_, v) => (*v as i128) as u128,
			IntLargeConst(value) => value.1
		}
	}
}


//FIXME: Change To Larger Enum (F32,F64,F128,F80,F16)
#[derive(Clone)]
enum FloatConstRepr {
	FloatSmallConst(NonZeroU8,u32),
	FloatLargeConst(Box<(NonZeroU8,u128)>)
}
use FloatConstRepr::{FloatLargeConst, FloatSmallConst};

impl FloatConstRepr {
	pub fn get_size(&self) -> NonZeroU8 {
		match self {
			FloatSmallConst(size, _) => *size,
			FloatLargeConst(value) => value.0
		}
	}
	pub fn get_sign(&self) -> bool {
		match self {
			FloatSmallConst(_,v) => (*v as i32) > 0,
			FloatLargeConst(value) => (value.1 as i128) > 0
		}
	}
	pub fn get_mantissa(&self) -> u128 {
		const MANTISSA_32 : u32 = 0;//FIXME: (1 << 32) - 1;
		const MANTISSA_128 : u128 = (1 << 112) - 1;
		match self {
			FloatSmallConst(_,v) => (v & MANTISSA_32) as u128,
			FloatLargeConst(value) => value.1 & MANTISSA_128
		}
	}
	pub fn get_exponent(&self) -> i16 {
		const EXP_32 : u32 = (1 << 8) - 1;
		const EXP_128 : u128 = (1 << 15) - 1;
		unimplemented!()
	}
	pub fn is_nan(&self) -> Option<u128> {
		unimplemented!()
	}
	pub fn is_inf(&self) -> Option<bool> {
		unimplemented!()
	}
}



#[cfg(test)]
mod tests {
	use super::*;
	
	fn test_int_constant(value: u128, small_repr: bool) {
		let constant = IntConstant::from_value(
			NonZeroU8::new(3).unwrap(),
			value
		);
		assert_eq!(constant.get_value(),value);
		assert_eq!(constant.get_size().get(),3);
		match &constant.0 {
			IntSmallConst(_, _) => assert!(small_repr),
			IntLargeConst(_) => assert!(!small_repr),
		}
	}
	
	#[test]
	pub fn test_integer_constants() {
		test_int_constant(-1i128 as u128,true);
		test_int_constant(0,true);
		test_int_constant(1,true);
		test_int_constant(std::i32::MAX as i128 as u128,true);
		test_int_constant(std::i32::MIN as i128 as u128,true);
		test_int_constant((std::i32::MAX as i128 + 1) as u128,false);
		test_int_constant((std::i32::MIN as i128 - 1) as u128,false);
	}
	
}
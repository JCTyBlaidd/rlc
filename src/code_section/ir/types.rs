use std::num::{NonZeroU8, NonZeroU16, NonZeroU64};
use parking_lot::{RwLock, RwLockReadGuard, MappedRwLockReadGuard};
use std::borrow::Cow;
use std::fmt::{Display, Result as FmtResult, Formatter};
use serde::{Serialize, Deserialize};

/// Simple Type Declaration
///  handles types that are used directly as parameter of ops
///  more complex types such as structures are referenced elsewhere
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum BasicType {
	
	/// Vector of Integers Type:
	///  e.g: <3 x i32>, i1
	IntType(IntType),
	
	/// Vector of Floats Type:
	///  e.g: <3 x f32>, f8
	FloatType(FloatType),
	
	/// Pointer Type, *$32,
	///  represents exactly one level
	///  of indirection,
	///  target type must be stored
	///  in the adv type array
	PointerType(PointerType),
	
	/// Undefined Type, used as padding,
	///  reading or writing to this location
	///  is undefined behaviour:
	/// Parameter is number of bytes in size
	UndefinedType(NonZeroU16),
	
	/// Represents a true or false value,
	///  is not a boolean, use i1 this
	///  represents the result of comparisons
	///  that can be consumed by different
	///  operations (if,toInt)
	FlagType,
	
	/// Flag that represents whether the code is running under an
	///  exception unwinding path or not, a non unwinding flag
	///  can be constructed trivially but an unwinding flag
	///  is only available as part of the exceptional section
	///  of the invoke operation
	ExceptionFlagType,
	
	/// Represents the type that can be returned by non-returning
	///  operations, this can be trivially converted into any
	///  other type since it will never be executed, incorrectly
	///  marking a function or loop as non-returning will result
	///  in undefined behaviour
	TerminationType,
}

impl BasicType {
	pub fn into_adv_type(self) -> AdvType {
		match self {
			BasicType::IntType(t) => AdvType::IntType(t),
			BasicType::FloatType(t) => AdvType::FloatType(t),
			BasicType::PointerType(t) => AdvType::PointerType(t),
			BasicType::UndefinedType(t) => AdvType::UndefinedType(t),
			BasicType::ExceptionFlagType => AdvType::ExceptionFlagType,
			BasicType::FlagType => AdvType::FlagType,
			BasicType::TerminationType => AdvType::TerminationType
		}
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct IntType {
	pub size: NonZeroU8,
	pub elements: NonZeroU8
}

impl IntType {
	
	#[inline(always)]
	pub fn into_type(self) -> BasicType {
		BasicType::IntType(self)
	}
	
	#[inline(always)]
	pub fn into_adv_type(self) -> AdvType {
		AdvType::IntType(self)
	}
	
	#[inline]
	pub fn new(size: NonZeroU8, elements: NonZeroU8) -> IntType {
		IntType{
			size,
			elements
		}
	}
	
	#[inline]
	pub fn from_size(size: NonZeroU8) -> IntType {
		IntType {
			size,
			elements: NonZeroU8::new(1).unwrap(),
		}
	}
	
	#[inline(always)]
	pub fn is_vector(&self) -> bool {
		self.elements.get() != 1
	}
	
	pub fn fmt_unsigned(&self) -> impl Display {
		struct UnsignedSelf(IntType);
		impl Display for UnsignedSelf {
			fn fmt(&self, f: &mut Formatter) -> FmtResult {
				if self.0.is_vector() {
					write!(f,"< {} x u{} >", self.0.elements, self.0.size)
				}else{
					write!(f,"u{}", self.0.size)
				}
			}
		}
		UnsignedSelf(*self)
	}
}

impl Display for IntType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		if self.is_vector() {
			write!(f,"< {} x i{} >", self.elements, self.size)
		}else{
			write!(f,"i{}", self.size)
		}
	}
}
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct FloatType {
	pub size: NonZeroU8,
	pub elements: NonZeroU8
}

impl FloatType {
	
	#[inline(always)]
	pub fn into_type(self) -> BasicType {
		BasicType::FloatType(self)
	}
	
	#[inline(always)]
	pub fn into_adv_type(self) -> AdvType {
		AdvType::FloatType(self)
	}
	
	#[inline]
	pub fn new(size: NonZeroU8, elements: NonZeroU8) -> FloatType {
		FloatType{
			size,
			elements
		}
	}
	
	#[inline]
	pub fn from_size(size: NonZeroU8) -> FloatType {
		FloatType {
			size,
			elements: NonZeroU8::new(1).unwrap()
		}
	}
	
	#[inline(always)]
	pub fn is_vector(&self) -> bool {
		self.elements.get() != 1
	}
}

impl Display for FloatType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		if self.is_vector() {
			write!(f,"< {} x f{} >", self.elements, self.size)
		}else{
			write!(f,"f{}", self.size)
		}
	}
}

//FIXME: Add Add AddressSpace Option<u8> or equiv in metadata?
#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct PointerType {
	pub target: TypeIdx,
}

impl PointerType {
	#[inline(always)]
	pub fn into_type(self) -> BasicType {
		BasicType::PointerType(self)
	}
	
	#[inline(always)]
	pub fn into_adv_type(self) -> AdvType {
		AdvType::PointerType(self)
	}
	
	#[inline(always)]
	pub fn new(target: TypeIdx) -> PointerType {
		PointerType {
			target,
		}
	}
	
	#[inline(always)]
	fn fmt_raw(&self, f: &mut Formatter) -> FmtResult {
		self.target.fmt(f)
	}
}

impl Display for PointerType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f,"ptr({})",self.target)
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct TypeIdx(u32);

impl Display for TypeIdx {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f,"%{}",self.0)
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct StructureType {
	size: NonZeroU64,
	align: NonZeroU64,
	stride: NonZeroU64,
	
	/// (Offset, Type): Offset(n) = Offset(n-1) + Stride(n-1), just for quick lookup
	///  the offsets are pre-calculated and not output
	contents: Vec<(u64, TypeIdx)>,
}

impl Display for StructureType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		writeln!(
			f,
			"struct( size={}, align={}, stride={} ) {{",
			self.size,
			self.align,
			self.stride
		)?;
		for (_,content) in self.contents.iter() {
			writeln!(f,"\t{}",content)?;
		}
		write!(f,"}}")
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct UnionType {
	size: NonZeroU64,
	align: NonZeroU64,
	stride: NonZeroU64
}

impl Display for UnionType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(
			f,
			"union( size={}, align={}, stride={} )",
			self.size,
			self.align,
			self.stride
		)
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct ArrayType {
	basic_type: TypeIdx,
	count: NonZeroU64,
}

impl Display for ArrayType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f, "[{} ; {}]", self.basic_type, self.count)
	}
}

#[derive(Serialize, Deserialize)]
#[derive(Clone, PartialEq, Eq, Hash)]
pub enum AdvType {
	
	/// Vector of Integers Type:
	///  e.g: <3 x i32>, i1
	IntType(IntType),
	
	/// Vector of Floats Type:
	///  e.g: <3 x f32>, f8
	FloatType(FloatType),
	
	/// Pointer Type, *$32,
	///  represents exactly one level
	///  of indirection,
	///  target type must be stored
	///  in the adv type array
	PointerType(PointerType),
	
	/// Undefined Type, used as padding,
	///  reading or writing to this location
	///  is undefined behaviour:
	/// Parameter is number of bytes in size
	UndefinedType(NonZeroU16),
	
	/// Represents a true or false value,
	///  is not a boolean, use i1 this
	///  represents the result of comparisons
	///  that can be consumed by different
	///  operations (if,toInt)
	FlagType,
	
	/// Flag that represents whether the code is running under an
	///  exception unwinding path or not, a non unwinding flag
	///  can be constructed trivially but an unwinding flag
	///  is only available as part of the exceptional section
	///  of the invoke operation
	ExceptionFlagType,
	
	/// Represents the type that can be returned by non-returning
	///  operations, this can be trivially converted into any
	///  other type since it will never be executed, incorrectly
	///  marking a function or loop as non-returning will result
	///  in undefined behaviour
	TerminationType,
	
	/// Represents a combined structure of types
	StructureType(StructureType),
	
	/// Is an abstract bag of bits should be changed
	///  from a pointer to the union to the pointer to
	///  the chosen variant
	UnionType(UnionType),
	
	/// Represents a fixed count of basic types
	ArrayType(ArrayType)
}

impl Display for AdvType {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		match self {
			AdvType::IntType(t) => t.fmt(f),
			AdvType::FloatType(t) => t.fmt(f),
			AdvType::PointerType(t) => t.fmt(f),
			AdvType::UndefinedType(size) => write!(f,"undef({})",size),
			AdvType::FlagType => write!(f,"flag"),
			AdvType::ExceptionFlagType => write!(f,"unwind-flag"),
			AdvType::TerminationType => write!(f,"terminated"),
			AdvType::StructureType(t) => t.fmt(f),
			AdvType::UnionType(t) => t.fmt(f),
			AdvType::ArrayType(t) => t.fmt(f),
		}
	}
}

///FIXME: ADD CUSTOM IMPLEMENTATION (merge lists)
///#[derive(Serialize, Deserialize)]
pub struct TypeCache {
	
	///Non Mutable Shared Types
	types: Vec<AdvType>,
	
	///Mutable Shared Types
	edit_types: RwLock<Vec<AdvType>>,
	
}

impl TypeCache {
	pub fn flush_types(&mut self) {
		let new_types = self.edit_types.get_mut();
		if new_types.len() != 0 {
			self.types.extend_from_slice(new_types.as_mut_slice());
			new_types.clear();
			new_types.shrink_to_fit();
		}
	}
	
	pub fn register_type(&mut self, t: AdvType) -> TypeIdx {
		self.flush_types();
		let idx = self.types.len();
		let new_idx = idx as u32;
		if new_idx as usize == idx {
			self.types.push(t);
			TypeIdx(new_idx)
		}else{
			/// Should never happen in reasonable code,
			///  but checked for to prevent errors
			panic!("Used up all types")
		}
	}
	
	pub fn register_type_shared(&self, t: AdvType) -> TypeIdx {
		let mut lock = self.edit_types.write();
		let idx = self.types.len().checked_add(lock.len()).unwrap();
		let new_idx = idx as u32;
		if new_idx as usize == idx {
			lock.push(t);
			TypeIdx(new_idx)
		}else{
			/// Should never happen in reasonable code,
			///  but checked for to prevent errors
			panic!("Used up all types")
		}
	}
	
	pub fn get_type_ref(&self, idx: TypeIdx) -> TypeRef {
		if idx.0 >= self.types.len() as u32 {
			let new_idx = idx.0 as usize - self.types.len();
			let read_lock = self.edit_types.read();
			let new_lock = RwLockReadGuard::map(read_lock,|v| &v[new_idx]);
			TypeRef(TypeRefCore::CaseMutex(new_lock))
		}else{
			TypeRef(TypeRefCore::CaseRef(&self.types[idx.0 as usize]))
		}
	}
	
	//FIXME: Add Recusive get_type_ref if applicable?
	
	pub fn get_basic_type(&self, idx: TypeIdx) -> BasicType {
		unimplemented!()
	}
	
	pub fn get_type_cloned(&self, idx: TypeIdx) -> Cow<AdvType> {
		if idx.0 >= self.types.len() as u32 {
			let new_idx = idx.0 as usize - self.types.len();
			let read_lock = self.edit_types.read();
			Cow::Owned(read_lock[new_idx].clone())
		}else{
			Cow::Borrowed(&self.types[idx.0 as usize])
		}
	}
}

impl Display for TypeCache {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		for (idx,val) in self.types.iter().enumerate() {
			writeln!(f,"%{} = {}", idx, val)?;
		}
		let offset = self.types.len();
		let read_next = self.edit_types.read();
		for (idx,val) in read_next.iter().enumerate() {
			writeln!(f,"%{} = {}",offset + idx, val)?;
		}
		Ok(())
	}
}


pub struct TypeRef<'a>(TypeRefCore<'a>);

impl<'a> std::ops::Deref for TypeRef<'a> {
	type Target = AdvType;
	
	fn deref(&self) -> &AdvType {
		match &self.0 {
			TypeRefCore::CaseRef(x) => x,
			TypeRefCore::CaseMutex(x) => x.deref()
		}
	}
}

enum TypeRefCore<'a> {
	CaseRef(&'a AdvType),
	CaseMutex(MappedRwLockReadGuard<'a,AdvType>)
}
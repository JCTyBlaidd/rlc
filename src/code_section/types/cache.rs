use std::ops::{Index, IndexMut};
use crate::util::arena::{
	InvariantLifetime,
	MarkedCollection,
	MarkedIndex,
	CollectionInsertError
};
use super::MemType;

/// Both type metadata and the
///  type cache, set for efficient
///  storage and passing of values
pub struct GlobalTypeData<'ctx> {
	pub meta: TypeMetadata,
	pub cache: TypeCache<'ctx>,
}

/// Metadata for the alignment and sizes
///  of basic types
pub struct TypeMetadata {
	pub align_i8: u64,
	pub align_i16: u64,
	pub align_i32: u64,
	pub align_i64: u64,
	pub align_i128: u64,
	pub align_i256: u64,
	pub align_ptr: u64,
	pub size_ptr: u64,//TODO: SEPARATE PTR_DIFF & PTR_INT??
}


//TODO: CONVERT TO SHARDED SET? (allow for shared-ref-add)
pub struct TypeCache<'ctx>(MarkedCollection<'ctx,u32,MemType<'ctx>>);

impl<'ctx> TypeCache<'ctx> {
	/// Safety: ensure that ctx will be referenced in a unique manner,
	///  static lifetimes are unsafe, they must be some unique
	///  lifetime
	pub unsafe fn new() -> TypeCache<'ctx> {
		TypeCache(MarkedCollection::new())
	}
	
	pub unsafe fn clear(&mut self) {
		self.0.clear();
	}
	
	pub fn add_obj(&mut self, data: MemType<'ctx>) -> Result<TypeIdx<'ctx>,CollectionInsertError> {
		self.0.add_obj(data).map(TypeIdx)
	}
}
impl<'ctx> Index<TypeIdx<'ctx>> for TypeCache<'ctx> {
	type Output = MemType<'ctx>;
	#[inline]
	fn index(&self, index: TypeIdx<'ctx>) -> &MemType<'ctx> {
		self.0.index(index.0)
	}
}
impl<'ctx> IndexMut<TypeIdx<'ctx>> for TypeCache<'ctx> {
	#[inline]
	fn index_mut(&mut self, index: TypeIdx<'ctx>) -> &mut MemType<'ctx> {
		self.0.index_mut(index.0)
	}
}

#[repr(transparent)]
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct TypeIdx<'ctx>(MarkedIndex<'ctx,u32,MemType<'ctx>>);
use super::{
	FloatSize, IntSize
};
use bitflags::bitflags;

#[derive(Clone)]
pub struct ConstInt {
	pub size: IntSize,
	pub data_1: u128,
	pub data_2: u128,
}//TODO: convert to use large_int u256

impl ConstInt {
	/// Zero extend or truncate to new size
	pub fn zero_extend(&mut self, new_size: IntSize) {
		unimplemented!()
	}
	/// Sign extend or truncate to new size
	pub fn sign_extend(&mut self, new_size: IntSize) {
		unimplemented!()
	}
	
	/// Add value then saturate as unsigned
	///  return if saturation occurred
	pub fn add_saturating_unsigned(&mut self, other: &ConstInt) -> bool {
		unimplemented!()
	}
	/// Add value then saturate as signed
	pub fn add_saturating_signed(&mut self, other: &ConstInt) -> bool {
		unimplemented!()
	}
	/// Add value then wrap as 2s complement
	///  return if wrapped (as unsigned, as signed)
	pub fn add_wrapping(&mut self, other: &ConstInt) -> (bool,bool) {
		unimplemented!()
	}
	
	/// Add value then saturate as unsigned
	///  return if saturation occurred
	pub fn sub_saturating_unsigned(&mut self, other: &ConstInt) -> bool {
		unimplemented!()
	}
	/// Add value then saturate as signed
	pub fn sub_saturating_signed(&mut self, other: &ConstInt) -> bool {
		unimplemented!()
	}
	/// Add value then wrap as 2s complement
	///  return if wrapped (as unsigned, as signed)
	pub fn sub_wrapping(&mut self, other: &ConstInt) -> (bool,bool) {
		unimplemented!()
	}
	
	
}

#[derive(Clone)]
pub struct ConstFloat {
	pub size: FloatSize,
	pub data: HPFloat,
}

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum RoundingMode {
	NearestTieEven = 0,
	ToPositive = 1,
	ToNegative = 2,
	ToZero = 3,
	NearestTieAway = 4,
}

/*
struct InternalFConst<const F: FloatSize> {
	_bytes: [u8;{F.total_bits()}]
}*/

bitflags! {
	pub struct FloatFlags: u8 {
		const INVALID_OPERATION = 1;
		const DIVIDE_BY_ZERO = 2;
		const OVERFLOW = 4;
		const UNDERFLOW = 8;
		const INEXACT = 16;
	}
}
pub enum FloatClassify {
	NegInfinity = 0,
	NegNormal = 1,
	NegSubnormal = 2,
	NegZero = 3,
	PosZero = 4,
	PosSubnormal = 5,
	PosNormal = 6,
	PosInfinity = 7,
	SignallingNaN = 8,
	QuietNaN = 9,
}
impl FloatClassify {
	#[inline]
	pub fn check_nan(self) -> bool {
		use FloatClassify::*;
		match self {
			QuietNaN | SignallingNaN => true,
			_ => false
		}
	}
	#[inline]
	pub fn check_inf(self) -> bool {
		use FloatClassify::*;
		match self {
			NegInfinity | PosInfinity => true,
			_ => false
		}
	}
	#[inline]
	pub fn check_zero(self) -> bool {
		use FloatClassify::*;
		match self {
			NegZero | PosZero => true,
			_ => false
		}
	}
	#[inline]
	pub fn check_sign(self) -> Option<bool> {
		use FloatClassify::*;
		match self {
			NegInfinity | NegNormal
			| NegSubnormal | NegZero => Some(false),
			PosZero | PosSubnormal
			| PosNormal |PosInfinity => Some(true),
			SignallingNaN | QuietNaN => None,
		}
	}
}


/// High Precision Float
///  enough for 256-bit with
///  rounding performed post
///  operation
#[derive(Clone)]
pub struct HPFloat {
	sign: bool,
	exp: u32,
	mantissa_1: u128, //TOO: INT256 {enough precision?} (this is slightly extended f256)
	mantissa_2: u128,
}

//TODO: NODE FMA, FMS, FNMA, FNMS are implemented as combined operations
// where the round_to_precision call is performed later
// add for f = add => round
// fma = mul => add => round, flags are union of other flags?
impl HPFloat {
	pub fn classify(&self) -> FloatClassify {
		unimplemented!()
	}
	pub fn round_to_precision(&mut self, mode: RoundingMode) -> FloatFlags {
		unimplemented!()
	}
	pub fn add(&self, other: &HPFloat) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn sub(&self, other: &HPFloat) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn mul(&self, other: &HPFloat) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn div(&self, other: &HPFloat) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn max(&self, other: &HPFloat) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn min(&self, other: &HPFloat) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn sqrt(&self) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn inverse(&self) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn sin(&self) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn cos(&self) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
	pub fn tan(&self) -> (HPFloat,FloatFlags) {
		unimplemented!()
	}
}
use std::cmp::Ordering;
use smallvec::SmallVec;

pub mod cache;
pub use self::cache::{
	TypeIdx, TypeCache, TypeMetadata,  GlobalTypeData
};

pub mod features;
pub use self::features::{
	FuncABI, TargetFeatures
};

pub mod constant;
pub use self::constant::{
	ConstFloat, ConstInt
};

/// Size of an integer,
///  value: self.0+1
///  range: 1..=256
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct IntSize(u8);

impl IntSize {
	#[inline(always)]
	pub fn to_size(self) -> usize {
		self.0 as usize + 1usize
	}
}

/// Size of a float,
///  set of supported
///  implementations
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum FloatSize {
	/// IEEE Half Precision
	FLOAT16 = 0,
	/// Brain Floating Point
	BFLOAT16 = 1,
	/// IEEE Single Precision
	FLOAT32 = 2,
	/// IEEE Double Precision
	FLOAT64 = 3,
	/// X86 Extended Double Precision
	XFLOAT80 = 4,
	/// IEEE Quadruple Precision
	FLOAT128 = 5,
	/// IEEE Octuple Precision
	FLOAT256 = 6,
}

impl FloatSize {
	pub const fn exp_bits(self) -> usize {
		match self {
			FloatSize::FLOAT16 => 5,
			FloatSize::BFLOAT16 => 8,
			FloatSize::FLOAT32 => 8,
			FloatSize::FLOAT64 => 11,
			FloatSize::XFLOAT80 => 15,
			FloatSize::FLOAT128 => 15,
			FloatSize::FLOAT256 => 19,
		}
	}
	pub const fn mantissa_bits(self) -> usize {
		match self {
			FloatSize::FLOAT16 => 10,
			FloatSize::BFLOAT16 => 7,
			FloatSize::FLOAT32 => 23,
			FloatSize::FLOAT64 => 52,
			FloatSize::XFLOAT80 => 64,
			FloatSize::FLOAT128 => 112,
			FloatSize::FLOAT256 => 236,
		}
	}
	pub const fn total_bits(self) -> usize {
		match self {
			FloatSize::FLOAT16 => 16,
			FloatSize::BFLOAT16 => 16,
			FloatSize::FLOAT32 => 32,
			FloatSize::FLOAT64 => 64,
			FloatSize::XFLOAT80 => 80,
			FloatSize::FLOAT128 => 128,
			FloatSize::FLOAT256 => 256,
		}
	}
}

/// Size of a vector,
///  support: 1..=256
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct VectorSize(u8);

impl VectorSize {
	#[inline(always)]
	pub fn to_size(self) -> usize {
		self.0 as usize + 1usize
	}
	#[inline(always)]
	pub fn is_vector(self) -> bool {
		self.0 != 0
	}
	#[inline(always)]
	pub fn is_single(self) -> bool{
		self.0 == 0
	}
}

/// Type of dynamic vector
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum DynVectorSize {
	
	/// Fully dynamic at runtime,
	///  if not supported choose
	///  1 as only valid size
	RuntimeDynamic,
	
	/// Dynamic size at compile time,
	///  fixed at runtime, may
	///  have known minimum size.
	/// If not support choose 1
	///  as the only valid size
	CompileDynamic,
}

/// Smaller subset of types
///  that are valid types to
///  be held produced by
///  operations
pub enum RegType<'ctx> {
	
	/// Integer (vector) of known size
	///  at compile time
	FixedInt {
		size: IntSize,
		vec: VectorSize,
	},
	
	/// Dynamically sized vector of integers
	///  with unknown size at compile time
	DynInt {
		size: IntSize,
		vec: DynVectorSize,
	},
	
	/// Float (vector) of known size
	///  at compile time
	FixedFloat {
		size: FloatSize,
		vec: VectorSize,
	},
	
	/// Dynamically sized vector of floats
	///  with unknown size at compile time
	DynFloat {
		size: FloatSize,
		vec: DynVectorSize,
	},
	
	/// Pointer to a value
	Pointer {
		target: TypeIdx<'ctx>,//Box or Arc or Intern Index?
	},
	
	/// Evaluated condition,
	EvalFlag,
	
	/// Dynamically set float rounding mode configuration,
	///  consumed by some float operations
	DynFloatCfg,
	
	/// Dynamically set vector size configuration, TODO: {2xdyn needed or not??}
	///  may have more information than size. TODO: expand for RISC-V V
	DynVecCfg,
}

impl<'ctx> RegType<'ctx> {
	pub fn get_mem_size(&self, types: &GlobalTypeData<'ctx>) -> Option<u64> {
		match self {
			RegType::FixedInt {size, vec} => {
				let real_size = size.to_size();
				let pow_2_size = real_size.next_power_of_two();//[1,2,4,8,16,32,64,128,256]
				let result = pow_2_size.max(8) as u64;//[8,16,32,64,128,256]
				Some(result * vec.to_size() as u64)
			},
			RegType::FixedFloat{size, vec} => {
				unimplemented!() //if supported(32) { 32 } else { int32.size }
			},
			RegType::Pointer {target} => {
				let mem: &MemType = &types.cache[*target];
				Some(mem.get_size(types))
			},
			RegType::DynInt {..}
			| RegType::DynFloat {..}
			| RegType::EvalFlag
			| RegType::DynFloatCfg
			| RegType::DynVecCfg => None
		}
	}
	pub fn get_mem_align(&self, types: &GlobalTypeData<'ctx>) -> Option<u64> {
		match self {
			RegType::FixedInt {size, vec} => {
				let real_size = size.0 as usize;
				let idx_count = real_size.leading_zeros();// 0..=8
				let real_idx = idx_count.min(5);//0..=5
				//TODO: ARRAY_LOOKUP
				Some(match real_idx { //TODO: CONSIDER VECTOR SIZE AS WELL?!? {convert to pow 2 and merge?}
					0 => types.meta.align_i256,//TODO: CONVERT TO ARRAY_LOOKUP
					1 => types.meta.align_i128,
					2 => types.meta.align_i64,
					3 => types.meta.align_i32,
					4 => types.meta.align_i16,
					5 => types.meta.align_i8,
					_ => unreachable!()
				})
			},
			RegType::FixedFloat{size, vec} => {
				unimplemented!() //if supported(32) { 32 } else { int32.align }
			},
			RegType::Pointer {target} => {
				let mem: &MemType = &types.cache[*target];
				Some(mem.get_align(types))
			},
			RegType::DynInt {..}
			| RegType::DynFloat {..}
			| RegType::EvalFlag
			| RegType::DynFloatCfg
			| RegType::DynVecCfg => None
		}
	}
}

/// Type that can reside inside of memory,
///  includes all register types and
///  all complex and undefined types
pub enum MemType<'ctx> {
	RegisterType(RegType<'ctx>),
	UndefinedType {
		size: u64,
		align: u64,
	},
	StructType {
		size: u64,
		align: u64,
		types: Box<[(u64, TypeIdx<'ctx>)]>,
	},
	UnionType {
		size: u64,
		align: u64,
		types: Box<[TypeIdx<'ctx>]>,
	},
	ArrayType {
		base: TypeIdx<'ctx>,
		count: u64,
	},
	FunctionType {
		/// Can this function unwind, if true then this function
		///  must be invoked and cannot be called without undefined
		///  behaviour. TODO: always or on unwind? (e.g. unwind-never opt?)
		unwind: bool,
		
		/// Function ABI, how the arguments are passed to the function,
		///  setting or disabling target-features may change the registers
		///  used for passing arguments
		abi: FuncABI,
		
		/// Enabled Features, the set of target features that may be used to
		///  call this function, as well as used during code-generation,
		///  may add constraints on inlining TODO: per-block features?
		target_features: TargetFeatures,
		
		/// Set of arguments passed into the function
		///  for it to be invoked,
		arguments: Box<[TypeIdx<'ctx>]>,
		
		/// Set of return paths, idx => set of returned values
		///  this means both: multiple returned objects
		///                   multiple return locations
		///  are supported,
		return_paths: SmallVec<[Vec<TypeIdx<'ctx>>;1]>,
	},
}

impl<'ctx> MemType<'ctx> {
	pub fn get_size(&self, types: &GlobalTypeData<'ctx>) -> u64 {
		match self {
			MemType::RegisterType(base) => {
				base.get_mem_size(types).expect("MemType register value does not have size")
			},
			MemType::ArrayType { base, count} => {
				types.cache[*base].get_size(types) * count
			},
			MemType::UndefinedType { size, .. }
			| MemType::StructType { size, ..}
			| MemType::UnionType {size, .. } => {
				*size
			},
			MemType::FunctionType { .. } => panic!("MemType Function does not have a size")
		}
	}
	pub fn get_align(&self, types: &GlobalTypeData<'ctx>) -> u64 {
		match self {
			MemType::RegisterType(base) => {
				base.get_mem_align(types).expect("MemType register value does not have size")
			},
			MemType::ArrayType { base, ..} => {
				types.cache[*base].get_align(types)
			},
			MemType::UndefinedType { align, .. }
			| MemType::StructType { align, ..}
			| MemType::UnionType {align, .. } => {
				*align
			},
			MemType::FunctionType { .. } => panic!("MemType Function does no have an alignment")
		}
	}
}


#[cfg(test)]
mod tests {
	use super::*;
	
	fn test_float_size(f: FloatSize) {
		assert_eq!(1 + f.exp_bits() + f.mantissa_bits(),f.total_bits());
	}
	
	#[test]
	pub fn test_float_sizes() {
		test_float_size(FloatSize::FLOAT16);
		test_float_size(FloatSize::BFLOAT16);
		test_float_size(FloatSize::FLOAT32);
		test_float_size(FloatSize::FLOAT64);
		test_float_size(FloatSize::XFLOAT80);
		test_float_size(FloatSize::FLOAT128);
		test_float_size(FloatSize::FLOAT256);
	}
}
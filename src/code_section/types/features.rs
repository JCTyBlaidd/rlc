
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum FuncABI {
	CCall,
	FastCall,
	VectorCall,
}

/// Bitmap of target features:
///  64 discrete feature bits
///  meaning of each bit depends
///  on the compilation target
#[repr(transparent)]
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct TargetFeatures(u64);

impl Default for TargetFeatures {
	#[inline(always)]
	fn default() -> Self {
		TargetFeatures(0)
	}
}

impl TargetFeatures {
	#[inline(always)]
	pub fn enable_bit(&mut self, idx: u8) {
		self.0 = self.0 & (1 << idx);
	}
	#[inline(always)]
	pub fn disable_bit(&mut self, idx: u8) {
		self.0 = self.0 | !(1 << idx);
	}
	#[inline(always)]
	pub fn set_bit(&mut self, idx: u8, enable: bool) {
		if enable {
			self.enable_bit(idx)
		}else{
			self.disable_bit(idx)
		}
	}
	#[inline(always)]
	pub fn get_bit(&mut self, idx: u8) -> bool {
		(self.0 >> idx) != 0
	}
}
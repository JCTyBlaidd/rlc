use super::{
	IntType,
	FloatType,
	PointerType,
};

/// Describes Metadata for a block of code
///  such as a function declaration of
///  an if/switch/loop block,
///  contains metadata for constraints
///  to all input and output values
///  as well as pointer capture,
///  read, write, metadata,
///  argument = return,
///  global-memory: read,write,clobber
///  synchronization: read,write,none
///
pub struct OperationBlockMetadata {
	//FIXME: INDEX N/A?
	pub block_inputs: Vec<BlockInput>,
	pub block_outputs: (),
	pub global_memory: (),
	pub sync_access: (), /* Global and on a per ptr level?!? */
	pub relations: (),
}

pub enum BlockInput {
	IntArgInput(IntType,Vec<IntArgMetadata>),
	FloatArgInput(FloatType,Vec<FloatArgMetadata>),
	PtrArgInput(PointerType,PtrArgMetadata),
	FlagArgInput(FlagArgMetadata),
	ExceptArgInput(ExceptArgMetadata),
}

pub struct IntArgMetadata {
	pub zero_bits: u128,
	pub one_bits: u128,
	pub min_sign: Option<u128>,
	pub max_sign: Option<u128>,
	pub min_unsign: Option<u128>,
	pub max_unsign: Option<u128>,
}

pub struct FloatArgMetadata {

}

pub struct PtrArgMetadata {
	//Add Metadata Destructuring: ?!?
}

pub struct FlagArgMetadata {

}

pub struct ExceptArgMetadata {

}
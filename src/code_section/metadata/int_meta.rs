use super::NodeEntry;
use hashbrown::{HashMap, HashSet};

/// Store Integer Facts
///  to allow for optimizations
///  and inferences about integer
///  relations:
///
/// Stored State:
///  - 0bits
///  - 1bits
///  - power_of_two
///  - value-range
///  - relational (min/max?) {currently only constant bounds, but}
///  - equality   {A == B (with given offset) {k=1,n=0 special case of below}}
///  - inequality {A != B}
///  - A = kB + n {k,n const?}
pub struct IntegerMetadata {
	
	/// bit-mask of bits in a value that are guaranteed
	///  to be zero, masks of 0 provide no useful
	///  information and hence are not allowed
	zero_bits: HashMap<NodeEntry,u128>,
	
	/// bit-mask of bits in a value that are guaranteed
	///  to be one, masks of 0 provide no useful
	///  information and hence are not allowed
	one_bits: HashMap<NodeEntry,u128>,
	
	/// set of values that are guaranteed to be a power
	///  of two; i.e. in set {1,2,4,8,16,...}
	///  of can be expressed as 1 << k (unknown k)
	power_of_two: HashSet<NodeEntry>,
	
	/// VALUE > constant_bound (signed)
	signed_lower_bound: HashMap<NodeEntry,i128>,
	
	/// VALUE > constant_bound (unsigned)
	unsigned_lower_bound: HashMap<NodeEntry,u128>,
	
	/// VALUE < constant_bound (signed)
	signed_upper_bound: HashMap<NodeEntry,u128>,
	
	/// VALUE > constant_bound (unsigned)
	unsigned_upper_bound: HashMap<NodeEntry,i128>,
	
	/// VALUE = value (always):
	///   used number of bits depends on node metadata,
	///   guaranteed to be zero extended
	known_value: HashMap<NodeEntry,u128>,
	
	
	// FIXME ADD? HashMap(NodeEntry,NodeEntry,(u128,u128)> {signed & unsigned?? (not applicable?)}
	// FIXME ADD? HashSet not_equal (NodeEntry,NodeEntry)
	// FIXME ADD? SOME FORM OF EQUALITY SETS??
}

impl IntegerMetadata {
// Internal Update and Query Methods
	
	fn get_zero_mask(&self, id: NodeEntry) -> u128 {
		match self.zero_bits.get(&id) {
			Some(v) => *v,
			None => match self.known_value.get(&id) {
				Some(v) => !v,
				None => 0 /* Unknown Bits */
			}
		}
	}
	fn get_one_mask(&self, id: NodeEntry) -> u128 {
		match self.one_bits.get(&id) {
			Some(v) => *v,
			None => match self.known_value.get(&id) {
				Some(v) => *v,
				None => 0 /* Unknown Bits */
			}
		}
	}
	fn get_power_of_two(&self, id: NodeEntry) -> bool {
		if self.power_of_two.contains(&id) {
			true
		}else{
			match self.known_value.get(&id) {
				Some(v) => v.is_power_of_two(),
				None => false
			}
		}
	}
	fn get_signed_lower_bound(&self, id: NodeEntry) -> i128 {
		unimplemented!() /* Consider all bounds & constant {maybe bits as well?} */
	}
	fn get_unsigned_lower_bound(&self, id: NodeEntry) -> u128 {
		unimplemented!() /* Consider all bounds & constant {maybe bits as wel?} */
	}
	fn get_signed_upper_bound(&self, id: NodeEntry) -> i128 {
		unimplemented!() /* Consider all bounds & constant {maybe bits as well?} */
	}
	fn get_unsigned_upper_bound(&self, id: NodeEntry) -> u128 {
		unimplemented!() /* Consider all bounds & constant {maybe bits as well?} */
	}
	fn get_known_value(&self, id: NodeEntry) -> Option<u128> {
		self.known_value.get(&id).copied()
	}
	
	//FIXME:
	// Add Update Methods that guarantee invariants (update unrelated facts as well)
	//  with flag that notifies if unrelated fact has been updated a well?? (ret bool)
	
// External Update and Query Methods

	//Add Queries for comparisons => constant flags
	//Add Queries for numeric transformations (e.g. * to <<) (1->1 & 1->n & n->1)
}


/* COPIED FROM OLD


	//TODO: Add Facts Such As
	// min, max, align, 0bits, 1bits, relational, equality, A = kB + c, is_power_two(A)
	// add non-null facts,
	
	//TODO: add method for 2-directional wavefront style propagation of facts
	// e.g. Generate fact (A+B) after A and after B but if A+B is constrained
	//      then attempt to propagate backwards as far as changes occur, mark
	//      new wavefront start points and then restart the wavefront changes
	//      till new data has been updated
	//      *CONSTRAINTS: nowrap arithmetic, UB inside if/switch, assume, load/store => not-null
	
	// Metadata associated with outputs of all operations & in/out of function calls
	// and sub-blocks: if/switch/loop/call/invoke etc...
	
	//TODO: add queries for when an nowrap operation is always UB to it can be transmuted to UB
	//  operation and the super-callee trimmed
	
	//TODO: Add metadata for section e.g BLOCKS + FUNCTIONS:
	//  constraints of subset of passed pointers that are:
	//    read / write / capture(whole only)  {allow handling of pointers hidden in structs & behind pointers?}
	//                                        {how should ptrs in unions be handled? : (bag of bits per byte is_kinda_pointer) }
	//                                        {no-edit-global-state (rw global) etc..}
	//    always-return as well:
	//    add tranform to convert fn to no-return?!


	//FIXME: Add Additional State Query Lists for Other Optimizations
	
	//FIXME ADD:
	// query_is_power_two
	// query_get_one_bit_mask
	// query_get_two_bit_mask
	// query_get_... (for all facts stored above)
	
	//FIXME ADD:
	// method_of updating and calculating new constraint facts
	//

*/
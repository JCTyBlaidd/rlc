use hashbrown::{HashMap, HashSet};
use std::num::NonZeroU64;
use super::{
	NodeEntry, PtrRef, IntRef,
};
use crate::util::QueryTree;


#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum AliasResult {
	NeverAlias = 0,
	MaybeAlias = 1,
	AlwaysAlias = 2
}
//FIXME: Change AlwaysAlias to AlwaysAliasDynamic && AlwaysAliasKnownOffset(offset)
//        this allows for conversion of some aliasing transmutes while removing the
//        loads and stores (? or look into something similar)


/// Represents a pointer offset
///  where the ptr addresses a known
///  subset of the parent pointer,
///  with fixed or dynamic offset
///  data
enum PtrOffset {
	FixedOffset {
		offset: NonZeroU64,
		size: NonZeroU64,
	},
	DynamicOffset {
		stride: NonZeroU64,
		value: IntRef,
	},
}



/// Pointer Metadata:
///  Each pointer has an associated src pointer
///   which may be itself: derived via get_elem
///   array_access or union_derive operations
///   this information may provide aliasing
///   information.
///  Each src pointer has associated aliasing guarantees
///   with all other src pointers and can be use to rule
///   out any remaining aliasing information in certain cases.
pub struct PointerMetadata {
	ptr_to_logical_tree: QueryTree<PtrRef,PtrOffset,usize>,
	logical_aliasing: (),
	not_null_set: HashSet<PtrRef>,
}

//FIXME: Convert to QueryTree< PointerValue => LAllocSet > | LAllocSet Mapping


impl PointerMetadata {
	
	//query aliasing
	//query is_null (deref..)
	//query has_escaped_at: (..or similar..)
	
	/// Query if two memory pointer memory locations
	///  might, always or never alias the same location.
	///
	/// Optimization:
	///   Never => Remove Ordering Dependencies + Allow Store/Load Merging
	///   Always => Remove unnecessary stores and loads
	///   Maybe => Assume Nothing
	pub fn query_alias(&self, p1: PtrRef, p2: PtrRef) -> AliasResult {
		let res = self.ptr_to_logical_tree.perform_query(
			&p1,
			&p2,
			|_,content| {
				match content {
					Ok(
						PtrOffset::FixedOffset {
							offset,
							size
						}
					) => {
						//offset,size,last_dynamic,used_dynamic
						(offset.get(),size.get(),None,false)
					},
					Ok(
						PtrOffset::DynamicOffset {
							stride,
							value
						}
					) => {
						(0,0,Some((stride.get(),*value)),false)
					},
					Err(_) => {
						(0,0,None,false)
					}
				}
			},
			|content,old| {
				let (o,s,m,u) = old;
				match content {
					PtrOffset::FixedOffset {
						offset,
						size
					} => {
						if let Some((stride,value)) = m {
							unimplemented!()
						}else{
							unimplemented!()
						}
					},
					PtrOffset::DynamicOffset {
						stride,
						value
					} => {
						if let Some((stride,value)) = m {
							unimplemented!()
						}else{
							unimplemented!()
						}
					}
				}
			},
			|a,b,c| {
				AliasResult::MaybeAlias
			}
		);
		match res {
			Some(AliasResult::NeverAlias) => return AliasResult::NeverAlias,
			_ => unimplemented!()
		}
		
		AliasResult::MaybeAlias
	}
	
	//FIXME: Add Query Difference for AlwaysAlias (where one is known to exist)
	// (this allows finding the exact target location and maybe deriving the result
	//  of a load from a previous store, saving memory usage)
}
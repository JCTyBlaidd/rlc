use super::{IntType, FloatType};
use hashbrown::HashMap;
use std::num::NonZeroU8;

///All Required Information
/// about the compile target
/// and global translation
/// unit configuration
pub struct CompileMeta {
	
	/// Supported Integer Sizes
	///  with alignment, all sizes
	///  must be part of the set:
	///  {8,16,32,64,128} x {1,2,4,8,16,32}
	align_info_int: HashMap<IntType,u32>,
	
	/// Supported Integer Sizes
	///  with alignment, all sizes
	///  must be part of the set:
	///  {8,16,32,64,128} x {1,2,4,8,16,32}
	///  FIXME: FP80 ?!? (SPLIT INTERNAL OPS W/ LOAD AND STORE?)
	align_info_float: HashMap<FloatType, u32>,
	
	/// Must be a supported integer size
	///  present in align_info_int
	ptr_int_size: NonZeroU8,
	
}


impl CompileMeta {
	
	pub fn get_ptr_int(&self) -> IntType {
		IntType::from_size(self.ptr_int_size)
	}
	
}


//FIXME: Implement Display



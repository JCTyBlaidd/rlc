use super::ir::{
	IntType, FloatType, PointerType
};
use super::NodeEntry;

/* FIXME: Entry or Element */
pub type PtrRef = NodeEntry;
pub type IntRef = NodeEntry;
pub type FloatRef = NodeEntry;

pub mod int_meta;
pub mod float_meta;
pub mod ptr_meta;
pub mod block_meta;
pub mod func_decl_meta;
pub mod code_unit_meta;


pub use self::int_meta::IntegerMetadata;
pub use self::float_meta::FloatMetadata;
pub use self::ptr_meta::PointerMetadata;
pub use self::block_meta::OperationBlockMetadata;
pub use self::func_decl_meta::FunctionMetadata;
pub use code_unit_meta::CompileMeta;

pub struct OpGraphMetadata {
	pub int_meta: IntegerMetadata,
	pub float_meta: FloatMetadata,
	pub ptr_meta: PointerMetadata
}
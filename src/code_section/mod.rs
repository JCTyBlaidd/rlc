use hashbrown::HashMap;
use crate::util::arena::InvariantLifetime;

/// Global data definitions:
///  types global and thread local variables
pub mod globals;
pub use self::globals::{
	GlobalDecl, GlobalIdx, GlobalMap, GlobalData
};

/// Function definitions:
///  may contain a defined interior or be a stub
///  for external use.
pub mod func;
pub use self::func::{
	FuncDecl, FuncBox, FuncIdx, FuncMap,
	Block, BlockIdx, BlockMap,
	ValueIdx, OpIdx, OpMap, OpBlockMeta, StateMeta,
};

/// Operation definitions:
///   smallest slices of programs, TODO: INTRINSIC FUNCTIONS??
pub mod ops;
pub use self::ops::{
	Op
};

/// Type definitions:
///  cache for stored in memory complex types
///  and simpler subset for types that are valid
///  for storage in registers
pub mod types;
pub use self::types::{
	TypeIdx, TypeCache, MemType, RegType,
	IntSize, FloatSize, VectorSize, DynVectorSize,
	TypeMetadata, GlobalTypeData,
};

///////////////////////////////////OLD//////////////////////////////////////
/*

/// Describe Conversion of Representations to intermediate
///  string or binary formats, either for testing
///  or as part of the compilation process.
/// Also contains utilities to minimise any variance caused
///  by changes to any formats, (i.e. different ids)
#[macro_use]
pub mod io;


/// Contains information about the internal format used
///  to describe high-level abstract operations
pub mod ir;

/// Describes the format for the value state
///  dependence graph, with associated utility
///  methods
pub mod graph;




//TODO: Add Metadata as a separate section
pub mod metadata;

*/

/// Set of utilities - TODO: NEEDED?, myabe move some global util to here?
pub mod utils;

////////////////////////////////////////////////////////////////////////////////

///
/// Describes a section of functions,
///  types and global metadata to
///  be processed
///#[derive(Clone, Eq, PartialEq)]
///#[derive(Serialize,Deserialize)] //FIXME: DROP SERDE?
pub struct CodeSection<'ctx> {
	
	/// List of global definitions, mapping from abstract index
	///  to definition for static data
	///  or for definition of FUNCTION?? (YES/NO)?
	pub globals: GlobalMap<'ctx>,
	
	/// List of function definitions, may be externally implemented
	///  stubs or implemented functions.
	pub functions: FuncMap<'ctx>,
	
	/// Definition to global index map, defines how
	///  globals are exported externally
	pub exported_globals: HashMap<Box<str>,GlobalIdx<'ctx>>,
	
	/// Definition to function index map, defines how
	///  functions are exported externally
	pub exported_functions: HashMap<Box<str>,FuncIdx<'ctx>>,
	
	/// Metadata for the generation and storage of type
	///  information, contains the required alignment
	///  of base types and their sizes
	pub types: GlobalTypeData<'ctx>,
	
	/// Invariant lifetime reference to prevent deconstruction
	_ctx: InvariantLifetime<'ctx>
}

//#[derive(Clone, Eq, PartialEq)]
pub struct BoxSection(Box<CodeSection<'static>>);

impl BoxSection {
	/// Run a function with a unique but unknown invariant lifetime.
	///  this will ensure that the lifetime enforces any usages are
	///  unique to this code-section instance: Shared Reference
	pub fn with_ref<'r,R,F: for<'l> FnOnce(&'r CodeSection<'l>) -> R>(&'r self,f: F) -> R {
		let ptr = self.0.as_ref() as *const CodeSection;
		let life_ext_ptr: *const CodeSection = unsafe {std::mem::transmute(ptr)};
		f(unsafe { &*life_ext_ptr })
	}
	/// Same as [with_ref], but for mutable borrows instead
	pub fn with_mut<'r,R,F: for<'l> FnOnce(&'r mut CodeSection<'l>) -> R>(&'r mut self, f: F) -> R {
		let ptr = self.0.as_mut() as *mut CodeSection;
		let life_ext_ptr: *mut CodeSection = unsafe {std::mem::transmute(ptr)};
		f(unsafe { &mut *life_ext_ptr })
	}
}


//FIXME: IMPLEMENT!
//impl ConvertWithIR for CodeSection {}
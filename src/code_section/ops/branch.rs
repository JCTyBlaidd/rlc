use super::super::{
	TypeIdx,
	ValueIdx,
	IntSize,
	BlockIdx,
};
use hashbrown::HashMap;

/// Metadata for a switch statement,
///  this version implicitely contains control
///  flow, there is another variant similar to
///  conditional move, that lacks control
///  flow and it value only, TODO: ADD
pub struct SwitchMeta<'ctx,'fun> {
	
	/// The size of the integer being switched on
	pub int_size: IntSize,
	
	pub int_src: ValueIdx<'ctx,'fun>,
	
	/// The block that is returned to if none of the edges match,
	///  if this is None, this is the same as a block target that
	///  only contains unreachable
	pub default_block: Option<BlockIdx<'ctx,'fun>>,//TODO: LOOP_BACKEDGE_META?
	
	/// Inclusive range => block target mapping
	///  this is used to generate the switch table code, may convert to set of
	///  arithmetic operations before a range-free switch, or a branch
	pub switch_block: HashMap<(u8,u8/*TODO: CONSTANT_INT,CONSTANT_INT*/),BlockIdx<'ctx,'fun>>,
}


/// Metadata for the branch statement,
///  that chooses one of two sets of control
///  flow depending on a conditional evaluated
///  before, if control flow switching is not
///  needed, then a conditional move may be used
pub struct BranchMeta<'ctx,'fun> {
	pub branch_src: ValueIdx<'ctx,'fun>,
	
	pub true_block: BlockIdx<'ctx,'fun>, //TODO: LOOP_BACKEDGE_META?
	
	pub false_block: BlockIdx<'ctx,'fun>,
}
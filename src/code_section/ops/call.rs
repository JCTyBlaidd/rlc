use super::super::{
	TypeIdx, RegType,
};

pub struct CallOp<'ctx> {
	//pub arguments: Vec<TypeIdx<'ctx>>,
	//pub return_values: Vec<TypeIdx<'ctx>>,
	pub register_arg: Vec<RegType<'ctx>>,
	pub register_ret: Vec<RegType<'ctx>>,
	pub function_type: TypeIdx<'ctx>,
	//TODO: MULTI_RETURN
	//TODO: UNWIND
}
use super::{
	BlockIdx,
	ValueIdx,
};

pub mod asm;
pub use self::asm::InlineASM;

pub mod call;

pub mod branch;

/// Set of all valid operations
///  that may be stateful
///  with value arguments
#[repr(u16)] /* TODO: CHOOSE BEST ENUM INT TYPE */
pub enum Op<'ctx,'fun> {
	
	/// Effect-free operation
	///  that is used to represent
	///  deleted operations and
	///  so should rarely be referenced
	NoOp = 0,
	
//==============================================//
// Begin: Terminators                           //
//==============================================//
	
	/// Stateful operation that is undefined
	///  behaviour for control flow to reach
	///  also is a valid block terminator
	Unreachable = 1,
	
	/// Stateful operation that represents
	///  the termination of the enclosing
	///  function through an unwind edge
	ResumeUnwind = 2,
	
	/// Stateful operation that represents
	///  the termination of the enclosing
	///  function through the standard
	///  return edge
	ReturnValue{
		//Box<ValueIdx<''>>, returned, values
	} = 3,
	
	/// Stateful operation, unless the function
	///  is pure. Execute another function with
	///  some arguments, and calculate the result.
	/// May have multiple return points, but if
	///  so cannot be pure.
	/// Can be used as either a terminator
	///  or a normal stateful operation if it
	///  only has one return point and may
	///  be treated as a pure operation if
	///  marked as such.
	Call{
		//Box<CallMeta>
	} = 4,
	
	/// Stateful operation, even if pure due to
	///  execution ordering, must return the same
	///  values as the encapsulating function,
	///  is equivalent to calling a function
	///  and then instantly returning the
	///  result, with no intermediate operations.
	TailCall{ //TODO: MERGE, tail: bool?
		//Box<CallMeta>
	} = 5,
	
	
	Invoke {
		//Box<CallMeta>
	} = 6,
	
	TailInvoke{
		//Box<CallMeta>
	} = 7,
	
	Branch{
		switch_value: ValueIdx<'ctx,'fun>,
		true_block: (BlockIdx<'ctx,'fun>,Option<()/*Loop ID*/>),
		false_block: (),
	} = 8,

	SwitchInt{
		//Box<SwitchIntMeta>
	} = 9,

	InlineAssembly{
		//Box<InlineAssembly>
	} = 10,
//
// Begin: Stateful Operations
//

//TODO: CMOV AS AN OPERATION?

//
// Begin: Stateful Operations - Dynamic Rounding Mode Floats
//  TODO: set_rounding_mode, fake_read_round_values {a => b, state edge to add dependency for section}


//
// Begin: Stateful Operations - Dynamic Vector Length
//


//
// Begin: Pure Operations - Constants
//

//
// Begin: Pure Operations - Integer //TODO:{size: 1..=256, length: 1..=256 | N | DYN}
//

//TODO: GET_VEC_N_LEN (not dyn, N-runtime-constant)

//
// Begin: Pure Operations - Floats
//

//
// Begin: Pure Operations - Flags
//

//
// Begin: Pure Operations - Pointers
//

//TODO: PURE VECTOR OPERATIONS <i32 x 5> && <i32 x dyn> (requires phantom vec-size argument)
	A,B,C,
	//DEBUG(OpIdx<'ctx,'fun>)
}

impl<'ctx,'fun> Default for Op<'ctx,'fun> {
	#[inline(always)]
	fn default() -> Self {
		Op::NoOp
	}
}

impl<'ctx,'fun> Op<'ctx,'fun> {
	/// Get the operations id, so
	///  quick operations can be used to
	///  determine resultant values.
	pub fn get_discriminant(&self) -> u16 {
		use std::mem;
		//TODO: CONVERT TO COMPILE TIME CHECK
		if mem::size_of::<u64>() != mem::size_of::<mem::Discriminant<Op<'ctx,'fun>>>() {
			panic!("EPIC FAIL");
		}
		
		let des = mem::discriminant(self);
		let val : u64 = unsafe {
			mem::transmute(des)
		};
		val as u16
	}
	
	/// Is the operation a no op
	pub fn is_no_op(&self) -> bool {
		if let Op::NoOp = self {
			true
		}else{
			false
		}
	}
	
	/// Is the operations a valid terminator
	pub fn is_terminator(&self) -> bool {
		//TODO: USE CONSTANTS FOR RANGES & ADD TEST
		let x = self.get_discriminant();
		x >= 1 && x <= 10
	}
	
	///Inexact calculation for presence of
	///  side-effects, if this returns false
	///  then the function does not need a state
	///  edge and can be removed from the state
	///  ordering.
	pub fn is_stateful_fuzzy(&self) -> bool {
		//let x = self.get_discriminant();
		//x >= 1 && x <= 10 //TODO: UPDATE
		unimplemented!()
	}
	
	///Exact calculation for if the function
	///  has side-effects that effect the
	///  state, will return false on more
	///  operations than the fuzzy function,
	///  but is more expensive to calculate.
	pub fn is_stateful_exact(&self) -> bool {
		unimplemented!()
	}
	
	/// Return true if this operation will always
	///  return, used to determine the propagation
	///  of unreachable states.
	pub fn will_always_complete(&self) -> bool {
		unimplemented!()
	}
	
	//TODO: ret_op_count & arg_op_count as state cast calculations?
	//  e.g. get_descrimanenant() % 3 (or 5), except for complex ops?
}

//  - Unreachable
//  - Invoke
//  - Call
//  - Branch
//  - SwitchInt
//  - Return
//  - Unwind
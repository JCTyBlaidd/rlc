
use std::sync::Arc;
use std::ops::{Index, IndexMut};
use crate::util::arena::{
	InvariantLifetime,
	MarkedCollection,
	MarkedIndex,
	CollectionInsertError
};
use super::{
	BlockIdx, Op
};

/// Value produces metadata: STUB, TODO: implement in code_section::meta
pub struct OpValueMeta {

}

///Simple Statefree instructions (int, float w/o dynamic rounding mode)

///Stateful instructions (memory-access, float w/ dynamic rounding mode, atomics, functions, asm)

/// Index that references a single operations,
///  that may be stateful,
#[repr(transparent)]
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct OpIdx<'ctx,'fun>(MarkedIndex<'fun,u32,(Op<'ctx,'fun>,OpBlockMeta<'ctx,'fun>,Arc<OpValueMeta>)>);

/// Index that references a value produced by
///  an instruction
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct ValueIdx<'ctx,'fun>(OpIdx<'ctx,'fun>,u32);

///Stateful ordering metadata - used for the
///  total order of stateful operations
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct StateMeta<'ctx,'fun> {
	/// If the op is the initial instruction,
	///  then this will point to itself
	pub state_prev: OpIdx<'ctx,'fun>,
	/// If the op is the terminal instruction,
	///  then this will point to itself,
	pub state_next: OpIdx<'ctx,'fun>,
	/// Guaranteed to be strictly increasing
	///  in order
	pub state_idx: u32,
	
}

///Operation metadata about block placement
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct OpBlockMeta<'ctx,'fun> {
	/// Which block this instruction is part of,
	///  for the state ordering
	pub block_idx: BlockIdx<'ctx,'fun>,
	
	/// If the operation is stateful,
	///  then this represents the states
	///  ordering in the block
	pub state: Option<StateMeta<'ctx,'fun>>,
}

/// Operation Map:
///  Indexed Op with metadata and stateful
///  TODO: Consider
///    - Structure of Arrays (split state + meta into separate array)
///    - Potentially allowing 2x value op in the same space as a state op
///    - Either<[ValueOp;2],StateOp> {might be more memory efficient}
pub struct OpMap<'ctx,'fun> {
	ops: MarkedCollection<'fun,u32,Op<'ctx,'fun>>,
	states: MarkedCollection<'fun,u32,OpBlockMeta<'ctx,'fun>>,
	metas: MarkedCollection<'fun,u32,Arc<OpValueMeta>>
}
impl<'ctx,'fun> OpMap<'ctx,'fun> {
	/// Safety: ensure that ctx will be referenced in a unique manner,
	///  static lifetimes are unsafe, they must be some unique
	///  lifetime
	pub unsafe fn new() -> OpMap<'ctx,'fun> {
		OpMap {
			ops: MarkedCollection::new(),
			states: MarkedCollection::new(),
			metas: MarkedCollection::new()
		}
	}
	
	pub unsafe fn clear(&mut self) {
		self.ops.clear();
		self.states.clear();
		self.metas.clear();
	}
	
	pub fn add_obj(
		&mut self, op: Op<'ctx,'fun>,
		state: OpBlockMeta<'ctx,'fun>, meta: Arc<OpValueMeta>
	) -> Result<OpIdx<'ctx,'fun>,CollectionInsertError> {
		let idx1 = self.ops.reserve_capacity()?;
		let idx2 = self.states.reserve_capacity()?;
		let idx3 = self.metas.reserve_capacity()?;
		if cfg!(debug_assertions) {
			debug_assert_eq!(idx1,idx2);
			debug_assert_eq!(idx2,idx3);
		}
		self.ops.add_obj_raw(op);
		self.states.add_obj_raw(state);
		self.metas.add_obj_raw(meta);
		unsafe {
			//Safety: Values should be equal by invariant
			// so index is guaranteed to be safe
			Ok(OpIdx(MarkedIndex::from_value(idx3)))
		}
	}
	
	pub fn get(
		&self, index: OpIdx<'ctx,'fun>
	) -> (&Op<'ctx,'fun>,&OpBlockMeta<'ctx,'fun>,&Arc<OpValueMeta>) {
		unsafe {
			///SAFETY: safe by fun context binding
			/// unsafety just required allowing multiple
			///  index types
			let val = index.0.into_value();
			let ref1 = self.ops.raw_index(val);
			let ref2 = self.states.raw_index(val);
			let ref3 = self.metas.raw_index(val);
			(ref1,ref2,ref3)
		}
	}
	pub fn get_mut(
		&mut self, index: OpIdx<'ctx,'fun>
	) -> (&mut Op<'ctx,'fun>,&mut OpBlockMeta<'ctx,'fun>,&mut Arc<OpValueMeta>) {
		unsafe {
			///SAFETY: safe by fun context binding
			/// unsafety just required allowing multiple
			///  index types
			let val = index.0.into_value();
			let ref1 = self.ops.raw_index_mut(val);
			let ref2 = self.states.raw_index_mut(val);
			let ref3 = self.metas.raw_index_mut(val);
			(ref1,ref2,ref3)
		}
	}
}
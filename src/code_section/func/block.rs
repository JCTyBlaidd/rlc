use std::sync::Arc;
use std::ops::{Index, IndexMut};
use hashbrown::{HashSet, HashMap};
use crate::util::arena::{
	InvariantLifetime,
	MarkedCollection,
	MarkedIndex,
	CollectionInsertError
};
use super::{
	OpIdx,
	ValueIdx
};

#[repr(transparent)]
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct BlockIdx<'ctx,'fun>(MarkedIndex<'fun,u32,Block<'ctx,'fun>>);

pub struct Block<'ctx,'fun> {//TODO: CONSTANTS SEPARATELY??, TODO: PER-BLOCk-TARGET_FEATURES
	pub alias_meta: &'fun (),
	pub compare_meta: &'ctx (),
	
	/// Set of phi nodes that load their values
	///  depending on the previous block in
	///  control flow
	pub phi_values: HashMap<OpIdx<'ctx,'fun>,HashMap<BlockIdx<'ctx,'fun>,OpIdx<'ctx,'fun>>>,
	
	/// Set of remap nodes that load their values
	///  from a common previous block in the control
	///  flow graph (TODO: rules regarding loops - should not depend on loop iteration?)
	///  TODO: this initial rules have been specified in common_previous_blocks
	pub remap_values: HashMap<OpIdx<'ctx,'fun>,(BlockIdx<'ctx,'fun>,OpIdx<'ctx,'fun>)>,
	
	/// Set of values in this block that have their
	///  values used in one or more successor blocks
	///  and hence cannot be removed.
	pub consumed_values: HashMap<OpIdx<'ctx,'fun>,HashSet<BlockIdx<'ctx,'fun>>>,
	
	/// Values produces by this block that are consumed
	///  by future blocks
	pub exported_values: HashSet<ValueIdx<'ctx,'fun>>,
	
	/// The first stateful operation inside this block,
	///  may be the same as last_state_op
	pub first_state_op: OpIdx<'ctx,'fun>,
	
	/// The last stateful operation inside this block
	///  may be the same as first_state_op.
	/// Must describe a valid terminator:
	///  - Unreachable
	///  - Invoke
	///  - Call
	///  - Branch
	///  - SwitchInt
	///  - Return
	///  - Unwind
	pub last_state_op: OpIdx<'ctx,'fun>,
	
	/// Set of operations that are part of this block
	///  of execution, should remain equal to
	///  the OpBlockMeta::block_idx of the instruction
	///  being used.
	pub block_ops: HashSet<OpIdx<'ctx,'fun>>,
	
	/// Is this node an initial block in a control
	///  flow loop, and if so the loop it is part of
	pub loop_start: Option<()>,
	
	/// The set of loop ids that this block is part
	///  of the inner control flow
	pub loop_inner: HashSet<()>,
	
	/// The set of blocks that control flow can come from
	///  to the current loop, therefore the blocks terminator
	///  must specify this block as a potential destination.
	/// The loop-id value is set if this block is a back-edge
	///  for a loop, TODO: is specify needed, only 1 or none loop-ids possible?!
	pub previous_blocks: HashMap<BlockIdx<'ctx,'fun>,Option<()>>,
	
	/// The set of blocks that have been unconditionally visited previously
	///  in execution, all paths of execution to this block must visit
	///  this block some time in the path previously, and non of the
	///  edges of this path can be back-edges.
	/// This specifies the set of blocks that are valid as sources
	///  for remap_values
	pub common_previous_blocks: HashSet<BlockIdx<'ctx,'fun>>,
}


pub struct BlockMap<'ctx,'fun>(MarkedCollection<'fun,u32,Block<'ctx,'fun>>);
impl<'ctx,'fun> BlockMap<'ctx,'fun> {
	/// Safety: ensure that ctx will be referenced in a unique manner,
	///  static lifetimes are unsafe, they must be some unique
	///  lifetime
	pub unsafe fn new() -> BlockMap<'ctx,'fun> {
		BlockMap(MarkedCollection::new())
	}
	
	pub unsafe fn clear(&mut self) {
		self.0.clear();
	}
	
	pub fn add_obj(&mut self, data: Block<'ctx,'fun>) -> Result<BlockIdx<'ctx,'fun>,CollectionInsertError> {
		self.0.add_obj(data).map(BlockIdx)
	}
}
impl<'ctx,'fun> Index<BlockIdx<'ctx,'fun>> for BlockMap<'ctx,'fun> {
	type Output = Block<'ctx,'fun>;
	#[inline]
	fn index(&self, index: BlockIdx<'ctx,'fun>) -> &Block<'ctx,'fun> {
		self.0.index(index.0)
	}
}
impl<'ctx,'fun> IndexMut<BlockIdx<'ctx,'fun>> for BlockMap<'ctx,'fun> {
	#[inline]
	fn index_mut(&mut self, index: BlockIdx<'ctx,'fun>) -> &mut Block<'ctx,'fun> {
		self.0.index_mut(index.0)
	}
}
use std::sync::Arc;
use std::ops::{Index, IndexMut};
use hashbrown::HashSet;
use crate::util::arena::{
	InvariantLifetime,
	MarkedCollection,
	MarkedIndex,
	CollectionInsertError
};
use super::Op;

/// Instruction Block Segments,
///  define sequence of linear operations
///  with simple control flow and associated
///  metadata, ends with terminating statement
pub mod block;
pub use self::block::{
	Block, BlockIdx, BlockMap
};

/// Instruction performed inside a block
pub mod instruction;
pub use self::instruction::{
	StateMeta, OpBlockMeta, ValueIdx, OpIdx, OpMap
};

pub struct FuncMap<'ctx>(MarkedCollection<'ctx,u32,FuncBox<'ctx>>);
impl<'ctx> FuncMap<'ctx> {
	/// Safety: ensure that ctx will be referenced in a unique manner,
	///  static lifetimes are unsafe, they must be some unique
	///  lifetime
	pub unsafe fn new() -> FuncMap<'ctx> {
		FuncMap(MarkedCollection::new())
	}
	
	pub unsafe fn clear(&mut self) {
		self.0.clear();
	}
	
	pub fn add_obj(&mut self, data: FuncBox<'ctx>) -> Result<FuncIdx<'ctx>,CollectionInsertError> {
		self.0.add_obj(data).map(FuncIdx)
	}
}
impl<'ctx> Index<FuncIdx<'ctx>> for FuncMap<'ctx> {
	type Output = FuncBox<'ctx>;
	#[inline]
	fn index(&self, index: FuncIdx<'ctx>) -> &FuncBox<'ctx> {
		self.0.index(index.0)
	}
}
impl<'ctx> IndexMut<FuncIdx<'ctx>> for FuncMap<'ctx> {
	#[inline]
	fn index_mut(&mut self, index: FuncIdx<'ctx>) -> &mut FuncBox<'ctx> {
		self.0.index_mut(index.0)
	}
}

#[repr(transparent)]
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct FuncIdx<'ctx>(MarkedIndex<'ctx,u32,FuncBox<'ctx>>);



pub struct FuncBox<'ctx>(Box<FuncDecl<'static,'static>>,InvariantLifetime<'ctx>);

impl<'ctx> FuncBox<'ctx> {
	/// Run a function with a unique but unknown invariant lifetime.
	///  this will ensure that the lifetime enforces any usages are
	///  unique to this code-section instance: Shared Reference
	pub fn with_ref<'r,R,F: for<'l> FnOnce(&'r FuncDecl<'ctx,'l>) -> R>(&'r self,f: F) -> R {
		let ptr = self.0.as_ref() as *const FuncDecl;
		let life_ext_ptr: *const FuncDecl = unsafe {std::mem::transmute(ptr)};
		f(unsafe { &*life_ext_ptr })
	}
	/// Same as [with_ref], but for mutable borrows instead
	pub fn with_mut<'r,R,F: for<'l> FnOnce(&'r mut FuncDecl<'ctx,'l>) -> R>(&'r mut self, f: F) -> R {
		let ptr = self.0.as_mut() as *mut FuncDecl;
		let life_ext_ptr: *mut FuncDecl = unsafe {std::mem::transmute(ptr)};
		f(unsafe { &mut *life_ext_ptr })
	}
}


pub struct FuncDecl<'ctx,'fun> {
	pub func_type: (),
	
	/// Function Metadata:
	///  valid inputs {i.e. does not result in undefined behaviour: num | alias | relation}
	///  valid outputs {constraints on return values}
	///  variable usage:
	///   used/ptr-read/ptr-write/...: {constraints on ptr-write?}
	///   global-memory
	///  may-diverge (will-return)
	///  other-flags
	///  privacy
	pub func_metadata: (),
	
	
	//TODO: ENABLED_FEATURES (BITFIELD_MEANING IS ISA DEPENDANT)
	//TODO: ADDRESS_SPACE_FEATURES: (address_space[x].enable_inavlid (nullptr))
	
	/// Internal function implementation
	pub func_contents: Option<Func<'ctx,'fun>>,
	
	/// Invariant global context
	__global: InvariantLifetime<'ctx>,
	
	/// Invariant function lifetime
	__function: InvariantLifetime<'fun>,
}

pub struct Func<'ctx,'fun> {
	pub ops: OpMap<'ctx,'fun>,
	
	//pub relation_info: (), //(ValueIdx,ValueIdx) -> RELATION
	
	pub blocks: BlockMap<'ctx,'fun>,
	
	pub init_block: BlockIdx<'ctx,'fun>,
	
	pub last_blocks: HashSet<BlockIdx<'ctx,'fun>>,
	
	//pub block_data: (), //any metadata?? (between block relations??)
}




//
// Old Definitions that have not been processed yet
//
/*

use bitflags::bitflags;
use std::fmt::{Display, Formatter, Result as FmtResult};
use super::graph::OperationGraph;
use super::ir::OpNode;
use super::metadata::{OpGraphMetadata, FunctionMetadata};



bitflags! {
	/* Flags for Function Decl: 0 is assumed to be the default */
	pub struct FunctionFlags: u64 {
		/* Function never returns */
		const NO_RETURN = 1 << 0;
		/* Function never throws an exception */
		const NO_EXCEPT = 1 << 1;
		/* Function never calls itself */
		const NO_RECURSE = 1 << 2;
		/* Function may return twice */
		const RETURN_TWICE = 1 << 3;
		/* Function should be always inline */
		const INLINE_ALWAYS = 1 << 4;
		/* Function recommended for inlining */
		const INLINE_HINT = 1 << 5;
		/* Function should never be inline */
		const INLINE_NEVER = 1 << 6;
		/* Address 0 is to be treated as valid */
		const ALLOW_NULLPTR = 1 << 7;
		/* Function will be never called on the hot path */
		const COLD = 1 << 8;
		/* Function is internal and TODO: REASONING? */
		const INTERNAL = 1 << 9;
	}
}

impl Default for FunctionFlags {
	fn default() -> Self {
		FunctionFlags::empty()
	}
}
impl Display for FunctionFlags {
	fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
		let mut write_or = false;
		let mut flag = |flag: FunctionFlags, name: &str| -> FmtResult {
			if self.contains(flag) {
				if write_or {
					write!(f," | ")?;
				}else{
					write_or = true;
				}
				write!(f,"{}",name)?;
			}
			Ok(())
		};
		flag(FunctionFlags::NO_RETURN,"no-return")?;
		flag(FunctionFlags::NO_EXCEPT,"no-except")?;
		flag(FunctionFlags::NO_RECURSE,"no-recurse")?;
		flag(FunctionFlags::RETURN_TWICE,"return-twice")?;
		flag(FunctionFlags::INLINE_ALWAYS,"inline-always")?;
		flag(FunctionFlags::INLINE_HINT,"inline-hint")?;
		flag(FunctionFlags::INLINE_NEVER,"inline-never")?;
		flag(FunctionFlags::ALLOW_NULLPTR,"allow-nullptr")?;
		flag(FunctionFlags::COLD,"cold")?;
		flag(FunctionFlags::INTERNAL,"internal")?;
		Ok(())
	}
}


/// Represents a function declaration
///  with flags and metadata regarding arguments
///  and return types.
/// May contain code-gen or may be stub
pub struct FunctionDecl {
	
	/// Simple Flags for Function Compilation Configuration
	pub flags : FunctionFlags,
	
	/// Optional OpGraph that represents the contents
	///  of the function declaration
	pub operations: Option<(OperationGraph<OpNode>,OpGraphMetadata)>,
	
	/// Function metadata:
	///  describes input and output types
	///  and any optimization metadata regarding
	///  facts for any of the input or output
	///  arguments with destructuring of output
	///  struct to multiple simple values
	pub metadata: FunctionMetadata, //FIXME: PROBALY DEPRECATE?!?
	
	//FIXME: ADD CALLING CONVENTION {ISA + ISA-MODS}
	//FIXME: OPTIMIZATION-MODE: (String => Configurable)
	//FIXME:
	
	//FIXME: IDEA (AND FOR IR) TREAT RETURN VALUE AS A SPECIAL POINTER
	//         EVEN FOR (A+B) Return Evaluations
	//   ALT:
	//     Store Complex Return Type (Ptr/Structure/Union)
	//     Find Method of Constructing in Agregate
	//     ?special return_ptr type?? {fake pointers to allow for optimization}
	//     {with minimal aliasing - allows for better optimizations and easier threading to}
	//     {a returned structure in the case that doing so is required for the ABI}
	//     {need to ensure that ptr-to-int is not a valid operation}
	
	//TODO: Additional Compilations Modifiers (+isa allowed)
	//TODO: Optimization Mode for the Function
	//TODO: ReturnArgAlways(idx), GlobalMemoryClobber(R/W) {detailed list of globals?}
	//TODO: Add All Applicable Flags for Function Usage
	//TODO: Add Calling Convention Data
	//TODO: Search LLVM Mode
	//TODO: MAYBE ALLOW MARK AS ALLOC/REALLOC/FREE (w/ ptr ref) to allow for mem-usage-opt
	
	//Add Metadata FOR ALIASING AND CONSTRAINTS
	
	//ADD OpBlockList: InternalCode
}

impl FunctionDecl {
	pub fn validate_function_decl() -> Result<(),String> {
		//Perform Validation of Function Contents
		Ok(())
	}
}


impl Display for FunctionDecl {
	fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
		writeln!(f,"{{")?;
		if self.flags != FunctionFlags::default() {
			writeln!(f,"\tflags = {}", self.flags)?;
		}
		//FIXME: OUTPUT METADATA
		if let Some((ops,meta)) = &self.operations {
			//FIXME: WRITE OPERATIONS OUTPUT
		}
		writeln!(f,"}}")
	}
}








/*
use super::*;
use hashbrown::{
	HashMap,
	hash_map::Entry
};
use super::utils::RwLock;
use std::io::{
	Result as IOResult,
	Write
};


mod ir_convert;

pub mod metadata;
pub use self::metadata::{
	FuncMetadata,
	InlineMode,
	OptimizerMode,
	MemoryAccess
};

#[derive(Serialize, Deserialize)]
#[derive(Clone, Eq, PartialEq)]
pub struct FunctionDecls {
	data: HashMap<String, FunctionDef>,
}

impl FunctionDecls {
	
	/// Create a new function definition map
	pub fn new() -> FunctionDecls {
		FunctionDecls {
			data: HashMap::new(),
		}
	}
	
	/// Attempt to request the function with given name
	///  will return none if the function does not exist
	pub fn get_func(&self, name: &str) -> Option<&FunctionDef> {
		self.data.get(name)
	}
	
	/// Attempt to remove the given function from the declaration
	///  will return the functions declaration if successful
	pub fn delete_func(&mut self, name: &str) -> Option<FunctionDef> {
		self.data.remove(name)
	}
	
	/// Attempt to add function
	///  will return None if successful
	///  otherwise will return [decl]
	pub fn add_func(&mut self, name: &str, decl: FunctionDef) -> Option<FunctionDef> {
		match self.data.entry(name.to_owned()) {
			Entry::Occupied(_) => Some(decl),
			Entry::Vacant(entry) => {
				entry.insert(decl);
				None
			},
		}
	}
}



#[derive(Serialize,Deserialize)]
#[derive(Clone, Eq, PartialEq)]
pub struct FunctionDef {
	pub arguments: Vec<SimpleDataType>,
	pub return_type: Option<SimpleDataType>,
	pub meta: RwLock<FuncMetadata>,
	pub code: Option<RwLock<FuncCodeGen>>
}



#[derive(Clone, Eq, PartialEq, Default, Debug)]
#[derive(Serialize,Deserialize)]
pub struct FuncCodeGen {
	pub nodes: OpNodeGroup,
	pub value_edges: ValueDependenceMap,
	pub state_edges: StateDependenceMap,
}

*/

*/
use std::collections::VecDeque;
use std::fmt::{Formatter, Result as FmtResult, Arguments};

mod lexer;
mod lexer_gen; //FIXME: REWORK LEXER_GEN TO USE PERFECT_HASH_FUNCTION


pub struct PrettyFmt<'r,'a> {
	fmt: &'r mut Formatter<'a>,
	padding: usize,
}

static PADDING_LINE: &'static str = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
const PADDING_LINE_LEN: usize = 16; //FIXME: PADDING_LINE.len()

impl<'r,'a> PrettyFmt<'r,'a> {
	
	
	#[inline(always)]
	pub fn new(fmt: &'r mut Formatter<'a>, padding: usize) -> PrettyFmt<'r,'a> {
		PrettyFmt {
			fmt,
			padding
		}
	}
	
	pub fn write_padding(&mut self) -> FmtResult {
		let mut count = self.padding;
		while count > PADDING_LINE_LEN {
			self.fmt.write_str(PADDING_LINE)?;
			count -= PADDING_LINE_LEN;
		}
		self.fmt.write_str(&PADDING_LINE[..count])
	}
	
	pub fn pad_and_write(&mut self, content: &str) -> FmtResult {
		self.write_padding()?;
		self.fmt.write_str(content)
	}
	
	pub fn pad_and_fmt(&mut self, args: Arguments) -> FmtResult {
		self.write_padding()?;
		self.fmt.write_fmt(args)
	}
	
	pub fn get_inc_padding<'s>(&'s mut self) -> PrettyFmt<'s,'a> where 'r: 's {
		PrettyFmt {
			fmt: self.fmt,
			padding: self.padding + 1
		}
	}
}

/*
pub struct OpTreeBuilder<'a> {
	last_used_id: usize,
	id_data: VecDeque<(usize,&'a dyn OpTreeElement)>,
}

impl<'a> OpTreeBuilder<'a> {
	pub fn fmt_tree(
		fmt: &mut Formatter,
		block: &'a dyn OpTreeElement
	) -> FmtResult {
		let mut builder = OpTreeBuilder {
			last_used_id: 0,
			id_data: VecDeque::new()
		};
		builder.id_data.push_front((0,block));
		while let Some((id,block)) = builder.id_data.pop_back() {
			block.write_data(id,fmt,&mut builder)?;
		}
		Ok(())
	}
	pub fn schedule_block(&mut self, block: &'a dyn OpTreeElement) -> usize {
		let id = self.last_used_id + 1;
		self.last_used_id = id;
		self.id_data.push_back((id,block));
		id
	}
}

pub trait OpTreeElement {
	fn write_data(&self,id: usize, f: &mut Formatter, b: &mut OpTreeBuilder) -> FmtResult;
}
*/

/*

use bincode::{
	serialize,
	serialize_into,
	deserialize,
	deserialize_from,
	Error as BinError
};
use serde::{
	Serialize,
	Deserialize
};
use std;
use std::io::{
	Write,
	Read,
	Error as IoError,
};

#[macro_use]
mod formatted_macros;
mod formatted_io;
pub use self::formatted_io::{
	ConvertWithIR,
	StringIrInput,
	NamedBracketMetadata,
};

/// Testing Function now also tests
///  serde + bincode serialization
#[cfg(test)]
pub fn test_ir_conversion<T>(obj: &T, ir: &str)
	where
		T: ConvertWithIR + PartialEq + std::fmt::Debug,
		T: for<'a> Deserialize<'a> + Serialize {
	self::formatted_io::test_ir_conversion(obj,ir);
	let vec = to_binary_vector(obj).unwrap();
	let new_obj = from_binary_bytes(&vec).unwrap();
	assert_eq!(obj,&new_obj);
}

#[derive(Debug)]
pub enum Error {
	BinaryError(BinError),
	IoError(IoError),
	CustomError(String),
}

pub fn to_formatted_string<T: ConvertWithIR>(t: &T) -> Result<String,Error> {
	match t.as_ir_string() {
		Ok(v) => Ok(v),
		Err(e) => Err(Error::IoError(e))
	}
}

pub fn to_binary_writer<T: Serialize + ?Sized, W: Write>(w: W, t: &T) -> Result<(),Error> {
	match serialize_into(w,t) {
		Ok(()) => Ok(()),
		Err(e) => Err(Error::BinaryError(e))
	}
}

pub fn to_binary_vector<T: Serialize + ?Sized>(t: &T) -> Result<Vec<u8>,Error> {
	match serialize(t) {
		Ok(vec) => Ok(vec),
		Err(e) => Err(Error::BinaryError(e))
	}
}

pub fn from_formatted_string<T: ConvertWithIR>(s: &str) -> Result<T,Error> {
	match T::from_ir_str_complete(s) {
		Ok(v) => Ok(v),
		Err(e) => Err(Error::CustomError(e))
	}
}

pub fn from_binary_reader<T, R: Read>(r: R) -> Result<T,Error>
	where
			for<'a> T: Deserialize<'a> {
	match deserialize_from(r) {
		Ok(t) => Ok(t),
		Err(e) => Err(Error::BinaryError(e))
	}
}

pub fn from_binary_bytes<T>(bytes: &[u8]) -> Result<T,Error>
	where
			for<'a> T: Deserialize<'a> {
	match deserialize(bytes) {
		Ok(t) => Ok(t),
		Err(e) => Err(Error::BinaryError(e))
	}
}

*/
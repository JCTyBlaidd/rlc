use std::str::{CharIndices, FromStr};
use std::num::NonZeroU8;
use super::lexer_gen;

trait ParseUnsigned: Sized + Copy + std::fmt::Debug {
	fn checked_add(self, v: Self) -> Option<Self>;
	fn checked_mul(self, v: Self) -> Option<Self>;
	fn from_char(v: char) -> Self;
	const ZERO : Self;
	const TEN : Self;
}
impl ParseUnsigned for u8 {
	fn checked_add(self, v: Self) -> Option<Self> {
		self.checked_add(v)
	}
	fn checked_mul(self, v: Self) -> Option<Self> {
		self.checked_mul(v)
	}
	fn from_char(v: char) -> Self {
		(v as u32 - '0' as u32) as u8
	}
	const ZERO : u8 = 0;
	const TEN : u8 = 10;
}
impl ParseUnsigned for u16 {
	fn checked_add(self, v: Self) -> Option<Self> {
		self.checked_add(v)
	}
	fn checked_mul(self, v: Self) -> Option<Self> {
		self.checked_mul(v)
	}
	fn from_char(v: char) -> Self {
		(v as u32 - '0' as u32) as u16
	}
	const ZERO : u16 = 0;
	const TEN : u16 = 10;
}
impl ParseUnsigned for u64 {
	fn checked_add(self, v: Self) -> Option<Self> {
		self.checked_add(v)
	}
	fn checked_mul(self, v: Self) -> Option<Self> {
		self.checked_mul(v)
	}
	fn from_char(v: char) -> Self {
		(v as u64 - '0' as u64)
	}
	const ZERO : u64 = 0;
	const TEN : u64 = 10;
}
impl ParseUnsigned for u128 {
	fn checked_add(self, v: Self) -> Option<Self> {
		self.checked_add(v)
	}
	fn checked_mul(self, v: Self) -> Option<Self> {
		self.checked_mul(v)
	}
	fn from_char(v: char) -> Self {
		(v as u32 - '0' as u32) as u128
	}
	const ZERO :u128 = 0;
	const TEN : u128 = 10;
}


#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum Tok<'i> {
	TokNewLine,                       // "\n"
	TokLSect,                         // "{"
	TokRSect,                         // "}"
	TokLSquare,                       // "["
	TokRSquare,                       // "]"
	TokLBracket,                      // "("
	TokRBracket,				      // ")"
	TokLCorner,                       // "<"
	TokRCorner,                       // ">"
	TokEquals,                        // "="
	TokComma,                         // ","
	TokPercentNum(u64),               // "%<n>"
	TokBangNum(u64),                  // "!<n>"
	TokDollarNum1(u64),               // "$<n1>"
	TokDollarNum2(u64,u8),            // "$<n1>:<n2>"
	
	TokBangBracket,                   // "!["
	TokHashBracket,                   // "#["
	
	TokConstSmallUnsigned(NonZeroU8), //small unsigned constant
	TokConstUnsigned(u128),           //unsigned constant
	TokConstSigned(i128),             //signed constant
	TokConstFloatVal(bool,u128,i16),  //floating point constant (sign,mantissa,exp)
	TokConstFloatInf(bool),           //floating point infinity
	TokConstFloatNaN(bool,u128),      //floating point not a number
	
	TokAtName(&'i str),               // "@<name>"
	
	TokTypeSignedInt(NonZeroU8),      // "i<bits>"  0 < bits <= 128
	TokTypeUnsignedInt(NonZeroU8),    // "u<bits>"  0 < bits <= 128
	TokTypeFloating(NonZeroU8),       // "f<bits>"  0 < bits <= 128
	
	TokKeywordX,			          // "x"
	TokLexedKeyword(lexer_gen::LexKeyTok),
}

pub struct Lexer<'input> {
	chars: CharIndices<'input>,
	peek: Option<Option<(usize,char)>>,
	peek_slice: &'input str,
}

impl<'input> Lexer<'input> {
	pub fn new(input: &'input str) -> Self {
		Lexer {
			chars: input.char_indices(),
			peek: None,
			peek_slice: ""
		}
	}
	
	#[inline(always)]
	pub fn next_char(&mut self) -> Option<(usize, char)> {
		match self.peek.take() {
			Some(v) => v,
			None => {
				self.peek_slice = self.chars.as_str();
				self.chars.next()
			},
		}
	}
	
	#[inline(always)]
	pub fn peek_char(&mut self) -> Option<(usize, char)> {
		let iter = &mut self.chars;
		let peek_slice = &mut self.peek_slice;
		self.peek.get_or_insert_with(|| {
			*peek_slice = iter.as_str();
			iter.next()
		}).clone()
	}
	
	#[inline(always)]
	pub fn get_current_slice(&self) -> &'input str {
		self.peek_slice
	}
	
	#[inline(always)]
	pub fn consume_peek(&mut self) {
		self.peek = None;
	}
	
	fn try_parse_unsigned<U: ParseUnsigned>(&mut self) -> Result<Option<(U, usize)>, usize> {
		if let Some((start_loc, start_char)) = self.peek_char() {
			if start_char >= '0' && start_char <= '9' {
				self.consume_peek();
				self.finish_parse_unsigned::<U>(start_loc,start_char)
			} else {
				Ok(None)
			}
		} else {
			Ok(None)
		}
	}
	
	fn finish_parse_unsigned<U: ParseUnsigned>(
		&mut self,
		start_loc: usize,
		start_char: char
	) -> Result<Option<(U,usize)>, usize> {
		if start_char == '0' {
			//Validate only single 0
			if let Some((next_loc, next_char)) = self.peek_char() {
				if next_char >= '0' && next_char <= '9' {
					Err(start_loc)
				} else {
					Ok(Some((U::ZERO, next_loc)))
				}
			} else {
				Ok(Some((U::ZERO, start_loc + 1)))
			}
		} else {
			let mut counter = U::from_char(start_char);
			let mut final_loc = start_loc + 1;
			loop {
				if let Some((next_loc, next_char)) = self.peek_char() {
					if next_char >= '0' && next_char <= '9' {
						self.consume_peek();
						let next_digit = U::from_char(next_char);
						counter = counter.checked_mul(U::TEN).ok_or(next_loc)?
							.checked_add(next_digit).ok_or(next_loc)?;
						final_loc = next_loc + 1;
					} else {
						break;
					}
				}else{
					break;
				}
			}
			Ok(Some((counter, final_loc)))
		}
	}
	
	pub fn match_slice(&mut self, mut last_loc: usize, slice: &[char]) -> Result<usize,usize> {
		for val in slice {
			match self.next_char() {
				Some((loc,value)) => {
					if value != *val {
						return Err(loc);
					}else{
						last_loc = loc;
					}
				},
				None => return Err(last_loc)
			}
		}
		Ok(last_loc)
	}
	
	pub fn peek_not_keyword(&mut self) -> bool {
		match self.peek_char() {
			Some((_,b)) => match b {
				'a'..='z' | 'A'..='Z' | '_' | '.' | '0'..='9' | '$' | '%' | '&' => false,
				_ => true
			}
			_ => true
		}
	}
	
	//Parse float up to f128
	// Mantissa: 112bit
	// Exponent: 15bit
	fn parse_float(
		&mut self,
		sign: bool,
		mantissa: u128,
		idx: usize,
		curr_loc: usize
	) -> Option<Result<(usize,Tok<'input>,usize),usize>> {
		if mantissa > 0xfff {
			return Some(Err(curr_loc))
		}
		let exp_sign = match self.next_char() {
			Some((_,'+')) => true,
			Some((_,'-')) => false,
			_ => return Some(Err(curr_loc))
		};
		match self.try_parse_unsigned::<u16>() {
			Ok(Some((value2, end_loc))) => {
				if value2 & 0x7FFF == value2 {
					//FIXME: INTO BIT-TWIDDLE?
					let exp = if exp_sign {
						value2 as i16
					} else {
						-(value2 as i16)
					};
					//FIXME: ADD CONSTRAINTS FOR 128bit float
					// FIXME: CORRECT OFFSET CONSTRAINTS!?!
					Some(Ok(
						(idx,
						 Tok::TokConstFloatVal(true, mantissa, exp),
						 end_loc)
					))
				} else {
					Some(Err(end_loc))
				}
			},
			Ok(None) => Some(Err(curr_loc)),
			Err(idx) => Some(Err(idx))
		}
	}
}

//   op parse planning
//   $1 = op.int.add u32 ($1, $2:4 !323) #[$3, $56] !423

impl<'input> Iterator for Lexer<'input> {
	type Item = Result<(usize,Tok<'input>,usize),usize>;
	
	fn next(&mut self) -> Option<Result<(usize,Tok<'input>,usize),usize>> {
		const INF_SLICE : &[char] = &['i','n','f'];
		while let Some((idx,val)) = self.next_char() {
			match val {
				' ' | '\t' | '\r' => () /* SKIP */,
				
				'\n' => return Some(Ok((idx,Tok::TokNewLine,idx+1))),
				'[' => return Some(Ok((idx,Tok::TokLSquare,idx+1))),
				']' => return Some(Ok((idx,Tok::TokRSquare,idx+1))),
				'{' => return Some(Ok((idx,Tok::TokLSect,idx+1))),
				'}' => return Some(Ok((idx,Tok::TokRSect,idx+1))),
				'(' => return Some(Ok((idx,Tok::TokLBracket,idx+1))),
				')' => return Some(Ok((idx,Tok::TokRBracket,idx+1))),
				'<' => return Some(Ok((idx,Tok::TokLCorner,idx+1))),
				'>' => return Some(Ok((idx,Tok::TokRCorner,idx+1))),
				'=' => return Some(Ok((idx,Tok::TokEquals,idx+1))),
				',' => return Some(Ok((idx,Tok::TokComma,idx+1))),
				
				'!' => {
					//Parse ![, or !number
					match self.try_parse_unsigned::<u64>() {
						Ok(Some((num,loc))) => {
							return Some(Ok((idx,Tok::TokBangNum(num),loc)))
						},
						Ok(None) => {
							match self.next_char() {
								Some((_,'[')) => return Some(Ok((idx,Tok::TokBangBracket,idx+2))),
								_ => return Some(Err(idx))
							}
						}
						Err(loc) => return Some(Err(loc))
					}
				},
				'%' => {
					//Parse %number
					match self.try_parse_unsigned::<u64>() {
						Ok(Some((num,loc))) => {
							return Some(Ok((idx,Tok::TokPercentNum(num),loc)))
						},
						_ => return Some(Err(idx))
					}
				}
				'$' => {
					//Parse $number  or $number:another_number
					match self.try_parse_unsigned::<u64>() {
						Ok(Some((num1,loc1))) => {
							if let Some((_,':')) = self.peek_char() {
								self.consume_peek();
								match self.try_parse_unsigned::<u8>() {
									Ok(Some((num2,loc2))) => {
										return Some(Ok(
											(idx,
											 Tok::TokDollarNum2(num1,num2),
											 loc2)
										))
									},
									_ => return Some(Err(loc1))
								}
							}else{
								return Some(Ok((idx,Tok::TokDollarNum1(num1),loc1)))
							}
						},
						_ => return Some(Err(idx))
					}
				},
				'#' => {
					//Parse #[
					match self.next_char() {
						Some((_,'[')) => return Some(Ok((idx,Tok::TokHashBracket,idx+2))),
						_ => return Some(Err(idx))
					}
				},
				'@' => {
					//Parse @string_name_of_function
					
					//as_str valid, peek=None since previous was next_char()
					let str_slice = self.chars.as_str();
					let mut counter = 0;
					loop {
						if let Some((_,val)) = self.peek_char() {
							match val {
								'a'..='z' | 'A'..='Z' | '_' | '.' | '0'..='9' | '$' | '%' | '&' => {
									self.consume_peek();
									counter += 1;
								},
								_ => break
							}
						}else{
							break
						}
					}
					if counter == 0 {
						return Some(Err(idx))
					}else{
						return Some(Ok((idx,Tok::TokAtName(&str_slice[..counter]),idx+counter+1)))
					}
				},
				
				
				val @ '0'..='9' => {
					//Parse Number (signed or unsigned up to 128)
					match self.finish_parse_unsigned::<u128>(idx,val) {
						Ok(Some((value,loc))) => {
							if value <= 255 {
								if let Some(small_value) = NonZeroU8::new(value as u8) {
									return Some(Ok((idx, Tok::TokConstSmallUnsigned(small_value), loc)))
								}
							}
							return Some(Ok((idx, Tok::TokConstUnsigned(value), loc)))
						},
						_ => return Some(Err(idx))
					}
				},
				'+' => {
					//See Below, however this is unambiguously a floating point number
					// +0.0, +inf, +<float>  (+Nan)??
					//alt f[323284732823] or similar
					//alt +1232353445366346455e2352937583
					// F128 = 1Sign + 15bit expr + fraction(112 bit)
					match self.try_parse_unsigned::<u128>() {
						Ok(Some((value,loc))) => {
							match self.next_char() {
								Some((loc2,'e')) => {
									return self.parse_float(true,value,idx,loc);
								},
								_ => return Some(Err(loc))
							}
						},
						Ok(None) => {
							//+inf or Error
							match self.match_slice(idx,INF_SLICE) {
								Ok(v) => return Some(Ok((idx,Tok::TokConstFloatInf(true),v))),
								Err(v) => return Some(Err(v))
							}
							//FIXME: match +Nan[42323] (num = mantissa bits)
						},
						Err(idx) => return Some(Err(idx))
					}
				},
				'-' => {
					//Parse Float (Manditory signedness) up to 128 bit
					//FP: MANTISSA * exp(EXP)
					//Only Accept Integer Mantissa and exponent - Format Conversion Performed Later
					match self.try_parse_unsigned::<u128>() {
						Ok(Some((value, loc))) => {
							match self.peek_char() {
								Some((_,'e')) => {
									self.consume_peek();
									return self.parse_float(false,value,idx,loc);
								},
								_ => if value <= std::i128::MAX as u128 {
									return Some(Ok((idx,Tok::TokConstSigned(-(value as i128)),loc)))
								}else{
									return Some(Err(idx))
								}
							}
						},
						Ok(None) => {
							//-inf or Error
							match self.match_slice(idx,INF_SLICE) {
								Ok(v) => return Some(Ok((idx,Tok::TokConstFloatInf(false),v))),
								Err(v) => return Some(Err(v))
							}
							//FIXME: match -Nan[42323] (num = mantissa bits)
						},
						Err(idx) => return Some(Err(idx))
					}
					
					
					//FIXME: HOW NaN as seperate constant?!?!
					//FIXME: Only accept exact mantissas? (faster - no disambigulation)
					//FIXME: See Lexical-Core Crate
				}
				v @ 'a'..='z' | v @ 'A'..='Z' | v @ '_' => {
					//Slice for keyword matching
					let start_slice = self.get_current_slice();
					
					//Forward Numeric Parse (if not int/float value)
					if v == 'i' || v == 'u' || v == 'f' {
						match self.try_parse_unsigned::<u8>() {
							Ok(Some((num,loc))) => {
								if num > 128 {
									return Some(Err(idx))
								}
								if let Some(num_zero) = NonZeroU8::new(num) {
									let num_token = if v == 'i' {
										Tok::TokTypeSignedInt(num_zero)
									}else if v == 'u' {
										Tok::TokTypeUnsignedInt(num_zero)
									}else {
										Tok::TokTypeFloating(num_zero)
									};
									return Some(Ok((idx,num_token,loc)))
								}else{
									return Some(Err(idx))
								}
							},
							Ok(None) => () /* Not a number - is keyword */,
							Err(loc) => return Some(Err(loc))
						}
					}
					if v == 'x' && self.peek_not_keyword() {
						return Some(Ok((idx,Tok::TokKeywordX,idx+1)));
					}
					
					// Match slice
					let mut char_count = 1;
					while !self.peek_not_keyword() {
						self.consume_peek();
						char_count += 1;
					}
					let new_slice = &start_slice[..char_count];
					return match lexer_gen::match_str(new_slice) {
						Some(tok) => Some(Ok((idx,tok,idx+char_count))),
						None => Some(Err(idx))
					};
				}
				
				//FIXME: parse int up to 128   (i32) or (u32)
				//FIXME: parse float up to 128    (f32)
				//FIXME: Call Node parse
				//FIXME: parse !32, ![, $32, $32:4, %32
				//FIXME: lexer_gen must parse 'x' (for vec), 'inf', '+inf, '-inf', '-0.0' ,
				_ => return Some(Err(idx))
			}
		}
		None
	}
}

#[cfg(test)]
pub use self::tests::perform_test;

#[cfg(test)]
pub(super) mod tests {
	use super::*;
	
	pub fn perform_test(input: &str, output: Tok) {
		{
			let mut lexer = Lexer::new(input);
			let (s,res1,f) = lexer.next().unwrap().unwrap();
			assert_eq!(res1,output);
			assert_eq!(s,0);
			assert_eq!(f,input.len());
			let res2 = lexer.next();
			assert!(res2.is_none());
		}
		{
			let mut new_str = " ".to_string();
			new_str += input;
			let mut lexer = Lexer::new(new_str.as_str());
			assert_eq!(lexer.next_char(),Some((0,' ')));
			assert!(lexer.peek_char().is_some());
			let (s,res1,f) = lexer.next().unwrap().unwrap();
			assert_eq!(res1,output);
			assert_eq!(s,1);
			assert_eq!(f,1 + input.len());
			let res2 = lexer.next();
			assert!(res2.is_none());
		}
		{
			let mut new_str = "   ".to_string();
			new_str += input;
			new_str += "   ";
			let mut lexer = Lexer::new(new_str.as_str());
			let (s,res1,f) = lexer.next().unwrap().unwrap();
			assert_eq!(res1,output);
			assert_eq!(s,3);
			assert_eq!(f,3 + input.len());
			let res2 = lexer.next();
			assert!(res2.is_none());
		}
		{
			let mut new_str = " ]".to_string();
			new_str += input;
			new_str += "] ";
			let mut lexer = Lexer::new(new_str.as_str());
			let res1 = lexer.next().unwrap().unwrap().1;
			assert_eq!(res1,Tok::TokRSquare);
			let res2 = lexer.next().unwrap().unwrap().1;
			assert_eq!(res2,output);
			let res3 = lexer.next().unwrap().unwrap().1;
			assert_eq!(res3,Tok::TokRSquare);
			let res4 = lexer.next();
			assert!(res4.is_none());
		}{
			let mut new_str = " ]  ".to_string();
			new_str += input;
			new_str += "  ] ";
			let mut lexer = Lexer::new(new_str.as_str());
			let res1 = lexer.next().unwrap().unwrap().1;
			assert_eq!(res1,Tok::TokRSquare);
			let res2 = lexer.next().unwrap().unwrap().1;
			assert_eq!(res2,output);
			let res3 = lexer.next().unwrap().unwrap().1;
			assert_eq!(res3,Tok::TokRSquare);
			let res4 = lexer.next();
			assert!(res4.is_none());
		}
	}
	
	#[test]
	pub fn test_lex_chars() {
		perform_test("\n",Tok::TokNewLine);
		perform_test("{",Tok::TokLSect);
		perform_test("}",Tok::TokRSect);
		perform_test("[",Tok::TokLSquare);
		perform_test("]",Tok::TokRSquare);
		perform_test("(",Tok::TokLBracket);
		perform_test(")",Tok::TokRBracket);
		perform_test("<",Tok::TokLCorner);
		perform_test(">",Tok::TokRCorner);
		perform_test("=",Tok::TokEquals);
		perform_test(",",Tok::TokComma);
		perform_test("![",Tok::TokBangBracket);
		perform_test("#[",Tok::TokHashBracket);
		perform_test("x",Tok::TokKeywordX);
		perform_test("@hello.awesome",Tok::TokAtName("hello.awesome"));
	}
	
	#[test]
	pub fn test_lex_numbered_idents() {
		perform_test("%42",Tok::TokPercentNum(42));
		perform_test("!42",Tok::TokBangNum(42));
		perform_test("$42",Tok::TokDollarNum1(42));
		perform_test("$42:69",Tok::TokDollarNum2(42,69));
	}
	
	#[test]
	pub fn test_lex_types() {
		perform_test("u1",Tok::TokTypeUnsignedInt(NonZeroU8::new(1).unwrap()));
		perform_test("i1",Tok::TokTypeSignedInt(NonZeroU8::new(1).unwrap()));
		
		perform_test("u8",Tok::TokTypeUnsignedInt(NonZeroU8::new(8).unwrap()));
		perform_test("i8",Tok::TokTypeSignedInt(NonZeroU8::new(8).unwrap()));
		perform_test("f8",Tok::TokTypeFloating(NonZeroU8::new(8).unwrap()));
		perform_test("u16",Tok::TokTypeUnsignedInt(NonZeroU8::new(16).unwrap()));
		perform_test("i16",Tok::TokTypeSignedInt(NonZeroU8::new(16).unwrap()));
		perform_test("f16",Tok::TokTypeFloating(NonZeroU8::new(16).unwrap()));
		perform_test("u32",Tok::TokTypeUnsignedInt(NonZeroU8::new(32).unwrap()));
		perform_test("i32",Tok::TokTypeSignedInt(NonZeroU8::new(32).unwrap()));
		perform_test("f32",Tok::TokTypeFloating(NonZeroU8::new(32).unwrap()));
		perform_test("u64",Tok::TokTypeUnsignedInt(NonZeroU8::new(64).unwrap()));
		perform_test("i64",Tok::TokTypeSignedInt(NonZeroU8::new(64).unwrap()));
		perform_test("f64",Tok::TokTypeFloating(NonZeroU8::new(64).unwrap()));
		perform_test("u128",Tok::TokTypeUnsignedInt(NonZeroU8::new(128).unwrap()));
		perform_test("i128",Tok::TokTypeSignedInt(NonZeroU8::new(128).unwrap()));
		perform_test("f128",Tok::TokTypeFloating(NonZeroU8::new(128).unwrap()));
		
		//"i129", "f0" ("f1"? add minimum fp size?) etc. => FAIL
	}
	
	#[test]
	pub fn test_lex_const_integer() {
		perform_test("0",Tok::TokConstUnsigned(0));
		//perform_test("-0")
		perform_test("123",Tok::TokConstSmallUnsigned(NonZeroU8::new(123).unwrap()));
		perform_test("1234",Tok::TokConstUnsigned(1234));
		perform_test("-123",Tok::TokConstSigned(-123));
	}
	
}

#[macro_export]
macro_rules! impl_convert_ir_struct{
	{pub struct $main:ident {
		$first_field:ident : $first_type:ident,
		$(
			$field:ident : $field_type:ident
		),*
	}} => {
		impl ConvertWithIR for $main {
			fn build_ir<W: Write>(&self, prefix: &str, w: &mut W) -> IOResult<()> {
				write!(w,"{}", prefix)?;
				ConvertWithIR::as_ir(&self.$first_field, w)?;
				$(
					write!(w,".")?;
					ConvertWithIR::as_ir(&self.$field, w)?;
				)*
				Ok(())
			}
			fn from_ir<S: StringIrInput>(input: S) -> IResult<S,Self> {
				map!(
					input,
					tuple!(
						call!($first_type::from_ir),
						$(
							char!('.'),
							call!($field_type::from_ir)
						),*
					),
					|( $first_field, $(_,$field),* )| {
						$main {
							$first_field,
							$(
								$field
							),*
						}
					}
				)
			}
		}
	}
}

#[macro_export]
macro_rules! impl_convert_ir_enum{
	{pub enum $main:ident {
		$(
			$variant:ident($($inner:ident),*) => $name:expr $(;$other:expr)*
		),+
	}} => {
		impl ConvertWithIR for $main {
			fn build_ir<W: Write>(&self, prefix: &str, w: &mut W) -> IOResult<()> {
				write!(w, "{}", prefix)?;
				match self {
					$(
						impl_convert_ir_enum!(v1 >> v2 >> v3 >> $main >> $variant( $($inner),*)) => {
							let _tmp = impl_convert_ir_enum!(v1 >> v2 >> v3 >> w >>
														$variant( $($inner),*) -> ($name $(;$other)?));
							_tmp
						}
					),+
				}
			}
			fn from_ir<S: StringIrInput>(input: S) -> IResult<S,Self> {
				alt!(
					input,
					$(
						impl_convert_ir_enum!(<< $variant($($inner),*) -> ($name $(;$other)?)) => {
							impl_convert_ir_enum!($main << $variant($($inner),*))
						}
					)|+
				)
			}
		}
	};
	
	//Zero Variable Variant
	($a:ident >> $b:ident >> $c:ident >> $main:ident >> $variant:ident() ) => (
		$main::$variant
	);
	($main:ident << $variant:ident()) => (
		|_| {
			$main::$variant
		}
	);
	
	//Zero Variant Name Representation
	($a:ident >> $b:ident >> $c:ident >> $w:ident >> $variant:ident() -> ($name:expr) ) => ({
		write!($w,"{}",$name)
	});
	($forward:ident, << $variant:ident() -> ($name:expr)) => (
		tag!(
			$forward,
			$name
		)
	);
	
	//Single Variable Variant
	($a:ident >> $b:ident >> $c:ident >> $main:ident >> $variant:ident($inner:ident) ) => (
		$main::$variant($a)
	);
	($main:ident << $variant:ident($inner:ident)) => (
		|(_,val)| {
			$main::$variant(val)
		}
	);
	
	//Single . Expansion Representation
	($a:ident >> $b:ident >> $c:ident >> $w:ident >> $variant:ident($inner:ident) -> ($name:expr) ) => ({
		write!($w,"{}",concat!($name,"."))?;
		ConvertWithIR::as_ir($a,$w)
	});
	($forward:ident, << $variant:ident($inner:ident) -> ($name:expr)) => (
		tuple!(
			$forward,
			tag!(concat!($name,".")),
			call!($inner::from_ir)
		)
	);
	
	//Single Variant Forward Representation
	($a:ident >> $b:ident >> $c:ident >> $w:ident >> $variant:ident($inner:ident) -> ($name:expr;$ignore:expr) ) => ({
		ConvertWithIR::as_ir($a,$w)
	});
	($forward:ident, << $variant:ident($inner:ident) -> ($name:expr;$ignore:expr)) => (
		tuple!(
			$forward,
			value!(()),
			call!($inner::from_ir)
		)
	);
	
	//Double Variable Variant
	($a:ident >> $b:ident >> $c:ident >> $main:ident >> $variant:ident($t1:ident,$t2:ident) ) => (
		$main::$variant($a,$b)
	);
	($main:ident << $variant:ident($t1:ident,$t2:ident)) => (
		|(_,_,val1,_,_,_,val2,_,_)| {
			$main::$variant(val1,val2)
		}
	);
	
	//Double Match Double reversed . expansion
	($a:ident >> $b:ident >> $c:ident >> $w:ident >> $variant:ident($t1:ident,$t2:ident) -> ($name:expr;$ignore:expr) ) => ({
		write!($w,"{}",concat!($name,"."))?;
		ConvertWithIR::as_ir($b,$w)?;
		write!($w,".")?;
		ConvertWithIR::as_ir($a,$w)
	});
	($forward:ident, << $variant:ident($t1:ident,$t2:ident) -> ($name:expr;$ignore:expr) ) => (
		map!(
			$forward,
			tuple!(
				tag!(concat!($name,".")),
				call!($t2::from_ir),
				char!('.'),
				call!($t1::from_ir)
			),
			|(_,val2,_,val1)| { ((),(),val1,(),(),(),val2,(),()) }
		)
	);
	
	//Double Match Standard Brackets
	($a:ident >> $b:ident >> $c:ident >> $w:ident >> $variant:ident($t1:ident,$t2:ident) -> ($name:expr) ) => ({
		write!($w,"{}",concat!($name,"("))?;
		ConvertWithIR::as_ir($a,$w)?;
		write!($w,", ")?;
		ConvertWithIR::as_ir($b,$w)?;
		write!($w,")")
	});
	($forward:ident, << $variant:ident($t1:ident,$t2:ident) -> ($name:expr) ) => (
		tuple!(
			$forward,
			tag!(concat!($name,"(")),
			call!(space0),
			call!($t1::from_ir),
			call!(space0),
			char!(','),
			call!(space0),
			call!($t2::from_ir),
			call!(space0),
			char!(')')
		)
	);
	
	//Triple Variable Variant
	($a:ident >> $b:ident >> $c:ident >> $main:ident >> $variant:ident($t1:ident,$t2:ident,$t3:ident) ) => (
		$main::$variant($a,$b,$c)
	);
	($main:ident << $variant:ident($t1:ident,$t2:ident,$t3:ident)) => (
		|(_,_,val1,_,_,_,val2,_,__,val3,_,_)| {
			$main::$variant(val1,val2,val3)
		}
	);
	
	//Triple Match Standard Brackets
	($a:ident >> $b:ident >> $c:ident >> $w:ident >> $variant:ident($t1:ident,$t2:ident,$t3:ident) -> ($name:expr) ) => ({
		write!($w,"{}",concat!($name,"("))?;
		ConvertWithIR::as_ir($a,$w)?;
		write!($w,", ")?;
		ConvertWithIR::as_ir($b,$w)?;
		write!($w,", ")?;
		ConvertWithIR::as_ir($c,$w)?;
		write!($w,")")
	});
	($forward:ident, << $variant:ident($t1:ident,$t2:ident,$t3:ident) -> ($name:expr) ) => (
		tuple!(
			$forward,
			tag!(concat!($name,"(")),
			call!(space0),
			call!($t1::from_ir),
			call!(space0),
			char!(','),
			call!(space0),
			call!($t2::from_ir),
			call!(space0),
			char!(','),
			call!(space0),
			call!($t3::from_ir),
			call!(space0),
			char!(')')
		)
	);
}
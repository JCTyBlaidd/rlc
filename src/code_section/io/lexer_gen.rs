//! Auto-Generated Code: rlc_generator::ir
//!  do not modify directly
//!  edit generator instead

use super::{
	lexer::{Lexer, Tok, Tok::TokLexedKeyword}
};
use phf::{Map, Slice};

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum LexKeyTok {
	LTokFpDotEq,
	LTokPtrDotUnionSpec,
	LTokOpsFalse,
	LTokConvDotFptouiDotSat,
	LTokIntDotNeq,
	LTokAtomicDotStoreDotSeqCst,
	LTokPtrDotStructElem,
	LTokFpDotGt,
	LTokTarget,
	LTokPasses,
	LTokIntDotSubDotSat,
	LTokIntDotDivRem,
	LTokPtrDotArrayElem,
	LTokFunctions,
	LTokFpDotAbs,
	LTokIntDotCttz,
	LTokIntDotMul,
	LTokAtomicDotLoadDotAcquire,
	LTokCode,
	LTokOpsIf,
	LTokFpDotExt,
	LTokScopeDotOutput,
	LTokPtrDotStoreDotVolatileDotUnAlign,
	LTokAtomicDotStoreDotRelease,
	LTokPtrDotLoadDotVolatileDotUnAlign,
	LTokConvDotUtofp,
	LTokAtomicDotLoadDotRelaxed,
	LTokIntDotSubDotOver,
	LTokOpDotBlackbox,
	LTokIntDotSub,
	LTokLifetimeDotStart,
	LTokIntDotBitreverse,
	LTokFlagDotGetint,
	LTokPtrDotStore,
	LTokIntDotAddDotOver,
	LTokIntDotAbs,
	LTokPtrDotLoad,
	LTokFlowDotLoop,
	LTokIntDotMax,
	LTokInduction,
	LTokPtrDotStoreDotUnAlign,
	LTokPtrDotUnionSpecDotInbounds,
	LTokOpDotAssume,
	LTokIntDotCtpop,
	LTokOpsUnwind,
	LTokFpDotNeg,
	LTokIntDotAnd,
	LTokIntDotMulDotWrap,
	LTokOps,
	LTokGlobals,
	LTokIntDotLshr,
	LTokFpDotMax,
	LTokFlowDotSwitch,
	LTokIntDotRotl,
	LTokIntDotTrunc,
	LTokIntDotShl,
	LTokIntDotAddDotWrap,
	LTokFpDotRem,
	LTokIntDotEq,
	LTokFpDotDiv,
	LTokMetadata,
	LTokFpDotMul,
	LTokPtrDotArrayElemDotNoWrap,
	LTokOpsReturn,
	LTokConvDotFptosiDotSat,
	LTokFlowDotBranch,
	LTokIntDotAdd,
	LTokConvDotItofp,
	LTokIntDotAshr,
	LTokConvDotIbitstofp,
	LTokOpsDotNone,
	LTokIntDotGt,
	LTokFpDotSub,
	LTokLifetimeDotFinish,
	LTokPtrDotStructElemDotInbounds,
	LTokOutputs,
	LTokIntDotZext,
	LTokInputs,
	LTokAtomicDotLoadDotSeqCst,
	LTokFpDotTrunc,
	LTokIntDotMulDotOver,
	LTokIntDotRotr,
	LTokFlowDotInvoke,
	LTokConvDotFptoui,
	LTokIntDotNot,
	LTokFpDotMin,
	LTokScopeDotInput,
	LTokConfig,
	LTokConvDotFptosi,
	LTokAlias,
	LTokPtrDotPsub,
	LTokIntDotSubDotWrap,
	LTokIntDotLt,
	LTokFpDotLt,
	LTokIntDotSext,
	LTokTypes,
	LTokPtrDotStructElemDotNoWrap,
	LTokConvDotFpbitstoi,
	LTokFpDotAdd,
	LTokFpDotNeq,
	LTokPtrDotLoadDotUnAlign,
	LTokPtrDotArrayElemDotInbounds,
	LTokAtomicDotStoreDotRelaxed,
	LTokIntDotXor,
	LTokIntDotCtlz,
	LTokIntDotAddDotSat,
	LTokOpsDefault,
	LTokPtrDotStoreDotVolatile,
	LTokIntDotBswap,
	LTokOpsTrue,
	LTokOpDotAllocLocal,
	LTokPtrDotLoadDotVolatile,
	LTokFlowDotCall,
}


pub fn match_str<'input>(v: &str) -> Option<Tok<'input>> {
	match LEXER_MAP.get(v) {
		Some(v) => Some(TokLexedKeyword(*v)),
		None => None
	}
}

static LEXER_MAP : Map<&'static str,LexKeyTok> = Map {
    key: 3213172566270843353,
    disps: Slice::Static(&[
        (0, 2),
        (0, 0),
        (6, 66),
        (0, 92),
        (16, 4),
        (0, 0),
        (0, 67),
        (0, 50),
        (1, 38),
        (2, 2),
        (0, 8),
        (2, 38),
        (0, 4),
        (0, 40),
        (0, 2),
        (1, 14),
        (0, 14),
        (0, 16),
        (0, 35),
        (3, 99),
        (12, 100),
        (1, 59),
        (1, 101),
    ]),
    entries: Slice::Static(&[
        ("induction", LexKeyTok::LTokInduction),
        ("int.shl", LexKeyTok::LTokIntDotShl),
        ("int.and", LexKeyTok::LTokIntDotAnd),
        ("int.sub", LexKeyTok::LTokIntDotSub),
        ("passes", LexKeyTok::LTokPasses),
        ("globals", LexKeyTok::LTokGlobals),
        ("int.ashr", LexKeyTok::LTokIntDotAshr),
        ("int.rotr", LexKeyTok::LTokIntDotRotr),
        ("ptr.load", LexKeyTok::LTokPtrDotLoad),
        ("metadata", LexKeyTok::LTokMetadata),
        ("int.ctpop", LexKeyTok::LTokIntDotCtpop),
        ("fp.sub", LexKeyTok::LTokFpDotSub),
        ("int.trunc", LexKeyTok::LTokIntDotTrunc),
        ("int.sub.wrap", LexKeyTok::LTokIntDotSubDotWrap),
        ("int.xor", LexKeyTok::LTokIntDotXor),
        ("op.alloc_local", LexKeyTok::LTokOpDotAllocLocal),
        ("functions", LexKeyTok::LTokFunctions),
        ("atomic.store.release", LexKeyTok::LTokAtomicDotStoreDotRelease),
        ("outputs", LexKeyTok::LTokOutputs),
        ("int.ctlz", LexKeyTok::LTokIntDotCtlz),
        ("int.add", LexKeyTok::LTokIntDotAdd),
        ("op.blackbox", LexKeyTok::LTokOpDotBlackbox),
        ("int.mul.over", LexKeyTok::LTokIntDotMulDotOver),
        ("flag.getint", LexKeyTok::LTokFlagDotGetint),
        ("fp.rem", LexKeyTok::LTokFpDotRem),
        ("config", LexKeyTok::LTokConfig),
        ("int.abs", LexKeyTok::LTokIntDotAbs),
        ("target", LexKeyTok::LTokTarget),
        ("atomic.store.relaxed", LexKeyTok::LTokAtomicDotStoreDotRelaxed),
        ("atomic.load.acquire", LexKeyTok::LTokAtomicDotLoadDotAcquire),
        ("ptr.struct_elem.inbounds", LexKeyTok::LTokPtrDotStructElemDotInbounds),
        ("fp.gt", LexKeyTok::LTokFpDotGt),
        ("scope.output", LexKeyTok::LTokScopeDotOutput),
        ("int.zext", LexKeyTok::LTokIntDotZext),
        ("int.eq", LexKeyTok::LTokIntDotEq),
        ("int.sub.over", LexKeyTok::LTokIntDotSubDotOver),
        ("inputs", LexKeyTok::LTokInputs),
        ("ptr.array_elem", LexKeyTok::LTokPtrDotArrayElem),
        ("ptr.array_elem.no_wrap", LexKeyTok::LTokPtrDotArrayElemDotNoWrap),
        ("ptr.load.volatile.un_align", LexKeyTok::LTokPtrDotLoadDotVolatileDotUnAlign),
        ("alias", LexKeyTok::LTokAlias),
        ("ptr.psub", LexKeyTok::LTokPtrDotPsub),
        ("fp.max", LexKeyTok::LTokFpDotMax),
        ("ptr.struct_elem", LexKeyTok::LTokPtrDotStructElem),
        ("ptr.union_spec", LexKeyTok::LTokPtrDotUnionSpec),
        ("ops_true", LexKeyTok::LTokOpsTrue),
        ("int.bswap", LexKeyTok::LTokIntDotBswap),
        ("int.neq", LexKeyTok::LTokIntDotNeq),
        ("ptr.struct_elem.no_wrap", LexKeyTok::LTokPtrDotStructElemDotNoWrap),
        ("int.add.wrap", LexKeyTok::LTokIntDotAddDotWrap),
        ("flow.call", LexKeyTok::LTokFlowDotCall),
        ("int.gt", LexKeyTok::LTokIntDotGt),
        ("conv.fptoui.sat", LexKeyTok::LTokConvDotFptouiDotSat),
        ("conv.fptosi.sat", LexKeyTok::LTokConvDotFptosiDotSat),
        ("lifetime.start", LexKeyTok::LTokLifetimeDotStart),
        ("int.max", LexKeyTok::LTokIntDotMax),
        ("ptr.store.volatile", LexKeyTok::LTokPtrDotStoreDotVolatile),
        ("fp.ext", LexKeyTok::LTokFpDotExt),
        ("ops_default", LexKeyTok::LTokOpsDefault),
        ("conv.fptosi", LexKeyTok::LTokConvDotFptosi),
        ("ops_return", LexKeyTok::LTokOpsReturn),
        ("atomic.load.relaxed", LexKeyTok::LTokAtomicDotLoadDotRelaxed),
        ("int.mul", LexKeyTok::LTokIntDotMul),
        ("fp.min", LexKeyTok::LTokFpDotMin),
        ("ptr.load.un_align", LexKeyTok::LTokPtrDotLoadDotUnAlign),
        ("fp.trunc", LexKeyTok::LTokFpDotTrunc),
        ("fp.neg", LexKeyTok::LTokFpDotNeg),
        ("atomic.load.seq_cst", LexKeyTok::LTokAtomicDotLoadDotSeqCst),
        ("fp.neq", LexKeyTok::LTokFpDotNeq),
        ("int.bitreverse", LexKeyTok::LTokIntDotBitreverse),
        ("atomic.store.seq_cst", LexKeyTok::LTokAtomicDotStoreDotSeqCst),
        ("scope.input", LexKeyTok::LTokScopeDotInput),
        ("lifetime.finish", LexKeyTok::LTokLifetimeDotFinish),
        ("conv.ibitstofp", LexKeyTok::LTokConvDotIbitstofp),
        ("ptr.load.volatile", LexKeyTok::LTokPtrDotLoadDotVolatile),
        ("ops.none", LexKeyTok::LTokOpsDotNone),
        ("int.cttz", LexKeyTok::LTokIntDotCttz),
        ("conv.fpbitstoi", LexKeyTok::LTokConvDotFpbitstoi),
        ("ptr.store.un_align", LexKeyTok::LTokPtrDotStoreDotUnAlign),
        ("conv.itofp", LexKeyTok::LTokConvDotItofp),
        ("fp.add", LexKeyTok::LTokFpDotAdd),
        ("int.lshr", LexKeyTok::LTokIntDotLshr),
        ("fp.eq", LexKeyTok::LTokFpDotEq),
        ("ptr.union_spec.inbounds", LexKeyTok::LTokPtrDotUnionSpecDotInbounds),
        ("flow.switch", LexKeyTok::LTokFlowDotSwitch),
        ("int.lt", LexKeyTok::LTokIntDotLt),
        ("int.add.sat", LexKeyTok::LTokIntDotAddDotSat),
        ("code", LexKeyTok::LTokCode),
        ("fp.div", LexKeyTok::LTokFpDotDiv),
        ("int.rotl", LexKeyTok::LTokIntDotRotl),
        ("conv.fptoui", LexKeyTok::LTokConvDotFptoui),
        ("conv.utofp", LexKeyTok::LTokConvDotUtofp),
        ("int.add.over", LexKeyTok::LTokIntDotAddDotOver),
        ("op.assume", LexKeyTok::LTokOpDotAssume),
        ("ptr.store", LexKeyTok::LTokPtrDotStore),
        ("int.sub.sat", LexKeyTok::LTokIntDotSubDotSat),
        ("int.not", LexKeyTok::LTokIntDotNot),
        ("ops_unwind", LexKeyTok::LTokOpsUnwind),
        ("int.sext", LexKeyTok::LTokIntDotSext),
        ("flow.loop", LexKeyTok::LTokFlowDotLoop),
        ("ops", LexKeyTok::LTokOps),
        ("ops_false", LexKeyTok::LTokOpsFalse),
        ("int.mul.wrap", LexKeyTok::LTokIntDotMulDotWrap),
        ("ptr.store.volatile.un_align", LexKeyTok::LTokPtrDotStoreDotVolatileDotUnAlign),
        ("flow.invoke", LexKeyTok::LTokFlowDotInvoke),
        ("types", LexKeyTok::LTokTypes),
        ("ptr.array_elem.inbounds", LexKeyTok::LTokPtrDotArrayElemDotInbounds),
        ("fp.abs", LexKeyTok::LTokFpDotAbs),
        ("fp.mul", LexKeyTok::LTokFpDotMul),
        ("fp.lt", LexKeyTok::LTokFpDotLt),
        ("int.div_rem", LexKeyTok::LTokIntDotDivRem),
        ("flow.branch", LexKeyTok::LTokFlowDotBranch),
        ("ops_if", LexKeyTok::LTokOpsIf),
    ]),
};




#[cfg(test)]
use super::lexer::perform_test;

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_lex_gen() {
		perform_test("fp.eq",TokLexedKeyword(LexKeyTok::LTokFpDotEq));
		perform_test("ptr.union_spec",TokLexedKeyword(LexKeyTok::LTokPtrDotUnionSpec));
		perform_test("ops_false",TokLexedKeyword(LexKeyTok::LTokOpsFalse));
		perform_test("conv.fptoui.sat",TokLexedKeyword(LexKeyTok::LTokConvDotFptouiDotSat));
		perform_test("int.neq",TokLexedKeyword(LexKeyTok::LTokIntDotNeq));
		perform_test("atomic.store.seq_cst",TokLexedKeyword(LexKeyTok::LTokAtomicDotStoreDotSeqCst));
		perform_test("ptr.struct_elem",TokLexedKeyword(LexKeyTok::LTokPtrDotStructElem));
		perform_test("fp.gt",TokLexedKeyword(LexKeyTok::LTokFpDotGt));
		perform_test("target",TokLexedKeyword(LexKeyTok::LTokTarget));
		perform_test("passes",TokLexedKeyword(LexKeyTok::LTokPasses));
		perform_test("int.sub.sat",TokLexedKeyword(LexKeyTok::LTokIntDotSubDotSat));
		perform_test("int.div_rem",TokLexedKeyword(LexKeyTok::LTokIntDotDivRem));
		perform_test("ptr.array_elem",TokLexedKeyword(LexKeyTok::LTokPtrDotArrayElem));
		perform_test("functions",TokLexedKeyword(LexKeyTok::LTokFunctions));
		perform_test("fp.abs",TokLexedKeyword(LexKeyTok::LTokFpDotAbs));
		perform_test("int.cttz",TokLexedKeyword(LexKeyTok::LTokIntDotCttz));
		perform_test("int.mul",TokLexedKeyword(LexKeyTok::LTokIntDotMul));
		perform_test("atomic.load.acquire",TokLexedKeyword(LexKeyTok::LTokAtomicDotLoadDotAcquire));
		perform_test("code",TokLexedKeyword(LexKeyTok::LTokCode));
		perform_test("ops_if",TokLexedKeyword(LexKeyTok::LTokOpsIf));
		perform_test("fp.ext",TokLexedKeyword(LexKeyTok::LTokFpDotExt));
		perform_test("scope.output",TokLexedKeyword(LexKeyTok::LTokScopeDotOutput));
		perform_test("ptr.store.volatile.un_align",TokLexedKeyword(LexKeyTok::LTokPtrDotStoreDotVolatileDotUnAlign));
		perform_test("atomic.store.release",TokLexedKeyword(LexKeyTok::LTokAtomicDotStoreDotRelease));
		perform_test("ptr.load.volatile.un_align",TokLexedKeyword(LexKeyTok::LTokPtrDotLoadDotVolatileDotUnAlign));
		perform_test("conv.utofp",TokLexedKeyword(LexKeyTok::LTokConvDotUtofp));
		perform_test("atomic.load.relaxed",TokLexedKeyword(LexKeyTok::LTokAtomicDotLoadDotRelaxed));
		perform_test("int.sub.over",TokLexedKeyword(LexKeyTok::LTokIntDotSubDotOver));
		perform_test("op.blackbox",TokLexedKeyword(LexKeyTok::LTokOpDotBlackbox));
		perform_test("int.sub",TokLexedKeyword(LexKeyTok::LTokIntDotSub));
		perform_test("lifetime.start",TokLexedKeyword(LexKeyTok::LTokLifetimeDotStart));
		perform_test("int.bitreverse",TokLexedKeyword(LexKeyTok::LTokIntDotBitreverse));
		perform_test("flag.getint",TokLexedKeyword(LexKeyTok::LTokFlagDotGetint));
		perform_test("ptr.store",TokLexedKeyword(LexKeyTok::LTokPtrDotStore));
		perform_test("int.add.over",TokLexedKeyword(LexKeyTok::LTokIntDotAddDotOver));
		perform_test("int.abs",TokLexedKeyword(LexKeyTok::LTokIntDotAbs));
		perform_test("ptr.load",TokLexedKeyword(LexKeyTok::LTokPtrDotLoad));
		perform_test("flow.loop",TokLexedKeyword(LexKeyTok::LTokFlowDotLoop));
		perform_test("int.max",TokLexedKeyword(LexKeyTok::LTokIntDotMax));
		perform_test("induction",TokLexedKeyword(LexKeyTok::LTokInduction));
		perform_test("ptr.store.un_align",TokLexedKeyword(LexKeyTok::LTokPtrDotStoreDotUnAlign));
		perform_test("ptr.union_spec.inbounds",TokLexedKeyword(LexKeyTok::LTokPtrDotUnionSpecDotInbounds));
		perform_test("op.assume",TokLexedKeyword(LexKeyTok::LTokOpDotAssume));
		perform_test("int.ctpop",TokLexedKeyword(LexKeyTok::LTokIntDotCtpop));
		perform_test("ops_unwind",TokLexedKeyword(LexKeyTok::LTokOpsUnwind));
		perform_test("fp.neg",TokLexedKeyword(LexKeyTok::LTokFpDotNeg));
		perform_test("int.and",TokLexedKeyword(LexKeyTok::LTokIntDotAnd));
		perform_test("int.mul.wrap",TokLexedKeyword(LexKeyTok::LTokIntDotMulDotWrap));
		perform_test("ops",TokLexedKeyword(LexKeyTok::LTokOps));
		perform_test("globals",TokLexedKeyword(LexKeyTok::LTokGlobals));
		perform_test("int.lshr",TokLexedKeyword(LexKeyTok::LTokIntDotLshr));
		perform_test("fp.max",TokLexedKeyword(LexKeyTok::LTokFpDotMax));
		perform_test("flow.switch",TokLexedKeyword(LexKeyTok::LTokFlowDotSwitch));
		perform_test("int.rotl",TokLexedKeyword(LexKeyTok::LTokIntDotRotl));
		perform_test("int.trunc",TokLexedKeyword(LexKeyTok::LTokIntDotTrunc));
		perform_test("int.shl",TokLexedKeyword(LexKeyTok::LTokIntDotShl));
		perform_test("int.add.wrap",TokLexedKeyword(LexKeyTok::LTokIntDotAddDotWrap));
		perform_test("fp.rem",TokLexedKeyword(LexKeyTok::LTokFpDotRem));
		perform_test("int.eq",TokLexedKeyword(LexKeyTok::LTokIntDotEq));
		perform_test("fp.div",TokLexedKeyword(LexKeyTok::LTokFpDotDiv));
		perform_test("metadata",TokLexedKeyword(LexKeyTok::LTokMetadata));
		perform_test("fp.mul",TokLexedKeyword(LexKeyTok::LTokFpDotMul));
		perform_test("ptr.array_elem.no_wrap",TokLexedKeyword(LexKeyTok::LTokPtrDotArrayElemDotNoWrap));
		perform_test("ops_return",TokLexedKeyword(LexKeyTok::LTokOpsReturn));
		perform_test("conv.fptosi.sat",TokLexedKeyword(LexKeyTok::LTokConvDotFptosiDotSat));
		perform_test("flow.branch",TokLexedKeyword(LexKeyTok::LTokFlowDotBranch));
		perform_test("int.add",TokLexedKeyword(LexKeyTok::LTokIntDotAdd));
		perform_test("conv.itofp",TokLexedKeyword(LexKeyTok::LTokConvDotItofp));
		perform_test("int.ashr",TokLexedKeyword(LexKeyTok::LTokIntDotAshr));
		perform_test("conv.ibitstofp",TokLexedKeyword(LexKeyTok::LTokConvDotIbitstofp));
		perform_test("ops.none",TokLexedKeyword(LexKeyTok::LTokOpsDotNone));
		perform_test("int.gt",TokLexedKeyword(LexKeyTok::LTokIntDotGt));
		perform_test("fp.sub",TokLexedKeyword(LexKeyTok::LTokFpDotSub));
		perform_test("lifetime.finish",TokLexedKeyword(LexKeyTok::LTokLifetimeDotFinish));
		perform_test("ptr.struct_elem.inbounds",TokLexedKeyword(LexKeyTok::LTokPtrDotStructElemDotInbounds));
		perform_test("outputs",TokLexedKeyword(LexKeyTok::LTokOutputs));
		perform_test("int.zext",TokLexedKeyword(LexKeyTok::LTokIntDotZext));
		perform_test("inputs",TokLexedKeyword(LexKeyTok::LTokInputs));
		perform_test("atomic.load.seq_cst",TokLexedKeyword(LexKeyTok::LTokAtomicDotLoadDotSeqCst));
		perform_test("fp.trunc",TokLexedKeyword(LexKeyTok::LTokFpDotTrunc));
		perform_test("int.mul.over",TokLexedKeyword(LexKeyTok::LTokIntDotMulDotOver));
		perform_test("int.rotr",TokLexedKeyword(LexKeyTok::LTokIntDotRotr));
		perform_test("flow.invoke",TokLexedKeyword(LexKeyTok::LTokFlowDotInvoke));
		perform_test("conv.fptoui",TokLexedKeyword(LexKeyTok::LTokConvDotFptoui));
		perform_test("int.not",TokLexedKeyword(LexKeyTok::LTokIntDotNot));
		perform_test("fp.min",TokLexedKeyword(LexKeyTok::LTokFpDotMin));
		perform_test("scope.input",TokLexedKeyword(LexKeyTok::LTokScopeDotInput));
		perform_test("config",TokLexedKeyword(LexKeyTok::LTokConfig));
		perform_test("conv.fptosi",TokLexedKeyword(LexKeyTok::LTokConvDotFptosi));
		perform_test("alias",TokLexedKeyword(LexKeyTok::LTokAlias));
		perform_test("ptr.psub",TokLexedKeyword(LexKeyTok::LTokPtrDotPsub));
		perform_test("int.sub.wrap",TokLexedKeyword(LexKeyTok::LTokIntDotSubDotWrap));
		perform_test("int.lt",TokLexedKeyword(LexKeyTok::LTokIntDotLt));
		perform_test("fp.lt",TokLexedKeyword(LexKeyTok::LTokFpDotLt));
		perform_test("int.sext",TokLexedKeyword(LexKeyTok::LTokIntDotSext));
		perform_test("types",TokLexedKeyword(LexKeyTok::LTokTypes));
		perform_test("ptr.struct_elem.no_wrap",TokLexedKeyword(LexKeyTok::LTokPtrDotStructElemDotNoWrap));
		perform_test("conv.fpbitstoi",TokLexedKeyword(LexKeyTok::LTokConvDotFpbitstoi));
		perform_test("fp.add",TokLexedKeyword(LexKeyTok::LTokFpDotAdd));
		perform_test("fp.neq",TokLexedKeyword(LexKeyTok::LTokFpDotNeq));
		perform_test("ptr.load.un_align",TokLexedKeyword(LexKeyTok::LTokPtrDotLoadDotUnAlign));
		perform_test("ptr.array_elem.inbounds",TokLexedKeyword(LexKeyTok::LTokPtrDotArrayElemDotInbounds));
		perform_test("atomic.store.relaxed",TokLexedKeyword(LexKeyTok::LTokAtomicDotStoreDotRelaxed));
		perform_test("int.xor",TokLexedKeyword(LexKeyTok::LTokIntDotXor));
		perform_test("int.ctlz",TokLexedKeyword(LexKeyTok::LTokIntDotCtlz));
		perform_test("int.add.sat",TokLexedKeyword(LexKeyTok::LTokIntDotAddDotSat));
		perform_test("ops_default",TokLexedKeyword(LexKeyTok::LTokOpsDefault));
		perform_test("ptr.store.volatile",TokLexedKeyword(LexKeyTok::LTokPtrDotStoreDotVolatile));
		perform_test("int.bswap",TokLexedKeyword(LexKeyTok::LTokIntDotBswap));
		perform_test("ops_true",TokLexedKeyword(LexKeyTok::LTokOpsTrue));
		perform_test("op.alloc_local",TokLexedKeyword(LexKeyTok::LTokOpDotAllocLocal));
		perform_test("ptr.load.volatile",TokLexedKeyword(LexKeyTok::LTokPtrDotLoadDotVolatile));
		perform_test("flow.call",TokLexedKeyword(LexKeyTok::LTokFlowDotCall));
	}
}

use std::io::{
	Write,
	Result as IOResult,
	Error as IOError,
	ErrorKind as IOErrorKind,
};
use nom::IResult;
use std::ops::{Deref, Range, RangeFrom, RangeTo};
use nom::{
	InputLength,
	Offset,
	AsBytes,
	InputIter,
	InputTake,
	InputTakeAtPosition,
	Compare,
	FindToken,
	Slice,
	AtEof,
	ParseTo,
	types::CompleteStr,
};

#[cfg(test)]
use std::fmt;

/// Trait With Extensions for all valid String Input
///  implementations, contains requirement traits for
///  all required nom actions on the input
pub trait StringIrInput
	where
		Self: Slice<Range<usize>> + Slice<RangeFrom<usize>> + Slice<RangeTo<usize>>,
		Self: Copy + Clone + Offset + InputLength + InputTake + AtEof + AsRef<str>,
		Self: AsBytes + FindToken<char> + FindToken<u8>,
		Self: ParseTo<u8> + ParseTo<u16> + ParseTo<u32> + ParseTo<u64>,
		Self: ParseTo<i8> + ParseTo<i16> + ParseTo<i32> + ParseTo<i64>,
		Self: ParseTo<usize> + ParseTo<isize> + ParseTo<i128> + ParseTo<u128>,
		Self: ParseTo<f32> + ParseTo<f64>,
		Self: InputIter<Item = char> + InputTakeAtPosition<Item = char>,
		Self: NewBuilder,
		for<'tmp> Self: Compare<&'tmp str>,
{
	/* No Implementation */
}
impl StringIrInput for &str {}
impl<'a> StringIrInput for CompleteStr<'a> {}


/// Trait Implementation required
///  in order for the String
///  implementation of ConvertIR
///  to be implemented correctly
pub trait NewBuilder {
	fn new_builder(&self) -> String {
		String::new()
	}
}
impl NewBuilder for &str {}
impl<'a> NewBuilder for CompleteStr<'a> {}


/// Trait that signals that the given
///  value is able to be converted into
///  the intermediate format for easy
///  reading
pub trait ConvertWithIR: Sized {
	fn build_ir<W: Write>(&self, prefix: &str, w: &mut W) -> IOResult<()>;
	fn from_ir<S: StringIrInput>(r: S) -> IResult<S,Self>;
	
	fn as_ir<W: Write>(&self, w: &mut W) -> IOResult<()> {
		self.build_ir("", w)
	}
	
	fn from_ir_str_complete(s: &str) -> Result<Self,String> {
		let complete_str = CompleteStr::from(s);
		let res: IResult<CompleteStr,Self> = Self::from_ir(complete_str);
		match res {
			Ok((_,res)) => Ok(res),
			Err(d) => Err(d.to_string())
		}
	}
	fn as_ir_bytes(&self) -> IOResult<Vec<u8>> {
		let mut out = Vec::new();
		self.as_ir(&mut out)?;
		Ok(out)
	}
	fn as_ir_string(&self) -> IOResult<String> {
		let result = self.as_ir_bytes()?;
		String::from_utf8(result).map_err(|e| {
			IOError::new(IOErrorKind::InvalidData,e)
		})
	}
}

/// Test a single value <-> IR pair
#[cfg(test)]
pub fn test_ir_conversion<T: ConvertWithIR + PartialEq + fmt::Debug>(obj: &T, ir: &str) {
	//Standard serialize back and to
	let string = obj.as_ir_string().unwrap();
	assert_eq!(string,ir);
	let new_obj = T::from_ir_str_complete(string.as_str()).unwrap();
	assert_eq!(&new_obj,obj);
	
	//Debug prefix
	let mut debug_vec = Vec::new();
	let _ = obj.build_ir("__PREFIX__",&mut debug_vec).unwrap();
	let debug_str = String::from_utf8(debug_vec).unwrap();
	assert!(debug_str.starts_with("__PREFIX__"));
	assert_eq!(debug_str,"__PREFIX__".to_string() + ir);
	
	println!("Success: {}",ir);
}

#[cfg(test)]
mod tests {
	use super::*;
	#[test]
	fn test_integral() {
		test_ir_conversion(&12u8,"12");
		test_ir_conversion(&100000u32,"100000");
		test_ir_conversion(&-143i32,"-143");
	}
	
	#[test]
	fn test_floating() {
		use std::f32;
		test_ir_conversion(&0.05f32,"0.05");
		test_ir_conversion(&-1.025f64,"-1.025");
		test_ir_conversion(&0.0,"0.0");
		test_ir_conversion(&-0.0,"-0.0");
		test_ir_conversion(&f32::INFINITY,"inf");
		test_ir_conversion(&-f32::INFINITY,"-inf");
		let v : f32 = f32::NAN;
		assert_eq!(v.as_ir_string().unwrap(),"NaN");
		assert_eq!(true,f32::from_ir_str_complete("NaN").unwrap().is_nan());
		println!("Success: Nan");
	}
	
	#[test]
	fn test_boolean() {
		test_ir_conversion(&false,"false");
		test_ir_conversion(&true,"true");
	}
	
	#[test]
	fn test_character() {
		test_ir_conversion(&'\n',"'\\n'");
		test_ir_conversion(&'\\',"'\\\\'");
		test_ir_conversion(&'\'',"'\\\''");
		test_ir_conversion(&'z',"'z'");
	}
	
	#[test]
	fn test_strings() {
		test_ir_conversion(&"hello".to_string(),"\"hello\"");
		test_ir_conversion(&"abc\n\\de".to_string(),"\"abc\\n\\\\de\"");
		test_ir_conversion(&"A\"B".to_string(),"\"A\\\"B\"");
		let s: String = ConvertWithIR::from_ir_str_complete(
			"\"hello\\\"again\"IGNORE_THIS\"\"AGAIN"
		).unwrap();
		assert_eq!(&s,"hello\"again");
	}
	
	#[test]
	fn test_bracket_metadata() {
		test_ir_conversion(&NamedBracketMetadata::new(),"");
		let bracket_0 = NamedBracketMetadata {
			data: vec![("testy".to_string(),String::new())]
		};
		test_ir_conversion(&bracket_0,"(testy)");
		let bracket_1 = NamedBracketMetadata {
			data: vec![("alpha".to_string(),String::new()),("beta".to_string(),String::new())]
		};
		test_ir_conversion(&bracket_1,"(alpha, beta)");
		let bracket_2 = NamedBracketMetadata {
			data: vec![("testy".to_string(),"valuation{32}".to_string())]
		};
		test_ir_conversion(&bracket_2,"(testy = valuation{32})");
		let bracket_3 = NamedBracketMetadata {
			data: vec![("alpha".to_string(),"A::A".to_string()),
					   ("beta".to_string(),"B::B".to_string())]
		};
		test_ir_conversion(&bracket_3,"(alpha = A::A, beta = B::B)");
		let bracket_4 = NamedBracketMetadata {
			data: vec![("alpha".to_string(),String::new()),
					   ("beta".to_string(),"B::B".to_string())]
		};
		test_ir_conversion(&bracket_4,"(alpha, beta = B::B)");
		let bracket_5 = NamedBracketMetadata {
			data: vec![("alpha".to_string(),"A::A".to_string()),
					   ("beta".to_string(),String::new())]
		};
		test_ir_conversion(&bracket_5,"(alpha = A::A, beta)");
	}
}


/// Parsing Utility for Brackets
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct NamedBracketMetadata {
	data: Vec<(String, String)>
}
impl NamedBracketMetadata {
	pub fn new() -> Self {
		NamedBracketMetadata {
			data: Vec::new()
		}
	}
	pub fn contains_flag(&self,name: &str) -> bool {
		self.data.iter().any(|(key,_)| {
			key == name
		})
	}
	pub fn load_value<T: ConvertWithIR>(&self, name: &str) -> Option<T> {
		self.data.iter().find_map(|(key,value)| {
			if key == name {
				T::from_ir_str_complete(value.deref()).ok()
			}else{
				None
			}
		})
	}
	pub fn maybe_add_flag(&mut self, name: &str, cond: bool) {
		if cond {
			self.add_flag(name)
		}
	}
	pub fn add_flag(&mut self, name: &str) {
		self.data.push((name.to_owned(),String::new()))
	}
	pub fn add_value<T: ConvertWithIR>(&mut self, name: &str, t: &T) -> IOResult<()> {
		self.data.push((name.to_owned(),t.as_ir_string()?));
		Ok(())
	}
}

/// Implementation of ConvertWithIR
///  for standard primitives:
///   f32,f64
///   u8,u16,u32,u64,u128,usize
///   i8,i16,i32,i64,i128,isize,
///   char,String,bool
///  and utility primitives:
///   NamedBracketMetadata
mod primitive_impl {
	use super::*;
	use nom::{
		IResult,
		recognize_float,
		anychar,
		flat_map, map_res, map,
		tuple, alt, opt,
		tag, value, char,
		parse_to,
		escaped_transform,
		call,
		separated_list,
		take_till,
		one_of,
		delimited,
		recognize,
		space0,
		digit,
	};
	use std::str::FromStr;
	
	impl<T: ConvertWithIR> ConvertWithIR for Vec<T> {
		fn build_ir<W: Write>(&self, prefix: &str, w: &mut W) -> IOResult<()> {
			write!(w, "{}", prefix);
			let mut iter = self.iter();
			if let Some(first) = iter.next() {
				ConvertWithIR::as_ir(first,w)?;
				for value in iter {
					write!(w, ", ")?;
					ConvertWithIR::as_ir(value,w)?;
				}
			}
			Ok(())
		}
		fn from_ir<S: StringIrInput>(input: S) -> IResult<S,Self> {
			separated_list!(
			input,
			char!(','),
			map!(
				tuple!(
					call!(space0),
					call!(T::from_ir),
					call!(space0)
				),
				|(_,value,_)| {
					value
				}
			)
		)
		}
	}
	
	
	impl ConvertWithIR for NamedBracketMetadata {
		fn build_ir<W: Write>(&self, prefix: &str, w: &mut W) -> IOResult<()> {
			write!(w, "{}", prefix);
			if !self.data.is_empty() {
				write!(w,"(")?;
				let mut is_first = true;
				for (key,value) in self.data.iter() {
					let val = value.deref();
					if is_first {
						is_first = false;
					}else{
						write!(w,", ")?;
					}
					if val.is_empty() {
						write!(w,"{}",key)?;
					}else{
						write!(w,"{} = {}",key,val)?;
					}
				}
				write!(w,")")
			}else{
				Ok(())
			}
		}
		
		fn from_ir<S: StringIrInput>(input: S) -> IResult<S,Self> {
			map!(
				input,
				opt!(tuple!(
					char!('('),
					separated_list!(
						char!(','),
						alt!(
							map!(
								tuple!(
									call!(space0),
									take_till!(|c| c == '=' || c == ' ' || c == '\t'),
									call!(space0),
									char!('='),
									call!(space0),
									take_till!(|c| c == ',' || c == ')' || c == ' ' || c == '\t'),
									call!(space0)
								),
								|(_,key,_,_,_,value,_)| {
									(key.as_ref().to_string(),value.as_ref().to_string())
								}
							)
							|
							map!(
								tuple!(
									call!(space0),
									take_till!(|c| c == ',' || c == ')' || c == '=' || c == ' ' || c == '\t'),
									call!(space0)
								),
								|(_,key,_)| {
									(key.as_ref().to_string(),String::new())
								}
							)
						)
					),
					char!(')')
				)),
				|v| {
					match v {
						Some((_,middle,_)) => NamedBracketMetadata {
							data: middle
						},
						None => NamedBracketMetadata {
							data: Vec::new()
						}
					}
				}
			)
		}
	}
	
	
	fn recognize_integral<T: StringIrInput>(input: T) -> IResult<T, T> {
		recognize!(input,
			tuple!(
				opt!(char!('-')),
				digit
			)
	  	)
	}
	fn integral<T: StringIrInput + ParseTo<R>,R: FromStr>(input: T) -> IResult<T, R> {
		flat_map!(input,recognize_integral, parse_to!(R))
	}
	
	fn recognize_floating<T: StringIrInput>(input: T) -> IResult<T,T> {
		recognize!(input,
			alt!(
				call!(recognize_float)
				| tag!("inf")
				| tag!("-inf")
				| tag!("NaN")
			)
		)
	}
	
	fn floating<T: StringIrInput + ParseTo<R>,R: FromStr>(input: T) -> IResult<T,R> {
		flat_map!(input,recognize_floating, parse_to!(R))
	}
	
	fn boolean<T: StringIrInput>(input: T) -> IResult<T,bool,u32> {
		alt!(
			input,
			value!(false, tag!("false"))
			| value!(true, tag!("true"))
		)
	}
	
	fn character<T: StringIrInput>(input: T) -> IResult<T,char> {
		delimited!(
			input,
    		char!('\''),
    		alt!(
    			tuple!(
    				char!('\\'),
    				one_of!("\"ntr'\\")
    			) => {|(_,v)| {
    				match v {
    					'n' => '\n',
    					't' => '\t',
    					'r' => '\r',
    					other => other
    				}
    			}}
    			|
    			tuple!(
    				map_res!(
    					call!(anychar),
    					|v: char| -> Result<char,()> {
    						if v == '\'' {
    							Err(())
    						}else{
    							Ok(v)
    						}
    					}
    				)
    			)
    		),
    		char!('\'')
		)
	}
	
	
	fn string<T: StringIrInput>(input: T) -> IResult<T,String> {
		delimited!(
			input,
			char!('\"'),
			escaped_transform!(
				map_res!(
					call!(anychar),
					|v: char| -> Result<char,()> {
						if v == '\\' || v == '"' {
							Err(())
						}else{
							Ok(v)
						}
					}
				),
				'\\',
				alt!(
					tag!("\"")         => { |_| "\"" }
					| tag!("n")        => { |_| "\n" }
					| tag!("t")        => { |_| "\t" }
					| tag!("r")        => { |_| "\r" }
					| tag!("'")        => { |_| "'"  }
					| tag!("\\")       => { |_| "\\" }
			   )
			),
			char!('\"')
		)
	}
	
	macro_rules! impl_convert_ir{
		($functor:path | $fmt:expr => $($tt:ty),+) => {
			$(
				impl ConvertWithIR for $tt {
					fn build_ir<W: Write>(&self,prefix: &str, w: &mut W) -> IOResult<()> {
						write!(w, "{}", prefix)?;
						write!(w, $fmt, self)
					}
					fn from_ir<S: StringIrInput>(r: S) -> IResult<S, Self> {
						$functor(r)
					}
				}
			)+
		};
		($functor:path => $($tt:ty),+) => {
			impl_convert_ir!($functor | "{}" => $($tt),+);
		};
	}
	
	impl_convert_ir!(integral => u8, u16, u32, u64, u128, usize);
	impl_convert_ir!(integral => i8, i16, i32, i64, i128, isize);
	impl_convert_ir!(boolean => bool);
	impl_convert_ir!(floating  | "{:?}" => f32, f64);
	impl_convert_ir!(string    | "{:?}" => String);
	impl_convert_ir!(character | "{:?}" => char);
}
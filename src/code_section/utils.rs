
















/*

use parking_lot::{
	RwLock as RawRwLock,
	ReentrantMutex as RawReentrantMutex,
};
use std::ops::{
	Deref,
	DerefMut,
};
use std::clone::Clone;
use serde::{
	Serialize,
	Serializer,
	Deserialize,
	Deserializer,
};
use std::sync::atomic::AtomicPtr;

/* FIXME: COME UP WITH IDEA TO REDUCE SIZE OF PTR TO 32Bit {lookup table}
pub struct BlockRefArray<T> {

}

pub struct BlockRef(u32);

impl<T> BlockRefArray<T> {
	pub fn new() -> BlockRefArray<T> {
		unimplemented!()
	}
	pub fn create_ref(&self, t: Box<T>) -> BlockRef {
		unimplemented!()
	}
	pub fn get_ref_mut<'s,'r>(&'s self,b: &'r mut BlockRef) -> &'r mut T where 's: 'r {
		unimplemented!()
	}
	pub fn get_ref<'s,'r>(&'s self, b: &'r BlockRef) -> &'r T where 's: 'r{
		unimplemented!()
	}
}*/














#[repr(transparent)]
#[derive(Default)]
pub struct ReentrantMutex<T>(RawReentrantMutex<T>);
impl <T> ReentrantMutex<T> {
	pub fn new(t: T) -> ReentrantMutex<T> {
		ReentrantMutex(RawReentrantMutex::new(t))
	}
}
impl <T> Deref for ReentrantMutex<T> {
	type Target = RawReentrantMutex<T>;
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
impl <T> DerefMut for ReentrantMutex<T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}
impl <T: Clone> Clone for ReentrantMutex<T> {
	fn clone(&self) -> Self {
		ReentrantMutex::new(self.lock().clone())
	}
}
impl <T: PartialEq> PartialEq for ReentrantMutex<T> {
	fn eq(&self, other: &Self) -> bool {
		let a = self.lock();
		let b = other.lock();
		a.eq(b.deref())
	}
}
impl <T: Eq> Eq for ReentrantMutex<T> {}

#[repr(transparent)]
#[derive(Default)]
pub struct RwLock<T>(RawRwLock<T>);
impl <T> RwLock<T> {
	pub fn new(t: T) -> RwLock<T> {
		RwLock(RawRwLock::new(t))
	}
}
impl <T> Deref for RwLock<T> {
	type Target = RawRwLock<T>;
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
impl <T> DerefMut for RwLock<T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}
impl <T: Serialize> Serialize for RwLock<T> {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let tmp = self.read();
		tmp.serialize(serializer)
	}
}
impl <'de,T: Deserialize<'de>> Deserialize<'de> for RwLock<T> {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let x : T = T::deserialize(deserializer)?;
		Ok(RwLock::new(x))
	}
}

impl <T: Clone> Clone for RwLock<T> {
	fn clone(&self) -> Self {
		RwLock::new(self.read().clone())
	}
}
impl <T: PartialEq> PartialEq for RwLock<T> {
	fn eq(&self, other: &Self) -> bool {
		let a = self.read();
		let b = other.read();
		a.eq(b.deref())
	}
}
impl <T: Eq> Eq for RwLock<T> {}


*/


/// Printer implementations
///  consume instruction streams
///  and output assembly or binary
pub mod printer;

/// Parser implementations
///  consume text or binary
///  and output instruction
///  streams
pub mod parser;

//TODO: DEPRECATE
pub mod insn;

/// Generated Module
///  from output format
///  description json
pub mod impls;

pub mod patch_seq;

/// Represents a bank
///  of registers that
///  support some operation
pub trait RegisterBank {

}
//FIXME: GENERATE MORE INFOMATION
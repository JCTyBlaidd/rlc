
/// Consume Abstract Instructions
///  and print the given instruction
///  type into binary format
pub struct BinaryPrinter {
	binary: Vec<u8>,
	swap_endian: bool,
	//FIXME: Handle Endian
	// Generated MCOps Are Native Endian
	// Convert if native != target
	//FIXME: Track Immediate Values
	// that must be updates when applicable
	// in the Binary Output Format
}

impl BinaryPrinter {
	pub fn new(swap_endian: bool) -> BinaryPrinter {
		BinaryPrinter {
			binary: Vec::new(),
			swap_endian
		}
	}
	
	pub fn into_bytes(self) -> Vec<u8> {
		self.binary
	}
	
	pub fn write_u8(&mut self, data: u8) {
		self.binary.push(data);
	}
	
	pub fn write_u16(&mut self, data: u16) {
	
	}
	
	pub fn write_u24(&mut self, data: u32) {
	
	}
	
	pub fn write_u32(&mut self, data: u32) {
	
	}
	
	pub fn write_u48(&mut self, data: u64) {
	
	}
	
	pub fn write_u64(&mut self, data: u64) {
	
	}
}


pub struct ASMPrinter {
	string: String,
}

impl ASMPrinter {
	pub fn new() -> ASMPrinter {
		ASMPrinter {
			string: String::new()
		}
	}
	
	pub fn into_string(self) -> String {
		self.string
	}
}
use std::hash::Hash;
use std::fmt::Debug;

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum OpValue {
	
	/// The constant 0 value
	///  (allows for optimizations)
	ZeroConstant,
	
	/// Uses Constant Signed Integer
	///  Value
	SignedConstant(i64),
	
	/// Uses Constant Unsigned Integer
	///   Value
	UnsignedConstant(u64),
	
	/// Uses the value stored in
	///  the given register
	RegisterValue(OpRegister),
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum OpRegister {
	
	/// Register has not been assigned
	///  in register bank(self.0)
	UnAssignedRegister(u8),
	
	/// Register has been assigned
	///  in register bank(self.0)
	///  at (self.1)
	AssignedRegister(u8,u8),
}

//FIXME: Register Banks, Registers, Register Aliasing
//FIXME: Machine Ops, Supported Ops / Substituion Ops
//FIXME: Register Allocation & Micro-Optimization
//FIXME: REST

/// The Size of the MachineOp in bytes,
///  used to reduce enum variants
#[repr(u8)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum OpSize { //FIXME: VEC SIZE?
	Byte1 = 0,
	Byte2 = 1,
	Byte4 = 2,
	Byte8 = 3,
	Byte16 = 4,
}

/// Method of handling overflow for the machine operation
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum OverflowMode {
	Wrapped,
	SignedSaturated,
	UnsignedSaturated,
	Checked(OpRegister)
}


/**
  [MachineOp] metadata for each of
   the instructions:
    - additional register clobbers
    - fixed register requirements
    - operation is supported
    - operation latency
    - operation throughput
**/
pub struct MachineOpMetadataTable {
	
	/// Vector for bytes
	///  first byte in u16: latency
	///  second byte: bits 0b00000011: throughput
	///               bits 0b01111100: UNDEFINED
	///               bit  0b10000000: supported
	standard_insn_metadata: Vec<u16>,
}

impl MachineOpMetadataTable {
	
	//FIXME: ADD CONSTRUCTOR
	
	/// Returns if the instruction is supported or not
	#[inline(always)]
	pub fn is_supported(&self, op_idx: usize) -> bool {
		self.standard_insn_metadata[op_idx] & 0x8000 != 0
	}
	
	/// Get the expected instruction latency in clock
	///  cycles:
	/// All results should be in the range 1..=255
	#[inline(always)]
	pub fn get_latency(&self, op_idx: usize) -> u8 {
		self.standard_insn_metadata[op_idx] as u8
	}
	
	/// Get the rough instruction throughput of the
	///  operation in operations per clock cycle
	/// Values are rounded to the nearest value, so
	///  0 is a possible result, that means it takes
	///  more than 2 cycles per instruction on average
	#[inline(always)]
	pub fn get_throughput(&self, op_idx: usize) -> u8 {
		((self.standard_insn_metadata[op_idx] & 0x0300) >> 8) as u8
	}
	
	//FIXME: REGISTER CLOBBERING & FLAG CLOBBERING
}


pub trait OpTrait: Clone + Eq + PartialEq + Hash + Debug {}
impl OpTrait for OpValue {}
impl OpTrait for OpRegister {}

/**
 Machine Code Operation:
  - Abstract Assembly (with minimal metadata)
  - Acts as an intermediate format for generic assembly
    generation regarding register assignment
    and optimization
  - 1-x conversion to valid assembly
  - not all operations may be supported for a given
    assembly target
**/
#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub enum MachineOp<ValueOp,RegisterOp>
	where ValueOp: OpTrait,
          RegisterOp: OpTrait
{
	
	/// Load the value at location [src] into [dst]
	/// Is guaranteed to be supported
	///  by the target architecture
	Load {
		size: OpSize,
		src: ValueOp,
		dst: RegisterOp,
	},
	
	/// Load instruction where the source location
	///  is given by [src] + ([offset] << [offset_shift])
	/// May not be supported for any given [offset_shift]
	///  value, and could not be supported in general
	LoadOffset {
		size: OpSize,
		src: ValueOp,
		offset: ValueOp,
		offset_shift: u8,
		dst: RegisterOp,
	},
	
	LoadShiftedOffset {
		size: OpSize,
		src: ValueOp,
		offset: ValueOp,
		offset_shift: ValueOp,
		dst: RegisterOp
	},
	
	/// Store the value in register [src] into the memory
	///  location given by [dst]
	/// Is guaranteed to be supported
	///  by the target architecture
	Store {
		size: OpSize,
		src: RegisterOp,
		dst: ValueOp,
	},
	
	/// Store instruction where the target location
	///  is given by [dst] + ([offset] << [offset_shift])
	/// May not be supported for any given [offset_shift]
	///  value, and could not be supported in general
	StoreOffset {
		size: OpSize,
		src: RegisterOp,
		offset: ValueOp,
		offset_shift: u8,
		dst: ValueOp,
	},
	
	/// Move the value of register [src] into register [dst]
	/// Is guaranteed to be supported
	///  by the target architecture
	Move {
		size: OpSize,
		src: RegisterOp,
		dst: RegisterOp,
	},
	
	/// Perform integer addition on register values
	///  FIXME: dst may == src0 {make separate?}
	///  FIXME: overflow mode (support in code_gen
	///   or require patch it not supported natively?)
	IntAdd {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  Option<RegisterOp>,
		overflow: OverflowMode
	},
	IntSub {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  Option<RegisterOp>,
		overflow: OverflowMode
	},
	IntMul {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  Option<RegisterOp>,
		overflow: OverflowMode
	},
	IntDivRemSigned {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst0: Option<RegisterOp>,
		dst1: Option<RegisterOp>,
	},
	IntDivRemUnsigned {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst0: Option<RegisterOp>,
		dst1: Option<RegisterOp>,
	},
	IntMaximum {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  RegisterOp,
		signed: bool
	},
	IntMinimum {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  RegisterOp,
		signed: bool
	},
	IntShiftLeft {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  RegisterOp,
	},
	IntLogicalShiftRight {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  RegisterOp,
		mask: bool,
	},
	IntArithmeticShiftRight {
		size: OpSize,
		src0: RegisterOp,
		src1: RegisterOp,
		dst:  RegisterOp,
		mask: bool,
	}
	//FIXME: FINISH THE REST
}

impl<ValueOp: OpTrait, RegisterOp: OpTrait> MachineOp<ValueOp,RegisterOp> {
	pub fn convert<
		NewValueOp: OpTrait,
		NewRegisterOp: OpTrait,
		FValue: FnMut(ValueOp) -> NewValueOp,
		FRegister: FnMut(RegisterOp) -> NewRegisterOp,
	>(
		fn_value: &mut FValue,
		fn_register: &mut FRegister,
	) -> MachineOp<NewValueOp,NewRegisterOp> {
		unimplemented!()
	}
}
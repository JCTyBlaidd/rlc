use std::fmt::Debug;


//Generated ISA Modules
mod rv32i;
mod rv32e;
mod rv64i;

trait ISA {

	fn get_pointer_size(&self) -> u8;

	fn supports_instruction<
		R: Clone + PartialEq + Eq + Debug,
		I: Clone + PartialEq + Eq + Debug
	>(&self, isa: &McInstruction<R,I>) -> bool;

}


#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
enum RegisterWriteMode{
	Undefined,
	SignExtend,
	ZeroExtend,
	Preserve,
}

#[derive(Clone, PartialEq, Eq, Debug)]
enum McInstruction<
	R: Clone + PartialEq + Eq + Debug,
	I: Clone + PartialEq + Eq + Debug
> {
	IntegerAdd {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerSub {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerShiftLeft {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerSetLessThanSigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerSetLessThanUnsigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerXor {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerShiftRightLogical {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerShiftRightArithmetic {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerOr {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerAnd {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerMulLower {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerMulUpperSignedSigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerMulUpperSignedUnsigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerMulUpperUnsignedUnsigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerDivSigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerDivUnsigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerRemSigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerRemUnsigned {
		outputs: [R;1],
		inputs: [R;2],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerShiftLeftImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerShiftRightLogicalImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerShiftRightArithmeticImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerAddImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerSetLessThanSignedImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerSetLessThanUnsignedImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerXorImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerOrImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerAndImmediate {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerLoadOffset {
		outputs: [R;1],
		inputs: [R;1],
		immediate: [I;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerCopy {
		outputs: [R;1],
		inputs: [R;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
	IntegerSetEqualZero {
		outputs: [R;1],
		inputs: [R;1],
		size: u8,
		write_mode: RegisterWriteMode,
	},
}


impl<
	R: Clone + PartialEq + Eq + Debug,
	I: Clone + PartialEq + Eq + Debug
> McInstruction<R,I> {

	fn get_inputs(&self) -> &[R] {
		use self::McInstruction::*;
		match self {
			IntegerAdd { inputs, .. } => {
				inputs
			}
			IntegerSub { inputs, .. } => {
				inputs
			}
			IntegerShiftLeft { inputs, .. } => {
				inputs
			}
			IntegerSetLessThanSigned { inputs, .. } => {
				inputs
			}
			IntegerSetLessThanUnsigned { inputs, .. } => {
				inputs
			}
			IntegerXor { inputs, .. } => {
				inputs
			}
			IntegerShiftRightLogical { inputs, .. } => {
				inputs
			}
			IntegerShiftRightArithmetic { inputs, .. } => {
				inputs
			}
			IntegerOr { inputs, .. } => {
				inputs
			}
			IntegerAnd { inputs, .. } => {
				inputs
			}
			IntegerMulLower { inputs, .. } => {
				inputs
			}
			IntegerMulUpperSignedSigned { inputs, .. } => {
				inputs
			}
			IntegerMulUpperSignedUnsigned { inputs, .. } => {
				inputs
			}
			IntegerMulUpperUnsignedUnsigned { inputs, .. } => {
				inputs
			}
			IntegerDivSigned { inputs, .. } => {
				inputs
			}
			IntegerDivUnsigned { inputs, .. } => {
				inputs
			}
			IntegerRemSigned { inputs, .. } => {
				inputs
			}
			IntegerRemUnsigned { inputs, .. } => {
				inputs
			}
			IntegerShiftLeftImmediate { inputs, .. } => {
				inputs
			}
			IntegerShiftRightLogicalImmediate { inputs, .. } => {
				inputs
			}
			IntegerShiftRightArithmeticImmediate { inputs, .. } => {
				inputs
			}
			IntegerAddImmediate { inputs, .. } => {
				inputs
			}
			IntegerSetLessThanSignedImmediate { inputs, .. } => {
				inputs
			}
			IntegerSetLessThanUnsignedImmediate { inputs, .. } => {
				inputs
			}
			IntegerXorImmediate { inputs, .. } => {
				inputs
			}
			IntegerOrImmediate { inputs, .. } => {
				inputs
			}
			IntegerAndImmediate { inputs, .. } => {
				inputs
			}
			IntegerLoadOffset { inputs, .. } => {
				inputs
			}
			IntegerCopy { inputs, .. } => {
				inputs
			}
			IntegerSetEqualZero { inputs, .. } => {
				inputs
			}
		}
	}
	fn get_outputs(&self) -> &[R] {
		use self::McInstruction::*;
		match self {
			IntegerAdd { outputs, .. } => {
				outputs
			}
			IntegerSub { outputs, .. } => {
				outputs
			}
			IntegerShiftLeft { outputs, .. } => {
				outputs
			}
			IntegerSetLessThanSigned { outputs, .. } => {
				outputs
			}
			IntegerSetLessThanUnsigned { outputs, .. } => {
				outputs
			}
			IntegerXor { outputs, .. } => {
				outputs
			}
			IntegerShiftRightLogical { outputs, .. } => {
				outputs
			}
			IntegerShiftRightArithmetic { outputs, .. } => {
				outputs
			}
			IntegerOr { outputs, .. } => {
				outputs
			}
			IntegerAnd { outputs, .. } => {
				outputs
			}
			IntegerMulLower { outputs, .. } => {
				outputs
			}
			IntegerMulUpperSignedSigned { outputs, .. } => {
				outputs
			}
			IntegerMulUpperSignedUnsigned { outputs, .. } => {
				outputs
			}
			IntegerMulUpperUnsignedUnsigned { outputs, .. } => {
				outputs
			}
			IntegerDivSigned { outputs, .. } => {
				outputs
			}
			IntegerDivUnsigned { outputs, .. } => {
				outputs
			}
			IntegerRemSigned { outputs, .. } => {
				outputs
			}
			IntegerRemUnsigned { outputs, .. } => {
				outputs
			}
			IntegerShiftLeftImmediate { outputs, .. } => {
				outputs
			}
			IntegerShiftRightLogicalImmediate { outputs, .. } => {
				outputs
			}
			IntegerShiftRightArithmeticImmediate { outputs, .. } => {
				outputs
			}
			IntegerAddImmediate { outputs, .. } => {
				outputs
			}
			IntegerSetLessThanSignedImmediate { outputs, .. } => {
				outputs
			}
			IntegerSetLessThanUnsignedImmediate { outputs, .. } => {
				outputs
			}
			IntegerXorImmediate { outputs, .. } => {
				outputs
			}
			IntegerOrImmediate { outputs, .. } => {
				outputs
			}
			IntegerAndImmediate { outputs, .. } => {
				outputs
			}
			IntegerLoadOffset { outputs, .. } => {
				outputs
			}
			IntegerCopy { outputs, .. } => {
				outputs
			}
			IntegerSetEqualZero { outputs, .. } => {
				outputs
			}
		}
	}
	fn get_immediate(&self) -> &[I] {
		use self::McInstruction::*;
		match self {
			IntegerAdd { .. } => {
				Default::default()
			}
			IntegerSub { .. } => {
				Default::default()
			}
			IntegerShiftLeft { .. } => {
				Default::default()
			}
			IntegerSetLessThanSigned { .. } => {
				Default::default()
			}
			IntegerSetLessThanUnsigned { .. } => {
				Default::default()
			}
			IntegerXor { .. } => {
				Default::default()
			}
			IntegerShiftRightLogical { .. } => {
				Default::default()
			}
			IntegerShiftRightArithmetic { .. } => {
				Default::default()
			}
			IntegerOr { .. } => {
				Default::default()
			}
			IntegerAnd { .. } => {
				Default::default()
			}
			IntegerMulLower { .. } => {
				Default::default()
			}
			IntegerMulUpperSignedSigned { .. } => {
				Default::default()
			}
			IntegerMulUpperSignedUnsigned { .. } => {
				Default::default()
			}
			IntegerMulUpperUnsignedUnsigned { .. } => {
				Default::default()
			}
			IntegerDivSigned { .. } => {
				Default::default()
			}
			IntegerDivUnsigned { .. } => {
				Default::default()
			}
			IntegerRemSigned { .. } => {
				Default::default()
			}
			IntegerRemUnsigned { .. } => {
				Default::default()
			}
			IntegerShiftLeftImmediate { immediate, .. } => {
				immediate
			}
			IntegerShiftRightLogicalImmediate { immediate, .. } => {
				immediate
			}
			IntegerShiftRightArithmeticImmediate { immediate, .. } => {
				immediate
			}
			IntegerAddImmediate { immediate, .. } => {
				immediate
			}
			IntegerSetLessThanSignedImmediate { immediate, .. } => {
				immediate
			}
			IntegerSetLessThanUnsignedImmediate { immediate, .. } => {
				immediate
			}
			IntegerXorImmediate { immediate, .. } => {
				immediate
			}
			IntegerOrImmediate { immediate, .. } => {
				immediate
			}
			IntegerAndImmediate { immediate, .. } => {
				immediate
			}
			IntegerLoadOffset { immediate, .. } => {
				immediate
			}
			IntegerCopy { .. } => {
				Default::default()
			}
			IntegerSetEqualZero { .. } => {
				Default::default()
			}
		}
	}
	fn convert_type<
	 R2: Clone + PartialEq + Eq + Debug,
	 I2: Clone + PartialEq + Eq + Debug,
	 FuncR: Fn(R) -> R2,
	 FuncI: Fn(I) -> I2
	>(self, op_r: FuncR, op_i: FuncI) -> McInstruction<R2,I2> {
		use self::McInstruction::*;
		match self {
			IntegerAdd {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerAdd {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerSub {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerSub {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerShiftLeft {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerShiftLeft {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerSetLessThanSigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerSetLessThanSigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerSetLessThanUnsigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerSetLessThanUnsigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerXor {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerXor {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerShiftRightLogical {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerShiftRightLogical {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerShiftRightArithmetic {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerShiftRightArithmetic {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerOr {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerOr {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerAnd {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerAnd {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerMulLower {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerMulLower {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerMulUpperSignedSigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerMulUpperSignedSigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerMulUpperSignedUnsigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerMulUpperSignedUnsigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerMulUpperUnsignedUnsigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerMulUpperUnsignedUnsigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerDivSigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerDivSigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerDivUnsigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerDivUnsigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerRemSigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerRemSigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerRemUnsigned {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0, in1] = inputs;
				let [out0] = outputs;
				IntegerRemUnsigned {
					inputs: [op_r(in0), op_r(in1)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerShiftLeftImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerShiftLeftImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerShiftRightLogicalImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerShiftRightLogicalImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerShiftRightArithmeticImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerShiftRightArithmeticImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerAddImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerAddImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerSetLessThanSignedImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerSetLessThanSignedImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerSetLessThanUnsignedImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerSetLessThanUnsignedImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerXorImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerXorImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerOrImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerOrImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerAndImmediate {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerAndImmediate {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerLoadOffset {
				inputs,
				outputs,
				immediate,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				let [imm0] = immediate;
				IntegerLoadOffset {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					immediate: [op_i(imm0)],
					size,
					write_mode,
				}
			},
			IntegerCopy {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				IntegerCopy {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
			IntegerSetEqualZero {
				inputs,
				outputs,
				size,
				write_mode,
			} => {
				let [in0] = inputs;
				let [out0] = outputs;
				IntegerSetEqualZero {
					inputs: [op_r(in0)],
					outputs: [op_r(out0)],
					size,
					write_mode,
				}
			},
		}
	}
}

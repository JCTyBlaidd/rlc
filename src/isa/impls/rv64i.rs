use super::*;

struct Rv64i ;

impl ISA for Rv64i{

	fn get_pointer_size(&self) -> u8 {
		64
	}

	fn supports_instruction<
		R: Clone + PartialEq + Eq + Debug,
		I: Clone + PartialEq + Eq + Debug
	>(&self, isa: &McInstruction<R,I>) -> bool{
		true
	}

}


#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Instruction {
	ADD {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SUB {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SLL {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SLT {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SLTU {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	XOR {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SRL {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SRA {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	OR {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	AND {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	ADDW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SUBW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SLLW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SRLW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SRAW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	MUL {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	MULH {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	MULHSU {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	MULHU {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	DIV {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	DIVU {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	REM {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	REMU {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	MULW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	DIVW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	DIVUW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	REMW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	REMUW {
		rd0: u8,
		rs1: u8,
		rs2: u8,
	},
	SLLI {
		rd0: u8,
		rs1: u8,
		imm1: u8,
	},
	SRLI {
		rd0: u8,
		rs1: u8,
		imm1: u8,
	},
	SRAI {
		rd0: u8,
		rs1: u8,
		imm1: u8,
	},
	ADDI {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	MOV {
		rd0: u8,
		rs1: u8,
	},
	SLTI {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	SLTIU {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	SEQZ {
		rd0: u8,
		rs1: u8,
	},
	ANDI {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	ORI {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	XORI {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	ADDIW {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LB {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LH {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LW {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LBU {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LHU {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LD {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	LWU {
		rd0: u8,
		rs1: u8,
		imm1: u16,
	},
	SB {
		rs1: u8,
		rs2: u8,
		imm1: u16,
	},
	SH {
		rs1: u8,
		rs2: u8,
		imm1: u16,
	},
	SW {
		rs1: u8,
		rs2: u8,
		imm1: u16,
	},
	SD {
		rs1: u8,
		rs2: u8,
		imm1: u16,
	},
}





/* Easy for arbitrary patching (Add movs if required to the abstract instruction stream */
pub struct Patchable<T> {
	optional_prefix: Option<Box<[T]>>,
	value: T
}

pub struct PatchSequence<T> {
	seq: Vec<Patchable<T>>
}

impl<T> PatchSequence<T> {
	pub fn into_iter(self) -> PatchIter<T> {
		PatchIter {
			seq_iter: self.seq.into_iter()
		}
	}
}

pub struct PatchIter<T> {
	seq_iter: std::vec::IntoIter<Patchable<T>>
}


//TODO: MAYBE COME UP WITH A BETTER METHOD?!?!


// Idea for General: Code ===(
//          {NB Code Should be removed of any untranslatable instructions}
//           Weighted Wavefront
//           (i.e. keep track of int/float simulated delay for instruction and choose best)
//           with similated simple max in flight registers (not proper reg-alloc - just weighting)
// )==> Linearized Generic Instructions ==(
//         Abstract Machine Instructions
//         with 1 => multi translation if applicable for specific operation
//         reg-alloc not performed yet
//         abstract registers used with no limits
// ) => Linearized Abstract Machine Instructions ==(
//        Run RegAlloc and add any required moves,
//        stack allocation etc....
// ) => Linearized Machine Instructions ==(
//        Specify Global Registers etc...
// ) => Output